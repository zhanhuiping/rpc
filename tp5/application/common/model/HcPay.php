<?php
namespace app\common\model;

use think\Model;
use app\v2\model\Apply;
use think\Db;

/**
 * 汇潮支付
 */
class HcPay extends Model
{
    private $key = '5Pjg0jngSk3lGggjl8g';//key值
    private $channel = '1002';//渠道
    private $aliPayUrl = 'http://api.qhweidai.com/aliPay';//支付地址
    public function __construct($url = '')
    {
        parent::__construct();
        date_default_timezone_set('Asia/Shanghai');

        if(empty($url))
            $this->notify_url = config('domainPath')['api'].'/v2/notify';  //回调地址
        else
            $this->notify_url = $url; //自定义回调
    }
    public function aliPay(array $param = [])
    {
        $data['merchantOutOrderNo'] = $param['no_order'];
        $data['sign'] = $this->signEncode($data);
        $data['noncestr'] = '123456789';
        $data['orderMoney'] = $param['money_order'];
        $data['channel'] = $this->channel;
        $data['notifyUrl'] = $this->notify_url;
        //获取url地址
        $result = json_decode(curlPost($this->aliPayUrl, $data),true);
        if($result['code'] != 200)
        {
            $this->error = '调用失败';
            return false;
        }
        return $result['data'];

    }
    /**
     * [signEncode sign加密]
     * @param  array  $param [description]
     * @param  string $pa    [description]
     * @return [type]        [description]
     */
    public function signEncode(array $param = [], $pa = '-')
    {
        return md5($param['merchantOutOrderNo'] . $pa . $this->channel . $pa . $this->key);
    }


     /**
     * [hcQuery 汇潮查询]
     * @param  string $order_number [description]
     * @return [type]               [description]
     */
    public function hcQuery($order_number = '')
    {
        if(!$order_number)
        {
            $this->error = '订单错误';
            return false;
        }
        $data['noncestr'] = mt_rand(1,10000000);
        $data['merchantOutOrderNo'] = $order_number;
        $data['sign'] = $this->signEncode($data);
        $data['channel'] = $this->channel;
        //查询订单接口
        $url = 'http://api.qhweidai.com/queryorder';
        $result = curlPost($url, $data);
        $result = json_decode($result,true);
        if(!$result)
        {
            $this->error = '查询失败';
            return false;
        }
        if($result['code'] != 200)
        {
            $this->error = $result['error'];
            return false;
        }
        if($result['data']['payResult'] != 1)
        {
            $this->error = '等待支付';
            return false;
        }
        return '交易成功';


    }

}
