<?php
// +----------------------------------------------------------------------
// | Description: 轮播图数据
// +----------------------------------------------------------------------
// | Author: llj <just05075@foxmail.com>
// +----------------------------------------------------------------------

namespace app\common\model;

use think\Model;

class Lunbo extends Model 
{

	/**
	 * 获取轮播图列表
	 * @param  array   $param  [description]
	 */
	public function getLunboList()
	{
		$list = $this->field('sort,img,links')->order('sort asc')->select();
        return $list;
	}
}