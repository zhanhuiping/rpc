<?php
// +----------------------------------------------------------------------
// | Description: 系统配置
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\common\model;

use think\Model;
use think\Db;
use think\Request;

class SystemConfig extends Model 
{

	 public function __construct()
    {
    	parent::__construct();
    	$this->param =  Request::instance()->post();
    	$this->os = isset($this->param['os']) ? $this->param['os'] : '';
    }


    /**
     * 后台获取配置列表
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-30T16:52:24+0800
     * @param    integer     $os  哪种手机系统类型
     * @return   [type]
     */
	public function getDataList()
	{
		# 依据不同的系统读取不同的数据表
		// switch ((int)$os) {
		// 	case 1:    //安卓
		// 		$db = Db::name('system_config');
		// 		break;
		// 	case 2:    //IOS
		// 		$db = Db::name('system_config_ios');
		// 		break;
		// 	case 1001: //腾讯应用宝
		// 		$db = Db::name('system_config_yyb');
		// 		break;
		// 	default:  //IOS,默认用ios数据表，因为之前版本没有传这个参数
		// 		$db = Db::name('system_config_ios');
		// 		break;
		// }
		cache('DB_CONFIG_DATA', null); //清除缓存
		# 默认表 2017-12-11 15:35 yhq
		$db = Db::name('system_config');

		$data = array();
		$list = $db->select();
        foreach ($list as $key => $val) {
            $data[$val['name']] = $val['value'];
        }
        // p($data);
        $data['SYSTEM_NAME'] = config('houtai_name');
        return $data;
	}


	/**
	 * app获取配置列表
	 * @author   yhq <934797303@qq.com>
	 * @DateTime 2017-08-30T17:00:36+0800
	 * @return   [type]                   [description]
	 */
	public function getSysConfigList()
	{		
		cache('DB_CONFIG_DATA', null); //清除缓存
		# 默认表 2017-12-11 15:35 yhq
		$db = Db::connect('db_business')->table('qh_system_config');

		$list = $db->select();
		if(!$list){
			$this->error = '获取配置失败';
			return false;
		}				
		$appSysConfig = array();
		foreach ($list as $key => $val) {
			$appSysConfig[$val['name']] = $val['value'];
		}
		return $appSysConfig;
	}


	/**
	 * 批量修改配置
	 * @param  array   $param  [description]
	 */
	public function createData($param)
	{
		$list = [
			['id' => 1, 'value' => $param['SYSTEM_NAME']],
			['id' => 2, 'value' => $param['SYSTEM_LOGO']],
			['id' => 3, 'value' => $param['LOGIN_SESSION_VALID']],
			['id' => 4, 'value' => $param['IDENTIFYING_CODE']],			
			['id' => 27,'value' => $param['LOGO_TYPE']],			
		];

        for($i = 0; $i<=4;$i++){
        	$map['id'] = $list[$i]['id'];
        	Db::name('system_config')->where($map)->update(['value'=>$list[$i]['value']]);
        }

	
			$data = $this->getDataList();
			cache('DB_CONFIG_DATA', $data, 10);
			if($data){
                 return $data;
			}

		  $this->error = '更新失败';
		  return false;
	}
	
}