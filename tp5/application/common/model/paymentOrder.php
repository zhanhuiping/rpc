<?php
namespace app\common\model;

use think\Model,
    think\Db,
    app\v2\model\UserInfo,
    app\admin\model\Appview;

/**
 * 打款订单处理类
 */
class paymentOrder extends Model 
{	
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
	protected $name = 'payment_order';
    protected $createTime = false;
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';
     

    public function  createOrder($apply_info=null)
    {
        if(is_null($apply_info)) {
            $this->error = "没有获取到申请信息,请检查参数";
            return false;
        }  
        $order_number = $apply_info->order_number;  
        $res = $this->getOrderByApplyid($order_number);
        if($res["status"]==1) {
            $this->error = $res["error"];
            return false;
        }
        if($res["status"]==2) {
            #查找未支付订单
            $order_info = $this->where(["apply_id"=>$order_number,"status"=>0])->order("id desc")->find();
            return $order_info;
        }
        if($res["status"]==3) {

            if(empty($apply_info['bank_card_number'])){
                $this->error = '银行卡为空，请线下放款';
                return false;
            }

            //总行代码
            $bankno = Db::name('user_bank_card')->where('bank_card_number', $apply_info['bank_card_number'])->value('user_nm');
            if(empty($bankno)){
                $this->error = '银行卡未签约，请线下放款';
                return false;
            }

            #构造打款订单数据
            $add_data["apply_id"]       = $apply_info['order_number'];      //打款申请单
            $add_data["uid"]            = $apply_info['uid'];               //收款人 uid
            $add_data["total_money"]    = $apply_info['loan_out_money'];    //订单金额
            $add_data["period"]         = $apply_info['period'];            //借款期限
            $add_data["no_order"]       = config('fuyou_channel').date('Ymd').str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
            $add_data["status"]         = 0;                            //默认状态
            $add_data["acct_name"]      = $apply_info['name'];      //收款人姓名
            $add_data["card_no"]        = $apply_info['bank_card_number'];   //卡号
            $add_data["info_order"]     = "富友放款" ;               //订单描述
            $add_data["flag_card"]      = 0;  
            $add_data["create_time"]    = date("Y-m-d H:i:s",time());  
            $add_data["dt_order"]       = date('YmdHis', time());  
            $add_data["bankno"]         = $bankno;  
            $add_data["admin_uid"]      = $GLOBALS['userInfo']['id'];  //操作员uid 

            $res = $this->create($add_data); 
            if(!$res) {
                $this->error = "创建订单失败";
                return false;
            }  
            return $res;
        }        
    }
    /**
     * 判断申请单是否已有成功支付的订单
     * @Author      火星车11号
     * @email       704184317@qq.com
     * @create_time 2017-10-11T16:27:15+0800
     * @param      apply_id  申请id
     * @return     bool 
     */
    public function  getOrderByApplyid($apply_id)
    {

        $count_1 = $this->where(["apply_id"=>$apply_id,"status"=>1])->count();
        if($count_1) {
            return ["status"=>1,"error"=>"该申请单存在已经支付的订单，无法再次支付"];
        }
        $count_2 = $this->where(["apply_id"=>$apply_id,"status"=>0])->count();
        if($count_2) {
            return ["status"=>2,"error"=>"存在未支付订单，使用旧订单支付"];
        }     
        return ["status"=>3];
    }
}