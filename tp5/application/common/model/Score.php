<?php
// +----------------------------------------------------------------------
// | Description: 个人余额数据
// +----------------------------------------------------------------------
// | Author: llj <just05075@foxmail.com>
// +----------------------------------------------------------------------

namespace app\common\model;

use think\Model;

class Score extends Model 
{
    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
	protected $name = 'score';
    protected $createTime = 'create_time';
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';

	/**
	 * 根据UID获取用户余额
	 * llj
	 * 2017年6月29日17:52:37
	 * @param  array   $param  [description]
	 */
	public function getScoreByUid($uid)
	{

		$data = $this->hasInitScore($uid);
		$data['phone'] = model('user')->where(['uid'=>$uid])->value('phone');

		if (!$data) {
			$this->error = '数据异常，请联系客服！';
			return false;
		}
		return $data;
	}


/**
 * 判断用户是否初始化过积分表
 * llj
 * 2017年6月29日17:56:27
 * 
 * @param  [type]  $uid [description]
 * @return boolean      [description]
 */
	private function hasInitScore($uid){

		$data = $this->get(['uid'=>$uid]);
		// echo json_encode($data);die;
		if ($data) {
			// echo 111;die;
			return $data;
		}

		$param['uid'] = $uid;
		$param['score'] = '0.00';
		$res = $this->data($param)->allowField(true)->save();

		if($res){
			return $param;
		}else{
			return false;
		}

	}


}