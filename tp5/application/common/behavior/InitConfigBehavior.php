<?php
// +----------------------------------------------------------------------
// | Description: 加载动态配置  初始化基本常量
// +----------------------------------------------------------------------
// | Author:  linchuangbin <linchuangbin@honghaiweb.com>
// +----------------------------------------------------------------------
namespace app\common\behavior;
use think\Request;
class InitConfigBehavior
{
    public function run(&$content)
    {
        // 读取数据库中的配置
        $system_config = cache('DB_CONFIG_DATA'); 
        if(!$system_config){
            //获取所有系统配置
            $system_config = \think\Loader::model('admin/SystemConfig')->getDataList();
            cache('DB_CONFIG_DATA', null);
            // cache('DB_CONFIG_DATA', $system_config, 36000); //缓存配置
        }
        config($system_config); //添加配置


        # 定义当前接口是否测试接口
        $isTestEnv = false;
        $request = Request::instance();
        if($request->ip() == '127.0.0.1' || strpos($request->domain(),'test') || strpos($request->domain(),'local')){
            $isTestEnv = true;
        }
        if(!defined('IS_TEST_ENV')){
            define('IS_TEST_ENV', $isTestEnv);
        }


        # 定义系统当前时间戳
        $time = time();
        if(IS_TEST_ENV){
            $time = empty(config('SYS_TIME'))||config('SYS_TIME')=='1970-01-01 08:00:00' ? $time : strtotime(config('SYS_TIME'));
        }
        if(!defined('SYS_TIMESTAMP')){
            define('SYS_TIMESTAMP',$time);
        }


        # 定义日志目录
        $logsDirName = 'logs';
        $dateDirName = 'logs/'.date('Ymd');
        if(!is_dir($logsDirName)){
            mkdir($logsDirName, 0755, true);
        }            
        if(!is_dir($dateDirName)){
            mkdir($dateDirName, 0755, true);
        }
        if(!defined('LOGS_DIR_NAME')){
            define('LOGS_DIR_NAME',$dateDirName.'/'); // 例如: logs/20180125/
        }


        # 定义申请失败可借款倒计时天数
        $day = config('COUNTDOWN_DAY') == '' ? 0 : (int)config('COUNTDOWN_DAY');
        if(!defined('COUNTDOWN_DAY')){
            define('COUNTDOWN_DAY',$day);
        }
        
    }

    
}