<?php
namespace app\v2\Model;
use think\Model;
use think\Db;
/**
 * 贷前审核模型
 * @author llj <llj465705075@gmail.com>
 */

class Tondun extends Model {
    protected $name = 'td_preloan_apply';

  //验证规则
    public function checkCredit ($data=array(),$uid)
    {
        $validate = validate($this->name);
        if (!$validate->check($data)) {
            $this->error = $validate->getError();
            return false;
        }
      // $account=Db::name('td_account')->where('uid',$uid)->find();
      //   if($account){
      //       //时间设置
      //       if(time() - $account['last_time'] <= 3600*24*7){
      //           $this->error = "同一用户，一周只能查询一次！" ;
      //           return false;
      //       }
      //   }
      //   //判断查询的用户这周内有没有被查询过
      //   $arr=Db::name('td_preloan_apply')->where('id_number',$data['id_number'])->order('create_time desc')->find();
      //   if($arr){
      //       //时间设置
      //       if(time() - $arr['create_time'] <= 3600*24*7){
      //           $this->error = "同一身份证号，一周只能查询一次！" ;
      //           return false;
      //       }
      //   }
      //   $arr1=Db::name('td_preloan_apply')->where('mobile',$data['mobile'])->order('create_time desc')->find();
      // if($arr1){
      //    if(time() - $arr1['create_time'] <= 3600*24*7){
      //        $this->error = "同一手机号，一周只能查询一次！" ;
      //        return false;
      //    }
      // }
        //设置默认贷款时间单位
        if($data['loan_term_unit']==''){
            $data['loan_term_unit']='DAY';
        }
        $data['create_time']=time();
        $data['create_time_str']=date("Y-m-d H:i:s");
        return $data;
    }
//查询历史审核列表
    public function lists($model, $where = array(), $order = '',$base = array(), $field = true)
    {
        $options = array();

        if (is_string($model)) {

            $model = Db::name($model);
        }
        if(is_array($model)){

            $model =Db::name($model[0]);
        }

        if ($order) {
            $options['order'] = $order;
        }

        $options['where'] = array_merge((array)$base, /*$REQUEST,*/
            (array)$where);
       /* dump($options);
        exit();*/
        if (empty($options['where'])) {
            unset($options['where']);
        }
        /*$field就是* 查所有字段  */

      $data=$model->alias('a')
           ->join('qh_td_preloan_apply p','a.report_id = p.report_id')
           ->field($field)
           ->where($options['where'])
           ->order($options['order'])
           ->select();
        foreach($data as &$v){
            if($v['status']==0){
                $v['status_str']='正在生成';
            }else{
                $v['status_str']='已生成';
            }
        }

     return $data;
    }

    //刷新列表信息
 public function refresh($param=array()){
     $where['uid']=$param['uid'];
     $where['status']=0;   //报告没生成的
     $where['report_id']=$param['report_id'];
     if(!$param['report_id']){
         $this->error = '报告id为空!';
         return false;
     }
     $model=Db::name('td_preloan_report');
     $result = preloan_report($where['report_id'], IS_TEST);
         $add = $result;
         $add['apply_time'] = substr($result['apply_time'],0,-3);
         $add['report_time'] = substr($result['report_time'],0,-3);
         $add['risk_items'] = json_encode($result['risk_items'],JSON_UNESCAPED_UNICODE);
         $add['address_detect'] = json_encode($result['address_detect'],JSON_UNESCAPED_UNICODE);
         $add['status']=1;
         unset($add['success']);

         /*将返回的征信信息保存在report表里*/
         if ($result['success'] == false) {   //查询报告失败则将数据库里status为0的状态改为1
             $this->error = $result['reason_desc'];
             return false;
         }else{
             $re=$model->where($where)->update($add);
             if($re!==false){
                 return true;
             }else{
                 $this->error = '修改状态失败!';
                 return false;
             }
         }


 }
    //报告详情页
   public function detail($param=array()){
       $report_id=$param['report_id'];
       $uid=$param['uid'];

       if(!$report_id){
           $this->error = '报告id为空!';
           return false;
       }

       $where=array();
       if($uid){
           $where['uid']=$uid;
       }
       $where['report_id']=$report_id;
       $is_set = Db::name('td_preloan_report')->where($where)->find();
       /* echo Db::name('td_preloan_report')->getlastsql();die;*/
       if(!$is_set){
           $this->error = '报告id不存在!';
           return false;
       }

       /* 调用 [贷前审核] 接口 */
       $result = preloan_report($report_id,IS_TEST);

       if($result['success'] == false){
           $this->error = $result['reason_desc'];
           return false;
       }

       //将风险报告分类
       $fenlei=array();
       if($result['risk_items']){
           foreach ($result['risk_items'] as $k => $v) {
               switch ($v['group']) {
                   case '个人基本信息核查':
                       $fenlei['gr'][]=$v;
                       break;
                   case '不良信息扫描':
                       $fenlei['bl'][]=$v;
                       break;
                   case '多平台借贷申请检测':
                       $fenlei['dp'][]=$v;
                       break;
                   case '关联人信息扫描':
                       $fenlei['gl'][]=$v;
                       break;
                   case '客户行为检测':
                       $fenlei['kh'][]=$v;
                       break;
                   default:
                       # code...
                       break;
               }
           }
       }

       /*在提交的信系表里 根据订单id查用户信息*/
       $preson_info = Db::name('td_preloan_apply')->field("name,id_number,mobile")->where("report_id",$report_id)->find();
       $fenlei['name']=$preson_info['name'];
       $fenlei['id_number']=$preson_info['id_number'];
       $fenlei['mobile']=$preson_info['mobile'];
       $fenlei['apply_time'] = substr($result['apply_time'],0,-3);
       $fenlei['report_time'] = substr($result['report_time'],0,-3);
       $fenlei['final_score']=$result['final_score'];
       $fenlei['final_decision']=$result['final_decision'];
       $fenlei['application_id']=$result['application_id'];
       $fenlei['address_detect']=$result['address_detect'];
       $fenlei['report_id']=$result['report_id'];
       // var_dump($fenlei);
       // exit();
       return $fenlei;
  }

}