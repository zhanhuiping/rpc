<?php
/**
 * 芝麻分
 */
namespace app\v2\model;
use think\Db;
use think\Model;

class Sesame extends Model {

	/**
	 * 芝麻分
	 * @param  [type] $param [description]
	 * @param  [type] $uid   [description]
	 * @return [type]        [description]
	 */
	
	private $sesame_url = 'http://api.qhweidai.com/api/zmopApply';
  private $user_home = 'http://i.100jieqian.com/app/home';
	private $notifyurl = 'http://api2-test.yueguangbaika.com/v2/get_sesame_notifyurl';
  private $key = 'H5SpZ4OI';
	public function get_sesame($uid)
	{
    // $phone = Db::name('user_info')->where(['uid'=>$uid])->value('phone');
		$phone = 18682400187;
		$params['phone'] = $phone;
		$params['uid'] = $uid;
		$params['channel'] = '1006';
		$params['notifyurl'] = $this->notifyurl;
		$sign = md5($uid.$this->key) ;
    $params['sign'] = $sign;
    p($params);exit;
		$data = curlPostForm($this->sesame_url,$params);
		return $data;
	}
   
   /**
    * 功能:     qq同步助手注册
    * @email    18702529695@163.com
    * @DateTime 2018-02-05
    * @return   [type]              [description]
    */
	public function get_qq_verify($uid)
    {
       
		$params['vali_token'] = md5(date('Y-m-d',time()) . md5('k5nn2g7Aqnn'));
		$params['channel']    = 10;
		$params['uid']        = $uid;
        return $params;
       
  }


 /**
  * 功能:     获取通讯录
  * @Author   Mrzhp
  * @email    18702529695@163.com
  * @DateTime 2018-02-05
  * @param    [type]              $param [description]
  * @return   [type]                     [description]
  */
  public function get_qq_home($uid)
  {
        $params['vali_token'] = md5(date('Y-m-d',time()) . md5('k5nn2g7Aqnn'));
		    $params['channel']    = 10;
		    $params['uid']        = $uid;
        $params['type']       = 0;

        $data = json_decode(curlPostForm($this->user_home,$params),true)['data'];
		    

        if(!$data)
        {
          $this->error = "此账号未同步，请按照下方步骤重新同步通讯录";
          return false;
        }

        $data = json_encode($data);
        
        $param['vali_token'] = md5(date('Y-m-d',time()) . md5('k5nn2g7Aqnn'));
        $param['channel']    = 10;
        $param['uid']        = $uid;
        $param['type']       = 1;


        $data_dx = json_decode(curlPostForm($this->user_home,$param),true)['data'];

        $data_dx = json_encode($data_dx);

        $res = Db::connect("db_mongo")->name('yueguangbaika')->insertGetId(["qqtb_data"=>$data]);


        if(!$res){
            $this->error = "插入失败";
            return false;
         }
 

       $res_dx = Db::connect("db_mongo")->name('yueguangbaika')->insertGetId(["qqtb_data"=>$data_dx]);
       
       

       $info_id = Db('user_info')->where(['uid'=>$uid])->update(['mongodb_qqtb_duanxin_id'=>$res_dx]);

       $info_id = Db('user_info')->where(['uid'=>$uid])->update(['mongodb_qqtb_id'=>$res,'directories'=>1,'qqtb_auth_date'=>date('Y-m-d H:i:s', time())]);
      
       if(!$info_id){
        $this->error = "插入失败";
        return false;
       }

        return $data = '认证成功';




		}


          




    /**
    * 功能:     用户通讯录
    * @Author   Mrzhp
    * @email    18702529695@163.com
    * @DateTime 2018-02-06
    * @param    [type]              $uid [description]
    * @return   [type]                   [description]
    */
   public function getTelDetails($uid)
   {
      $local_url = $_SERVER['SERVER_NAME'];
      $url = 'http://'.$local_url.'/admin/Scriptfile/report_split?uid='.$uid;
      httpGet($url);
      $mongodb_qqtb_id =  Db::name('user_info')->where(['uid'=>$uid])->value('mongodb_qqtb_id');

      $res = Db::connect("db_mongo")->name('yueguangbaika')->where("_id",$mongodb_qqtb_id)->find();
     p($res);exit;

      // 通话清单
      $mongodb_call_log_id =  Db::name('report_call')->where(['uid'=>$uid])->value('mongodb_call_log_id');
      $res_call_log = Db::connect("db_mongo")->name('yueguangbaika')->where("_id",$mongodb_call_log_id)->find();
       

      $res_call_log = json_decode($res_call_log['call_log_data'],true);

      $qqtb_data = $res['qqtb_data'];
      $qqtb_data = json_decode($qqtb_data,true);




    /**
     * 通讯录匹配
     */
 
   $call_log = array();
   foreach($res_call_log as $key1=>$call_value)
   {
       $v = strtotime($call_value['last_contact_date']);
       $call_log[$v] = $call_value['phone'];

   }

   $i = 0;
   foreach($qqtb_data as $key=>$qqtb_value)
   {
          
           $phone = str_replace('-','',$qqtb_value['phone']);
           
           if(in_array($phone,$call_log))
           {
               $last_contact_date =  array_search($phone,$call_log);
               if($last_contact_date > (time()-(180*24*60*60)))
               {
                 
                 $i++;
                 $qqtb_data[$key]['match'] = 1;

               }else{

                  $qqtb_data[$key]['match'] = 0;

               }
                  

           }else{

                $qqtb_data[$key]['match'] = 0;

           }
               
         


    }
    
     if($i>=15)
     {

        Db::name('report_call')->where(['uid'=>$uid])->update(['is_fifteen'=>1]);
     }

      $count = count($qqtb_data);
      if(!$qqtb_data){

          return false;

      }

    return $i;
          

   }
    




}
