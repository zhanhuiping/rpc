<?php
// +----------------------------------------------------------------------
// | Description: 提现记录
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;

use think\Model;
class Withdraw extends Model 
{	
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
	protected $name = 'score_withdraw';
    protected $createTime = 'create_time';
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';


	/**
	 * 获取今日提现次数
	 * @zwr
	 * @DateTime 2017-06-30T16:48:20+0800
	 * @param    int                   $uid 用户id
	 * @return   [type]                       
	 */
	public function getTxNumToday($uid)
	{
		return   $this->whereTime('create_time', 'today')->count();
	}
    public function getTxCount($uid)
    {
        return   $this->where('uid', $uid)->count();
    }
    public function getstatusAttr($value)
    {
        $type = [0=>'未处理',-1=>'关闭提现',1=>"提现成功"];
        return $type[$value];
    }

    public function getDataList($map=array(),$field="*",$orderby = "",$limit = "")
    {
        $list =$this->where($map)
                    ->field($field)
                    ->limit($limit['start'],$limit['count'])
                    ->order($orderby)
                    ->select();

        $list = modelo2array($list);   
        return $list;               
    }	

}