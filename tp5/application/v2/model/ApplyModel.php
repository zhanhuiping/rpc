<?php 
namespace app\v2\model;
use think\Model;
class ApplyModel extends Model
{
    protected $name = 'apply';
    /**
     * [getOne 获取一条数据]
     * @param  string $field [description]
     * @param  array  $where [description]
     * @param  string $order [description]
     * @return [type]        [description]
     */
    public function getOne($field = '*', $where = [], $order = '')
    {
        return $this->field($field)->where($where)->order($order)->find();
    }
}

 ?>