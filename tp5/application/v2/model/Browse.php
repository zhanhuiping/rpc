<?php
// +----------------------------------------------------------------------
// | Description: 邀请码
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;

use think\Model;
class browse extends Model 
{	
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
	protected $name = 'user_browse';
    protected $createTime = 'create_time';

    // 开启自动写入时间戳之后 数据表没有 如果updateTime字段 必须填写 false
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';


    public function getDataList($map=array(),$field="*",$orderby = "",$limit = "")
    {
        $list =$this->where($map)
                    ->field($field)
                    ->limit($limit['start'],$limit['count'])
                    ->order($orderby)
                    ->select();

        $list = modelo2array($list);   
        return $list;               
    }
    public function getBrowseCount($uid)
    {
        return   $this->where('uid', $uid)->count();
    }    
   	


}