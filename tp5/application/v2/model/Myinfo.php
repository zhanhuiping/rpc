<?php
/**
 * 贷款申请处理model
 */
namespace app\v2\model;
use think\Db;
use think\Model;
class Myinfo extends Model {

    protected $name = 'user_tel_address';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = 'DateTime';

    /**
     * 通讯录保存
     * @author 湛慧平
     * @DateTime 2017-08-1
     * @param    $param  接收参数
     * @return   boolean                     
     */
    public function myinfosave($uid,$tel_address_list)
    {
      $map['uid'] = $uid;
      $info = $this ->field('uid')->where($map)->find();
      $data = ['uid' => $uid, 'tel_address_list' => $tel_address_list];
      if($info)
      {
          $data = $this->save($data,$map);
          if($data)
          {
             $this->error = "存取失败";
             return false;
          }
          return true;
      }

      $data = $this->save($data);
      if(!$data)
      {
        $this->error = '没有保存';
        return false;
      }
      return true;
    }

    /**
     * APP保存通讯录
     * @param  [type] $contact          [json格式通讯录]
     * @param  [type] $uid              [用户id]
     * @return [type]                   [description]
     */
    public function saveContact($contact, $uid)
    {
        // 重组数据
        $data = json_decode($contact, true);
        foreach ($data as $key => $value) {
            $data[$key]['qq']    = '';
            $data[$key]['email'] = '';
            $data[$key]['note']  = '';
        }
        $contact = json_encode($contact);

        $res = Db::connect("db_mongo")->name('yueguangbaika')->insertGetId(["qqtb_data"=>$contact]);
        if(!$res){
            $this->error = "获取失败";
            return false;
        }

        //新增认证时间
        $info_id = Db('user_info')->where(['uid'=>$uid])->update(['mongodb_qqtb_id'=>$res,'directories'=>1,'qqtb_auth_date'=>date('Y-m-d H:i:s', time())]);
        if(!$info_id){
            $this->error = "获取失败";
            return false;
        }

        return '获取成功';
    }

    
}
