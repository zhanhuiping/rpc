<?php
namespace app\v2\Model;
use think\Model;
use think\Db;
/**
 * 贷前审核模型
 * @author llj <llj465705075@gmail.com>
 */

class Watermelon extends Model {
    protected $name = 'td_preloan_apply';

     //报告详情页
   public function detail($param=array()){
       $report_id=$param['label'];
       $uid=$param['uid'];

       if(!$report_id){
           $this->error = '报告id为空!';
           return false;
       }

       $where=array();
       if($uid){
           $where['uid']=$uid;
       }
       $where['report_id']=$report_id;
       $is_set = Db::name('td_preloan_report')->where($where)->find();
       if(!$is_set){
           $this->error = '报告id不存在!';
           return false;
       }

       /* 调用 [贷前审核] 接口 */
       $result = preloan_report($param);
       if($result['code'] == 400){
           $this->error = $result['error'];
           return false;
       }

       //将风险报告分类
       $fenlei=array();

       /*在提交的信系表里 根据订单id查用户信息*/
       $preson_info = $this->field("name,id_number,mobile,create_time_str,application_id")->where("report_id",$report_id)->find();
       $preson_info['final_decision'] = $is_set['final_decision'];
       $preson_info['final_level'] = $is_set['final_level'];
       $preson_info['status_code'] = $is_set['status_code'];
       $preson_info['return_message'] = $is_set['return_message'];
       $preson_info['report_id'] = $report_id;
       $fenlei['userInfo'] = $preson_info;
       $fenlei['watermelon'] = $result['data']['JsonData'];
       return $fenlei;
  }

  //刷新列表信息
  public function refresh($param=array()){
      $where['uid']       = $param['uid'];
      $where['report_id'] = $param['label'];
      if(!$param['label']){
        $this->error = '报告id为空!';
        return false;
      }
      $model=Db::name('td_preloan_report');
      unset($param['uid']);
      $result = preloan_report($param);
      if ($result['code'] == 400) {
        $this->error = $result['error'];
        return false;
      }elseif($result['code'] == 200){
        $add['apply_time']  = time();
        $add['report_time'] = time();
        $add['status'] = 1;
        $re = $model->where($where)->update($add);
        if(!$re){
            $this->error = '修改状态失败!';
            return false;
        }
        return $result['data'];
      }else{
        $this->error = '未知错误';
        return false;
      }
  }
    //之前代码
    //验证规则
    public function checkCredit ($data=array(),$uid)
    {
        $validate = validate($this->name);
        if (!$validate->check($data)) {
            $this->error = $validate->getError();
            return false;
        }
        //设置默认贷款时间单位
        if($data['loan_term_unit']==''){
            $data['loan_term_unit']='DAY';
        }
        $data['create_time']=time();
        $data['create_time_str']=date("Y-m-d H:i:s");
        return $data;
    }
    //查询历史审核列表
    public function lists($model, $where = array(), $order = '',$base = array(), $field = true)
    {
        $options = array();

        if (is_string($model)) {

            $model = Db::name($model);
        }
        if(is_array($model)){

            $model =Db::name($model[0]);
        }

        if ($order) {
            $options['order'] = $order;
        }

        $options['where'] = array_merge((array)$base,
            (array)$where);
        if (empty($options['where'])) {
            unset($options['where']);
        }
        /*$field就是* 查所有字段  */

      $data=$model->alias('a')
           ->join('qh_td_preloan_apply p','a.report_id = p.report_id')
           ->field($field)
           ->where($options['where'])
           ->order($options['order'])
           ->select();
        foreach($data as &$v){
            if($v['status']==0){
                $v['status_str']='正在生成';
            }else{
                $v['status_str']='已生成';
            }
        }

     return $data;
    }
}