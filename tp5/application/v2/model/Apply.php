<?php
/**
 * 借款申请处理model
 */
namespace app\v2\model;
use app\admin\model\ApplyAuth;
use think\Db;
use think\Model;
use think\Cache;
use think\cache\driver\Redis;
use app\common\model\SystemConfig;
use app\common\model\YunFengModel;
use app\common\model\HcPay;
use app\v2\model\UserBankCard;
use app\v2\model\Order;
use app\v2\model\User;
use fuiou\FuiouApi;
use app\v2\model\ApplyOrder;
class Apply extends Model {

    /**
     * 借款明细--申请前的预览
     * @param  [type] $param [description]
     * @param  [type] $uid   [description]
     * @return [type]        [description]
     */
    public function loanDetails($param, $uid)
    {
        // 根据申请信息查询相应的利率
        $config = Db::name('config')->where('id', $param['cid'])->find();
        $userInfo = Db::name('userInfo')->where('uid', $uid)->find();

        if(!$config||!$userInfo){
            $this->error = '系统错误';
            return false;
        }

        $money             = formatMoney($config['money']);
        $period            = $config['period'];
        $creditRate        = $config['credit_rate'];      //信审费率
        $dailyInterestRate = $config['rate'];             //日利率
        $manageRate        = $config['service_rate'];     //管理费率

        // 信审费 = 本金*借款周期*信审费率
        $creditMoney = formatMoney($money*$period*$creditRate);

        // 利息 = 本金*借款周期* 日利率
        $interestMoney = formatMoney($money*$period*$dailyInterestRate);

        // 管理费 = 本金*借款周期*管理费率
        $manageMoney = formatMoney($money*$period*$manageRate);

        // 到期应还 = 本金+利息+管理费
        $shouldPayMoney = formatMoney($money + $interestMoney + $manageMoney);

        //实际到账 = 申请金额 - 信审费
        $receivedMoney = formatMoney($money - $creditMoney);

        // 应还日期
        $shouldLoanDay = date('Y-m-d', SYS_TIMESTAMP + $period*24*3600);

        $data['money']            = $money;
        $data['period']           = $period;
        $data['creditMoney']      = $creditMoney;
        $data['interestMoney']    = $interestMoney;
        $data['manageMoney']      = $manageMoney;
        $data['shouldPayMoney']   = $shouldPayMoney;
        $data['shouldLoanDay']    = $shouldLoanDay;
        $data['receivedMoney']    = $receivedMoney;
        $data['bank_card_number'] = $userInfo['bank_card_number'];
        $data['name']             = $userInfo['name'];
        $data['id_card']          = $userInfo['id_card'];
        $data['bind_mob']         = $userInfo['bind_mob'];

//        # 认证收费方式 暂时固定
//        $data['auth_type'] = [
//            ['type' => '2', 'name' => 'VIP认证', 'remark' => '1小时内审核', 'money' => '1.1'],
//            ['type' => '1', 'name' => '极速认证', 'remark' => '6小时内审核', 'money' => '1.0'],
//            ['type' => '0', 'name' => '普通认证', 'remark' => '48小时内审核', 'money' => '0.0']
//        ];
        $data['auth_type'] = model('admin/ApplyAuth')->order('type desc')->select();


        return $data;
    }



    /**
     * 申请
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-07-25 15:00
     * @param    array    $param  接收参数
     * @param    array    $uid    用户id
     * @return   boolean
     */
    public function apply($param,$uid){
        # 防止重复数据并发请求
        $requstLock = $this->requstLock($param);
        if($requstLock) {
            $this->error = '请求过于频繁';
            return false;
        }

        # 校验支付密码 并且为银行卡
        if (in_array($param['type'], [1, 2, 3]) && $param['pay_type'] == 1) {
            $UserModel = new User;
            $res = $UserModel->checkPayPassword($uid, $param['pay_password']);
            if(!$res){
                $this->error = $UserModel->getError();
                return false;
            }
        }

        # 验证接收字段
        $validate = validate($this->name);
        if (!$validate->check($param)) {
            $this->error = $validate->getError();
            return false;
        }

        $UserInfoModel = new UserInfo;
        $userinfo = $UserInfoModel->where('uid', $uid)->find();

        # 验证是否同步QQ助手
        if(!$userinfo['directories']){
            $this->error = 'QQ同步助手未认证';
            return false;
        }

        # 检测是否验证运营商信息
        $operatorStatus = $UserInfoModel->operatorStatus($userinfo);
        $result['operator'] = $operatorStatus['status'];
        if($operatorStatus['status'] != '1'){
            $this->error = $operatorStatus['info'];
            return false;
        }

        # 同一身份证多个账号并且某个账号有未完成的订单 禁止申请
        $map['id_card'] = $userinfo['id_card'];
        $userArr = $UserInfoModel->where($map)->select();
        if(count($userArr) > 1){
            foreach ($userArr as $key => $value) {
                $hasApply = $this->hasApply($value['uid']);
                if($hasApply){
                    $this->error = "身份异常：身份证绑定多个账号";
                    return false;
                }
            }
        }

        // 云峰校验
        // $isYunFengAuth = YunFengModel::isYunFengAuth($uid);
        // if($isYunFengAuth!=='1'){
        // 	$this->error = '请先进行其他认证';
        // 	return false;
        // }

        if(!($this->rejectCanApply($uid))){
            # 借款失败后未到可申请时间不能申请
            $countdown = $this->countdown($uid);
            if($countdown && SYS_TIMESTAMP< strtotime($countdown['endTime'])){
                $this->error = '还未到可申请时间';
                return false;
            }
        }

        # 限制重复申请
        $status = $this->where('uid', $uid)->order('id desc')->value('status');
        if(isset($status) && in_array($status, array(0,1,2,3,4,9,12))){
            $this->error = '不能重复申请';
            return false;
        }

        # 根据申请信息查询相应的档位配置
        $cid = $param['cid'];
        $config = Db::name('config')->where('id', $cid)->find();
        if(!$config){ //未查询到档位信息
            $this->error = '档位参数错误';
            return false;
        }

        # 2017-12-22  限制可申请额度
        if(empty($userinfo['max_money'])){
            $config2 = Db::name('config')->order('money')->field('money')->find();
            $userinfo['max_money'] = $config2['money'];
        }
        $max_money = $userinfo['max_money'];
        if($param['money']>$max_money){
            $this->error = '申请总额大于可申请额度';
            return false;
        }

        # 防止乱刷金额
        if($param['money']!=$config['money'] || $param['period']!=$config['period']){
            $this->error = '参数错误';
            return false;
        }

        $loan_out_money = $param['money'] - formatMoney($param['money']*$config['credit_rate']*$param['period']);
        $param['bind_phone']       = $param['phone'];
        $param['loan_out_money']   = formatMoney($loan_out_money); //应该放款金额
        $param['rate']             = $config['rate'];              //日利率
        $param['credit_rate']      = $config['credit_rate'];       //信审费率
        $param['service_rate']     = $config['service_rate'];      //服务费率
        $param['procedure_rate']   = $config['procedure_rate'];    //手续费率
        $param['overdue_rate']     = $config['overdue_rate'];      //逾期费率
        $param['delay_rate']       = $config['delay_rate'];        //延期手续费率
        $param['name']             = $userinfo['name'];
        $param['id_card']          = $userinfo['id_card'];
        $param['phone']            = $userinfo['phone'];
        $param['bank_card_number'] = $userinfo['bank_card_number'];
        $time                      = SYS_TIMESTAMP;
        $param['uid']              = $uid;
        $param['time']             = date("Y-m-d H:i:s",$time);  //申请时间
        $param['order_number']     = $this->order_number();      //订单编号
        $param['contract_number']  = $this->contract_number();   //合同编号

        # 在拒绝放款地区或年龄不符合，将状态改成8（其他驳回）
        $is_reject_area = $this->is_reject_area($userinfo['address']);
        $is_reject_age  = $this->is_reject_age($userinfo['id_card']);
        if($is_reject_area||$is_reject_age){
            $param['status'] = 8;
        }

        # 年龄不符合直接不让申请，地区不作处理，因为可能会误判
        if($is_reject_age){
            $this->error = '您的年龄不符合条件';
            return false;
        }

        # 上次还款时间小于七天 到复审通过列表
        $apply = $this->where(['uid'=>$uid])->order('id desc')->find(); //上次借款单
        if($apply['hk_time']&&$apply['status']==5){
            $lastTime = (SYS_TIMESTAMP-strtotime($apply['hk_time']))/(24*3600);
            if($lastTime<7){
                $remark                     = '七天内借款自动审核';
                $time                       = date('Y-m-d H:i:s');
                $param['check_aid']         = $apply['check_aid'];
                $param['check_time']        = $apply['check_time'];
                $param['check2_aid']        = $apply['check2_aid'];
                $param['check2_time']       = $apply['check2_time'];
                // $param['merchandiser']      = $apply['merchandiser'];
                // $param['treasurer']         = $apply['treasurer'];
                $param['meets']             = $apply['meets'];
                // $param['return_visit']      = $apply['return_visit'];
                $param['remark']            = $remark;
                $param['review']            = $remark;
                $param['first_claim_date']  = $time;
                // $param['monepassdate']      = $time;
                $param['second_claim_date'] = $time;
                // $param['third_claim_date']  = $time;
                // $param['fives_claim_date']  = $time;
                $param['status']            = 2;
                $param['is_re_apply']       = 1;
            }
        }

//		var_dump($param['type']);exit;
        #是否为收费认证类型
        if(!empty( $param['type'] )) {
            if (in_array($param['type'], [1, 2, 3])) {
                $authFee = $this->getAuthFee($param['type']);
                if ($authFee) {
                    //创建认证订单
                    return $this->createApplyOrder($param);
                }
            } else {
                $this->error = '类型错误';
                return false;
            }
        }

        $res = $this -> data($param)-> allowField(true)->save();
        if($res){
            return '申请成功';
        }else{
            $this->error = '申请失败';
            return false;
        }
    }

    /**
     * 创建收费认证申请订单
     * @author cxr <1481746137@qq.com>
     */
    public function createApplyOrder($param){

        $authFee = $this->getAuthFee($param['type']);
        /* 认证类型 认证金额 */
        $param['auth_type'] = $param['type'];
        $param['auth_money'] = $authFee;

        $appy_order = [];
        $no_order = $this->order_number();  //支付订单号 重新生成
       // $no_order = $param['order_number'];  //回滚回滚

        /* 申请数据json */
        $appy_order['apply_data'] = json_encode( $param );
        $appy_order['money_order'] = $authFee;
        $appy_order['apply_id'] = $param['order_number'];
        $appy_order['no_order'] = $no_order;
        $appy_order['auth_type'] = $param['type'];

        $appy_order['uid'] = $param['uid'];
        $appy_order['actual_name'] = $param['name'];
        $appy_order['mobile'] = $param['phone'];


        $res['money_order'] = (string)$authFee;  //金额
        $res['info_order']  = '收费认证';   //描述
        $res['no_order']    = $no_order;    //支付订单号

        $param['pay_type'] = $param['pay_type'] ?: 0;
        /*检查允许的支付方式*/
        if( !Order::allowPayType($param['pay_type'])){
            $this->error = '正在维护，暂停使用';
            return false;
        }

        //支付方式 0 支付宝 1富友
        if(empty($param['pay_type'])){
            //调用支付宝支付
            $notify_url = config('domainPath')['api'].'/v2/notify_auth';  //认证收费回调
            $hp = new HcPay($notify_url);
            $data = $hp->aliPay($res);
            if(!$data)
            {
                $this->error = $hp->getError();
                return false;
            }

            # 防止频繁支付
            $requstLock = $this->payLock($param);
            if($requstLock) {
                $this->error = '请3分钟后再试,以免重复支付';
                return false;
            }

            //创建订单
            $order = ApplyOrder::create($appy_order);
            if(!$order){
                $this->error = '订单创建失败';
                return false;
            }
            return $data;
        }elseif($param['pay_type'] == 1){
            //检查银行卡的合法性
            $userBankCard =  new UserBankCard;
            $cardInfo = $userBankCard->checkBank($param['bankId'], $param['uid']);
            if(!$cardInfo)
            {
                $this->error = $userBankCard->getError();
                return false;
            }

            $redis = new Redis;
            //redis防止重复操作
            $redis->select(config('redis_select'));
            if($redis->get('auth_pay:r'.$cardInfo['uid']))
            {
                $this->error = '请求支付中,请10秒后再试';
                return false;
            }
            $data['no_order']         = $appy_order['no_order'];//订单号
            $data['money_order']      = $appy_order['money_order'];//订单金额
            $data['order_number']     = $appy_order['no_order'];//商户订单号
            $data['name']             = $cardInfo['name'];////姓名
            $data['id_card']          = $cardInfo['id_card'];//身份真
            $data['user_nm']          = $cardInfo['user_nm'];//行别
            $data['bank_card_number'] = $cardInfo['bank_card_number'];//银行卡
            $data['phone']            = $cardInfo['bind_mob'];//手机号
            //项目录入
            $order = new Order;
            //获取参数
            $re = $order->getFuiouProjectParam($data);
            $fuiouApi = new FuiouApi;
            //项目录入
            $projectRes = $fuiouApi->project($re);
            $projectRes = json_decode($projectRes,true);
            //项目录入成功
            if($projectRes['ret'] != '0000')
            {
                $this->error = $projectRes['memo'];
                return false;
            }
            //创建订单
            $appy_order['project_id'] = $projectRes['project_id'];
            $order = ApplyOrder::create($appy_order);
            if(!$order){
                $this->error = '订单创建失败';
                return false;
            }
            //支付参数
            $data['orderno']   = $appy_order['no_order'];//订单号
            $data['bankno']    = $cardInfo['user_nm'];//总行代码 
            $data['accntno']   = $cardInfo['bank_card_number'];//账号
            $data['accntnm']   = $cardInfo['name'];//账户名称
            $data['amt']       = $re['project_amt'] * 100;//金额
            $data['certno']    = $cardInfo['id_card'];//证件号
            $data['mobile']    = $param['bind_phone'] ?: $cardInfo['bind_mob'];//手机号
            $data['entseq']    = $appy_order['no_order'];//企业流水号
            $data['memo']      = '用户还款';//备注
            $data['projectid'] = $projectRes['project_id'];//项目id
            $data['txncd']     = '09';//业务定义
            //请求支付
            $fuiouApi = new FuiouApi;
            $json = $fuiouApi->incomforreq($data);
            //支付结果
            $jsonRe = json_decode($json,true);
            if(!$jsonRe)
            {
                $this->error = '请求支付失败';
                return false;
            }
            $datas['pay_code'] = $jsonRe['ret'];
            $datas['pay_info'] = $jsonRe['memo'];
            $applyOrder =new ApplyOrder;
            $applyOrder->allowField(true)->save($datas,['id' => $order->id]);
            //未签约单独判断
            if($jsonRe['ret'] == '100011')
            {
                if($jsonRe['memo'] === '未签约')
                {
                    $this->error = $jsonRe['memo'];
                    return 'noContract';
                }
            }
            if($jsonRe['ret'] != '000000')
            {
                $this->error = $jsonRe['memo'];
                return false;
            }
            //更改手机号
            if($param['bind_phone'] !== $cardInfo['bind_mob'])
            {
                $userBankCard->allowField(true)->save(['bind_mob' => $param['bind_phone']],['bank_card_number' => $cardInfo['bank_card_number']]);
            }
            //十秒误操作
            $redis->set('auth_pay:r'.$cardInfo['uid'],'1',10);
            //创建订单
            $applyParam['merchantOutOrderNo'] = $appy_order['no_order'];
            $applyParam['payResult'] = 1;
            $applyParam['orderNo'] = $appy_order['no_order'];
            $applyParam['pay_result'] = 'SUCCESS';
            $afterRe = $applyOrder->ApplyNotify($applyParam);
            if(!$afterRe)
            {
                file_put_contents(LOGS_DIR_NAME."authError_log.txt", '支付时间:'.date('Y-m-d H:i:s',time())." 结果:".$applyOrder->getError()." 回调详情:".json_encode($applyParam)."\n\n", FILE_APPEND);
                $this->error = '支付成功,但申请订单失败';
                return false;   
            }
            return '成功';

        }
    }

    /**
     * 转认证收费
     * @author cxr <1481746137@qq.com>
     */
    public function transferAuthVip($param_data,$uid){
        unset($param_data['money'],$param_data['cid']);
        if($param_data['pay_type'] == 1) {

            # 校验支付密码
            if (in_array($param_data['type'], [1, 2, 3])) {
                $UserModel = new User;
                $res = $UserModel->checkPayPassword($uid, $param_data['pay_password']);
                if (!$res) {
                    $this->error = $UserModel->getError();
                    return false;
                }
            }
        }

        if(!isset($param_data['order_number'])) {
            $this->error = '参数错误';
            return false;
        }

        $applyInfo = $this->field('id,uid,order_number,phone,name')->where(['order_number' => $param_data['order_number']])->find();
        if(!$applyInfo){
            $this->error = '出现异常';
            return false;
        }

        $UserInfoModel = new UserInfo;
        $userinfo = $UserInfoModel->field('rand_code,phone')->where('uid', $uid)->find();
        $param_data['bind_phone'] = $param_data['phone'];
        $param_data['phone'] = $userinfo['phone'];
        $applyInfo = array_merge($applyInfo->toArray(), $param_data);
        $applyInfo['rand_code'] = $userinfo['rand_code'];


        #是否为收费认证类型
        if(!empty( $param_data['type'] )) {
            if (in_array($param_data['type'], [1, 2, 3])) {
                $authFee = $this->getAuthFee($param_data['type']);
                if ($authFee) {
                    //创建认证订单
                    return $this->createApplyOrder($applyInfo);
                }
            } else {
                $this->error = '类型错误';
                return false;
            }
        }else{
            $this->error = '缺少参数';
            return false;
        }
    }

    /**
     * 申请进度
     * @author cxr <1481746137@qq.com>
     */
    public function applyProgress($param,$uid){

        $applyInfo = $this->field('order_number,auth_type as type,status,check_aid,merchandiser,treasurer')
            ->where('uid',$uid)->order('time DESC')->find();

        if(!$applyInfo){
            $this->error = '未找到订单';
            return false;
        }

        # 进度
        if(!empty( $applyInfo['check_aid'])){
            $status = 1;
        }if(!empty( $applyInfo['merchandiser']) ){
            $status = 2;
        }if(in_array($applyInfo['status'],[3,4,12]) || in_array($applyInfo['status'],[9]) ){
            $status = 3;
        }if(in_array($applyInfo['status'],[5,6,7,8,10,11])){
            $status = 4;
        }
        $applyInfo['status'] = $status;

        # 认证收费方式
        $applyInfo['auth_type'] = ApplyAuth::all(function($query){
            $query->order('type DESC');
        });

        # 提示
        $typeName = $applyInfo['auth_type'][2 - $applyInfo['type']]['name'];
        $typeRemark = $applyInfo['auth_type'][2 - $applyInfo['type']]['remark'];
        $applyInfo['tip'] = "您已选择$typeName,需$typeRemark";

        #只保留VIP
        $applyInfo['auth_type'] = $applyInfo['auth_type'][0];

        return $applyInfo;
    }

    /**
     * 返回认证费用
     * @author cxr <1481746137@qq.com>
     * @param $type
     * @return mixed
     */
    public function getAuthFee($type){
        $authFee = [
            2 => ['money' => '28.8'],
            1 => ['money' => '18.8'],
            0 => ['money' => '0']
        ]; 
//        return $authFee[$type]['money'];

        $authFee = model('admin/ApplyAuth')->where('type',$type)->find();
        return $authFee['money'];


    }



    /**
     * 生成订单编号
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-07-31 09:30
     * @return   string
     */
    public function order_number(){
        $number = date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8); //订单编号
        $count = $this->where("order_number",$number)->count();
        if($count>0){
            return $this->order_number();
        }

        # Redis去重
        $config = config();
        $redis  = new Redis();
        $redis -> select($config['redis_select']); //17号库
        $redisKey = $config['project_name'].":order_number_".$number;
        if(false!==$redis->get($redisKey)){
            return $this->order_number();
        }
        $redis -> set($redisKey, '', 10); //缓存10s

        return $number;
    }

    /**
     * 生成合同编号
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-07-31 09:30
     * @return   string
     */
    public function contract_number(){
        $contract = 'C'.date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8); //合同编号
        $count = $this->where("contract_number", $contract)->count();
        if($count>0){
            return $this->contract_number();
        }

        # Redis去重
        $config = config();
        $redis    = new Redis();
        $redisKey = $config['project_name'].":contract_number_".$contract;
        $redis -> select($config['redis_select']); //17号库
        if(false!==$redis->get($redisKey)){
            return $this->contract_number();
        }
        $redis -> set($redisKey, '', 10); //缓存10s

        return $contract;
    }


    /**
     * 我的借款列表
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-07-31 10:50
     * @param    int    $uid     用户id
     * @param    int    $status  状态码
     * @return   json
     */
    public function myApply($uid, $status='0'){
        $where['uid'] = $uid;
        // switch ($status) {  //根据状态筛选
        // 	case '0':  //全部
        // 		break;
        // 	case '1':  //待审核
        // 		$where['status'] = 0;
        // 		break;
        // 	case '2': //审核中
        // 		$where['status'] = array('in', [1,2]);
        // 		break;
        // 	case '3': //放款中
        // 		$where['status'] = 9;
        // 		break;
        // 	case '4': //待还款
        // 		$where['status'] = 3;
        // 		break;
        // 	case '5': //审核未通过
        // 		$where['status'] = array('in', [6,7,8,10,11]);
        // 		break;
        // 	case '6': //已逾期
        // 		$where['status'] = 4;
        // 		break;
        // 	case '7': //已结清
        // 		$where['status'] = 5;
        // 		break;
        // 	case '8': //已续贷
        // 		$where['status'] = 12;
        // 		break;
        // 	default:
        // 		break;
        // }

        switch ($status) {  //根据状态筛选
            case '0':  //全部
                break;
            case '1': //进行中
                $where['status'] = array('in', [0,1,2,3,9,12]);
                break;
            case '2': //已完成
                $where['status'] = 5;
                break;
            case '3': //已违约
                $where['status'] = 4;
                break;
            case '4': //未通过
                $where['status'] = array('in', [6,7,8,10,11]);
                break;
            default:
                break;
        }

        $data = $this ->where($where) ->order('id desc')->select();
        if(!$data){
            return [];
        }
        $data = modelo2array($data);
        foreach ($data as $key => $value) {            
            $loanMoney = $this->loanMoney($value);   //各种费用换算
            $result[$key] = array(
                'time'            => $value['time'],
                'money'           => formatMoney($value['money']),
                'status'          => $loanMoney['status'],
                'statusInfo'      => $loanMoney['statusInfo'],
                'order_number'    => $value['order_number'],
                // 'reloanTime'      => empty($value['hk_time']) ? $loanMoney['appoint_time'] : date('Y-m-d', strtotime($value['hk_time'])),
                'reloanTime'      => $loanMoney['appoint_time'],
                'period'          => $loanMoney['period'],
                'creditMoney'     => $loanMoney['creditMoney'],
                'contract_number' => $value['contract_number'],
                'loan_out_money'  => $value['loan_out_money'],
            );
        }

        return  $result;
    }

    /**
     * 借款详情
     * @param  [type] $order_number 订单号
     * @return [type]               [description]
     */
    public function myApplyDetails($order_number) {
        $apply = $this->where('order_number', $order_number)->find();
        if (!$apply) {
            $this->error = '订单无效';
            return false;
        }

        $apply = $apply->toArray();

        $money             = $apply['money'];            //本金
        $time              = $apply['time'];             //申请时间
        $period            = $apply['period'];           //借款周期
        $dailyInterestRate = $apply['rate'];             //日利率
        $manageRate        = $apply['service_rate'];     //管理费率
        $delayRate         = $apply['delay_rate'];       //延期手续费率
        $delayCount        = empty($apply['renewal_count']) ? 0 : $apply['renewal_count']; //延期次数

        // 计算各种费用
        $loanMoney = $this->loanMoney($apply);

        // 重算费用
        if(!empty($apply['hk_time'])){// 已还款
            $costDay = ceil((strtotime($apply['hk_time'])-strtotime($apply['time']))/(24*3600));//已借天数
            // 利息 = 本金*已借天数* 日利率
            $loanMoney['interestMoney'] = formatMoney($money*$costDay*$dailyInterestRate);
            // 管理费 = 本金*已借天数*管理费率
            $loanMoney['manageMoney'] = formatMoney($money*$costDay*$manageRate);
            $loanMoney['overdueMoney']    = formatMoney($apply['overdue_money']);   // 总逾期费
            $loanMoney['delayMoney']      = formatMoney($apply['delay_money']);     // 总延期费
        }

        // 未放款的 清空放款时间和约定还款时间
        if(empty($apply['fk_time'])){
            $loanMoney['fk_time']      = '';
            $loanMoney['appoint_time'] = '';
        }

        $loanMoney['overdueCount']    = $apply['overdue_count'];   // 总逾期次数
        $loanMoney['delayCount']      = $delayCount;
        $loanMoney['status']          = $loanMoney['status'];
        $loanMoney['statusInfo']      = $loanMoney['statusInfo'];
        $loanMoney['actual_money']    = empty($apply['actual_money']) ? 0 : $apply['actual_money'];
        $loanMoney['contract_number'] = $apply['contract_number'];
        $loanMoney['loan_out_money']  = $apply['loan_out_money'];// 借款金额

        // 应还本金 2018/2/27
        $loanMoney['cur_money']       = empty($loanMoney['cur_money']) ? $loanMoney['money'] : $loanMoney['cur_money'];

        unset($loanMoney['info_order']);
        unset($loanMoney['no_order']);
        unset($loanMoney['delay_money']);
        unset($loanMoney['overdue_money']);

        return $loanMoney;

    }


    /**
     * 还款页面详情 2018/3/9
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-09-28
     * @param    [type]     $uid [description]
     * @param    [type]     $type [还款方式 1:全额  2:延期  3:部分还款]
     * @param    [type]     $days [延期天数 type=2时才有]
     * @return   [type]          [description]
     */
    public function repayment($uid, $type, $days='')
    {
        $map['uid'] = $uid;
        $map['status'] = array('in',[3,4,12]);
        $res = $this->where($map)->order('id desc')->find();
        if(!$res){
            $this->error="没有需要归还的订单";
            return false;
        }
        $apply = $res->toArray();

        switch ($type) {
            case '1': //全额还款
                $order_details = $this->loanMoney($apply);
                break;
            case '2': //延期
                if(empty($days)){
                    $this->error="缺少参数:days";
                    return false;
                }
                $order_details = $this->delayMoney($apply,$days);
                if(!$order_details){
                    $this->error="延期天数非法";
                    return false;
                }
                break;
            case '3': //部分还款
                $order_details = $this->loanMoney($apply);
                break;
            default:
                $this->error='参数错误:pay_type';
                return false;
                break;
        }

        $data['order_details']  = $order_details;  //还款账单详情
        $data['agreement_list'] = $this->getUserBankCard($uid);  //银行卡列表
        $data['cuishou_tips']   = empty($apply['cuishou_tips']) ? "": $apply['cuishou_tips']; // 催收提示语 2018/2/13
        return $data;
    }
    /**
     * [getUserBankCard 获取用户银行卡]
     * @param  string $uid [description]
     * @return [type]      [description]
     */
    public function getUserBankCard($uid = '')
    {
        $UbcModel = new UserBankCard();
        $bank = $UbcModel->getDataList(['uid' => $uid,'status' => 1],'bank_card_number,user_nm,bind_mob,id');
        $bankList = [];
        if($bank)
        {
            foreach ($bank as $v) {
                 $bcNumber = substr($v['bank_card_number'],-4,4);
                 $nm = $this->switchBank($v['user_nm']);
                 $bankList[] = ['bank_card_number' => $bcNumber, 'nm' => $nm, 'phone' => $v['bind_mob'], 'bank_id' => $v['id']];
            }
        }

        return $bankList;
    }
    /**
     * [switchBank 选择银行卡]
     * @param  [type] $nm [description]
     * @return [type]     [description]
     */
    public function switchBank ($nm){
        switch($nm){
        case '0102': return '中国工商银行';
        case '0103': return '中国农业银行';
        case '0105': return '中国建设银行';
        case '0301': return '交通银行';
        case '0308': return '招商银行';
        case '0309': return '兴业银行';
        case '0305': return '中国民生银行';
        case '0306': return '广东发展银行';
        case '0307': return '平安银行股份有限公司';
        case '0310': return '上海浦东发展银行';
        case '0304': return '华夏银行';
        case '0313': return '其他城市商业银行 (北京银行、上海银行)';
        case '0314': return '其他农村商业银行';
        case '0315': return '恒丰银行';
        case '0403': return '中国邮政储蓄银行股份有限公司';
        case '0303': return '中国光大银行';
        case '0302': return '中信银行';
        case '0316': return '浙商银行股份有限公司';
        case '0318': return '渤海银行股份有限公司';
        case '0319': return '徽商银行';
        case '0320': return '村镇银行';
        case '0322': return '上海农村商业银行';
        case '0402': return '其他农村信用合作社';
        case '0104': return '中国银行';
        default: return '未知';
}
    }


    /**
     * 判断订单是否处于贷打款状态
     * @Author      火星车11号
     * @email       704184317@qq.com
     * @create_time 2017-10-11T15:56:19+0800
     * @param       id  申请id
     * @return      bool
     */
    public function checkApplyStatusByid($order_number)
    {
        $apply_info = $this->where(["order_number"=>$order_number])->find();
        if(!$apply_info) {
            $this->error = "没有获取到申请单信息";
            return false;
        }
        //一天只能放一次款
        $where['uid'] = $apply_info->uid;
        $where['create_time'] = ['egt',date('Y-m-d')];
        $where['result_pay'] = 'SUCCESS';
        $count = Db::name('payment_order')->where($where)->count('id');
        if($count > 0)
        {
            $this->error = "当天不可重复放款！";
            return false;
        }
        //特殊情况 档位会变，正在查找原因中......
        if($apply_info['money'] < $apply_info['loan_out_money'])
        {
            $this->error = '订单异常，请联系技术人员';
            return false;
        }
        if(9!=$apply_info->status){
            $this->error = "此订单状态不能放款，请先确认";
            return false;
        }

        return $apply_info;
    }


    /**
     * 是否在拒绝放款的地区
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-22T16:46:27+0800
     * @param    string      $address    定位的地址
     * @return   boolean     true：表示在拒绝的地区
     */
    public function is_reject_area($address='')
    {
        $appSysConfig = controller('Config')->getAppConfigs();
        $area = $appSysConfig['data']['APP_REJECT_AREA'];
        $area_arr = explode('/', $area);
        foreach ($area_arr as $key => $value) {
            $find = stripos($address,$value);
            if(false !== $find){
                return true;
            }
        }
        return false;
    }


    /**
     * 是否在拒绝放款年龄
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-25T16:09:11+0800
     * @param    string      $id_card   身份证号码
     * @return   boolean     true：表示在拒绝放款年龄
     */
    public function is_reject_age($id_card='')
    {
        $appSysConfig = controller('Config')->getAppConfigs();
        $age_min = $appSysConfig['data']['APP_ALLOW_AGE_MIN']; //允许最小年龄
        $age_max = $appSysConfig['data']['APP_ALLOW_AGE_MAX']; //允许最大年龄
        $year = (int)substr($id_card,6,4);
        $age  = (int)date('Y',time()) - $year;  //用户年龄
        if($age > (int)$age_max || $age < (int)$age_min){
            return true;
        }
        return false;
    }


    /**
     * 应还日期、放款时间和当前日期对比
     * @param  [string] $fk_time      [放款日期]
     * @param  [string] $appoint_time [应还日期]
     * @return [array]  $data         [description]
     */
    public function timeStatus($fk_time, $appoint_time) {
        $overdue_day = $advance_day = $cost_day = 0;
        $info   = '未知';
        $status = '-1';

        if(!empty($fk_time)){
            if(!defined(SYS_TIMESTAMP))
            {
                define(SYS_TIMESTAMP,time());
            }
            $date_now     = date("Y-m-d",SYS_TIMESTAMP);
            $date_fk      = date("Y-m-d", strtotime($fk_time));
            $date_appoint = date("Y-m-d", strtotime($appoint_time));

            $day = (strtotime($date_now) - strtotime($date_appoint)) /(24*3600);    //提前或逾期天数

            $cost_day = (strtotime($date_now)-strtotime($date_fk))/(24*3600);
            $cost_day = $cost_day == 0 ? 1 : $cost_day ;    //已借天数 不足一天算一天

            $cost_info = '已借款'.$cost_day.'天';
            if($day>0){
                $info = '逾期'.$day.'天';
                $status = '2';
                $overdue_day = $day;
            }elseif($day<0){
                $day = abs($day);
                $info = '提前'.$day.'天';
                $status = '1';
                $advance_day = $day;
            }elseif($day==0){
                $info = '正常';
                $status = '0';
            }
        }

        $data = array(
            'status'       => $status,       //状态 -1:未知 0:正常 1:提前 2:逾期
            'info'         => $info,         //描述
            'overdue_day'  => $overdue_day,  //逾期天数
            'advance_day'  => $advance_day,  //提前天数
            'cost_day'     => $cost_day,     //已借天数
            'cost_info'    => $cost_info,    //已借天数描述
            'date_now'     => $date_now,     //现在日期
            'date_fk'      => $date_fk,      //放款日期
            'date_appoint' => $date_appoint, //应还日期
        );

        return $data;
    }

    /**
     * 延期金额换算 2018/3/9
     * @param  [type] $apply [申请信息]
     * @param  [type] $days  [延期天数 默认展期]
     * @return [type]        [description]
     */
    public function delayMoney($apply, $days=7)
    {
        if(!$apply){
            $this->error='订单无效';
            return false;
        }

        $timeStatus = $this->timeStatus($apply['fk_time'], $apply['appoint_time']);

        $money             = $apply['money'];            //原始借款金额
        $cur_money         = empty($apply['cur_money']) ? $money : $apply['cur_money'];  // 应还本金 2018/2/27
        $period            = $apply['period'];           //借款周期
        $creditRate        = $apply['credit_rate'];      //信审费率
        $dailyInterestRate = $apply['rate'];             //日利率
        $manageRate        = $apply['service_rate'];     //管理费率
        $costDay           = $timeStatus['cost_day'];    //已借天数
        $overdueRate       = $apply['overdue_rate'];     //逾期费率
        $overdueDay        = $timeStatus['overdue_day']; //逾期天数
        $advanceRate       = $apply['procedure_rate'];   //提前还款手续费率
        $delayRate         = $apply['delay_rate'];       //延期手续费率
        $advanceDay        = $timeStatus['advance_day']; //提前天数
        $renewalCount      = empty($apply['renewal_count']) ? 0 : $apply['renewal_count']; //续期次数
        $zhangqi           = empty($apply['zhangqi']) ? 0 : $apply['zhangqi']; //是否已经展期

        // $overdueCount      = empty($apply['overdue_count']) ? 0 : $apply['overdue_count']; //逾期次数

        // 部分还款后 逾期天数 = 当前日期 - 上次部分还款日期  2018/3/9
        // 部分还款后又延期 逾期天数还是按照原来的计算 2018/05/17
        if(!empty($apply['part_date']) && strtotime($apply['part_date']) > strtotime($apply['appoint_time'])){
            $part_date  = date("Y-m-d", strtotime($apply['part_date']));
            $date_now   = date("Y-m-d",time());
            $overdueDay = (strtotime($date_now) - strtotime($part_date)) /(24*3600);
        }

        // 信审费 = 本金*借款周期*信审费率
        $data['creditMoney'] = formatMoney($money*$period*$creditRate);

        // 利息 = 本金*已借天数* 日利率
        $data['interestMoney'] = formatMoney($money*$costDay*$dailyInterestRate);

        // 管理费 = 本金*已借天数*管理费率
        $data['manageMoney'] = formatMoney($money*$costDay*$manageRate);

        // 逾期费 = 本金*逾期天数*逾期费率
        $data['overdueMoney'] = formatMoney($money*$overdueDay*$overdueRate);

        // 提前还款手续费 = 本金*提前天数*提前还款手续费率
        $data['advanceMoney'] = formatMoney($money*$advanceDay*$advanceRate);

        // 延期手续费 = 本金*借款周期*延期手续费率
        $data['delayMoney'] = formatMoney($money*$period*$delayRate);

        // 展期是否
        $data['zhanqi'] = $zhangqi;

        // 逾期费上限止于放款金额3倍 2018年5月31日14:24:12
        if ($data['overdueMoney'] > 3*$apply['loan_out_money']) {
            $data['overdueMoney'] = 3*$apply['loan_out_money'];
        }

        // 此次延期需要归还的本金 = 剩余本金*0.2
        // $data['currentMoney'] = formatMoney($cur_money * 0.2);
        $data['currentMoney'] = 0;
        if($days==7){
            // 延期还款总额 = 利息 + 管理费 + 延期手续费 + 逾期费
            $data['money_order'] = $data['overdueMoney'];
            $data['delayMoney'] = formatMoney(config('YANQI_ZHANQI')*$days*$cur_money);
            $data['currentMoney'] = formatMoney($cur_money * 0.2);
        }elseif ($days == 1) {
            $data['money_order'] =$data['overdueMoney'];
            $YANQI_ONEDAY = config('YANQI_ONEDAY');
            $data['delayMoney'] = formatMoney($money*0.1*$YANQI_ONEDAY);

        }elseif ($days == 2) {
            $data['money_order'] =$data['overdueMoney'];
            $YANQI_TWODAY = config('YANQI_TWODAY');
            $data['delayMoney'] = formatMoney($money*0.1*$YANQI_TWODAY*$days);

        }elseif ($days == 3) {
            $data['money_order'] =$data['overdueMoney'];
            $YANQI_THIRDDAY = config('YANQI_THIRDDAY');
            $data['delayMoney'] = formatMoney($money*0.1*$YANQI_THIRDDAY*$days);
        }else{
            $this->error='延期天数非法';
            return false;
        }

        // 延期总费 = 延期还款总额 + 此次延期需要归还的本金    2018/2/27
        $data['money_order'] = $data['money_order'] + $data['delayMoney'] + $data['currentMoney'];

        // 取两位小数并去掉.00
        $data['money_order'] = formatMoney($data['money_order']);

        $arr = array(
            '-1' => '默认延期',
            '0'  => '正常延期',
            '1'  => '提前延期',
            '2'  => '逾期延期'
        );
        $info_order              = $arr[$timeStatus['status']]; //订单描述
        $data['renewalCount']    = $renewalCount;
        $data['overdueCount']    = $overdueCount;
        $data['receivedMoney']   = $money - $data['creditMoney'];  //实际到帐金额
        $data['money']           = formatMoney($apply['money']);
        $data['cur_money']       = formatMoney($cur_money);  // 应还本金 2018/2/27
        $data['period']          = $apply['period'];
        $data['statusInfo']      = $this->statusInfo($apply['status']);
        $data['status']          = $apply['status'];
        $data['time']            = $apply['time'];
        $data['fk_time']         = $apply['fk_time'];
        $data['appoint_time']    = empty($apply['appoint_time']) ? '' : date("Y-m-d", strtotime($apply['appoint_time']));
        $data['hk_time']         = $apply['hk_time'];
        $data['order_number']    = $apply['order_number'];
        $data['no_order']        = $apply['order_number'];
        $data['loan_out_money']  = $apply['loan_out_money'];
        $data['contract_number'] = $apply['contract_number'];

        // $data['delay_appoint_time'] = date("Y-m-d", strtotime($apply['appoint_time']) + $days*24*3600);

        // 延期到什么时候  2018/3/23
        if(strtotime(date("Y-m-d", strtotime($data['appoint_time']))) > strtotime(date("Y-m-d"))){
            $startTime = strtotime($data['appoint_time']);
        }else{
            $startTime = time();
        }
        $data['delay_appoint_time'] = date("Y-m-d", $startTime + $days*24*3600);


        $data['bank_card_number']   = $apply['bank_card_number'];

        $data['info_order']  = $info_order . ' 延期次数:' . ($renewalCount+1);
        // 续期订单号【格式：原单号Q续期次数】 如：2017101257514850XQ2
        // $data['no_order'] = $apply['order_number'].'Q'.($apply['renewal_count']+1);

        // null 处理
        foreach ($data as $key => $value) {
            if(is_null($value)){
                $data[$key] = '';
            }
        }
        $handOverdue = $this->handOverdue($apply); //实时逾期处理
        $data['statusInfo'] = $handOverdue['statusInfo'];
        $data['status']     = $handOverdue['status'];
        return $data;
    }


    /**
     * 还款金额换算
     * @param  [type] $apply [申请信息]
     * @return [type]        [description]
     */
    public function loanMoney($apply) {
        if(!$apply){
            $this->error='订单无效';
            return false;
        }

        $timeStatus = $this->timeStatus($apply['fk_time'], $apply['appoint_time']);

        $money             = $apply['money'];            //本金
        $cur_money         = empty($apply['cur_money']) ? $money : $apply['cur_money'];  // 应还本金 2018/2/27
        $period            = $apply['period'];           //借款周期
        $creditRate        = $apply['credit_rate'];      //信审费率
        $dailyInterestRate = $apply['rate'];             //日利率
        $manageRate        = $apply['service_rate'];     //管理费率
        $costDay           = $timeStatus['cost_day'];    //已借天数
        $overdueRate       = $apply['overdue_rate'];     //逾期费率
        $overdueDay        = $timeStatus['overdue_day']; //逾期天数
        $advanceRate       = $apply['procedure_rate'];   //提前还款手续费率
        $advanceDay        = $timeStatus['advance_day']; //提前天数
        $renewalCount      = empty($apply['renewal_count']) ? 0 : $apply['renewal_count']; //续期次数
        // $overdueCount      = empty($apply['overdue_count']) ? 0 : $apply['overdue_count']; //逾期次数

        // 部分还款后 逾期天数 = 当前日期 - 上次部分还款日期  2018/3/9
        // 部分还款后又延期 逾期天数还是按照原来的计算 2018/05/17
        if(!empty($apply['part_date']) && strtotime($apply['part_date']) > strtotime($apply['appoint_time'])){
            $part_date  = date("Y-m-d", strtotime($apply['part_date']));
            $date_now   = date("Y-m-d",time());
            $overdueDay = (strtotime($date_now) - strtotime($part_date)) /(24*3600);
        }

        // 信审费 = 本金*借款周期*信审费率
        $data['creditMoney'] = formatMoney($money*$period*$creditRate);

        // 利息 = 本金*已借天数* 日利率
        $data['interestMoney'] = formatMoney($money*$costDay*$dailyInterestRate);

        // 管理费 = 本金*已借天数*管理费率
        $data['manageMoney'] = formatMoney($money*$costDay*$manageRate);

        // 逾期费 = 本金*逾期天数*逾期费率
        $data['overdueMoney'] = formatMoney($money*$overdueDay*$overdueRate);

        // 提前还款手续费 = 本金*提前天数*提前还款手续费率
        $data['advanceMoney'] = formatMoney($money*$advanceDay*$advanceRate);

        // 延期手续费 = 本金*借款周期*延期手续费率
        $data['delayMoney'] = formatMoney($money*$period*$delayRate);

        // 还款总额 = 本金+本金*已借天数*(日利率+管理费率) + 本金*(提前天数*提前还款手续费率+逾期费率*逾期天数)
        // $data['money_order'] = $money + $money*$costDay*($dailyInterestRate+$manageRate)+ $money*($advanceDay*$advanceRate+$overdueRate*$overdueDay);
        // 也等于  本金+利息+管理费+逾期费+提前还款手续费
        // $data['money_order'] = $money+$data['interestMoney']+$data['manageMoney']+$data['overdueMoney']+$data['advanceMoney'];

        // 订单完成后 重算费用 2018/05/17
        if(!empty($apply['hk_time'])){// 已还款
            $costDay = ceil((strtotime($apply['hk_time'])-strtotime($apply['time']))/(24*3600));//已借天数
            // 利息 = 本金*已借天数* 日利率
            $data['interestMoney'] = formatMoney($money*$costDay*$dailyInterestRate);
            // 管理费 = 本金*已借天数*管理费率
            $data['manageMoney']  = formatMoney($money*$costDay*$manageRate);
            $data['overdueMoney'] = formatMoney($apply['overdue_money']);   // 总逾期费
            $data['delayMoney']   = formatMoney($apply['delay_money']);     // 总延期费
        }

        // 逾期费上限止于放款金额3倍 2018年5月31日14:24:12
        if ($data['overdueMoney'] > 3*$apply['loan_out_money']) {
            $data['overdueMoney'] = 3*$apply['loan_out_money'];
        }
        
        // 还款总额 = 应还本金+利息+管理费+逾期费+提前还款手续费   2018/2/27
        $data['money_order'] = $cur_money+$data['interestMoney']+$data['manageMoney']+$data['overdueMoney']+$data['advanceMoney'];

        $data['money_order'] = formatMoney($data['money_order']);  //取两位小数并去掉.00

        //状态 -1:未知 0:正常 1:提前 2:逾期
        $arr = array(
            '-1' => '默认归还',
            '0'  => '正常归还',
            '1'  => '提前归还',
            '2'  => '逾期归还'
        );        

        $data['renewalCount']     = $renewalCount;
        $data['info_order']       = $arr[$timeStatus['status']];    //订单描述
        $data['receivedMoney']    = $money - $data['creditMoney'];  //实际到帐金额
        $data['money']            = formatMoney($apply['money']);
        $data['cur_money']        = formatMoney($cur_money);  // 应还本金 2018/2/27
        $data['period']           = $apply['period'];
        $data['time']             = $apply['time'];
        $data['fk_time']          = $apply['fk_time'];
        $data['appoint_time']     = empty($apply['appoint_time']) ? '' : date("Y-m-d", strtotime($apply['appoint_time']));
        $data['hk_time']          = $apply['hk_time'];
        $data['order_number']     = $apply['order_number'];
        $data['no_order']         = $apply['order_number'];
        $data['bank_card_number'] = $apply['bank_card_number'];
        $data['loan_out_money']   = $apply['loan_out_money'];
        $data['contract_number']  = $apply['contract_number'];

        // null 处理
        foreach ($data as $key => $value) {
            if(is_null($value)){
                $data[$key] = '';
            }
        }
        $handOverdue = $this->handOverdue($apply); //实时逾期处理
        $data['statusInfo'] = $handOverdue['statusInfo'];
        $data['status']     = $handOverdue['status'];

        return $data;

    }


    /**
     * 申请单状态对应描述
     * @param  integer $status [申请单状态]
     * @param  string  $show   [展示给谁看，展示给用户就不需要展示后台操作流程]
     * @return [type]          [description]
     */
    public function statusInfo($status = -1, $show ='user')  {
        $showDefault = array(
            -1 => '未知状态',
            0  => '申请中',
            1  => '初审通过',
            2  => '复审通过',
            3  => '还款中',
            4  => '逾期中',
            5  => '已还清',
            6  => '初审驳回',
            7  => '复审驳回',
            8  => '其他驳回',
            9  => '回访通过',
            10 => '回访驳回',
            11 => '拒绝放款',
            12 => '延期中',
        );

        // $showUser = array(
        // 	-1 => '未知状态',
        // 	0  => '待审核 ',
        // 	1  => '审核中',
        // 	2  => '审核中',
        // 	3  => '待还款',
        // 	4  => '已逾期',
        // 	5  => '已结清',
        // 	6  => '审核未通过',
        // 	7  => '审核未通过',
        // 	8  => '审核未通过',
        // 	9  => '放款中',
        // 	10 => '审核未通过',
        // 	11 => '审核未通过',
        // 	12 => '已续贷',
        // );

        $showUser = array(
            -1 => '未知状态',
            0  => '进行中',
            1  => '进行中',
            2  => '进行中',
            3  => '进行中',
            4  => '已违约',
            5  => '已完成',
            6  => '未通过',
            7  => '未通过',
            8  => '未通过',
            9  => '进行中',
            10 => '未通过',
            11 => '未通过',
            12 => '进行中',
        );

        $statusInfo = $show == 'user' ? $showUser : $showDefault;
        return  $statusInfo[$status];
    }

    /**
     * 实时逾期处理状态。仅做显示
     * @param  [type] $apply [借款单信息]
     * @return [type]        [description]
     */
    public function handOverdue($apply){
        $apply['statusInfo'] = $this->statusInfo($apply['status']);
        $sys_timestamp = strtotime(date('Y-m-d', SYS_TIMESTAMP));
        $appoint_time  = strtotime($apply['appoint_time']);
        if($apply['status']==3||$apply['status']==12){
            if($sys_timestamp-$appoint_time>0){ //逾期
                $apply['status']     = 4;
                $apply['statusInfo'] = '已违约';
            }
        }
        return $apply;
    }

    /**
     * 审核不通过再次借款倒计时
     */
    public function countdown($uid) {
        $map['uid'] = $uid;
        $apply = $this
            ->where($map)
            ->field('status,time,check_time,check2_time,monepassdate,merchandiser2_time,lendmoneydate,first_claim_date,second_claim_date,third_claim_date,fourth_claim_date')
            ->order('id desc')
            ->find(); //上次借款单
        if(!$apply){
            $this->error = '暂无订单';
            return false;
        }
        if(!in_array($apply['status'], [6,7,8,10,11])){
            $this->error = '可以申请';
            return false;
        }

        $apply = $apply->toArray();
        unset($apply['status']);

        foreach ($apply as $key => $value) {
            if(!empty($value)){
                $timeArr[] = strtotime($value);
            }
        }

        $endTime = max($timeArr) + COUNTDOWN_DAY*24*3600;
        $res = [
            'startTime' => date('Y-m-d H:i:s', SYS_TIMESTAMP),
            'endTime'   => date('Y-m-d H:i:s', $endTime)
        ];
        return $res;
    }

    /**
     * 查询合同
     * @param  [type] $contract_number [合同编号]
     * @return [type]                  [description]
     */
    public function getContractById($contract_number) {
        if(is_null($contract_number)){
            $this->error='合同id不存在';
            return false;
        }
        $res = $this->where('contract_number', $contract_number)->find();
        if(!$res){
            $this->error='合同无效';
            return false;
        }

        $money             = $res['money'];            //本金
        $cur_money         = empty($res['cur_money']) ? $money : $res['cur_money'];  // 应还本金 2018/2/27
        $period            = $res['period'];           //借款周期
        $creditRate        = $res['credit_rate'];      //信审费率
        $dailyInterestRate = $res['rate'];             //日利率
        $manageRate        = $res['service_rate'];     //管理费率
        $overdueRate       = $res['overdue_rate'];     //逾期费率
        $advanceRate       = $res['procedure_rate'];   //提前还款手续费率
        $delayRate         = $res['delay_rate'];       //延期手续费率

        // 利息 = 本金*借款周期*日利率
        $interestMoney = $money*$period*$dailyInterestRate;

        $data['contract_number']   = $res['contract_number'];
        $data['platform']          = '土豪钱庄';              //出借方
        $data['name']              = $res['name'];
        $data['id_card']           = $res['id_card'];
        $data['bank_card_number']  = $res['bank_card_number'];
        $data['phone']             = $res['phone'];
        $data['time']              = date('Y-m-d',strtotime($res['time']));
        $data['money']             = formatMoney($money);
        $data['period']            = $period;
        $data['interestMoney']     = $interestMoney;                    //利息
        $data['creditRate']        = $creditRate*100 .'%';              //信审费率
        $data['manageRate']        = $manageRate*100 .'%';              //管理费率
        $data['dailyInterestRate'] = $dailyInterestRate*100 .'%';       //日利率

        $loanMoney            = $this->loanMoney($res);
        $data['creditMoney']  = $loanMoney['creditMoney'];
        $data['appoint_time'] = $loanMoney['appoint_time'];
        $data['cur_money']    = $loanMoney['cur_money'];
        $data['money_order']  = $loanMoney['money_order'];

        $userinfoModel       = new UserInfo;
        $userinfo            = $userinfoModel->where('id_card', $data['id_card'])->field('address')->find();
        $data['address']     = $userinfo['address'];
        $data['mobile_type'] = $res['mobile_type'];

        //完成后  残值为0
        if($res['status'] == 5){
            $data['money_order']  = 0;
        }

        return $data;
    }

    /**
     * 当前是否有未处理的订单【有的话不让签约/解绑银行卡/修改资料】
     * @param  [type]  $uid [description]
     * @return boolean      [description]
     */
    public function hasApply($uid) {
        $map['uid']    = $uid;
        $map['status'] = array('in', [0,1,2,3,4,9,12]);
        $res           = $this->where($map)->order('id desc')->find();
        return $res ? true : false;
    }

    /**
     * 能否绑定银行卡
     * @param  [type]  $uid [description]
     * @return boolean      [description]
     */
    public function canBindBank($uid) {
        $map['uid']    = $uid;
        $map['status'] = array('in', [0,1,2,9]);
        $res           = $this->where($map)->order('id desc')->find();
        return $res ? false : true;
    }


    /**
     * 防止重复数据并发请求
     * @param  [array]   $param [请求参数]
     * @return [boolean]
     */
    protected  function  requstLock($param)
    {
        if(!$param || empty($param)){
            return false;
        }
        $name = 'requstLock'.$param['token'];
        $cache = Cache($name); //读取缓存

        if($cache){
            return true;
        }
        Cache($name, '123', 3); //写入缓存
        return false;
    }

    /**
     * 防止重复发起支付
     * @param  [array]   $param [请求参数]
     * @return [boolean]
     */
    protected  function  payLock($param)
    {
        if(!$param || empty($param)){
            return false;
        }

        $redis = new Redis;
        $redis->select(config('redis_select'));
        $name = config('project_name').':payLock:'.$param['uid'];
        if($redis->get($name))
        {
            return true;
        }

        //3分钟
        $redis->set($name,'1',3*60);
        return false;
    }


    /**
     * [getDatas 获取单条数据]
     * @param  string $field [description]
     * @param  array  $where [description]
     * @param  string $order [description]
     * @param  string $group [description]
     * @param  string $limit [description]
     * @return [type]        [description]
     */
    public function getDatas($field = '*' , $where = [] , $order = '', $group = '' ,$limit = '')
    {
        return $this->alias('a')->field($field)->join('qh_user_info b','a.uid = b.uid',"LEFT")->join('qh_payment_order c','a.order_number = c.apply_id',"LEFT")->where($where)->order($order)->group($group)->find();
    }
    /**
     * [getDataList 获取数据列表]
     * @param  string $field [description]
     * @param  array  $where [description]
     * @param  string $order [description]
     * @param  string $group [description]
     * @param  string $limit [description]
     * @return [type]        [description]
     */
    public function getDataList($field = '*' , $where = [] , $order = '', $group = '' ,$limit = '')
    {
        return $this->field($field)->where($where)->order($order)->group($group)->limit($limit)->select();
    }


    /**
     * 驳回后是否可以立即申请
     * 用户自动放弃后可以立即申请
     * @return boolean
     */
    public function rejectCanApply($uid)
    {
        $map['uid'] = $uid;
        $apply = $this->where($map)->field('is_give_up,status')->order('id desc')->find();
        if($apply['is_give_up'] == 1 && $apply['status'] == 10) {
            return true;
        }

        return false;
    }



   /**
    * 功能:     剩余邮寄时间
    * @Author   Mrzhp
    * @email    18702529695@163.com
    * @DateTime 2018-05-11
    * @return   [type]              [description]
    */
    public function overdow_day($uid)
    {


      $map['uid']          = $uid;
      $field = 'appoint_time,status,time,check_time,check2_time,time,monepassdate,lendmoneydate,id,order_number,is_give_up,huishou_status';
      $info = $this->order('id desc')->field($field)->where($map)->find();


      # first_status = 5 评估拿钱
      if(!$info){
        $data['time_days'] = 0;
        $data['unit'] = '0';
        $data['first_status'] = 5;
        return $data;
      }

      # first_status = 5 评估拿钱
      if($info['status'] == 5 ){
        $data['time_days'] = 0;
        $data['unit'] = '0';
        $data['first_status'] = 5;
        return $data;
      }
       
      # 已经寄出
      # 当first_status = 2 为逾期状态（对于已经违约）
      # 当first_status = 4 已经寄出（未逾期）
      # 当first_status = 6 已经签收
      # 当first_status = 9 已经退回（未到期）
      # 当first_status = 3 未到期
      $res_status = Db::name('consignment')->field('status,id')->where(['apply_id'=>$info['order_number']])->order('id desc')->find();
      if($res_status['id']){
         
         if($res_status['status'] == 1){
       
            if(in_array($info['status'], [3,4,12])){

                  $appoint_time_strtotime = strtotime($info['appoint_time']);
                  if(time()>$appoint_time_strtotime){
                    $data['time_days'] = (int)((time()-$appoint_time_strtotime)/(24*60*60))+1;
                    $data['first_status'] = 2;
                    $data['unit'] = '天';
                  }else{
                    $data['time_days']    = 0;
                    $data['unit']         = '0';
                    $data['first_status'] = 4;
                  }

             }

            
         }elseif ($res_status['status'] == 2) {
             $data['time_days']    = 0;
             $data['unit']         = '0';
             $data['first_status'] = 6;
         }elseif ($res_status['status'] == 3) {
             if(in_array($info['status'], [3,4,12])){

                  $appoint_time_strtotime = strtotime($info['appoint_time']);
                  if(time()>$appoint_time_strtotime){
                    $data['time_days'] = (int)((time()-$appoint_time_strtotime)/(24*60*60))+1;
                    $data['first_status'] = 2;
                    $data['unit'] = '天';
                  }else{
                      $data['time_days'] = (int)(($appoint_time_strtotime-time())/(24*60*60))+1;
                      $data['unit'] = '天';
                    if($res_status['status'] == 3){
                        $data['first_status'] = 9;
                    }else{
                        $data['first_status'] = 3;
                       
                    }
                    
                  }

             }
         }elseif ($res_status['status'] == 4) {
             $data['time_days'] = 0;
             $data['unit'] = '0';
             $data['first_status'] = 8;
         }

        
        return $data;
      }



      # 审核中
      # first_status = 1 审核中
      if(in_array($info['status'], [0,1,2,9])){
        $data['time_days'] = 0;
        $data['unit'] = '0';
        $data['first_status'] = 1;
      }


      # 审核失败
      # first_status = 0 审核失败
      if(in_array($info['status'], [6,7,8,10,11])){
        
        switch ($info['status']) {
            case '6':

                $data = $this->Convert_time($info['check_time']);
                break;
            case '7':
                $data = $this->Convert_time($info['check2_time']);
                break;
            case '8':
                $data = $this->Convert_time($info['time']);
                break;
            case '10': 
                $data = $this->Convert_time($info['monepassdate'],$info['is_give_up']);
                break;
            case '11':
                $data = $this->Convert_time($info['lendmoneydate']);
                break;

            default:
                # code...
                break;
        }
       
      }


     if(in_array($info['status'], [3,4,12])){

          $appoint_time_strtotime = strtotime($info['appoint_time']);
          if(time()>$appoint_time_strtotime){
            $data['time_days'] = (int)((time()-$appoint_time_strtotime)/(24*60*60))+1;
            $data['first_status'] = 2;
            $data['unit'] = '天';
          }else{

            $data['time_days'] = (int)(($appoint_time_strtotime-time())/(24*60*60))+1;
            $data['first_status'] = 3;
            $data['unit'] = '天';
          }

     }
      return $data;


    }


  /**
   * 功能:    首页时间转化
   * @Author   Mrzhp
   * @email    18702529695@163.com
   * @DateTime 2018-05-12
   * @param    [type]              $Convert_time [description]
   */
   public function Convert_time($Convert_time,$is_give_up = 0)
   {

      if($is_give_up == 1){
         $data['time_days'] =  0;
         $data['unit'] = '0';
         $data['first_status'] = 0;
         return $data;
      }

      $Convert_time_str = strtotime($Convert_time);
      $Countdown = $Convert_time_str+config('COUNTDOWN_DAY')*24*60*60;
      $strconvert = (int)($Countdown-time());
      if((24*60*60)<$strconvert){
       $data['time_days'] =  (int)(($strconvert/(24*60*60))+1);
       $data['unit'] = '天';
      }

      if((24*60*60)>$strconvert){

         if((60*60)<$strconvert){
            $data['time_days'] =  (int)(($strconvert/(60*60))+1);
            $data['unit'] = '时';
         }

         if((60*60)>$strconvert&&60<$strconvert){
            $data['time_days'] =  (int)(($strconvert/60)+1);
            $data['unit'] = '分';
         }

         if(60>$strconvert&&0<$strconvert){
            $data['time_days'] = $strconvert;
            $data['unit'] = '秒';
         }

         if($strconvert <= 0){
            $data['time_days'] =  0;
            $data['unit'] = '0';
         }
          
        
     }
      $data['first_status'] = 0;
      return $data;
      
   }




}
