<?php
// +----------------------------------------------------------------------
// | Description: 邀请奖励关系表
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;
use think\cache\driver\Redis;
use think\Model;
class InviteCommission extends Model 
{	
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
	protected $name = 'invite_commission';
    protected $createTime = 'create_time';
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';

    /**
     * 邀请奖励表 创建
     * @zwr
     * @DateTime 2017-06-22T21:51:56+0800
     * @param    [type]                   $uid [description]
     * @return   [type]                        [description]
     */
    public function addCom($uid)
    {
       
        $puid = Db::table('qh_invite_code')->where(array('uid'=>$uid))->value('puid');
        if((int)$puid===-1){          

        }else{
            $rs = $this->where(array('uid'=>$puid))->select();
            $rs = modelo2array($rs);
            $data['uid'] = $uid;
            $data['puid'] = $puid;
            $data['level'] = 1;
            $data['is_concern'] = 0;
            $data['is_register'] = 1;
            $data['is_information'] = 0;
            $data['is_frist_loan'] = 0;
            $data['commission_concern'] = 0;
            $data['commission_frist_loan'] = 0;
            if($rs){
                foreach ($rs as $key => $value) {
                     $rs[$key]['is_concern'] = 0;
                     $rs[$key]['is_register'] = 1;
                     $rs[$key]['is_information'] = 0;
                     $rs[$key]['is_frist_loan'] = 0;
                     $rs[$key]['commission_concern'] = 0;
                     $rs[$key]['commission_frist_loan'] = 0;
                     $rs[$key]['uid']=$uid;
                     $rs[$key]['level'] =$rs[$key]['level'] + 1;
                     unset($rs[$key]['id']);
                }
            }
            $rs[]=$data;
            $res = $this->saveAll($rs);
        }
    }
    public function sendReward($uid)
    {
        #获取系统配置
        $SystemConfig = model('v2/SystemConfig')->getSysConfigList();
        $reword[1] = $SystemConfig['REWORD_REGISTER_LEVEL_1'];
        $reword[2] = $SystemConfig['REWORD_REGISTER_LEVEL_2'];

        $where = array('is_register'=>1,'commission_register'=>0,'uid'=>$uid);
        $where['level']=array('ELT',2);

        $list = $this->where($where)
                   ->order("level ASC")
                   ->select();
        $list = modelo2array($list);

        $redis  = new Redis();
        $redis->select(15); #选择15号库 
        if(!$list){
            return false;
        }else{
            foreach ($list as $key => $value) {
                    #触发奖励 uid   获得奖励 puid   奖励金额 score    
                    $rewards=array('uid'=>$value['uid'],'puid'=>$value['puid'],'score'=>$reword[$key+1],'time'=>date("Y-m-d H:i:s",time()),'id'=>$value['id'],'level'=>$value['level']);
                    #hash 去重
                    if ($redis->hset('rewards_concern',$value['uid']."_".$value['puid'],$rewards['score'])){
                        #入 队列
                        $redis->rpush('rewards_concern_list',$rewards);
                    }
            }
        }
    }
    public function SignLoginReward($uid)
    {
            
    }

}