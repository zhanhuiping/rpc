<?php
/**
 * 贷款申请处理model
 */
namespace app\v2\model;
use think\Db;
use think\Model;
class Message extends Model {

    protected $name = 'user_message';
    protected $createTime = 'time';
    // protected $updateTime = false;
    // protected $autoWriteTimestamp = 'datetime';

    /**
     * 消息列表
     * @author 湛慧平 <934797303@qq.com>
     * @DateTime 2017-08-1
     * @param    $param  接收参数
     * @return   boolean                     
     */
    public function messagePage($page,$uid,$pagesize)
    {
      
      
      $list = $this->field('id,type,admin_uid,from_uid,uid,is_read,title,content,create_time')
            ->where('uid',$uid)
            ->page($page,$pagesize)
            ->select();
      
     
      foreach($list as $key=>$value)
      {
          $map['id'] = $value['admin_uid'];
          $list[$key]['create_time'] = substr($value['create_time'],0,16);
          $list[$key]['content']  = mb_substr($value['content'],0,19,'utf-8');
          $admin_name = Db::name('admin_user')->field('username')->where($map)->find();
          $list[$key]['admin_name'] = $admin_name['username'];
          
      }

      foreach($list as $key=>$value)
      {
          $map1['uid'] = $value['from_uid'];
          $username1 = Db::name('user')->field('username')->where($map1)->find();
          $list[$key]['user_name'] = $username1['username'];
      }
      
      $data = $list;
      return $data;

    }

    
    public function gettypeAttr($value)
    {
        $data = ['1'=>"管理员消息",'0'=>"系统消息",'2'=>"其它用户消息"];
        return $data[$value];
    }

    //消息详情接口
    public function messageinfo($id)
    {
        $map['id'] = $id;
        $list = $this->field('title,content,create_time,type')
            ->where($map)
            ->find();
        if(!$list)
        {
           $this->error = '失败';
           return false;
        }

        $data = $list;
        return $data;
    }

     /**
     * 消息阅读
     * @author 湛慧平 <934797303@qq.com>
     * @DateTime 2017-08-1
     * @param    $param  接收参数
     * @return   boolean                     
     */
    
    public function messageRead($id)
    {
       $info = $this->update(['is_read' => 1,'id'=>$id]);
        if($info){
        return $data = array('data'=>'success');
      }else{
        $this->error = '失败';
        return false;
      }
    }
    
    
}
