<?php
/**
 * 借款申请处理model
 */
namespace app\v2\model;
use think\Db;
use think\Model;
use think\Cache;
class PaymentOrder extends Model {
	public $name = 'payment_order';
	/**
	 * [getDatalist 获取数据列表]
	 * @param  string $field [description]
	 * @param  array  $where [description]
	 * @param  string $order [description]
	 * @param  string $group [description]
	 * @return [type]        [description]
	 */
	public function getDataList($field = '*' , $where = [] , $order = '', $group = '' ,$limit = '')
	{
		return $this->field($field)->where($where)->order($order)->group($group)->limit($limit)->select();
	}
}
