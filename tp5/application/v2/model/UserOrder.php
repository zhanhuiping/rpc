<?php 
namespace app\v2\model;
use think\Model;

class UserOrder extends Model
{
	
	public $name = 'user_order';
	/**
	 * [getOne 一条数据]
	 * @param  array  $where [description]
	 * @param  string $field [description]
	 * @return [type]        [description]
	 */
	public function getOne($where = [], $field = '*')
	{
		return $this->where($where)->field($field)->find();
	}
}


 ?>