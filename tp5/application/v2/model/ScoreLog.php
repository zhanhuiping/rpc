<?php
// +----------------------------------------------------------------------
// | Description: 积分日志操作model
// +----------------------------------------------------------------------
// | Author: 火星车11号 <704184317@qq.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;

use think\Model;
class ScoreLog extends Model 
{	
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
	protected $name = 'score_log';
    protected $createTime = 'create_time';
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';
	/**
	 * 日志添加类
	 * @zwr
	 * @DateTime 2017-7-5 16:54:34
	 * @param    integer      $uid     积分变动用户id
	 * @param    integer      $recored 出发积分变动的id 一般用于邀请奖励  此id为被邀请的id 其他时候传0
	 * @param    integer      $value   积分变动的值
	 * @param    string       $action  积分变动的类型 setInc 或 setDec
	 * @param    integer      $logid   日志remark 的id
	 * @param    string       $type    积分出发事件
	 * @return   bool                  
	 */
	public function addlog($uid,$recored=0,$value=0,$action="",$logid=1,$type="")
	{
        if(!$uid){
			$this->error = "请选择用户";
			return false;
        }
        $recored = $recored?$recored:$uid;
		if(!is_numeric($value)&& $value < 0){
			$this->error = "积分值必须为正数";
			return false;
		}

        switch ($action) {
        	case 'setInc':
        		$action = 1;
        		break;
        	case 'setDec':
        		$action = 0;
        		break;        	
        	default:
        		$this->error = "操作类型错误";
        		return false;
        		break;
        }
		#构造日志数据
		$data['uid'] 		    = $uid;
		$data['record_uid']     = $recored;
		$data['action']         = $action;
		$data['value']          = $value;
		$data['finally_value']  = model('v2/Score')->balance($uid);
		$data['remark']         = $this->creatRemark($uid,$recored,$value,$logid);
		$data['type']           = $type; 

		$res = $this->allowField(true)->save($data);
		if(!$res){
			$this->error = "日志写入错误";
			return false;
		}
		return true;
	}
	/**
	 * 构造remark字符串的方法
	 * @zwr
	 * @DateTime 2017-07-05T17:00:24+0800
	 * @param    integer      $uid     积分变动用户id
	 * @param    integer      $recored 出发积分变动的id 一般用于邀请奖励  此id为被邀请的id 其他时候传0
	 * @param    integer      $value   积分变动的值
	 * @param    integer      $logid   remark 模板类型
	 * @return   string              
	 */
	public function creatRemark($uid,$recored,$value,$logid)
	{	
		$usermodel    = model('v2/UserInfo');
		$uid_name     = $usermodel->getNameByuid($uid);
		$recored_name = $usermodel->getNameByuid($recored);
		switch ($logid) {
			case '1':
				return "用户: ".$uid_name." 购买征信查询消耗余额:".$value;
				break;
			case '2':
				return "用户: ".$uid_name." 提现 ".$value;
				break;	
			case '3':
				return "用户: ".$uid_name." 获得邀请好友".$recored_name."注册平台的奖励,奖励金额为：".$value;
				break;							
			default:
				return "未知操作记录";
				break;
		}

	}
}