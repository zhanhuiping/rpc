<?php
/**
 * 反馈
 */

namespace app\v2\model;
use think\Db;
use think\Model;

class FeedBack extends Model 
{   
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
    protected $name = 'feedback';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = 'datetime';



    public function getDataList($map=array(),$field="*",$orderby = "",$limit = "")
    {
        $list =$this->where($map)
                    ->field($field)
                    ->limit($limit['start'],$limit['count'])
                    ->order($orderby)
                    ->select();

        $list = modelo2array($list);   
        return $list;               
    }


    public function addFeedback($param)
    {
        $arr = [
            '0' => '提建议',
            '1' => '想咨询',
            '2' => '要投诉',
        ];
        $param['title'] = $arr[$param['type']];
        $param['img_array'] = json_encode($param['img_array']);        
        $res = $this->data($param)->allowField(true)->isUpdate(false)->save();
        if(!$res){
            $this->error = "反馈失败";
            return false;
        }
        return true;            
    }

}