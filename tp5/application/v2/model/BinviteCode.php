<?php
// +----------------------------------------------------------------------
// | Description: 邀请码
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;

use think\Model;
class BinviteCode extends Model
{

    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
    protected $name = 'binvite_code';
    protected $createTime = 'create_time';

    // 开启自动写入时间戳之后 数据表没有 如果updateTime字段 必须填写 false
    protected $updateTime = false;
    protected $autoWriteTimestamp = 'datetime';


    public function check_bcode($bcode='')
    {
        if(!$bcode){
            $this->error = '商家邀请码为空！';
            return false;
        }
        $res=$this->where('bcode',$bcode)->find();
        if(!$res){
            $this->error = '商家邀请码不存在！';
            return false;
        }
        return true;
    }

    public function update_invitecode($bcode='')
    {
        $where['bcode']=$bcode;
        $codearr=$this->where($where)->find();
        $data['update_time']=date('Y-m-d H:i:s',time());;
        $data['browsetotal']=$codearr['browsetotal']+1;
        $re=$this->update($data,$where);
        if($re){
            return true;
        }
    }


}