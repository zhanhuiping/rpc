<?php
// +----------------------------------------------------------------------
// | Description: 登录日志操作model
// +----------------------------------------------------------------------
// | Author: 火星车11号 <704184317@qq.com>
// +----------------------------------------------------------------------

namespace app\v2\model;
use think\Db;
use think\Model;
class LoginLog extends Model 
{	
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
	protected $name = 'login_log';
    protected $createTime = 'time';
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';


	public function addLog($uid,$uuid,$imsi,$os,$version)
	{
		$data['uid']        = $uid ;
        $data['login_ip']   = getClientIP();
        $data['uuid']       = $uuid;
        $data['imsi']       = $imsi;
        $data['os']         = $os;
    	$data['version']    = $version;
    	$this->create($data);		
	}
}