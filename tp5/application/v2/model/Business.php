<?php
// +----------------------------------------------------------------------
// | Description: 邀请码
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;

use think\Model;
class business extends Model 
{	
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
	protected $name = 'business';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
	protected $autoWriteTimestamp = 'datetime';


    public function gettimeTypeAttr($value)
    {
        $type = [1=>'天',2=>'月',3=>"年"];
        return $type[$value];
    }
    public function getspeedTypeAttr($value)
    {
        $type = [1=>'小时',2=>'天'];
        return $type[$value];
    }    

    public function getDataList($map=array(),$field="*",$orderby = "",$limit = "")
    {
        $list =$this->where($map)
                    ->field($field)
                    ->limit($limit['start'],$limit['count'])
                    ->order($orderby)
                    ->select();

        $list = modelo2array($list);   
        return $list;               
    }
    public function getBname($bid)
    {
       $res = $this->field('bname,icon,links')->where(array('id'=>$bid,'status'=>1))->find();
       return  $res?$res->toArray():array();
    }     



}