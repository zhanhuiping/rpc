<?php
// +----------------------------------------------------------------------
// | Description: 邀请码
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;

use think\Model;
class Invite extends Model 
{	
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
	protected $name = 'invite_code';
    protected $createTime = 'create_time';
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';

    /**
     * 创建邀请关系和邀请码
     * @zwr
     * @DateTime 2017-06-30T09:47:04+0800
     * @param    integer        $uid    用户id
     * @param    integer        $length 邀请码长度
     * @param    string         $aCode  父级邀请码
     * @return   bool                 
     */
     public function creatcode($uid,$length = 11,$aCode = "",$bcode = "")
    {
        if($bcode != ""){
          $is_not = Db::name('binvite_not_code')->where(['bcode'=>$bcode])->count('id');

           if($is_not)
           {
             
               $this->error = '无法注册';
               return false;
           }
           $count = Db::name('binvite_commission')->where(['bcode'=>$bcode])->count();
           $info = Db::name('binvite_commission')->where(['uid'=>$uid])->find();
           if(!$info){
              $data['uid'] = $uid;
              $data['bcode'] = $bcode;
              $data['yaoqingtongji'] = $count+1;
              $data['creat_time'] = date('Y-m-d H:i:s',time());
              Db::name('binvite_commission')->insert($data);
              $code = create_rand($length);
              $data1['status']    = 1 ;
              $data1['code']      = $code;
              $data1['uid']       = $uid;
              $data1['client_ip'] = getClientIP();
              $data1['puid']       = -1;   #父id 默认为 -1 
              $data1['level']      =  1;
              $ls = $this->create($data1);
                if(!$ls){
                    $this->error = '添加失败';
                    return false;
                }
           }else{
              $this->error = '该用户已经邀请';
              return false;
           }
            
        }else{
            do {
            $code = create_rand($length);
            } while ($this->where(array('code'=>$code))->count());
            $data['status']      = 1 ;
            $data['code']        = $code;
            $data['uid']         = $uid;
            $data['client_ip']   = getClientIP();
            
            #如果 没有邀请码 则为自己注册
            if($aCode != ''){
                $result = $this->where(array('code'=>$aCode))->find();
                $data['puid']    = $result['uid'];
                $data['level']   = $result['level'] + 1;
            }else {
                $data['puid']    = -1;   #父id 默认为 -1 
                $data['level']   =  1;
            }

            $ls = $this->create($data);
            if(!$ls){
                $this->error = '添加失败';
                return false;
            }
        }

        
    }
    public function getParent($uid)
    {
        return $this->where('uid',$uid)->value('puid');
    }

    public function getSon($uid)
    {
        $res = $this->where("puid",$uid)->field('uid,id,create_time')->select();
        return $res?modelo2array($res):array();
    }
    public function getInviteCode($uid)
    {
        return $this->where("uid",$uid)->value('code');
    }
    public function getInviteshortUrl($uid)
    {
        return $this->where("uid",$uid)->value('short_url');
    }     

}