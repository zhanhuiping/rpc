<?php
// +----------------------------------------------------------------------
// | Description: 邀请码
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;

use think\Model;
class BuserBrowse extends Model
{

    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
    protected $name = 'buser_browse';
    protected $createTime = 'create_time';
    // 开启自动写入时间戳之后 数据表没有 如果updateTime字段 必须填写 false
    protected $updateTime = false;
    protected $autoWriteTimestamp = 'datetime';


    public function add_browse($param=array(),$bcode='')
    {
        /*添加浏览记录*/
        $param['create_time']=date('Y-m-d H:i:s',time());;
        $param['bcode']=$bcode;
        $param['login_ip']=getClientIP();
        $add=$this->create($param);
        /*添加该商家被浏览总数和浏览时间*/
        if($add){
            $CodeModel = new BinviteCode();
            $update=$CodeModel->update_invitecode($bcode);
            if($update){
                return true;
            }
        }
    }



}