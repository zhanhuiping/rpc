<?php
// +----------------------------------------------------------------------
// | Description: 邀请奖励关系表
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;
use think\cache\driver\Redis;
use think\Model;
class BinviteCommission extends Model
{

    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
    protected $name = 'binvite_commission';
    protected $createTime = 'creat_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 邀请奖励表 创建
     * @zwr
     * @DateTime 2017-06-22T21:51:56+0800
     * @param    [type]                   $uid [description]
     * @return   [type]                        [description]
     */
    public function addCom($uid,$bcode)
    {
        /*根据邀请码找商家*/
        $bid = Db::table('qh_binvite_code')->where(array('bcode'=>$bcode))->value('id');
        if(!$bid){

        }else{
            $data['uid'] = $uid;
            $data['creat_time'] =date('Y-m-d H:i:s',time());;
            $data['bcode'] = $bcode;
            $res = $this->save($data);
            if(!$res){
                $this->error = '添加失败';
                return false;
            }
        }
        return true;
    }
}