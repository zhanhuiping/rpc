<?php
/**
 * 反馈
 */

namespace app\v2\model;
use think\Db;
use think\Model;

class Partner extends Model
{   
    
    /**
     *  默认时间戳是字段  creat_time
     *  默认时间戳是格式  datetime
     */
    protected $name = 'partner_list';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = 'datetime';



    public function addPartner($param)
    {
        if(is_array($param['category']))
            $param['category'] = join( ',' , $param['category']);
        if(is_array($param['type']))
            $param['type'] = join( ',' , $param['type']);
        if(is_array($param['city']))
            $param['city'] = join( ',' , $param['city']);

        $res = $this->data($param)->allowField(true)->isUpdate(false)->save();
        if(!$res){
            $this->error = "添加失败";
            return false;
        }
        return true;            
    }

}