<?php
/**
 * 银行卡信息model
 */
namespace app\v2\model;
use think\Db;
use think\Model;
class BankInfo extends Model {
  protected $name = 'bank_info';
  protected $createTime = false;
  protected $updateTime = false;
  protected $autoWriteTimestamp = 'datetime';

}