<?php
// +----------------------------------------------------------------------
// | Description: 用户 银行卡
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\v2\model;

use think\Db;

use think\Model;
class PayRecharge extends Model 
{	

    protected $name = 'pay_recharge';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = 'datetime';
    /**
     * [autoWrite 自动写入]
     * @param  [type] &$param [description]
     * @return [type]         [description]
     */
    public function autoWrite(&$param = [], $where = [])
    {
        return $this->allowField(true)->save($param,$where);
    }
}