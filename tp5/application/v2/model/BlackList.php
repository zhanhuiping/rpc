<?php
/**
 * 黑名单
 */
namespace app\v2\model;
use think\Db;
use think\Model;
class BlackList extends Model {

  protected $name = 'black_list';
  protected $createTime = 'time';
  protected $updateTime = false;
  protected $autoWriteTimestamp = 'datetime';

  /**
   * 用户是否黑名单
   * @author   yhq <934797303@qq.com>
   * @DateTime 2017-08-11T14:54:50+0800
   * @param    string     $id_card 身份证号码
   * @param    string     $phone   手机号码
   * @return   integer    0:否  1:是                  
   */
  public function is_black($id_card = '', $phone = '')
  {
    $where['id_card']  = $id_card;
    $where2['phone']   = $phone;
    $count = $this->where($where)->whereOr($where2)->count();
    return $count>0 ? 1 : 0;
  }

}
