<?php 
namespace app\v2\model;
use think\Model;
class Consignment extends Model
{
	
	protected $name = 'consignment';//表名
	/**
	 * [add 插入]
	 * @param array &$data [description]
	 */
	public function add(&$data = [])
	{
		return $this->insert($data);
	}
	/**
     * [getOne 获取一条数据]
     * @param  string $field [description]
     * @param  array  $where [description]
     * @param  string $order [description]
     * @return [type]        [description]
     */
	public function getOne($field = '*', $where = [], $order = '')
    {
        return $this->field($field)->where($where)->order($order)->find();
    }
    /**
     * [getDatas 获取全部数据]
     * @param  string $field [description]
     * @param  array  $where [description]
     * @param  string $order [description]
     * @param  string $group [description]
     * @return [type]        [description]
     */
    public function getDatas($field = '*', $where = [], $order = '', $group = '', $page = '')
    {
    	return $this->field($field)->where($where)->order($order)->group($group)->page($page)->select();
    }
    /**
     * [getCount 获取总数]
     * @param  array  $where [description]
     * @return [type]        [description]
     */
    public function getCount($where = [], $value = 'id')
    {
    	return $this->where($where)->count($value);
    }
}

 ?>