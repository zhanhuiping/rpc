<?php
/**
 * 主页
 */
namespace app\v2\controller;
use think\Request;
class Home extends Base
{

    public function __construct()
    {
        parent::__construct();
        $this->gearModel =  model('v2/Gear');
        $isLogin =  $this->isLogin();  //是否登录
        if($isLogin['status']){
            $this->uid =  $isLogin['uid'];
        } 
    }
    /**
     * 获取首页卡片数据
     * @author   yhq
     * @DateTime 2017-07-26 10:15
     * @return   json                      
     */
    public function getCard()
    {
        $data  = $this->gearModel->card($this->uid);
        if (!$data) {
            return resultArray(['error' => $this->gearModel->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 获取档位数据
     * @author   yhq
     * @DateTime 2017-07-26 10:15
     * @return   json                      
     */
    public function getGear()
    {
        $data  = $this->gearModel->gear($this->uid);
        if (!$data) {
            return resultArray(['error' => $this->gearModel->getError()]);
        }
        return resultArray(['data' => $data]);
    }

}
