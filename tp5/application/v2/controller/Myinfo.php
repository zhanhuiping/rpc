<?php
/**
 * 申请类
 */
namespace app\v2\controller;
use think\Request;
use think\Db;
use  app\v2\model\Myinfo as MM;

class Myinfo extends Base
{
    // /**
    //  * 通讯录获取接口
    //  * @湛慧平
    //  * @DateTime 2017-07-25
    //  * @param    Request $request 
    //  * @return   json                      
    //  */
    // public function Myinfo(Request $request)
    // {
    //     $myinfoModel = model('v2/Myinfo');
    //     $tel_address_list = $request->post("tel_address_list",'');
    //     $isLogin =  $this->isLogin();  //是否登录
    //     if(!$isLogin['status']){
    //         return resultArray(['error' => $isLogin['info']]); 
    //     }
    //       $uid = $isLogin['uid'];
    //       if(!$tel_address_list)
    //       {
    //         $data = array("code"=>400,"data"=>"请传入内容","error"=>'');
    //         return $data;
    //         exit;
    //       }
    //       $tel_address_list = serialize($tel_address_list);
    //       $data = $myinfoModel->myinfosave($uid,$tel_address_list);
    //       return resultArray(['data' => $data]);
    // }

  /**
   * 保存通讯录  新
   * @author   yhq <934797303@qq.com>
   * @DateTime 2018-03-16T16:46:32+0800
   * @param    Request                  $request [description]
   */
    public function Myinfo(Request $request)
    {

        $myinfoModel = new MM();
        $contact = $request->post("contact",'');

        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];

        $data = $myinfoModel->saveContact($contact, $uid);
        if(!$data){
            return resultArray(['error' =>$myinfoModel->getError()]);
        }
        return resultArray(['data' =>$data]);
    }
}
