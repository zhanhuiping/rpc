<?php
/**
 * 商家处理类
 */
namespace app\v2\controller;
use think\Request;

class Merchant extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->businessModel =  model('v2/Business');

    }

    public function getList(Request $request)
    {
    	/*分页*/
    	$page  = $request->post("page","1");
    	$count  = $request->post("count","100"); 
    	$type  = $request->post("type","0"); 
		$start = ($page-1)*$count;    	
    	$limit['start'] = $start;
    	$limit['count'] = $count;
    	/*搜索条件*/
    	if($type){
    		$map['type'] = $type;
    	}
    	/*需要的字段*/
    	$field = "id,bname,bsort,icon,lend_rate,interest,tip_detial,tip_index,links,speed,speed_type,tips,type,minloan,maxloan,mintime,maxtime,time_type,needdata,condition,remark";
    	/*排序*/
    	$orderby= "id desc";
    	// $orderby= "id asc";

    	$list =  $this->businessModel->getDataList($map,$field,$orderby,$limit);
        foreach ($list as $key => $value) {
            $list[$key]['speed'] = $list[$key]['speed'].$list[$key]['speed_type'];
            unset($list[$key]['speed_type']);
            $list[$key]['tips'] = explode(",", $list[$key]['tips']);
            $list[$key]['count']['loan_count'] =1000;
            $list[$key]['count']['loan_count_remark'] ="人已申请";
            $list[$key]['tipList'][]=array('value'=>$list[$key]['maxloan']."元","key"=>"最高额度");
            $list[$key]['tipList'][]=array('value'=>$list[$key]['interest']."%","key"=>"利息");
            $list[$key]['tipList'][]=array('value'=>$list[$key]['minloan']."元","key"=>"最低额度");
        }
    	return resultArray(['data' => $list]);
    }
    /**
     * 商家详情
     * @zwr
     * @DateTime 2017-06-29T21:13:29+0800
     * @param    Request                  $request [description]
     * @return   [type]                            [description]
     */
    public function details(Request $request)
    {
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid']; 
    	$bid  = $request->post("id","0");
    	if(!$bid){
    		return resultArray(['error' => "没有获取到商家id"]);
    	}
    	$map['id'] = $bid ;
    	$map['status'] = 1 ;
    	$info = $this->businessModel->where($map)->find();
        if($info){
            $info = $info->toArray();
            $info['list'][] = array('key'=>'申请条件','value'=>$info['condition']);
            $info['list'][] = array('key'=>'所需材料','value'=>$info['needdata']);
            $info['list'][] = array('key'=>'注意事项','value'=>$info['remark']);
            #写入浏览历史
            $this->addBrowse($bid);
            return resultArray(['data' => $info]);                      
    	}else{
    		return resultArray(['error' => "没有获取到数据"]);
    	}
    }
    /**
     * 添加浏览历史
     * @zwr
     * @DateTime 2017-06-30T09:41:04+0800
     * @param    [type]                   $bid [description]
     */
    public function addBrowse($bid)
    {
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];

        #构造写入数据
        $data['uid'] =$uid; 
        $data['bid'] =$bid;

        $browse =  model('v2/Browse');

        $browse->where(['uid'=>$uid,'bid'=>$bid])->delete();
        $browse->allowField(true)->create($data);
    }
}
