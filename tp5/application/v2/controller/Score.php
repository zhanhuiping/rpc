<?php
/**
 * 积分处理类
 */
namespace app\v2\controller;
use think\Request;
use think\Db;

class Score extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->Score =  model('v2/Score');

    }

    public function createWithdraw(Request $request)
    {
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];
        $value  = $request->post("value","0");
        if(!$value){
            return resultArray(['error' => "请填写提现金额度"]);
        }
        $name  = $request->post("name",""); 
        if(!$name){
            return resultArray(['error' => "姓名不能为空"]);
        }
        $alipay  = $request->post("alipay","");
        if(!$alipay){
            return resultArray(['error' => "支付宝账户不能为空"]);
        }
        $remark  = $request->post("remark","");

        $accountInfo = '姓名:'.$name.',支付宝账号:'.$alipay;

        #判断 是否 余额不足
        $balance = $this->Score->balance($uid);
        if($balance<$value){
            return resultArray(['error' => "余额不足"]);
        }
        $finally_value = $balance - $value;
        #获取系统配置
        $SystemConfig = model('v2/SystemConfig')->getSysConfigList();
        
        #判断最低 提现金额
        if($value<$SystemConfig['WITHDRAW_LOW']){
            return resultArray(['error' => "提现金额不能低于".$SystemConfig['WITHDRAW_LOW']]);
        }
        #判断 单日提现次数
        $withdrawModel = model('v2/Withdraw');
        $numToday = $withdrawModel->getTxNumToday($uid);

        // if($numToday>=$SystemConfig['WITHDRAW_DAILY_TIMES']){
        //     return resultArray(['error' => "每天提现次数不能大于".$SystemConfig['WITHDRAW_DAILY_TIMES']."次"]);
        // }
        #构造提现数据 
        $data['uid']                 = $uid;
        $data['value']               = $value;
        $data['name']                = $name;
        $data['finally_value']       = $finally_value;
        $data['phone']               = model('UserInfo')->getPhone($uid);
        $data['alipay_account']      = $alipay;
        $data['remark']              = $remark;
        $data['accountinfo']         = $accountInfo;

        Db::startTrans();
        try {
                #积分操作
                $this->Score->handle($uid,$value,$opreation="setDec");
                #写入提现申请表
                $withdrawModel->allowField(true)->save($data);
                #写日志
                model('ScoreLog')->addlog($uid,$recored=0,$value,$opretion="setDec",$logid=2,$type="提现");
                Db::commit();
                return resultArray(['data' => "提现成功"]);
        } catch(\Exception $e) {
                Db::rollback();

                return resultArray(['error' => "提现失败"]);
        }            
    }

    /**
     * 提现记录
     * @zwr
     * @DateTime 2017-06-30T17:50:21+0800
     * @return   [type]                   [description]
     */
    public function txHistory(Request $request)
    {
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid']; 
        /*分页*/
        $page  = $request->post("page","1");
        $count  = $request->post("count","100");

        $start = ($page-1)*$count;  
        $limit['start'] = $start;
        $limit['count'] = $count;        

        /*搜索条件*/
        $map['uid'] = $uid; 
        $field = "*";
        $orderby= "id desc";

        $list = model('v2/Withdraw')->getDataList($map,$field,$orderby,$limit);
        if(!$list){
            return resultArray(['error' => "没有提现记录"]);        
        }
        return resultArray(['data' => $list]);        
    }

}
