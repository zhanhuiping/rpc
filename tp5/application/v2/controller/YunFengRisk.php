<?php
namespace app\v2\controller;
/**
 * 云峰风控类
 */
use think\Db;
use think\Request;
use yunfeng\TaskUtil;
use think\cache\driver\Redis;
use app\common\model\YunFengModel;

class YunFengRisk extends Base
{
   
    /**
     * 异步回调  查询和授信都会回调
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function notify(Request $request)
    {
        $post = $request->post();
        $YunFengModel = new YunFengModel;
        $res = $YunFengModel->notifySaveData($post);
        if(!$res){
            @file_put_contents(LOGS_DIR_NAME."yunfeng_notify.txt", "时间:[".date('Y-m-d H:i:s')."] 结果:".$YunFengModel->getError()." 回调详情:".json_encode($post, JSON_UNESCAPED_UNICODE)."\n\n", FILE_APPEND);
            return resultArray(['error' => $YunFengModel->getError()]);
        }
        @file_put_contents(LOGS_DIR_NAME."yunfeng_notify.txt", "时间:[".date('Y-m-d H:i:s')."] 结果:成功 回调详情:".json_encode($post, JSON_UNESCAPED_UNICODE)."\n\n", FILE_APPEND);
        return resultArray(['data' =>'成功']);
    }



    /**
     * 获取链接
     * @return [type] [description]
     */
    public function getUrl()
    {
        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        } 
        $uid    = $isLogin['uid'];

        $YunFengModel = new YunFengModel;
        $url = $YunFengModel->getUrl($uid);
        if(!$url) {
            return resultArray(['error' => $YunFengModel->getError()]);
        }
        return resultArray(['data' => $url]);  
    }


    /**
     * 查询结果
     * @return [type] [description]
     */
    public function getResult()
    {

        dump(date('Ymd'));
        die;
//         $url = "http://api2-test.yueguangbaika.com/v2/fengkongNotify";
//         $param = '{"code":"general_0","message":"","taskNo":"dcfd17bb95ce4db29a27cbc2710a71ff1516852862032","taskStatus":"success","taskResult":{"refuseReason":"108","creditResult":[{"creditAmount":0,"creditDueDay":21,"feeRate":0.078}]}}';

// echo(pushNotify($url, $param));

        // #查询风控模型结果
        // $uid = 2139;

        // $YunFengModel = new YunFengModel;
        // $data = $YunFengModel->getFengKongResult($uid);
        // if(!$data) {
        //     return resultArray(['error' => $YunFengModel->getError()]);
        // }

        // return resultArray(['data' => $data]);  
        // die;
        

        // #批量清除云峰数据
        // $arr = [
        //     '3cd77034f11d42febd7e68b85809c34b',
        //     'e7b935cfdec04017a4c58881ecd573a2',
        //     '32303caef9154144ab09feb8ed7abacb',
        // ];
        // foreach ($arr as $userGid) {
        //     dump(TaskUtil::delUserGid($userGid));
        // }
        // die;


        // // #直接查询结果
        // $userGid = '24ab7cad2ecf4538adc413039bc05618';
        // echo "<br>debitcard:";
        // dump(TaskUtil::getResult($userGid, 'debitcard'));
        // echo "<br>creditcard:";
        // dump(TaskUtil::getResult($userGid, 'creditcard'));
        // echo "<br>carrier:";
        // dump(TaskUtil::getResult($userGid, 'carrier'));
        // echo "<br>taobao:";
        // dump(TaskUtil::getResult($userGid, 'taobao'));
        // echo "<br>jd:";
        // dump(TaskUtil::getResult($userGid, 'jd'));
        // echo "<br>h5_risk_management_1:";
        // dump(TaskUtil::getResult($userGid, 'h5_risk_management_1'));
        // die;


        // // #从mongodb查询结果
        // $uid = 2139;
        // $YunFengModel = new YunFengModel;
        // $res= $YunFengModel->getResult($uid);
        // if(!$res){
        //     return resultArray(['error' => $YunFengModel->getError()]);
        // }
        // dump($res);
        // die;
    }


    /**
     * 风控模型异步回调
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function fengkongNotify(Request $request)
    {
        $post = $request->post();
        $YunFengModel = new YunFengModel;
        $res = $YunFengModel->fengkongNotifySaveData($post);

        if(!$res){
            @file_put_contents(LOGS_DIR_NAME."yunfeng_notify.txt", "时间:[".date('Y-m-d H:i:s')."] 结果:".$YunFengModel->getError()." 回调详情:".json_encode($post, JSON_UNESCAPED_UNICODE)."\n\n", FILE_APPEND);
            return resultArray(['error' => $YunFengModel->getError()]);
        }
        @file_put_contents(LOGS_DIR_NAME."yunfeng_notify.txt", "时间:[".date('Y-m-d H:i:s')."] 结果:成功 回调详情:".json_encode($post, JSON_UNESCAPED_UNICODE)."\n\n", FILE_APPEND);
        return resultArray(['data' =>'成功']);
    }


}