<?php
namespace app\v2\controller;
use think\Request;


class Baccount extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->userModel =  model('v2/user');
    }

    /**
     * 前台商家邀请码注册
     * @zwr
     * @DateTime 2017-06-28T19:16:12+0800
     * @param    Request                  $request
     * @return   json
     */
    public function businessRegister(Request $request)
    {
        $param['username']  = $request->post("phone","");
        $param['password']  = $request->post("password","");
        $param['verify']    = $request->post("verify","");
        $bcode              = $request->post("bcode","");
        $bcodeModel =  model('v2/BinviteCode');
        $re=$bcodeModel->check_bcode($bcode);
        if(!$re){
            return resultArray(['error' => $bcodeModel->getError()]);
        }
        $verifyModel =  model('v2/Verify');
        $checkRes = $verifyModel->checkVerify($param['username'],$param['verify'],0);
        if (!$checkRes) {
            return resultArray(['error' => $verifyModel->getError()]);
        }
        /*注册  返回uid*/
        $regRes = $this->userModel->register($param);
        if (!$regRes) {
            return resultArray(['error' =>$this->userModel->getError()]);
        }
        $regtotal = $bcodeModel->where('bcode',$bcode)->setInc('regtotal');
        #添加 奖励表
        $CommissionModel =  model('v2/BinviteCommission');
        $addCommission=$CommissionModel->addCom($regRes,$bcode);
        if(!$addCommission){
            return resultArray(['error' =>$CommissionModel->getError()]);
        }
        /*获取用户信息*/
        $uid = $regRes ;
        $data = $this->userModel->getInfoByUid($uid);
        $tokenModel =  model('v2/Token');
        $token = $tokenModel->apiEncode($uid);
        if (!$token) {
            return resultArray(['error' => $tokenModel->getError()]);
        }

        $data['token'] = $token;

        return resultArray(['data' => $data]);
    }
    /*增加浏览记录*/
    public function addBrowse(Request $request)
    {
        $param['uuid']  = $request->post("uuid","");
        $param['imsi']  = $request->post("imsi","");
        $param['os']    = $request->post("os","");
        $bcode              = $request->post("bcode","");

        $bcodeModel =  model('v2/BinviteCode');
        $re=$bcodeModel->check_bcode($bcode);
        if(!$re){
            return resultArray(['error' => $bcodeModel->getError()]);
        }
        $browseModel =  model('v2/BuserBrowse');
        $re=$browseModel->add_browse($param,$bcode);
        if($re){
            return true;
        }
    }
    /**
     * 发送注册验证码
     * @zwr
     * @DateTime 2017-06-28T19:16:42+0800
     * @param    Request                  $request
     * @return   json
     */
    public function sendRegVerify(Request $request)
    {
        $phone  = $request->post("phone","");
        #区分 注册短信 和修改密码短信
        $verifyModel =  model('v2/Verify');
        $data = $verifyModel->sendVerify($phone);
        if (!$data) {
            return resultArray(['error'=> $verifyModel->getError()]);
        }
        return resultArray(['data' => "发送成功"]);
    }
    /**
     * 登陆
     * @zwr
     * @DateTime 2017-06-28T19:18:15+0800
     * @param    Request     $request
     * @return   json
     */
    public function login(Request $request)
    {

        $username = $request->post("phone","");
        $password = $request->post("password","");
        #获取设备信息
        $uuid     = $request->post("uuid","");
        $imsi     = $request->post("imsi","");
        $os       = $request->post("os","0");
        $data = $this->userModel->login($username, $password);
        if (!$data) {
            return resultArray(['error'=> $this->userModel->getError()]);
        }
        $tokenModel =  model('v2/Token');
        $token = $tokenModel->apiEncode($data['uid']);
        if (!$token) {
            return resultArray(['error' => $tokenModel->getError()]);
        }
        $data['token'] = $token;
        model('v2/LoginLog')->addLog($data['uid'],$uuid,$imsi,$os);
        return resultArray(['data' => $data]);
    }
    /**
     * 修改密码
     * @zwr
     * @DateTime 2017-06-28T19:25:11+0800
     * @param    Request                  $request
     * @return
     */
    public function changPassword(Request $request)
    {

        $username = $request->post("phone","");
        $password = $request->post("password","");
        $verify    = $request->post("verify","");

        $verifyModel =  model('v2/Verify');
        $checkRes = $verifyModel->checkVerify($username,$verify,0);
        if (!$checkRes) {
            return resultArray(['error' => $verifyModel->getError()]);
        }

        $data = $this->userModel->changPassword($username,$password);
        if (!$data) {
            return resultArray(['error' => $this->userModel->getError()]);
        }
        return resultArray(['data' => "修改密码成功"]);
    }
    /**
     * 发送找回密码短信
     * @zwr
     * @DateTime 2017-06-28T19:40:10+0800
     * @param    Request                  $request [description]
     * @return   [type]                            [description]
     */
    public function sendFindVerify(Request $request)
    {
        $phone  = $request->post("phone","");
        #区分 注册短信 和修改密码短信
        $verifyModel =  model('v2/Verify');
        $data = $verifyModel->sendVerify($phone,$type=2,0);
        if (!$data) {
            return resultArray(['error' => $verifyModel->getError()]);
        }
        return resultArray(['data' => "发送成功"]);
    }
    /**
     * 个人中心
     * @zwr
     * @DateTime 2017-06-29T14:19:27+0800
     */
    public function memberCenter()
    {
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid = $isLogin['uid'];

        $userInfoModel =  model('v2/UserInfo');

        $infoRes = $userInfoModel->where('uid',$uid)->find()->toArray();
        $infoRes['phone'] = $infoRes['phone'];

        if($infoRes['zhima']==0){
            $infoRes['zhima']='';
        }
        if($infoRes['qq']==0){
            $infoRes['qq']='';
        }
        if($infoRes['age']==0){
            $infoRes['age']='';
        }
        return resultArray(['data' => $infoRes]);
    }
    public function feedback(Request $request)
    {
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            $uid = 0;
        }
        $realte_content  = $request->post("realte_content","");
        $realte_type  = $request->post("realte_type","");
        switch ($realte_type) {
            case 'phone':
                $phone = $realte_content;
                break;
            case 'email':
                $email = $realte_content;
                break;
            case 'qq':
                $qq = $realte_content;
                break;
            default:
                return resultArray(['error' =>"类型错误"]);
                break;
        }

        $content  = $request->post("content","");
        $uid = $isLogin['uid'];
        $data['uid']   = $uid;
        $data['qq']    = isset($qq)?$qq:0;
        $data['email'] = isset($email)?$email:0;
        $data['phone'] = isset($phone)?$phone:0;
        $data['content'] = $content;
        $feedbackModel = model('v2/Feedback');
        $res = $feedbackModel->addFeedback($data);
        if(!$res){
            return resultArray(['error' =>$feedbackModel->getError()]);
        }
        return resultArray(['data' => "反馈成功"]);
    }
}
