<?php
namespace app\v2\controller;
use think\Controller;
use think\Request;
use com\IP;
use app\admin\controller\Upload;

class Config extends Base
{

 public function __construct()
    {
        parent::__construct();
        $this->systemConfigModel =  model('common/SystemConfig');
        $this->lunbo =  model('common/Lunbo');
    }

    /**
     * 获取app系统配置
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-09-01T11:58:04+0800
     * @return   [type]                   [description]
     */
     public function getAppConfigs()
    {
        // 获取所有系统配置
        $appSysConfig = $this->systemConfigModel->getSysConfigList();
        if(!$appSysConfig){
                return resultArray(['error' => $this->systemConfigModel->getError()]);
        }

        $request = Request::instance();
        $post = $request->post();

        # 根据版本强制升级app
        // if($post['version'] == '1.0.0'){
        //     $appSysConfig['APP_SYS_NOTICE_STATUS']  = 1;   //通知状态(app强制弹框不关闭)
        //     $appSysConfig['APP_SYS_NOTICE_CONTENT'] = '当前版本过低，强烈建议您升级。如有疑问请关注公众号 “土豪钱庄”。';  //通知内容
        //     $appSysConfig['APP_SYS_NOTICE_TITLE']   = '升级提示';  //通知标题
        //     $appSysConfig['APP_SYS_NOTICE_LINKS']   = 'http://a.app.qq.com/o/simple.jsp?pkgname=com.qhweidai.yueguangbaika';  //通知跳转链接
        // }

        # 根据ip切换IOS审核模式
        // $ip = $request->ip();
        // $res = IP::find($ip);
        // if($res[0]!=='中国'){
        //     $appSysConfig['APP_IS_ONLINE'] = 0;  //这个字段为0表示为送审模式
        // }

        // dump($res);
        // dump($appSysConfig);
        // die();
        return resultArray(['data' => $appSysConfig]);
    }

/**
 * 获取轮播图数据
 * llj
 * 2017年6月29日15:46:48
 * 
 * @return [type] [description]
 */
     public function getLunbo()
    {
        $appLunbo = cache('APP_LUNBO');
        if (!$appLunbo) {
            //获取所有系统配置
            $appLunbo = $this->lunbo->getLunboList();
            if(!$appLunbo){
                return resultArray(['error' => $this->lunbo->getError()]);
            }
            cache('APP_LUNBO', null);
            cache('APP_LUNBO', $appLunbo, 10); //缓存配置
        }
        return resultArray(['data' => $appLunbo]);
    }

    /**
     * 获取七牛Token 用于前端上传图片
     * @author yhq <934797303@qq.com>
     * @date   2018-03-29
     * @return [type]     [description]
     */
    public function qiniuToken()
    {
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }

        $token = (new Upload)->getToken();
        if(!$token) {
            return resultArray(['error' => '获取失败']);
        }

        $data = [
            'qiniuToken' => $token,
            'imgUrl' => 'http://upload.yueguangbaika.com/'
        ];

        return resultArray(['data' => $data]);
    }



}
