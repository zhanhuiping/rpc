<?php
namespace app\v2\controller;
use think\Log;
use think\Request;
use com\verify\HonrayVerify;
use think\Db;
use think\cache\driver\Redis;
use think\Config;
use app\v2\logic\Repayment;
class Account extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->userModel =  model('v2/User');
    }
 

     /**
     * 图片验证码
     * @Author      火星车11号
     * @email       704184317@qq.com
     * @create_time 2017-08-17T10:47:22+0800
     * @return      [type]                   [description]
     */
    public function pic(Request $request)
    {
        
        $ss = $request->get("ss","");
        $captcha = new HonrayVerify(config('captcha'));
        return $captcha->entry($ss);
       
    }

    
    /**
     * 发送注册验证码
     * @zwr
     * @DateTime 2017-06-28T19:16:42+0800
     * @param    Request                  $request
     * @return   json                           
     */
    public function tg_sendRegVerify (Request $request)
    {
          
        $phone  = $request->post("phone","");
        $ss  = $request->post("ss","");
        $verify_img  = $request->post("verify_img","");
        //return $q = $_POST;DIE();
        $checkVerify = $this->ver($verify_img,$ss);
        //return $checkVerify;die();
        if(!$checkVerify){
             return resultArray(['error' => "请输入正确的图形验证码"]);
        }
        #区分 注册短信 和修改密码短信
        $verifyModel =  model('v2/Verify');
        $data = $verifyModel->sendVerify($phone);
        if (!$data) {
            return resultArray(['error'=> $verifyModel->getError()]);
        } 
       return resultArray(['data' => "发送成功"]);
    }


    /**
     * 图片验证
     * @Author      火星车11号
     * @email       704184317@qq.com
     * @create_time 2017-08-17T10:47:32+0800
     * @param       Request                  $request [description]
     * @return      [type]                            [description]
     */
    public function ver($verify,$ss)
    {
        // $captcha = new CaptchaHelper();
        $captcha = new HonrayVerify(config('captcha'));
        $res = $captcha->check($verify,$ss);
        return $res;
    } 

    /**
     * 发送找回密码短信
     * @zwr
     * @DateTime 2017-06-28T19:40:10+0800
     * @param    Request                  $request [description]
     * @return   [type]                            [description]
     */
    public function sendFindVerify(Request $request)
    {
     
        $phone  = $request->post("phone","");
        #区分 注册短信 和修改密码短信
        $verifyModel =  model('v2/Verify');
        $data = $verifyModel->sendVerify($phone,$type=2,0);
        if (!$data) {
             return resultArray(['error' => $verifyModel->getError()]);
        } 
        return resultArray(['data' => "发送成功"]);
    } 
    /**
     * 发送语音短信验证码
     * @zwr
     * @DateTime 2017-06-28T19:40:10+0800
     * @param    Request                  $request [description]
     * @return   [type]                            [description]
     */
    public function tg_sendRegVoiceVerify(Request $request)
    {
        $phone  = $request->post("phone","");
        $type  = $request->post("type",1);
        #区分 注册短信 和修改密码短信
        $verifyModel =  model('v2/Verify');
        $data = $verifyModel->sendVerify($phone,$type,0,1);
        if (!$data) {
             return resultArray(['error' => $verifyModel->getError()]);
        } 
        return resultArray(['data' => "发送成功"]);
    } 
   

   

 /**
     * 记录下载
     * @return json
     * @AUTHER 范俊成
     * @DATE 2018/4/16
     */
    public function download()
    {
        $param = $this->param;
        $type = empty($param['type'])?'':$param['type'];
        $flag = empty($param['flag'])?'':$param['flag'];
        $token = $param['token'];
        if($token){
            $isLogin =  $this->isLogin();
            if(!$isLogin['status']){
                return resultArray(['error' => $isLogin['info']]); 
            }
            $uid = $isLogin['uid'];
        }
        $uid = empty($uid)?'':$uid;
        $result = $this->userModel->download($flag,$type,$uid);
        return resultArray($result);
    }
        /**
     * 记录商家
     * 
     *
     */
    public function incrByBusinessId(Request $request)
    {

    	$id = $request->param('business');
//        $id = '976849-388708-839089';
    	$data['business'] = $id;
    	$data['type']     = 1;
        $token = md5(date('Y-m-d',time()) . md5('kdw'));
    	$data['token']     = $token;
        $res = http_post('kdw.weidaiyun123.com/index.php?s=/traffic/open/business_register',$data);
    }



    /**
     * 功能:   新注册接口
     * @param    Request             $request [description]
     * @return   [type]                       [description]
     */
    private $weidaiyun_url = 'http://kdw.weidaiyun123.com/index.php?s=/traffic/open/is_business';
    public function newRegister(Request $request)
    {

        $param = $this->param;
        $check['account'] = $param['phone'];

        # 注册加锁，5秒失效时间
        $redis = new Redis();
        $redis->select(3);
        $phone_key = 'newRegister'.$param['phone']; 
        $phone_value = $redis->get($phone_key);
        if($phone_value){
            return resultArray(['error' => '请不要频繁点击']);
        }
        $redis->set($phone_key,1,5);


        $validate = validate('user_verify');
        if (!$validate->check($check)) {
            return resultArray(['error' =>$validate->getError()]);
        }
        $param['username']  = trim($request->post("phone",""));
        //return $param;die();
        if($param['verify']){
           $verifyModel =  model('v2/Verify');
           $checkRes = $verifyModel->checkVerify($param['username'],$param['verify'],0);
           //return $checkRes;die(0);
           if (!$checkRes) {
               return resultArray(['error' => $verifyModel->getError()]);
           }
        }else{
           return resultArray(['error' => '缺少短信验证码']);
        }
        //die();
        $bidData['bid'] = $param['business'];
        $is_business = json_decode(http_post($this->weidaiyun_url,$bidData),true);
        if($is_business['code'] == 400)
        {
            $param['business'] = Null;
        }

        $userMolde =   model('user');
        $acode     = $param['acode'];
        $bcode     = $param['bcode'];
        $result = $userMolde->newRegister($param,$acode,$bcode);

        if(!$result){
            return resultArray(array('error'=>$userMolde->getError()));
        }

        $uid = $result ;
        $data = $userMolde->getInfoByUid($uid);

        $tokenModel =  model('v2/Token');
        $token = $tokenModel->apiEncode($uid);

        if (!$token) {
            return resultArray(['error' => $tokenModel->getError()]);
        }

        $data['token'] = $token;
        return resultArray(['data' => $data]);
    }


   /**
    * 功能:     第二个页面注册
    * @DateTime 2018-06-13
    * @return   [type]              [description]
    */
    public function secondRegister()
    {
      
        $param = $this->param;
        $token = $param['token'];
        $tokenRes = Db::connect('db_business')->table('qh_user_token')->field('uid,time')
                                              ->where(['token'=>$token])->find();
        
        $tokenRes['time'] = strtotime($tokenRes['time']);
        if(time() - $tokenRes['time'] >= 3600*24*30){
            $this->error = "token过期" ;
            return false;
        }   
        $param['uid'] = $tokenRes['uid'];
        # 加锁，五秒失效
        $redis = new Redis();
        $redis->select(3);
        $uid_key = 'secondRegister'.$uid;
        $value_uid = $redis->get($uid_key);
        if($value_uid){
            return resultArray(['error'=>'请不要频繁点击']);
        }
        $redis->set($uid_key,1,5);


        $userMolde  =   model('user');
        $result = $userMolde->finishUser($param);
        if(!$result){
            return resultArray(array('error'=>$userMolde->getError()));
        }
        return resultArray(array('data'=>$result));
    }

}

