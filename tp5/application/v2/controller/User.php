<?php
namespace app\v2\controller;
use think\Request;
use think\Db;
use think\Model;
use app\v2\model\Apply;
use app\v2\model\UserInfo;
class User extends Base
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 个人中心
     * @zwr
     * @DateTime 2017-06-29T14:19:27+0800
     */
    public function memberCenter()
    {   
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];
        $userInfoModel =  model('v2/UserInfo');

        $infoRes          = $userInfoModel->where('uid',$uid)->find()->toArray();
        $infoRes['tx_count'] = model('v2/Withdraw')->getTxCount($uid);
        $infoRes['apply_count'] = model('v2/Browse')->getBrowseCount($uid);
        return resultArray(['data' => $infoRes]);    
    }
    /**
     * 浏览历史
     * @zwr
     * @DateTime 2017-06-29T21:06:55+0800
     * @return   [type]                   [description]
     */
    public function getBrowseList(Request $request)
    {

        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];
        /*分页*/
        $page  = $request->post("page","1");
        $count  = $request->post("count","10"); 
        // $type  = $request->post("type","0"); 
        $start = ($page-1)*$count;      
        $limit['start'] = $start;
        $limit['count'] = $count;
        /*搜索条件*/
        $map['uid'] = $uid;
        /*需要的字段*/
        $field = "*";
        /*排序*/
        $orderby= "id desc";
        // $orderby= "id asc";

        $browse =  model('v2/Browse');
        $list =  $browse->getDataList($map,$field,$orderby,$limit);
        foreach ($list as $key => $value) {
            $resArr =  model('v2/Business')->getBname($list[$key]['bid']);
            if(!$resArr){
                unset($list['$key']);
                continue;
            }
            $list[$key]['bname'] = $resArr['bname'];
            $list[$key]['icon'] = $resArr['icon'];
            $list[$key]['links'] = $resArr['links'];
        }
        return resultArray(['data' => $list]);
    }     

    /**
     * 获取个人余额
     * llj
     * 2017年6月29日17:11:49
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function score()
    {   
        $scoreModel = model('common/score');
        $userInfo =  $this->isLogin();
        if(!$userInfo['status']){
            return resultArray(['error' => $userInfo['info']]); 
        }

        $data = $scoreModel->getScoreByUid($userInfo['uid']);
        if (!$data) {
            return resultArray(['error' =>$scoreModel->getError()]);
        }

        return resultArray(['data' => $data]);

    }

/**
 * 编辑个人信息
 * llj
 * 2017年6月29日22:54:54
 * 
 * @return [type] [description]
 */
    public function update(){

        $userModel = model('UserInfo');
        $param = $this->param;

        $userInfo =  $this->isLogin();

        if(!$userInfo['status']){
            return resultArray(['error' => $userInfo['info']]);
        }

        unset($param['token']);
        unset($param['phone']);
        $param['uid'] = $userInfo['uid'];
        // echo json_encode($param);die;
        
        $data = $userModel->updateDataByUId($param, $param['uid']);
        if (!$data) {
            return resultArray(['error' => $userModel->getError()]);
        } 
        return resultArray(['data' => '编辑成功']);
    }
    /**
     * 我的团队
     * @zwr
     * @DateTime 2017-06-30T11:07:24+0800
     * @return   json
     */
    public function myTeam()
    {
        $userInfo =  $this->isLogin();

        if(!$userInfo['status']){
            return resultArray(['error' => $userInfo['info']]);
        }
        $uid = $userInfo['uid'];
        
        $inviteModel = model('v2/Invite');
        $userModel = model('UserInfo');

        $puid = $inviteModel->getParent($uid);
        if((int)$puid===-1){
             $data['parent']=array();
        }else{
            $data['parent']['name'] = $userModel->getNameByuid($puid);
            $data['parent']['uid'] =$puid;            
            $data['parent']['phone'] =substr_replace($userModel->getPhone($puid), '****', 3, 4); 
            $data['parent'][ $key]['create_time'] = $value['create_time'];           
        }

        $son = $inviteModel->getSon($uid);
        if(!$son){
            $data['son'] = array();
        }else{
            foreach ($son as $key => $value) {
                $data['son'][ $key]['name']  = $userModel->getNameByuid($value['uid']);
                $data['son'][ $key]['uid']   = $value['uid'];
                $data['son'][ $key]['phone'] = substr_replace($userModel->getPhone($value['uid']), '****', 3, 4);
                $data['son'][ $key]['level'] = 1 ;
                $data['son'][ $key]['create_time'] = $value['create_time'];
            }
        }
        return resultArray(['data' => $data]);
    }
    /**
     * 获取邀请码链接
     * @zwr
     * @DateTime 2017-06-30T15:22:19+0800
     * @return   [type]                   [description]
     */
    public function getInviteInfo()
    {
        $userInfo =  $this->isLogin();

        if(!$userInfo['status']){
            return resultArray(['error' => $userInfo['info']]);
        }
        $uid = $userInfo['uid'];
        
        $inviteModel = model('Invite');
        $inviteCode = $inviteModel ->getInviteCode($uid);
        if(!$inviteCode) {
            return resultArray(['error' =>"获取邀请码失败"]);
        }
        $config = model('common/SystemConfig')->getSysConfigList();
        $invite_info['invite_url']            = $config['APP_INVITE_URL'].'?acode='.$inviteCode;
        $invite_info['Qrcode_url']            = $invite_info['invite_url'];
        $invite_info['background_img_haibao'] = $config['APP_HAIBAO_URL'].'?t='.time();

        return resultArray(['data' =>$invite_info]);

        #短链接 先不使用 2017-7-6 10:14:53   火星车11号 
        
        // $inviteUrl = $inviteModel->getInviteshortUrl($uid);
        // #判断数据库是否存有短链接
        // if(!$inviteUrl){
        //     $res =  get_short_url($invite_info['invite_url']);
        //     $invite_info['Qrcode_url']     = $invite_info['invite_url'];
        //     $inviteModel->save(['short_url'=>$res],['uid'=>$uid]);
        // }else{
        //     $invite_info['Qrcode_url']     = $inviteUrl;
        // }
        // return resultArray(['data' =>$invite_info]);
    }

    /**
     * 个人资料
     * @author yhq
     * @DateTime 2017-07-27 15:45
     * @return json
     */
    public function myInfo(){
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];
        $param = $this->param;

        $UserInfo = model('UserInfo');
        $data = $UserInfo ->getMyInfo($uid,$param);
        if(!$data) {
            return resultArray(['error' =>$UserInfo->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 运营商认证
     * @param  [type] $param [description]
     * @return [type]        [description]
     */
    public function operatorAuth()
    {        
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];
        $param = $this->param;

        $UserInfo = model('UserInfo');
        $data = $UserInfo ->operatorAuth($uid,$param);
        if(!$data) {
            return resultArray(['error' =>$UserInfo->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 是否验证运营商
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-09T23:21:54+0800 
     */
    public function Operator()
    {
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];
        $UserInfo = model('UserInfo');
        $data = $UserInfo ->checkOperator($uid);
        if(!$isLogin['status']){ 
            return resultArray(['data' => $data,'login' =>0]);
        }else{
            return resultArray(['data' => $data,'login' =>1]);   
        }
    }

    /**
     * 验证状态
     * @return [type] [description]
     */
    public function authState()
    {
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];
        $UserInfo = model('UserInfo');
        $data = $UserInfo ->authState($uid);
        if(!$data){
            return resultArray(['error' =>$UserInfo->getError()]);
        }
        return resultArray(['data' => $data]);   

    }


    //通话详单同步回调
    public function returnUrl(Request $request)
    {
        $param = $request->get();
        $UserInfo = model('UserInfo');
        $data = $UserInfo->returnUrl($param);
        if(!$data) {
             echo"<script>alert('失败');history.go(-1);</script>"; 
             die;
        }
        echo "<script>window.history.go(-3)</script>";
        die;
    }

    //通话详单异步回调
    public function notifyUrl(Request $request)
    {
        $param = $request->get();
        $UserInfo = model('UserInfo');
        $data = $UserInfo->notifyUrl($param);
        if(!$data) {
            return resultArray(['error' =>$UserInfo->getError()]);
        }
        return resultArray(['data' => '成功']);
    }


    //验证身份证后六位数
    public function checkIdLastSix()
    {
        $param = $this->param;
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid = $isLogin['uid'];
        $userInfo = model('UserInfo');
        //获取身份证后六位数
        $re = $userInfo->getIdLastSix($uid,$param['sixnum']);
        if(!$re) {
            return resultArray(['error' =>$userInfo->getError()]);
        }
        return resultArray(['data' => '验证成功']);
    }

    // // 删除帐号-测试专用
    // public function delUser(){
    //     $param = $this->param;
    //     $isLogin =  $this->isLogin();
    //     if(!$isLogin['status']){
    //         return resultArray(['error' => $isLogin['info']]);
    //     }
    //     $uid = $isLogin['uid'];
    //     $delUser = model('user')->where('uid',$uid)->delete();
    //     $delUserInfo = model('user_info')->where('uid',$uid)->delete();
    //     if($delUser){
    //        return resultArray(['data' => '删除账号成功']); 
    //     }
    //     return resultArray(['error' => '删除账号失败']); 
    // }

    //     // 删除上一笔订单-测试专用
    // public function delApply(){
    //     $param = $this->param;
    //     $isLogin =  $this->isLogin();
    //     if(!$isLogin['status']){
    //         return resultArray(['error' => $isLogin['info']]);
    //     }
    //     $uid = $isLogin['uid'];
    //     $applyModel = new Apply;
    //     $delApply= $applyModel->where('uid',$uid)->order('id desc')->delete();
    //     if($delApply){
    //        return resultArray(['data' => '上一笔订单删除成功']); 
    //     }
    //     return resultArray(['error' => '上一笔订单删除失败']); 
    // }

    /**
     * 绑定银行卡列表资料
     * @author yhq <934797303@qq.com>
     * @date   2018-03-30
     * @return [type]     [description]
     */
    public function bindBankInfo()
    {
        $param = $this->param;
        $isLogin =  $this->isLogin();
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid = $isLogin['uid'];
        $userInfo = (new UserInfo)->where('uid', $uid)->field('phone,name,id_card')->find();

        $supportBankStr = config('SUPPORT_BANK');
        $supportBankArr = explode('/', $supportBankStr); 
        if (is_array($supportBankArr)) {
            foreach ($supportBankArr as $key => $value) {
                if(empty($value)){
                    unset($supportBankArr[$key]);
                }
            }
            foreach ($supportBankArr as $key => $value) {
                $supportBank[] = $value;
            }
        }

        $data = [
            'name'        => $userInfo['name'],    // 姓名
            'phone'       => $userInfo['phone'],   // 手机号
            'id_card'     => $userInfo['id_card'], // 身份证
            'supportBank' => $supportBank,         // 支持的银行
        ];

        // null 处理
        foreach ($data as $key => $value) {
           if (empty($value)) {
               $data[$key] = '';
           }
        }

        return resultArray(['data' => $data]); 
    }

    /**
     * 根据手机号查询用户信息
     * @author Warner <934797303@qq.com>
     * @date   2018-04-28
     * @return [type]
     */
    public function getUserInfoByPhone(){
        $param = $this->param; 
        $userInfoModel = new UserInfo;

        $data = $userInfoModel->getUserInfoByPhone($param['phone']);
        if(!$data){
            return resultArray(['error' => $userInfoModel->getError()]);
        }

        return resultArray(['data' => $data]);
    }

    /**
     * 根据手机号更新用户信息
     * @author Warner <934797303@qq.com>
     * @date   2018-04-28
     * @return [type]     [description]
     */
    public function updateUserInfoByPhone()
    {
        $param = $this->param; 
        $userInfoModel = new UserInfo;

        $data = $userInfoModel->updateUserInfoByPhone($param);
        if(!$data){
            return resultArray(['error' => $userInfoModel->getError()]);
        }

        return resultArray(['data' => '更新成功']);
    }


    public function getMyOverdueDays()
    {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $param = $this->param;
        $oderNumber = $param['orderNumber'];
//        $oderNumber = '2018042310248489';
//        $uid = 1855;
        $uid = $isLogin['uid'];
        $model  = new UserInfo();
        $data = $model -> getMyOverdueDays($uid,$oderNumber);
        return resultArray(array('data'=>$data));
    }


    public function getLinkman()
    {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid    = $isLogin['uid'];
        $model  = new UserInfo();
        $result = $model -> getLinkman($uid);
        if(!$result){
            return resultArray(array('error'=>'暂无数据'));
        }
        return resultArray(array('data'=>$result));
    }

}
