<?php
/**
 * 我的订单
 */
namespace app\v2\controller;
use think\Request;
use think\Db;
use app\v2\validate\MyOrder as VM;
use app\v2\logic\MyOrder as LM;
class MyOrder extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->userModel =  model('v2/User');
    }
    /**
     * [consignment 发货]
     * @return [type] [description]
     */
    public function consignment()
    {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid = $isLogin['uid'];
        //验证参数
        $validate = new VM();
        if (!$validate->check($this->param)) {
            return resultArray(['error' => $validate->getError()]);
        }
        //处理参数 并且插入
        $MyOrderLogic = new LM();
        $re = $MyOrderLogic->consignment($uid, $this->param);
        if(!$re)
        {
            return resultArray(['error' => $MyOrderLogic->getError()]);
        }
        return resultArray(['data' => '发货成功']);
    }
    /**
     * [consignmentLog 发货记录]
     * @return [type] [description]
     */
    public function consignmentLog()
    {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid = $isLogin['uid'];
        //处理参数 并且插入
        $MyOrderLogic = new LM();
        return resultArray(['data' => $MyOrderLogic->consignmentLog($uid, $this->param)]);
    }

}
