<?php
/**
 * 定时脚本处理类
 */
namespace app\v2\controller;
use think\Controller;
use think\Request;
use think\Db;
use think\Model;
use think\cache\driver\Redis;
use app\v2\model\UserInfo;
use app\v2\model\Apply;
use common\Utils;
use app\common\model\VoiceNotice;
use fuiou\FuiouApi;
use app\common\model\Payment;

class Script extends Base
{
    public function __construct()
    {
        parent::__construct();
        $config   = config();
        $message_prefix = $config['message_prefix']; //短信前缀
        $project_name   = $config['project_name'];   //项目标识
        $this->SELECT   = $config['redis_select'];

        //完善资料短信redis KEY
        $this->REDISKEY  = $project_name . ':userInfoPhoneList';
        //完善资料短信模板
        $this->WS_MSG    = $message_prefix . '您的初审已通过，请在72小时内点击链接 '.$config['ws_msg_url'].' 提交取款，逾期自动取消，退订回T';
        
        //明日还款提醒短信redis KEY
        $this->REDISKEY1 = $project_name . ':tomoRepaymentPhoneList';
        //明日还款提醒电话redis KEY
        $this->VoiceTomoRepaymentList = $project_name . ':VoiceTomoRepaymentList';
        //明日还款提醒短信模板
        $this->TOMO_HK_MSG = $message_prefix . '尊敬的客户 ，您的订单将于明天到期，请于明天下午17点前还款或延期，以免对您的生活及工作造成不必要的麻烦。点击 '.$config['hk_msg_url'].' 马上还款。';

        //当天还款提醒短信redis KEY
        $this->REDISKEY2 = $project_name . ':repaymentPhoneList';
        //当天还款提醒电话redis KEY
        $this->VoiceRepaymentList = $project_name . ':VoiceRepaymentList';
        //当天还款提醒短信模板
        $this->HK_MSG    = $message_prefix . '尊敬的客户 ，您的订单于今日到期，请于下午17点前还款或延期，以免对您的生活及工作造成不必要的麻烦。点击 '.$config['hk_msg_url'].' 马上还款。';

        //逾期当天还款提醒短信redis KEY
        $this->REDISKEY3 = $project_name . ':overdueRepaymentPhoneList';
        //逾期当天还款提醒电话redis KEY
        $this->VoiceOverdueRepaymentList = $project_name . ':VoiceOverdueRepaymentList';
        //逾期当天还款提醒短信模板
        $this->OV_HK_MSG = $message_prefix . '尊敬的客户 ，您的订单已经逾期，请尽快还款或延期，以免对您的生活及工作造成不必要的麻烦。点击 '.$config['hk_msg_url'].' 马上还款。';

        $this->saveOverdueOrderNumberList = $project_name.':saveOverdueOrderNumberList'; //逾期订单队列redis KEY
        $this->handelOverdueLimit = 50; //每分钟处理逾期条数

        // 打款处理中订单队列redis KEY
        $this->payUserOrderList = $project_name . ':payUserOrderList';

        // 安全提醒 2018/05/17
        $this->REDISKEY4 = $project_name . ':safeNoticePhoneList';
        $this->SAFE_MSG  = $message_prefix.'近来有非法分子私下联系可转账销账的，请勿相信，用户请自行登录平台账号进行还款续租 ，私下转账造成的后果一律自行承担，平台概不负责。 平台链接 '.$config['domainPath']['h5'];
    }

    /**
     * 通用测试接口
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function ceshi(Request $request)
    {
        $config = config();
        echo '<br/>数据库：'.$config['database']['database'];
        echo '<br/>系统名称：'.$config['system_name'];
        // die;

        // 测试redis写入
        $redis = new Redis();
        $time = time();
        if((new Redis)->set('test'.$time, $time, 60)){
            echo '<br/>redis写入成功: '.(new Redis)->get('test'.$time);
        }else{
            echo '<br/>redis写入失败';
        }

        //测试mongodb写入
        $mongo = Db::connect("db_mongo")->name('yueguangbaika')->insertGetId(["uid"=>1,"user_portrait"=>date("Y-m-d H:i:s",time())]);
        echo $mongo ? "<br/>mongoDb: ".$mongo : '<br/>mongoDb: 写入失败'; 
    }

    /**
     * 保存需要发送短信的手机号 插入HASH【每天9点执行一次】
     * 注意：每种短信插入不同的Redis KEY中，发送短信时候也是从相应的KEY中取出
     */
    public function savePhoneList()
    {
        $this->checkApiUrl(); // 检查接口环境

        $userInfoModel = new UserInfo;
        $applyModel    = new Apply;
        $redis         = new Redis();
        $redis -> select($this->SELECT);

        $yesterday = date('Y-m-d', strtotime(date('Y-m-d',time())) - 24*3600);
        $today     = date('Y-m-d', strtotime(date('Y-m-d',time())));
        $tomorrow  = date('Y-m-d', strtotime(date('Y-m-d',time())) + 24*3600);
        $time      = date('Y-m-d :H:i:s');
        $date      = date('Y-m-d');

        $result = array();
        $logoFileName = LOGS_DIR_NAME."savePhoneList.log";

        # 完善资料
        $phoneArr0 = $userInfoModel
                ->where("create_time like '%{$yesterday}%' and operator_state is null")
                ->field('phone,uid')
                ->select();
        $phoneArr0 = modelo2array($phoneArr0);
        $len0 = 0;
        foreach ($phoneArr0 as $value) {
            if ($redis -> hSet($this->REDISKEY, $value['phone'], $date)) {
                $len0 += 1;
            } 
        }
        $llen0 = count($redis -> hGetAll($this->REDISKEY));
        $result[0] = "[{$time}] {$len0}条完善资料号码插入Redis队列，共有{$llen0}条待发送。";
        @file_put_contents($logoFileName, $result[0]."\n", FILE_APPEND);  //记录日志


        # 明日到期
        $phoneArr1 = $applyModel
                ->where("appoint_time like '%{$tomorrow}%' and status in(3,12)")
                ->where($where)
                ->field('phone,uid')
                ->select();
        $phoneArr1 = modelo2array($phoneArr1);
        $len1 = 0;
        foreach ($phoneArr1 as $value) {
            if ($redis -> hSet($this->REDISKEY1, $value['phone'], $date)) {
                $len1 += 1;
            } 
            $value['date'] = $date;
            $redis -> hSet($this->VoiceTomoRepaymentList, $value['phone'], json_encode($value)); //语音电话队列
        }
        $llen1 = count($redis -> hGetAll($this->REDISKEY1));
        $result[1] = "[{$time}] {$len1}条明日到期号码插入Redis队列，共有{$llen1}条待发送。";
        @file_put_contents($logoFileName, $result[1]."\n", FILE_APPEND);  //记录日志   


        # 今日到期
        $phoneArr2 = $applyModel
                ->where("appoint_time like '%{$today}%' and status in(3,12)")
                ->where($where)
                ->field('phone,uid')
                ->select();
        $phoneArr2 = modelo2array($phoneArr2);
        $len2 = 0;
        foreach ($phoneArr2 as $value) {
            if ($redis -> hSet($this->REDISKEY2, $value['phone'], $date)) {
                $len2 += 1;
            } 
            $value['date'] = $date;
            $redis -> hSet($this->VoiceRepaymentList, $value['phone'], json_encode($value)); //语音电话队列
        }
        $llen2 = count($redis -> hGetAll($this->REDISKEY2));
        $result[2] = "[{$time}] {$len2}条今日到期号码插入Redis队列，共有{$llen2}条待发送。";
        @file_put_contents($logoFileName, $result[2]."\n", FILE_APPEND);  //记录日志  


        # 今日逾期
        $phoneArr3 = $applyModel
                ->where("appoint_time like '%{$yesterday}%' and status=4")
                ->where($where)
                ->field('phone,uid')
                ->select();
        $phoneArr3 = modelo2array($phoneArr3);
        $len3 = 0;
        foreach ($phoneArr3 as $value) {
            if ($redis -> hSet($this->REDISKEY3, $value['phone'], $date)) {
                $len3 += 1;
            } 
            $value['date'] = $date;
            $redis -> hSet($this->VoiceOverdueRepaymentList, $value['phone'], json_encode($value)); //语音电话队列
        }
        $llen3 = count($redis -> hGetAll($this->REDISKEY3));
        $result[3] = "[{$time}] {$len3}条今日逾期号码插入Redis队列，共有{$llen3}条待发送。";
        @file_put_contents($logoFileName, $result[3]."\n", FILE_APPEND);  //记录日志  


        dump($result);
        die;
    }


    /**
     * 从HASH取出并逐一发送短信【每分钟执行一次】
     * 注意：每次不要发送过多条数，防止超时
     */
    public function sendMessages()
    {
        $this->checkApiUrl(); // 检查接口环境

        $userInfoModel = new UserInfo;
        $applyModel    = new Apply;
        $redis         = new Redis();
        $redis -> select($this->SELECT);
        $time = date('Y-m-d :H:i:s');
        $result = array();
        $logoFileName = LOGS_DIR_NAME."sendMessages.log";
        $limit = 10;

        # 完善资料
        $len0  = 0;
        foreach ($redis->hGetAll($this->REDISKEY) as $key => $value) {
            $res0 = Utils::curl_heart_verify($key, $this->WS_MSG);
            if($res0['status'] == 1) Utils::SmsCount(2);
            $redis -> hdel($this->REDISKEY, $key);      
            $len0 ++;
            if($len0 >= $limit)  break;
        }
        $llen0 = count($redis->hGetAll($this->REDISKEY));
        $result[0] = "[{$time}] {$len0}条完善资料提醒短信已经发送，还有{$llen0}条待发送";
        file_put_contents($logoFileName, $result[0]."\n", FILE_APPEND);  //记录日志


        # 明日到期
        $len1 = 0;
        foreach ($redis->hGetAll($this->REDISKEY1) as $key => $value) {
            $res1 = Utils::curl_heart_notic($key, $this->TOMO_HK_MSG);
            if($res1['status'] == 1) Utils::SmsCount(2);
            $redis -> hdel($this->REDISKEY1, $key);      
            $len1 ++;
            if($len1 >= $limit)  break;
        }
        $llen1 = count($redis->hGetAll($this->REDISKEY1));
        $result[1] = "[{$time}] {$len1}条明日到期提醒短信已经发送，还有{$llen1}条待发送";
        file_put_contents($logoFileName, $result[1]."\n", FILE_APPEND);  //记录日志


        # 今日到期
        $len2 = 0;
        foreach ($redis->hGetAll($this->REDISKEY2) as $key => $value) {
            $res2 = Utils::curl_heart_notic($key, $this->HK_MSG);
            if($res2['status'] == 1) Utils::SmsCount(2);
            $redis -> hdel($this->REDISKEY2, $key);      
            $len1 ++;
            if($len1 >= $limit)  break;
        }
        $llen2 = count($redis->hGetAll($this->REDISKEY2));
        $result[2] = "[{$time}] {$len2}条今日到期提醒短信已经发送，还有{$llen2}条待发送";
        file_put_contents($logoFileName, $result[2]."\n", FILE_APPEND);  //记录日志


        # 今日逾期
        $len3 = 0;
        foreach ($redis->hGetAll($this->REDISKEY3) as $key => $value) {
            $res3 = Utils::curl_heart_notic($key, $this->OV_HK_MSG);
            if($res3['status'] == 1) Utils::SmsCount(2);
            $redis -> hdel($this->REDISKEY3, $key);      
            $len3 ++;
            if($len3 >= $limit)  break;
        }
        $llen3 = count($redis->hGetAll($this->REDISKEY3));
        $result[3] = "[{$time}] {$len3}条今日逾期提醒短信已经发送，还有{$llen3}条待发送";
        file_put_contents($logoFileName, $result[3]."\n", FILE_APPEND);  //记录日志

        dump($result);
        die;
    }

    /**
     * 逾期处理的脚本【不自动执行】
     */
    public function handOverdueScript(Request $request)
    {

        // 针对某个订单
        $order_number = $request->get('order_number');
        if(!empty($order_number)){
            $map['order_number'] =  $order_number;
        }

        $ApplyModel = new Apply;
        $today = date('Y-m-d', SYS_TIMESTAMP); // 现在的日期
        $map['status'] = array('in', [3,4,12]);
        $map['appoint_time'] = ['lt', $today];
        $res = $ApplyModel
            ->where($map)
            ->where("overdue_date is null OR to_days(overdue_date) <> to_days(now())")
            ->field('overdue_date,order_number,renewal_count,overdue_count,overdue_rate,money,appoint_time,overdue_money,status')
            ->select();
        $res = modelo2array($res);

        foreach ($res as $key => $apply) {
            $time = strtotime($today) - strtotime(date('Y-m-d',strtotime($apply['appoint_time'])));
            $overduDate = date('Y-m-d', strtotime($apply['overdue_date']));
            if($time>0){  //逾期
                // 续期次数大于等于逾期次数时递增逾期次数
                if($apply['renewal_count']>=$apply['overdue_count']){
                    $param['overdue_count'] = (int)$apply['overdue_count'] + 1;
                }
                // 处理日期不一样就再次叠加逾期费用
                if($overduDate!=$today){
                    $overdueMoney = $apply['money']*$apply['overdue_rate']; //每天逾期费
                    $param['overdue_money'] = $apply['overdue_money'] + $overdueMoney;
                }
                $param['status']       = 4;
                $param['overdue_date'] = $today;
                $where['order_number'] = $apply['order_number'];
                $update = Db::name('apply')->where($where)->update($param);
                if($update){
                    $arr[] = $apply['order_number'];
                }
            }
        }
        $count = count($arr);

        $time   = date('Y-m-d :H:i:s');
        $result = "[{$time}] {$count}条逾期订单已经处理，处理订单号：".json_encode($arr);

        @file_put_contents(LOGS_DIR_NAME."handOverdueScript.log", $result."\n", FILE_APPEND);  //记录日志

        dump($result);
        die;
    }

    /**
     * 逾期订单入队列【每天00:01执行一次】
     * @author   yhq <934797303@qq.com>
     * @DateTime 2018-03-17T12:30:11+0800
     * @return   [type]                   [description]
     */
    public function saveOverdueOrderNumberList()
    {
        $redis  = new Redis();
        $redis -> select($this->SELECT);
        $redisKey = $this->saveOverdueOrderNumberList;

        $ApplyModel = new Apply;
        // $today = date('Y-m-d', 24*3600+SYS_TIMESTAMP); // 现在的日期
        $today = date('Y-m-d'); // 现在的日期
        $map['status'] = array('in', [3,4,12]);
        $map['appoint_time'] = ['lt', $today];
        $res = $ApplyModel
            ->where($map)
            ->where("overdue_date is null OR to_days(overdue_date) <> to_days(now())")
            ->field('overdue_date,order_number,renewal_count,overdue_count,overdue_rate,money,appoint_time,overdue_money,status,loan_out_money')
            ->select();
        $res = modelo2array($res);
        $len = 0;

        foreach ($res as $key => $apply) {
            $time = strtotime($today) - strtotime(date('Y-m-d',strtotime($apply['appoint_time'])));
            $overduDate = date('Y-m-d', strtotime($apply['overdue_date']));
            if($time>0){  //逾期
                $add = $redis -> hSet($redisKey, $apply['order_number'], json_encode($apply));
                if($add){
                    $len += 1;
                } 
            }
        }

        $time   = date('Y-m-d :H:i:s');
        $llen   = count($redis -> hGetAll($redisKey));
        $result = "[{$time}] {$len}条逾期订单存入队列，还有{$llen}条待处理";
        @file_put_contents(LOGS_DIR_NAME.'saveOverdueOrderNumberList.log', $result."\n", FILE_APPEND);  //记录日志

        dump($result);
        die;
    }

    /**
     * 消费逾期订单队列【每分钟执行一次】
     * @author   yhq <934797303@qq.com>
     * @DateTime 2018-03-17T12:30:11+0800
     * @return   [type]                   [description]
     */
    public function costOverdueOrderNumber()
    {
        $redis  = new Redis();
        $redis -> select($this->SELECT);
        $redisKey = $this->saveOverdueOrderNumberList;

        $res = $redis -> hGetAll($redisKey);
        $limit = $this->handelOverdueLimit; //每次取出条数 
        $i = 0;     

        $ApplyModel = new Apply;
        $today = date('Y-m-d'); // 现在的日期
        $map['status'] = array('in', [3,4,12]);
        $map['appoint_time'] = ['lt', $today];

        foreach ($res as $key => $value) {
            $redis -> hdel($redisKey, $key); //取出hash
            $apply = json_decode($value, true); //订单信息
            $time  = strtotime($today) - strtotime(date('Y-m-d',strtotime($apply['appoint_time'])));
            $overduDate = date('Y-m-d', strtotime($apply['overdue_date']));

            $param = [];
            if($time > 0){  //逾期
                // 续期次数大于等于逾期次数时递增逾期次数
                if((int)$apply['renewal_count']>=(int)$apply['overdue_count']){
                    $param['overdue_count'] = (int)$apply['overdue_count'] + 1;
                }
                // 处理日期不一样就再次叠加逾期费用
                if($overduDate!=$today){
                    $overdueMoney = $apply['money']*$apply['overdue_rate']; //每天逾期费
                    $param['overdue_money'] = $apply['overdue_money'] + $overdueMoney;
                }
                // 逾期费上限止于放款金额3倍 2018年5月31日14:24:12
                if ($param['overdue_money'] > 3*$apply['loan_out_money']) {
                    $param['overdue_money'] = 3*$apply['loan_out_money'];
                }

                $param['status']       = 4;
                $param['overdue_date'] = $today;
                $where['order_number'] = $apply['order_number'];
                $update = Db::name('apply')->where($where)->update($param);

                if(false!==$update){
                    $arr[] = $apply['order_number'];
                }else{
                    $redis -> hSet($redisKey, $apply['order_number'], json_encode($apply)); //处理失败回归hash
                }
            }

            $i ++;
            if($i >= $limit)  break;
        }

        $len = count($redis -> hGetAll($redisKey));   
        $count = count($arr);

        $time   = date('Y-m-d :H:i:s');
        $result = "[{$time}] {$count}条逾期订单已经处理，还有{$len}条待处理。处理订单号：".json_encode($arr);

        @file_put_contents(LOGS_DIR_NAME."costOverdueOrderNumber.log", $result."\n", FILE_APPEND);  //记录日志

        dump($result);
        die;
    }

    /**
     * 是否能创建支付订单   
     * 逾期处理中，为了防止数据冲突，阻止用户创建支付订单
     * @author   yhq <934797303@qq.com>
     * @DateTime 2018-03-19T18:28:49+0800
     * @param    string    $order_number [订单号]
     * @return   array   code为1 可以创建支付订单     code为0不可以
     */
    public function canCreatePayOrder($order_number)
    {
        $redis  = new Redis();
        $redis -> select($this->SELECT);
        $redisKey = $this->saveOverdueOrderNumberList;
        $res = $redis -> hGetAll($redisKey);

        $result = [
            'code' => 1,
            'msg'  => ''
        ];

        $count = count($res);
        $limit = $this->handelOverdueLimit;
        $min   = ceil($count/$limit);

        // 提示用户不能创建支付订单
        foreach ($res as $key => $value) {
            if($order_number == $key){
                $result['code'] = 0;
                $result['msg']  = "系统维护中，请{$min}分钟后重试";
            }
        }

        return $result;
    }


    /**
     * 拉取运营商报告【每分钟执行一次】
     */
    public function pullNotifyUrl()
    {
        $userInfoModel = new UserInfo;
        $list = $userInfoModel
            ->where('mongo_operator_report is null and operator_outUniqueId is not null and to_days(operator_update_time) = to_days(now())')
            ->order('operator_update_time desc')
            ->field('operator_outUniqueId')
            ->select();
        $list = modelo2array($list);

        $redis = new Redis();
        $redis -> select($this->SELECT); //17号库
        $redisKey = config('project_name').':OPERATOR_ERROR_HASH';

        foreach ($list as $key => $value) {
            $redis->hSet($redisKey, $value['operator_outUniqueId'],  0); //插入新数据
        }

        $outCount     = 0;
        $successCount = 0;
        $limit        = 5; //每次取出次数

        $list = $redis->hGetAll($redisKey);
        $param['channel'] = config('operator_channel');
        foreach ($list as $key => $value) {
            $outCount++;
            $redis->hdel($redisKey, $key);
            if($outCount >= $limit) break;

            $operator_outUniqueId = $key;

            $param['outUniqueId'] = $operator_outUniqueId;
            $res = curlPost('http://api.qhweidai.com/api/automatic', $param);
            $res = json_decode($res,true);
            if($res['code']!=200 || empty($res['data']['search_id'])||empty($res['data']['mongoRes'])||empty($res['data']['userId'])){
                // $details[] = $operator_outUniqueId.' 失败:'.json_encode($res,JSON_UNESCAPED_UNICODE);
                continue;
            }

            $where2['operator_outUniqueId'] = $operator_outUniqueId;
            $map['operator_search_id']      = $res['data']['search_id'];
            $map['mongo_operator_report']   = $res['data']['mongoRes'];
            $map['operator_id']             = $res['data']['userId'];
            $map['operator_state2']         = $res['data']['notifystate'];
            $map['operator_account']        = $res['data']['account'];
            $map['operator_update_time']    = date('Y-m-d H:i:s');
            $update = Db::name('user_info')->where($where2)->update($map);
            if($update){
                 $details[] = $operator_outUniqueId;
                 $successCount++;
            }
        }

        $remainCount  = count($list) - $successCount;
        $time = date('Y-m-d H:i:s');
        $result = "[{$time}] 成功拉取运营商报告{$successCount}条，待拉取{$remainCount}条。详情:".json_encode($details,JSON_UNESCAPED_UNICODE);

        // $result = json_encode($result,JSON_UNESCAPED_UNICODE);
        @file_put_contents(LOGS_DIR_NAME."pullNotifyUrl.log", $result."\n", FILE_APPEND);  //记录日志

        return $result;
    }



    /**
     * 判断手机号所属运营商
     * @return [type] [description]
     */
    static function phoneOperator($phone)
    {
        if(!preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $phone)){
            return false;
        }
        $header = substr($phone, 0, 3); //前三位号码

        $CM = [134,135,136,137, 138,139,147,150,151,152,157,158,159,178,182,183,184,187,188,198,148]; // 移动号段
        $CU = [130,131,132,155,156,185,186,145,176,166,146]; // 联通号段
        $CT = [133,153,177,180,181,189,173,177,199];     // 电信号段

        if (in_array($header, $CM)) {
            $operator_code = 'CM';
            $operator_info = '中国移动';
        }elseif (in_array($header, $CU)) {
            $operator_code = 'CU';
            $operator_info = '中国联通';
        }elseif (in_array($header, $CT)) {
            $operator_code = 'CT';
            $operator_info = '中国电信';
        }else{
            $operator_code = '';
            $operator_info = '未知';
        }

        $result=[
            'phone'         => $phone,
            'operator_code' => $operator_code,
            'operator_info' => $operator_info,
        ];

        return $result;
    }


    /**
     * 批量修改手机系统正式
     */
    public function changeOS()
    {
        $redis     = new Redis();
        $redis -> select($this->SELECT); //17号库
        $change_os_id = $redis->get('change_os_id');
        if(!$change_os_id){
             $change_os_id = 0;
        }

        $limit = 20;
        $userInfoModel = new UserInfo;
        $list = $userInfoModel
        ->field('mobile_type,os,uid,id')
        ->where('os',3)
        ->where('id','>',$change_os_id)
        ->limit($limit)
        ->select();
        $list = modelo2array($list);

        foreach ($list as $key => $value) {
            if(strstr($value['mobile_type'], 'iPhone') || strstr($value['mobile_type'], 'iPod') || strstr($value['mobile_type'], 'iPad')){
                $map['os'] = 2;
            }else if(empty($value['mobile_type'])) {
                $map['os'] = 3;
            }else{
                $map['os'] = 1;
            }
            $where['uid'] = $value['uid'];
            $update = Db::name('user_info')->where($where)->update($map);
            if($update){
                $arr[] = $value['mobile_type'];
            }

            $redis->set('change_os_id',$value['id']);
        }

        $count = count($arr);
        $result = [
            'date'    => date('Y-m-d  H:i:s'), 
            'msg'     => "成功修改手机系统". $count .'条', 
            // 'details' => $arr
        ];

        $result = json_encode($result,JSON_UNESCAPED_UNICODE);
        @file_put_contents(LOGS_DIR_NAME."changeOS.log", $result."\n", FILE_APPEND);  //记录日志
        
        return $result;
    }


    /**
     * 批量修改手机系统正式
     */
    public function changeOS_test()
    {
        $redis     = new Redis();
        $redis -> select($this->SELECT); //17号库
        $test_change_os_id = $redis->get('test_change_os_id');
        if(!$test_change_os_id){
             $test_change_os_id = 0;
        }

        $limit = 20;
        $userInfoModel = new UserInfo;
        $list = $userInfoModel
        ->field('mobile_type,os,uid,id')
        ->where('os',3)
        ->where('id','>',$test_change_os_id)
        ->limit($limit)
        ->select();
        $list = modelo2array($list);

        foreach ($list as $key => $value) {
            if(strstr($value['mobile_type'], 'iPhone') || strstr($value['mobile_type'], 'iPod') || strstr($value['mobile_type'], 'iPad')){
                $map['os'] = 2;
            }else if(empty($value['mobile_type'])) {
                $map['os'] = 3;
            }else{
                $map['os'] = 1;
            }
            $where['uid'] = $value['uid'];
            $update = Db::name('user_info')->where($where)->update($map);
            if($update){
                $arr[] = $value['mobile_type'];
            }

            $redis->set('test_change_os_id',$value['id']);
        }

        $count = count($arr);
        $result = [
            'date'    => date('Y-m-d  H:i:s'), 
            'msg'     => "成功修改手机系统". $count .'条', 
            // 'details' => $arr
        ];

        $result = json_encode($result,JSON_UNESCAPED_UNICODE);
        @file_put_contents(LOGS_DIR_NAME."changeOS.log", $result."\n", FILE_APPEND);  //记录日志
        
        return $result;
    }

    /**
     * 消费语音电话队列
     * @author yhq <934797303@qq.com>
     * @date   2018-04-08
     */
    public function sendVoice()
    {
        $this->checkApiUrl(); // 检查接口环境

        $redis = new Redis();
        $redis -> select($this->SELECT);
        $time = date('Y-m-d :H:i:s');
        $result = array();
        $logoFileName = LOGS_DIR_NAME."sendVoice.log";
        $voiceNoticeModel = new VoiceNotice;
        $limit = 10;


        # 明天到期
        $redisKey0 = $this->VoiceTomoRepaymentList;
        $len0 = 0;
        foreach ($redis->hGetAll($redisKey0) as $key => $value) {
            $arr0 = json_decode($value,true);
            if($arr0['date'] == date('Y-m-d')) {
                $uid     = $arr0['uid'];
                $phone   = $arr0['phone'];
                $voiceTemplateId = config('VOICE_TEMPL')[0];

                if ($voiceNoticeModel->createVoice($uid, $phone, $voiceTemplateId, 0)) {
                    $res0 = '拨打成功';
                }else{
                    $res0 = $voiceNoticeModel->getError();
                }

                $result[0][$len0] = "[{$time}] 明天到期语音通知结果：{$res0} 详情：".json_encode($arr0);
                file_put_contents($logoFileName,$result[0][$i]."\n", FILE_APPEND);  //记录日志
            }
            $redis -> hdel($redisKey0, $key); 
            $len0 ++;
            if($len0 >= $limit)  break;
        }

       
        # 今天到期
        $redisKey1 = $this->VoiceRepaymentList;
        $len1 = 0;
        foreach ($redis->hGetAll($redisKey1) as $key => $value) {
            $arr1 = json_decode($value,true);
            if($arr1['date'] == date('Y-m-d')) {
                $uid     = $arr1['uid'];
                $phone   = $arr1['phone'];
                $voiceTemplateId = config('VOICE_TEMPL')[1];

                if ($voiceNoticeModel->createVoice($uid, $phone, $voiceTemplateId, 1)) {
                    $res1 = '拨打成功';
                }else{
                    $res1 = $voiceNoticeModel->getError();
                }

                $result[1][$len1] = "[{$time}] 今天到期语音通知结果：{$res1} 详情：".json_encode($arr1);
                file_put_contents($logoFileName,$result[1][$i]."\n", FILE_APPEND);  //记录日志
            }
            $redis -> hdel($redisKey1, $key); 
            $len1 ++;
            if($len1 >= $limit)  break;
        }


        # 今天逾期
        $redisKey2 = $this->VoiceOverdueRepaymentList;
        $len2 = 0;
        foreach ($redis->hGetAll($redisKey2) as $key => $value) {
            $arr2 = json_decode($value,true);
            if($arr2['date'] == date('Y-m-d')) {
                $uid     = $arr2['uid'];
                $phone   = $arr2['phone'];
                $voiceTemplateId = config('VOICE_TEMPL')[2];

                if ($voiceNoticeModel->createVoice($uid, $phone, $voiceTemplateId, 2)) {
                    $res2 = '拨打成功';
                }else{
                    $res2 = $voiceNoticeModel->getError();
                }

                $result[2][$len2] = "[{$time}] 今天逾期语音通知结果：{$res2} 详情：".json_encode($arr2);
                file_put_contents($logoFileName,$result[2][$i]."\n", FILE_APPEND);  //记录日志
            }
            $redis -> hdel($redisKey2, $key); 
            $len2 ++;
            if($len2 >= $limit)  break;
        }


        dump($result);
        die;
    }


    /**
     * 语音话单回调
     * @author yhq <934797303@qq.com>
     * @date   2018-04-08
     * @param  Request    $request [回调参数]
     * @return [type]              
     */
    public function voiceNotify(Request $request)
    {   
        $notifyResp = $request->post();
        $voiceNoticeModel = new VoiceNotice;
        $res = $voiceNoticeModel->updateVoice($notifyResp);

        if($res){
            $info = '更新成功';
        }else{
            $info = $voiceNoticeModel->getError();
        }

        // 未接通重新回归队列 【待完善】 
        // if($notifyResp['duration'] < 1){
            //语音类型
        // }

        $time   = date('Y-m-d :H:i:s');
        $result = "[{$time}] 处理结果: {$info} 回调详情：".json_encode($notifyResp, JSON_UNESCAPED_UNICODE);
        @file_put_contents(LOGS_DIR_NAME."voiceNotify.log", $result."\n\n", FILE_APPEND);  //记录日志

        return $result;
    }

    /**
     * 查询代付结果
     * @author Warner <934797303@qq.com>
     * @date   2018-05-02
     * @param  Request    $request [description]
     * @return [type]              [description]
     */
    public function payResQuery(Request $request)
    {
        $param = $request->param();
        $orderno = $param['orderno'];

        $FuiouApi = new FuiouApi;
        $res = $FuiouApi->payResQuery($orderno);
        dump($res);
    }


    /**
     * 处理特殊的代付订单【每天17点执行一次】
     * 新疆等地区 代付走对公通道 没有回调 依靠查询结果做相应处理
     * @author Warner <934797303@qq.com>
     * @date   2018-05-02
     * @return [type]     [description]
     */
    public function handleSpecialPayOrder()
    {
        $map['type']   = 0;
        $map['status'] = 0;
        $res = Db::name('payment_order')
            ->where($map)
            ->field('no_order')
            ->select();

        if(!empty($res)){
            foreach ($res as $key => $value) {
                $orderno[] = $value['no_order'];
            }
            
            $FuiouApi = new FuiouApi;
            $PaymentModel = new Payment;
            $res = $FuiouApi->payResQuery($orderno);
            // file_put_contents('1.txt', json_encode($res));
            // $res = json_decode(file_get_contents('1.txt'),true);

            if(!empty($res) && $res['trans']){
                foreach ($res['trans'] as $key => $value) {
                    $notifyResp = $res['trans'][$key];
                    $up = $PaymentModel->updatePaymentInfo($notifyResp);

                    $details[$key] = $notifyResp;
                    $details[$key]['handelRes'] = $up ? '更新成功' : $PaymentModel->getError();
                }
            }            
        }

        $time   = date('Y-m-d :H:i:s');
        $result = "[{$time}] 特殊代付订单处理详情：". json_encode($details,JSON_UNESCAPED_UNICODE);
        @file_put_contents(LOGS_DIR_NAME."handleSpecialPayOrder.log", $result."\n\n", FILE_APPEND);  //记录日志       

        dump($result);
        die;
    }


}
