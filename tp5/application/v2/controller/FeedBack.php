<?php
/**
 * 反馈类
 */
namespace app\v2\controller;

class FeedBack extends Base
{

	public function add()
	{
        // $FeedBackModel = new FeedBack;
        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        } 

        $param['uid'] = $isLogin['uid'];
        $cerate = model('FeedBack')->addFeedback($param);
        if(!$cerate){
            return resultArray(['error'=> model('FeedBack')->getError()]);
        }
        return resultArray(['data' => '成功']);
	}

}
