<?php
namespace app\v2\controller;
use think\Request;
// use oeynet\addCaptcha\CaptchaHelper;
use com\verify\HonrayVerify;
use think\Db;
use common\zhima;
class Sesame extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->SesameModel =  model('v2/Sesame');
    }



  /**
   * 功能:    对接qq同步助手
   * @email    18702529695@163.com
   * @DateTime 2018-02-05
   * @return   [type]              [description]
   */
   public function qqtb_verify()
    { 

        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid = $isLogin['uid'];
        $re = $this->SesameModel->get_qq_verify($uid);
        if(!$re)
        {
           return resultArray(['error'=>$this->SesameModel->getError()]);
        }
        return resultArray(['data'=>$re]);
    }

   
   
   /**
    * 功能:     获取QQ同步助手是否认证接口
    * @Author   Mrzhp
    * @email    18702529695@163.com
    * @DateTime 2018-02-05
    * @return   [type]              [description]
    */
   public function qqtb_directories()
   {
      $isLogin =  $this->isLogin();  //是否登录
      if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
      $uid = $isLogin['uid'];

      $directories = Db::name('user_info')->where(['uid'=>$uid])->value('directories');
      return resultArray(['data'=>['directories'=>$directories]]);
   }




   /**
    * 功能:     获取通讯录
    * @email    18702529695@163.com
    * @DateTime 2018-02-05
    * @return   [type]              [description]
    */
   public function qqtb_home()
   {
      $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
      $uid = $isLogin['uid'];

      $re = $this->SesameModel->get_qq_home($uid);
      if(!$re)
        {
           return resultArray(['error'=>'此账号未同步，请按照下方步骤重新同步通讯录']);
        }
        return resultArray(['data'=>$re]);
   }

   



   /**
    * 功能:     检测是否超过15
    * @Author   Mrzhp
    * @email    18702529695@163.com
    * @DateTime 2018-02-08
    * @return   [type]              [description]
    */
   public function is_fifeen()
   {
        $param = $this->param;
        $uid = $param['uid'];
        $data = $this->SesameModel->getTelDetails($uid);
        if(!$data_sesame){
            return resultArray(['error'=> $this->SesameModel->getError()]);
        }
        return resultArray(['data' => $data]);

   }


  /**
     * 芝麻分
     * @DateTime 2017-06-28T19:16:12+0800
     * @param    Request                  $request 
     * @return   json                      
     */
    public function Sesame(Request $request)
    {

        // $isLogin =  $this->isLogin();  //是否登录
        // if(!$isLogin['status']){
        //     return resultArray(['error' => $isLogin['info']]); 
        // }
        // $uid = $isLogin['uid'];
        
        $uid = 343;
        $data_sesame = $this->SesameModel->get_sesame($uid);
        if(!$data_sesame){
            return resultArray(['error'=> $this->SesameModel->getError()]);
        }
        return resultArray(['data' => $data]);
      
    }
   
    /**
     * 功能:    芝麻分回调
     * @Author   Mrzhp
     * @email    18702529695@163.com
     * @DateTime 2018-01-29
     * @return   [type]              [description]
     */
    public function get_sesame_notifyurl()
    {
      echo 343;
      $result       = $this->param;
      // $result = json_encode($param,JSON_UNESCAPED_UNICODE);
      @file_put_contents("zhima.txt", $result."\n", FILE_APPEND);  //记录日志
      
    }
  
}
