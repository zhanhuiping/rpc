<?php
/**
 * 申请类
 */
namespace app\v2\controller;
use think\Request;
use think\cache\driver\Redis;
use app\v2\model\BlackList;
use app\v2\model\UserInfo;
class Apply extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->applyModel    =  model('v2/Apply');
        // $this->UserInfoModel =  model('v2/UserInfo');
    }

    /**
     * 借款明细 - 申请前的预览
     * @return [type] [description]
     */
    public function loanDetails()
    {
        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        } 
        $uid    = $isLogin['uid'];
        $data = $this->applyModel->loanDetails($param, $uid);
        if(!$data){
            return resultArray(['error' =>$this->applyModel->getError()]);
        }
        return resultArray(['data' =>$data]);
    }


    /**
     * 借款申请
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-07-25 15:00
     * @return   json                      
     */
    public function apply()
    {
        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        } 
        $uid    = $isLogin['uid'];

        $userInfo = (new UserInfo)->where('uid', $uid)->field('phone, id_card')->find();
        $msg = empty(config('APP_IS_BLACK')) ? '您已被列入黑名单' : config('APP_IS_BLACK');
        $is_black = (new BlackList)->is_black($userInfo['id_card'], $userInfo['phone']);
        if($is_black){ //黑名单
            return resultArray(['error' => $msg]);
        }
        
        // $update = $this->UserInfoModel->updateDataByUId($param,$uid);  //申请前先更新个人信息
        // if (!$update) {
        //     return resultArray(['error' =>$this->UserInfoModel->getError()]);
        // }

        $apply = $this->applyModel->apply($param,$uid);
        if($apply === 'noContract')
        {
            return resultArray(['error' =>$this->applyModel->getError(),'code' => 401]);
        }
        if (!$apply) {
            return resultArray(['error' =>$this->applyModel->getError()]);
        }

        return resultArray(['data' => $apply]);
    }

    /**
     * 申请进度
     * @author cxr <1481746137@qq.com>
     */
    public function applyProgress()
    {
        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid    = $isLogin['uid'];
        $apply = $this->applyModel->applyProgress($param,$uid);
        if (!$apply) {
            return resultArray(['error' =>$this->applyModel->getError()]);
        }
        return resultArray(['data' => $apply]);
    }

    /**
     * 已申请转vip认证
     * @author cxr <1481746137@qq.com>
     */
    public function transferAuthVip()
    {
        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid    = $isLogin['uid'];
        $apply = $this->applyModel->transferAuthVip($param,$uid);
        if (!$apply) {
            return resultArray(['error' =>$this->applyModel->getError()]);
        }
        return resultArray(['data' => $apply]);
    }

    /**
     * 我的借款
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-07-31 10:45
     * @return   json                      
     */
    public function myApply()
    {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid   = $isLogin['uid'];
        $param = $this->param;
        $data  = $this->applyModel->myApply($uid, $param['status']);
        if($data===false){
            return resultArray(['error' =>$this->applyModel->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 借款详情
     * @param  [type] $order_number [description]
     * @return [type]               [description]
     */
    public function myApplyDetails($order_number)
    {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid   = $isLogin['uid'];
        $param = $this->param;
        $data  = $this->applyModel->myApplyDetails($param['order_number']);
        if($data!==false){
            return resultArray(['data' => $data]);
        }
        return resultArray(['error' =>$this->applyModel->getError()]);
    }


    /**
     * 还款页面
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-09-28
     * @return   [type]     [description]
     */
    public function repayment()
    {
        $param = $this->param;       
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid   = $isLogin['uid'];
        $data  = $this->applyModel->repayment($uid, $param['pay_type'], $param['days']);
        if($data===false){
            return resultArray(['error' =>$this->applyModel->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 还款页面
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-09-28
     * @return   [type]     [description]
     */
    // public function repayment_test()
    // {
    //     $param = $this->param;        
    //     $isLogin =  $this->isLogin();  //是否登录
    //     if(!$isLogin['status']){
    //         return resultArray(['error' => $isLogin['info']]); 
    //     }
    //     $uid   = $isLogin['uid'];
        
    //     $data  = $this->applyModel->repayment_test($uid, $param['pay_type'],$param['days']);
    //     if($data===false){
    //         return resultArray(['error' =>$this->applyModel->getError()]);
    //     }
    //     return resultArray(['data' => $data]);
    // }



    /**
     * 还款
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-09-28
     * @return   [type]     [description]
     */
    public function pay()
    {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $uid   = $isLogin['uid'];
        $data  = $this->applyModel->pay($uid);
        if($data===false){
            return resultArray(['error' =>$this->applyModel->getError()]);
        }
        return resultArray(['data' => $data]);
    }


    /**
     * 审核不通过再次借款倒计时
     */
    public function countdown() {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }

        $uid   = $isLogin['uid'];
        $data  = $this->applyModel->countdown($uid);

        if(!$data){
            return resultArray(['error' =>$this->applyModel->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 查询合同
     * @return [type] [description]
     */
    public function contract(Request $request)
    {
        $param = $request->param();
        if($param['type'] != 'pc'){
            $isLogin =  $this->isLogin();  //是否登录
            if(!$isLogin['status']){
                return resultArray(['error' => $isLogin['info']]); 
            }
        }
        $contract_number = $param['contract_number'];
        $data  = $this->applyModel->getContractById($contract_number);
        if(!$data){
            return resultArray(['error' =>$this->applyModel->getError()]);
        }
        return resultArray(['data' => $data]);
        
    }


    /**
     * 计算逾期天数
     */
    public function overdow_day() {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }

        $uid   = $isLogin['uid'];
        $data  = $this->applyModel->overdow_day($uid);

        if(!$data){
            return resultArray(['error' =>$this->applyModel->getError()]);
        }
        return resultArray(['data' => $data]);
    }


}
