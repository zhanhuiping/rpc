<?php
/**
 * Created by Chen.
 * author: 1481746137@qq.com
 * Date: 2018/1/31
 * Time: 11:25
 */

namespace app\v2\controller;


use think\Controller;
use app\v2\model\IdcardOcr as IdcardOcrModel;
use think\Request;

class IdcardOcr extends Base
{
    /**
     * 身份证OCR
     * @author cxr <1481746137@qq.com>
     * @param Request $request
     * @return array
     */
    public function ocr(Request $request)
    {
        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid    = $isLogin['uid'];

//        $uid = 26447;
        $type = $request->post('type','');

        $ocr = new IdcardOcrModel();
        $data = $ocr->Identify($request->post('img'),$uid,$type);
        if (!$data) {
            return resultArray(['error' =>$ocr->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 获取语音验证
     * @author cxr <1481746137@qq.com>
     * @param Request $request
     * @return array
     */
    public function getNumber(Request $request){
        $ocr = new IdcardOcrModel();
        $data = $ocr->getNumber($request->post('img'));
        if (!$data) {
            return resultArray(['error' =>$ocr->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 验证语音视频(活体)
     * @author cxr <1481746137@qq.com>
     * @param Request $request
     * @return array
     */
    public function validateVideo(Request $request){

        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid    = $isLogin['uid'];
//        $uid = '26447';

        $file = request()->file('video');
        if (!$file) {
            return resultArray(['error' => '请上传文件']);
        }
        $path = ROOT_PATH . 'custom' . DS . 'video'.DS;
        $info = $file->move(ROOT_PATH . 'custom' . DS . 'video');
        if ($info) {
            $video = $path.$info->getSaveName();
        }
//
//        $this->error = '速度测试';
//        return false;

        $ocr = new IdcardOcrModel();
        $data = $ocr->putVideo($video,$request->post('token_number'),$uid);
        if (!$data) {
            return resultArray(['error' =>$ocr->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 人脸验证结果
     * @author cxr <1481746137@qq.com>
     * @param Request $request
     * @return array
     */
    public function faceVerify(Request $request){

        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid    = $isLogin['uid'];
//        $uid = 11;

        $ocr = new IdcardOcrModel();
        $data = $ocr->verify( $request->param(),$uid);
        if (!$data) {
            return resultArray(['error' =>$ocr->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    /**
     * 获取临时身份证OCR信息
     * @author cxr <1481746137@qq.com>
     * @return array
     */
    public function ocrInfo(){
        $param = $this->param;
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]);
        }
        $uid    = $isLogin['uid'];
//        $uid = 26447;
        $ocr = new IdcardOcrModel();
        $data = $ocr->ocrInfo($uid);
        if (!$data) {
            return resultArray(['error' =>$ocr->getError()]);
        }
        return resultArray(['data' => $data]);
    }

    //认证同步回调
    public function returnUrl(Request $request)
    {
        $param = $request->param();
        $UserInfo = model('IdcardOcr');
        $data = $UserInfo->returnUrl($param);
        if(!$data) {
            echo"<script>alert('失败');</script>";
            die;
        }
//        echo "<script>window.history.go(-3)</script>";
//        die;
        return $data;
    }

    //认证异步回调
    public function notifyUrl(Request $request)
    {
        $param = $request->param();
        $UserInfo = model('IdcardOcr');
        $data = $UserInfo->notifyUrl($param);
        if(!$data) {
            return resultArray(['error' =>$UserInfo->getError()]);
        }
        return resultArray(['data' => '成功']);
    }


}