<?php
/**
 * 申请类
 */
namespace app\v2\controller;
use think\Request;
use think\Db;
class Message extends Base
{
    /**
     * 消息列表接口
     * @湛慧平
     * @DateTime 2017-07-25
     * @param    Request $request 
     * @return   json                      
     */
    public function messpages(Request $request)
    {
        $page = $request->post("page",1);
        $pagesize = $request->post("pagesize",4);
        $messageModel = model('v2/Message');
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
       $uid = $isLogin['uid'];
       $data = $messageModel->messagePage($page,$uid,$pagesize);
      if(!$data)
          {
             $data = array("code"=>400,"data"=>[],"error"=>'');
             return $data;
             exit;
          }
       return resultArray(['data' => $data]);
    }
    

     /**
         * 消息阅读
         * @zhp
         * @DateTime 2017-07-25
         * @param    Request $request 
         * @return   json                      
     */
     public function messred(Request $request)
     {
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
        $id = $request->post("id",'');
        if(!$id)
        {
            $data = array("code"=>400,"data"=>"请传入id","error"=>'');
            return $data;
            exit;
        }
        $messageModel = model('v2/Message');
        $data = $messageModel->messageRead($id);
        return resultArray(['data' => $data]);
     }

     /**
     * 消息详情接口
     * @湛慧平
     * @DateTime 2017-07-25
     * @param    
     * @return   json                      
     */
    public function messageinfo(Request $request)
    {
    	$id = $request->post("id",'');
        $messageModel = model('v2/Message');
        $isLogin =  $this->isLogin();  //是否登录
        if(!$isLogin['status']){
            return resultArray(['error' => $isLogin['info']]); 
        }
       
       $data = $messageModel->messageinfo($id);
       if(!$data)
          {
             $data = array("code"=>400,"data"=>[],"error"=>'');
             return $data;
             exit;
          }
       return resultArray(['data' => $data]);
    }


}
