<?php
/**
 * Created by Chen.
 * author: 1481746137@qq.com
 * Date: 2018/3/19
 * Time: 17:28
 */

namespace app\v2\controller;

use app\v2\model\ApplyOrder as ApplyOrderModel;
use app\v2\model\Pay as Paymodel;
use think\Request;


class ApplyOrder extends Base
{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Shanghai');
    }

    public function ApplyNotify(Request $request){


        $data = $request->post();
        // $data = json_decode('{"merchantOutOrderNo":"2018041150541024","payResult":1,"orderNo":"2018041150541024","pay_result":"SUCCESS"}',true);

//        $data2['merchantOutOrderNo'] = $data[ 'merchantOutOrderNo' ];
//        $data2['noncestr'] = $data[ 'noncestr'];
//        $data2['orderNo'] = $data[ 'orderNo'];
//        $data2['payResult'] = $data['payResult'];

        $PayModel = new Paymodel();
        echo $PayModel->apply_notice($data);

    }



}