<?php 
namespace app\v2\logic;
use app\v2\model\Apply;
use think\App;
use think\Model;
use app\v2\model\Consignment;
use app\v2\model\ApplyModel;
class MyOrder extends Model
{
    /**
     * [consignment 处理数据]
     * @param  string $uid    [description]
     * @param  array  &$param [description]
     * @return [type]         [description]
     */
    public function consignment($uid = '', &$param = [])
    {
        //查询订单是否存在
        $applyModel = new ApplyModel;
        $where = ['uid' => $uid];
        $re = $applyModel->getOne('order_number,status', $where, 'id DESC');
        if(!$re)
        {
            $this->error = '订单不存在';
            return false;
        }
        if(!in_array($re['status'],[3,4,12]))
        {
            $this->error = '未放款,不能发货!';
            return false;
        }
        $consignment =new Consignment;
        //快递单号是否已存在
        $mentWhere['logistics_no'] = $param['logistics_no'];
        $result = $consignment->getCount($mentWhere);
        if($result > 0)
        {
            $this->error = '快递单号已存在,发货失败!';
            return false;
        }
        //判断申请单号是否已经申请
        $mentField = 'status';
        $statusWhere['apply_id'] = $re['order_number'];
        $statusRe = $consignment->getOne($mentField, $statusWhere, 'id DESC');
        //不可重复申请
        if(!empty($statusRe) && in_array($statusRe['status'], [1,2]))
        {
            $this->error = '有正在进行中的订单,不可重复发货!';
            return false;
        }
        $data['uid'] = $uid;
        $data['consignment_name'] = $param['name'];
        $data['consignment_phone'] = $param['u_phone'];
        $data['logistics_company'] = $param['logistics_company'];
        $data['logistics_no'] = $param['logistics_no'];
        $data['apply_id'] = $re['order_number'];

        #更新Apply表已发货
        Apply::update(['huishou_status' => 1],['order_number' => $re['order_number']]);
        return $consignment->add($data);
    }
    /**
     * [consignmentLog 发货记录]
     * @param  string $uid     [description]
     * @param  [type] $param   [description]
     * @return [type]          [description]
     */
    public function consignmentLog($uid = '', &$param = [])
    {
        //查询订单是否存在        
        $page = ($param['page'] ?: 1) .','. ($param['limit'] ?: 10);
        $where = ['uid' => $uid];
        $consignment =new Consignment;
        //查询的字段
        $field = 'id,status,create_time as c_time,logistics_no,return_no,return_time,return_name';
        $order = 'id DESC';
        $data['data'] = $consignment->getDatas($field, $where, $order, '', $page);
        $data['count'] = $consignment->getCount($where);
        return $data;
    }
}

 ?>