<?php
namespace app\v2\validate;

use think\Validate;

class Operator extends Validate
{  
    // 验证规则
	protected $rule = [
		['emergencyName1', 'require|chs','紧急联系人1姓名必填|紧急联系人1姓名必须中文'],
		['emergencyRelation1', 'require','紧急联系人1关系必填'],
		['emergencyPhone1', 'require|checkPhone','紧急联系人1手机号必填'],		
		['emergencyName2', 'require|chs','紧急联系人2姓名必填|紧急联系人2姓名必须中文'],
		['emergencyRelation2', 'require','紧急联系人2关系必填'],
		['emergencyPhone2', 'require|checkPhone','紧急联系人2手机号必填'],		
		['emergencyName3', 'require|chs','紧急联系人3姓名必填|紧急联系人3姓名必须中文'],
		['emergencyRelation3', 'require','紧急联系人3关系必填'],
		['emergencyPhone3', 'require|checkPhone','紧急联系人3手机号必填'],		
		// ['emergencyName4', 'require|chs','紧急联系人4姓名必填|紧急联系人4姓名必须中文'],
		// ['emergencyRelation4', 'require','紧急联系人4关系必填'],
		// ['emergencyPhone4', 'require|checkPhone','紧急联系人4手机号必填'],
	];

	/**
	 * 验证是否是手机号码
	 *
	 * @param string $phone 待验证的号码
	 * @return boolean 如果验证失败返回false,验证成功返回true
	 */
	protected function checkPhone($phone,$rule) {
		if (strlen ( $phone ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $phone )) {
			return "您的手机格式不正确！".$rule;
		} else {
			return true;
		}
	}	 

	/**
	 * 验证身份证号
	 * @param  string  $id_number 身份证号码
	 * @param  array   $rule      验证规则
	 * @return boolean            
	 */
	protected function checkidnumber($id_number,$rule) {
		if (strlen ( $id_number ) != 18 || ! preg_match ( '/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/', $id_number )) {
			return "您的身份证格式不正确！".$rule;
		} else {
			return true;
		}
	}   
}