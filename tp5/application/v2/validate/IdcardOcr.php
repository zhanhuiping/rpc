<?php
/**
 * Created by PhpStorm.
 * User: cxr 148174637@qq.com
 * Date: 2018/2/6
 * Time: 10:21
 */

namespace app\v2\validate;

use think\Validate;

class IdcardOcr extends Validate
{
    // 验证规则
    protected $rule = [
        #上线时去掉以下注释
        ['name', 'require|chs','姓名必填|姓名必须中文'],
        ['id_card', 'require|checkidnumber','身份证必填|身份证号码错误'],
//        ['mon_income', 'require|number','月收入必填|月收入必须是数字'],
        ['token', 'require','token无效'],
//        ['id_card_img2', 'require','必须上传身份证照片'],
//        ['id_card_img3', 'require','必须上传身份证照片'],
//        ['tb_address', 'require','必须上传淘宝收货地址'],
//        ['zhima', 'require','必须上传芝麻分截图'],
//        ['company', 'min:2','请填写正确公司名称'],
    ];


    /**
     * 验证身份证号
     * @param  string  $id_number 身份证号码
     * @param  array   $rule      验证规则
     * @return boolean
     */
    protected function checkidnumber($id_number,$rule) {
        if (strlen ( $id_number ) != 18 || ! preg_match ( '/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/', $id_number )) {
            return "您的身份证格式不正确！".$rule;
        } else {
            return true;
        }
    }
}