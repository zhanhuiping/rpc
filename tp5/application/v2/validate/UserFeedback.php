<?php
namespace app\v2\validate;

use think\Validate;

class UserFeedback extends Validate
{
    // 验证规则
    protected $rule = [
        ['phone', 'number|checkPhone'],
        ['qq', 'number|length:5,12'],
        ['email', 'checkEmail'],
        ['content', 'require'],
    ];
	protected $message = array(
		'qq.number'    	=> 'qq必须是数字',
		'qq.length'    	=> 'qq长度5~12',
		'phone.number'    	=> '手机格式错误',
	    'content.require'    	=> '不能为空',
	    
	);
	/**
	 * 验证是否是手机号码
	 *
	 * @param string $phone 待验证的号码
	 * @return boolean 如果验证失败返回false,验证成功返回true
	 */
	protected function checkPhone($phone,$rule) {
	    if (strlen ( $phone ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $phone )) {
	        return "您的手机格式不正确！".$rule;
	    } else {
	        return true;
	    }
	}	    
	/**
	 * 验证邮箱
	 *
	 * @param string $email 待验证的邮箱
	 * @return boolean 如果验证失败返回错误信息,验证成功返回true
	 */
	protected function checkEmail($email,$rule) {
	    if ( ! preg_match('/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-\.])+[A-Za-z\d]{2,5}$/',$email)) {
	        return "您的邮箱格式不正确！".$rule;
	    } else {
	        return true;
	    }
	}	    
}