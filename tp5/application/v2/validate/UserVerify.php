<?php
namespace app\v2\validate;

use think\Validate;

class UserVerify extends Validate
{
    // 验证规则
    protected $rule = [
        ['account', 'require|checkPhone'],
    ];
	protected $message = array(
		'account.require'    	=> '手机号必须',
		'account.checkPhone'    => '手机号格式错误'
	);
	/**
	 * 验证是否是手机号码
	 *
	 * @param string $phone 待验证的号码
	 * @return boolean 如果验证失败返回false,验证成功返回true
	 */
	protected function checkPhone($phone,$rule) {
	    if (strlen ( $phone ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $phone )) {
	        return false;
	    } else {
	        return true;
	    }
	}	    
}