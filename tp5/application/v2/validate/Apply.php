<?php
namespace app\v2\validate;

use think\Validate;

class Apply extends Validate
{
    // 验证规则
	protected $rule = [
		['money', 'require'],
		['period', 'require'],
		['cid', 'require'],
	];
	protected $message = array(
		'money.require'   => '金额必填',
		'period.require' => '借款天数必填',
		'cid.require'    => '档位cid必填',
	);   
}