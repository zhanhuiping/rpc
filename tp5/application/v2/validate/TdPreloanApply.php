<?php
namespace app\v2\validate;

use think\Validate;

class TdPreloanApply extends Validate
{
    // 验证规则
    protected $rule = [
        ['name', 'require|chs'],
        ['phone', 'require|checkPhone'],
        ['idnumber', 'require|checkidnumber']
    ];
    protected $message = array(
        'name.require'    	=> '姓名必填',
        'name.chs'    	=> '姓名必须中文',
        'idnumber.require'    	=> '身份证必填',
        'phone.require'    	=> '手机号必填'
    );
    /**
     * 验证是否是手机号码
     *
     * @param string $phone 待验证的号码
     * @return boolean 如果验证失败返回false,验证成功返回true
     */
    protected function checkPhone($mobile,$rule) {
        if (strlen ( $mobile ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $mobile )) {
            return "您的手机格式不正确！".$rule;
        } else {
            return true;
        }
    }
    //验证身份证号
    protected function checkidnumber($id_number,$rule) {
        if (strlen ( $id_number ) != 18 || ! preg_match ( '/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/', $id_number )) {
            return "您的身份证格式不正确！".$rule;
        } else {
            return true;
        }
    }
}