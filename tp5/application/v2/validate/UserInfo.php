<?php
namespace app\v2\validate;

use think\Validate;

class UserInfo extends Validate
{  
    // 验证规则
	protected $rule = [
		#上线时去掉以下注释
		['name', 'require|chs','姓名必填|姓名必须中文'],
		['id_card', 'require|checkidnumber','身份证必填'],
		['mon_income', 'require|number','月收入必填|月收入必须是数字'],
		['id_card_img1', 'require','必须上传身份证照片'],
		['id_card_img2', 'require','必须上传身份证照片'],
		['id_card_img3', 'require','必须上传身份证照片'],
		['tb_address', 'require','必须上传淘宝收货地址'],
		// ['wx_wallet', 'require','必须上传微信钱包明细'],
		// ['alipay_bill', 'require','必须上传支付宝月账单'],
		['zhima', 'require','必须上传芝麻分截图'],
		['company', 'min:2','请填写正确公司名称'],
		// ['wechat_img', 'require','必须上传微信个人二维码 '],
		// ['negative_img', 'require','必须上传支付宝负面记录']
		// 
	];

	/**
	 * 验证是否是手机号码
	 *
	 * @param string $phone 待验证的号码
	 * @return boolean 如果验证失败返回false,验证成功返回true
	 */
	protected function checkPhone($phone,$rule) {
		if (strlen ( $phone ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $phone )) {
			return "您的手机格式不正确！".$rule;
		} else {
			return true;
		}
	}	 

	/**
	 * 验证身份证号
	 * @param  string  $id_number 身份证号码
	 * @param  array   $rule      验证规则
	 * @return boolean            
	 */
	protected function checkidnumber($id_number,$rule) {
		if (strlen ( $id_number ) != 18 || ! preg_match ( '/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/', $id_number )) {
			return "您的身份证格式不正确！".$rule;
		} else {
			return true;
		}
	}   
}