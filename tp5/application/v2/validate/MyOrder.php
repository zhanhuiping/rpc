<?php
namespace app\v2\validate;

use think\Validate;

class MyOrder extends Validate
{
    // 验证规则
    protected $rule = [
        ['u_phone', 'require|checkPhone'],
        ['name', 'require'],
        ['logistics_company', 'require'],
        ['logistics_no', 'require|number|length:12']
    ];
	protected $message = array(
		'u_phone.require'    	=> '手机号必须',
		'u_phone.checkPhone'    => '手机号格式错误',
		'name.require'    	=> '姓名不能为空',
		'logistics_company.require'    	=> '快递公司不能为空',
		'logistics_no.require'    	=> '快递单号不能为空',
		'logistics_no.number'    	=> '快递单号必须为数字',
		'logistics_no.length'    => '请输入正确的物流单号'
	);
	/**
	 * 验证是否是手机号码
	 *
	 * @param string $phone 待验证的号码
	 * @return boolean 如果验证失败返回false,验证成功返回true
	 */
	protected function checkPhone($phone,$rule) {
	    if (strlen ( $phone ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $phone )) {
	        return false;
	    } else {
	        return true;
	    }
	}	    
}