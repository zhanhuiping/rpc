<?php
/**
 * Created by Chen.
 * User: 148174637@qq.com
 * Date: 2018/1/11
 * Time: 9:50
 */

namespace app\admin\lib\enum;


class ApplyStatus
{
    /**
     * 当前状态
     */
    // 0申请中
    const APPLY = 0;

    // 1初审通过
    const FIRST = 1;

    // 2复审通过
    const REVIEW = 2;

    // 3已放款
    const LEND = 3;

    // 4逾期中
    const OVERDUE = 4;

    // 5已完成
    const COMPLETE = 5;

    // 6初审驳回
    const FIRST_REFUSE = 6;

    // 7复审驳回
    const REVIEW_REFUSE = 7;

    // 8其他驳回
    const OTHER_REFUSE = 8;

    // 9回访通过
    const RETURN_PASS = 9;

    // 10回访拒绝
    const RETURN_REFUSE = 10;

    // 11拒绝放款
    const LEND_REFUSE = 11;

    // 12延期
    const DELAY = 12;

    /**
     * 当前阶段
     */
    //初审认领阶段
    const FIRST_CLAIM_STAGE = 13;

    //初审通过阶段
    const FIRST_STAGE = 14;

    //M1认领阶段
    const M1_CLAIM_STAGE = 15;

    //M1通过阶段
    const M1_STAGE = 16;

    //财务认领阶段
    const TREAS_CLAIM_STAGE = 17;

    //M1转移M2阶段
    const TRANFER_M2_STAGE = 18;

    //初审拒绝阶段
    const FIRST_REFUSE_STAGE = 19;

    //M1拒绝阶段
    const M1_REFUSE_STAGE = 20;

    //财务拒绝阶段
    const LEND_REFUSE_STAGE = 21;

    /**
     * 检查当前所属阶段
     * @param $para
     * @return mixed
     */
    static public function CheckStage($para)
    {
        $currentStatus = $para['status'];

        if ($currentStatus == ApplyStatus::APPLY && $para['check_aid'] != '')
            return FIRST_CLAIM_STAGE;
        if ($currentStatus == ApplyStatus::REVIEW && $para['merchandiser'] == '')
            return FIRST_STAGE;
        if ($currentStatus == ApplyStatus::REVIEW && $para['merchandiser'] != '')
            return M1_CLAIM_STAGE;
        if ($currentStatus == ApplyStatus::RETURN_PASS && $para['treasurer'] == '')
            return M1_STAGE;
        if ($currentStatus == ApplyStatus::RETURN_PASS && $para['treasurer'] != '')
            return TREAS_CLAIM_STAGE;
        if (($currentStatus == ApplyStatus::LEND || $currentStatus == ApplyStatus::OVERDUE) && $para['transfer'] && $para['merchandiser2'] == '')
            return TRANFER_M2_STAGE;
        if (($currentStatus == ApplyStatus::LEND || $currentStatus == ApplyStatus::OVERDUE) && $para['transfer'] && $para['merchandiser2'] != '') return M2_CLAIM_STAGE;
        if ($currentStatus == ApplyStatus::FIRST_REFUSE)
            return FIRST_REFUSE_STAGE;
        if ($currentStatus == ApplyStatus::RETURN_REFUSE)
            return M1_REFUSE_STAGE;
        if ($currentStatus == ApplyStatus::LEND_REFUSE)
            return LEND_REFUSE_STAGE;

    }

}