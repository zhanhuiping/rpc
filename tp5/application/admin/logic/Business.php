<?php 
namespace app\admin\logic;
use think\Model;
use think\Db;
class Business extends Model
{
	/**
    * 商家匹配更新及插入
    * @param array $param 
    * @param int $bid
    * @param bool $isPost
    */
	public function match(&$param = [],$bid,$isPost = false) 
	{
		$business = model('Business');
		$business_match = $business->getMatchOne(['bid'=>$bid]);
        if($isPost)
        {
            if(!$param['limit_auto_order'] && !$param['limit_hand_order'] )
            {
                unset($param['limit_auto_order']);
                unset($param['limit_hand_order']);

            }elseif($param['limit_all_order']){
                //获取we_config配置文件一条字段数据
                $bili = $business->getWeconfigField(['name'=>'_WX_AS_AU_RATE3'],'value');
                $fbili = 1-$bili;
                $param['limit_auto_order'] = $param['limit_all_order'] * $fbili;
                $param['limit_hand_order'] = $param['limit_all_order'] * $bili;
            }
            unset($param['limit_all_order']);
            if($business_match){

                $param['update_time'] = time();
                //保存数据
                $res = $business->saveMatchData(['bid'=>$bid],$param);
            }else{
                $param['uniacid'] = session("mp.uniacid");
                $param['bid'] = $bid;
                //获取指定数据
                $param['bname'] = $business->getBusinessField(['bid'=>$bid],'bname');
                $param['create_time'] = time();
                //添加数据
                $res = $business->addMatchData($param);
            }

            if(!$res){
                    /*set_order_limit($bid,'limit_auto_order',$info['limit_auto_order']);
                    set_order_limit($bid,'limit_hand_order',$info['limit_hand_order']);
                    set_order_limit($bid,'limit_dig_order',$info['limit_dig_order']);*/
                $this->error = '操作失败';
                return false;
            }
        }
        if(!$business_match) $business_match['bname'] = $business->getBusinessField(['bid'=>$bid],'bname');
        return $business_match;
	}
    /**
     * [reAuth 账号重新授权]
     * @param  array  &$param [description]
     * @return [type]         [description]
     */
    public function reAuth($param = [])
    {
        if($param['uid'] <= 0)
        {
            $this->error = '参数错误';
            return false;
        }
        if(!is_array($param['groupId'])){
            $this->error = '参数类型错误';
            return false;   
        }
        Db::transaction(function () use(&$param){
            $authGroupAccess = model('AuthGroupAccess');
            //删除授权
            $authGroupAccess->delAuthGroup(['uid'=>$param['uid']]);
            if(!empty($param['groupId'])){
                //重新授权
                foreach($param['groupId'] as $v){
                    $data['uid'] = $param['uid'];
                    $data['group_id'] = $v;
                    $authGroupAccess->add($data);
                }
            }
        });
        return true;
    }
	
}

 ?>