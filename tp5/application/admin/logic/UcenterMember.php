<?php 
namespace app\admin\logic;
use think\Model;
use app\common\count\Count;
use think\Loader;
class UcenterMember extends Model
{
    protected $name = "wd_ucenter_member";
    /**
     * 添加用户
     * @param array $param [description]
     */
    public function addUsers($param = [])
    {
        $validate = validate('qh_member');
        if (!$validate->check($param)) {
            $this->error = $validate->getError();
            return false;
        }
        //取用户登录信息
        $gid = $param['gid'];
        if(empty($gid)){
            $this->error = '请选择权限';
            return false;
        }
        /* 注册用户 */
        $uid = $this->register($param['username'],$param['nickname'],$param['password'],$param['email'],$param['mobile'],$param['type'],$param['cid'],$param['aid'],$param['bid'],$param['ip']);
        if( 0 < $uid ){
            $ucenter_member = model('UcenterMember')->getMemberFind(['id'=>$uid]);
             //用户组授权
             foreach ($gid as $k => $v) {
                $auth_group_access = array('uid'=>$uid,'group_id'=>$v);//2:平台管理员
                model('AuthGroupAccess')->add($auth_group_access);
            }
            $add = array();
            $add['oauth_token'] = md5($ucenter_member['uid'].time().time());
            $add['expires'] = time()+3600*24*365;
            $add['uid'] = $ucenter_member['uid'];
            $add['created'] = time();
            $add['updated'] = time();
            /*$add['created_by'] = $user['uid'];
            $add['updated_by'] = $user['uid'];*/
            $res = model('OauthTokens')->add($add);
            if(!$res){
                model('UcenterMember')->delMember(['id'=>$uid]);
                model('Member')->delMember(['uid'=>$uid]);
                model('AuthGroupAccess')->delAuthGroup(['uid'=>$uid]);
                $this->error = '添加失败';
                return false;
            } 

            return true;
        }else{
            $this->error = $this->getError();
            return false;
        }
    }
    /**
     * [saveUser 更改用户信息]
     * @param  array  $param [description]
     * @return [type]        [description]
     */
    public function saveUser(&$param = [])
    {
        $validate = validate('qh_member');
        if (!$validate->check($param)) {
            $this->error = $validate->getError();
            return false;
        }
        if($param['uid'] <= 0){
            $this->error = '参数错误';
            return false;
        }
        //判断用户名是否改动
        $userInfo =  model('UcenterMember')->getMemberFind(['username'=>$param['username']]);
        if($userInfo['username'] !== $param['username']){
            $this->error = '该用户名已存在';
            return false;
        }
        unset($param['username']);
        $uid = $param['uid']; 
        unset($param['uid']);
        $memberParam['nickname'] = $param['nickname'];
        //修改昵称
        $result = model('Members')->saveData(['uid'=>$uid] ,$memberParam);
        if (!$result) {
            $this->error = '昵称注册失败';
            return false;
        }
        $md5PW = strtoupper(md5($param['password']));
        $param['password'] = $md5PW;
        $param['app_pwd'] = $md5PW;
        //保存信息
        $reInfo = model('UcenterMember')->saveData(['id'=>$uid],$param);
        if($reInfo === false)
        {
            $this->error = '修改信息失败';
            return false;   
        }
        return $reInfo;

    }
    public function register($username, $nickname, $password, $email='', $mobile='', $type=1,$cid=0,$aid=0,$bid=0,$ip = 0)
    {
        $data = array(
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'mobile' => $mobile,
            'status' => 1,
            'type' => $type,
        );

        if($_SERVER['HTTP_HOST'] == 'a.rrjiekuan.com'){
            $data['uniacid'] == 3;
        }else if($_SERVER['HTTP_HOST'] == 'www.wanka178.com' || $_SERVER['HTTP_HOST'] == 'www.wanka518.com'){
            $data['uniacid'] == 2;
        }else{
            $data['uniacid'] == 3;
        }
        $userInfo =  model('UcenterMember')->getMemberFind(['username'=>$username]);
        if($userInfo){
            $this->error = '该用户名已存在';
            return false;
        }
        $result = model('Members')->registerMember($nickname);
        $mUid = $result->uid;
        if ($mUid <= 0) {
            $this->error = '昵称注册失败';
            return false;
        }
        $pwd = strtoupper(md5($password));
        $time = time();
        $data['uid'] = Count::myrand2(8,'wd_uid',4);
        $data['nickname'] = $nickname;
        $data['cid'] = $cid;
        $data['aid'] = $aid;
        $data['bid'] = $bid;
        $data['reg_time'] = $time;
        $data['reg_ip'] = $ip;
        $data['last_login_time'] = $time;
        $data['last_login_ip'] = $ip;
        $data['password'] = $pwd;
        //$data['created_by'] = $app_user['uid'];
        $data['app_pwd'] = $pwd;
        $data['id'] = $mUid;
        $data['reg_ip'] = $ip;
        /* 添加用户 */
        $re = $this->create($data);
        $uid = $re->id;
        if ($uid === false) {
            //如果注册失败，则回去Memeber表删除掉错误的记录
            model('Member')->delMember(['uid' => $mUid]);
        }
        model('Action','logic')->action_log('reg','ucenter_member',1,1);
        return $uid; //0-未知错误，大于0-注册成功
        
    }
    
}

 ?>