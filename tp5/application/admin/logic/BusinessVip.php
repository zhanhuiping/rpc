<?php 
namespace app\admin\logic;
use think\Model;
class BusinessVip extends Model
{   
    /**
     * [getBusinessVip 获取套餐信息]
     * @param  array  $param [description]
     * @return [type]        [description]
     */
	public function getBusinessVip($param = [])
    {
        $bid = $param['id'];
        if($bid <= 0)
        {
            $this->error = '参数错误';
            return false;
        }
        $re = model('BusinessVip')->getInfo('*',['bid'=>$bid]);
        if($re)
        {
            return $re;
        }
        $bname = model('Business')->getBusinessField(['bid'=>$bid],'bname');
        if(!$bname)
        {
            $this->error = '无此商家信息';
            return false;
        }
        return ['bname'=>$bname];
    }
    /**
     * [saveBusinessVip 保存数据]
     * @param  array  $param [description]
     * @return [type]        [description]
     */
    public function saveBusinessVip($param = [])
    {
        $validate = validate('wd_business_vip');
        if (!$validate->check($param)) {
            $this->error = $validate->getError();
            return false;
        }
        $businessvip =  model('BusinessVip');        
        $bInfo = $businessvip->getInfo('bid',['bid'=>$param['bid']]);
        $type_arr = array(10=>'体验套餐',20=>'标准套餐',30=>'升级版(原高级套餐)',101=>'流量套餐一',102=>'流量套餐二',103=>'流量套餐三');
        $param['name'] = $type_arr[$param['type']];
        $param['start_time'] = strtotime($param['start_time']);
        $param['end_time'] = strtotime($param['end_time']);
        $param['start_time_str'] = date('Y-m-d H:i:s',$param['start_time']);
        $param['end_time_str'] = date('Y-m-d H:i:s',$param['end_time']);
        $param['uniacid'] = 3;
        if($bInfo){
            $where['bid'] = $param['bid'];
            unset($param['bid']);
            $param['update_time'] = time();
            $param['update_time_str'] = date('Y-m-d H:i:s');
            $result = $businessvip->saveInfo($param,$where);
        }else{
            $bname = model('Business')->getBusinessField(['bid'=>$param['bid']],'bname');
            $param['bid'] = $param['bid'];
            $param['bname'] = $bname;
            $param['create_time'] = time();
            $param['create_time_str'] = date('Y-m-d H:i:s');
            $result = $businessvip->addInfo($param);
        }
        if(!$result)
        {   
            $this->error = '操作失败';
            return false;
        }
        return true;

    }
}

 ?>