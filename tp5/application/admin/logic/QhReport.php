<?php 
namespace app\admin\logic;
use think\Model;
use think\Db;
use app\admin\model\Appview;
use app\admin\model\Appuser;
class QhReport extends Model
{
	private $yxb_url = 'http://i.100jieqian.com/app/query';

    public function getReport($id)
    {

        if(!$id)
        {
            $this->error = '参数错误';
            return false;
        }
        $appview = new Appview;
        $uid = $appview->getValue(['id'=>$id],'uid');
        if(!$uid)
        {
            $this->error = '用户不存在';
            return false;
        }
        // $info = Db::name('report_xinyong')->where(['uid'=>$uid])->find();
        
        // $time_s = time();
        // $time_e = strtotime($info['insert_date']);
        // if($info&&$info['conten']!='null'&&$time_s<($time_e+2*24*60*60)){
        //  $data =  json_decode($info['conten'],true);
        //  return $data;
        // }else{
           
            $crawlerConfig = config('crawler');
            $appuser = new Appuser;
            $re = $appuser->getDatas('name,phone,id_card',['uid'=>$uid]);
            $params['uid'] = $uid;
            $params['phone'] = $re['phone'];
            $params['id']    = $re['id_card'];
            $params['name']  = $re['name'];
            $params['vali_token']  = md5(date('Y-m-d',time()) . md5($crawlerConfig['key']));
            $params['channel']  = $crawlerConfig['channel'];
            $data = json_decode(curlPostForm($this->yxb_url,$params),true)['data'];
            // $data_json = json_encode($data);
            // $data_s = ['conten'=>$data_json,'insert_date'=>date('Y-m-d H:i:s',time())];
            // if($info['uid']){
            //   Db::name('report_xinyong')->where(['uid'=>$uid])->update($data_s);
            // }else{
            //     $data_insert = ['conten'=>$data_json,'insert_date'=>date('Y-m-d H:i:s',time()),'uid'=>$uid];
            //     Db::name('report_xinyong')->insert($data_insert);
            // }
            // $data = json_decode($data_json);
            return $data;
        // }
        
    }
	
}

 ?>