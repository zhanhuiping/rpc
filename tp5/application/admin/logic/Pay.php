<?php 
namespace app\admin\logic;
use think\Model;
use think\Db;
use app\admin\model\PaymentOrder;
use app\admin\model\UserBankCard;
use app\admin\model\Appuser;
use app\admin\model\Appview;
class Pay extends Model
{
	/**
    * 获取支付需要的参数
    * @param string $pay_no 订单号
    * @param bool $isPost
    */
	public function getPaymentParam($param) 
	{
		$paymentOrderModel = new PaymentOrder;
        //获取订单信息
        $order_info = $paymentOrderModel->getDatas(["no_order"=>$param['no_order'],"status"=>0])->toArray();
        $order_info['paymentid'] = $order_info['apply_id'];
        if(!$order_info) {
            $this->error = "没有获取到订单信息";
            return false;
        }
        if($order_info->confirm_code){
            $this->error = "该订单号为疑似重复订单,不能重复提交";
            return false;
        }
        /*$userBankCard = new UserBankCard;
        //获取协议号  
        $userBankInfo = $userBankCard->getDatas('id paymentid,no_agree,bind_mob,id_card',['uid'=>$order_info['uid'],'status'=>1,'bank_card_number'=>$order_info['card_no']]);*/
        $appuser = new Appuser;
        $userBankInfo = $appuser->getDatas('no_agree,bind_mob,id_card',['uid'=>$order_info['uid']])->toArray();
        //获取计划
       // var_dump($order_info['uid']);die;
        $plan = $this->repaymentPlan($order_info['apply_id']);
        if(!$plan)
        {
            $this->error = $this->getError();
            return false;
        }
        $order_info['plan'] = $plan;
        /*$appView = new Appview;
        $userLitterInfo = $appView->getIdCard($order_info['uid']);*/

        return array_merge($order_info,$userBankInfo);
	}
    /**
     * [repaymentPlan 获取还款计划]
     * @param  integer $appyId [description]
     * @return [type]          [description]
     */
    public function repaymentPlan($applyId = 0,$today = 0)
    {
        if(!$applyId)
        {
            $this->error = '参数错误';
            return false;
        }

        $appView = new Appview;
        //获取申请信息
        $applyInfo = $appView->getDatas('period,money,rate,service_rate',['order_number'=>$applyId]);
        $money = $applyInfo['money'];//本金
        $costDay = $applyInfo['period'];//已借天数
        $dailyInterestRate = $applyInfo['rate'];//日率费
        $manageRate        = $applyInfo['service_rate'];     //管理费率
        // 还款总额 = 本金+本金*已借天数*(日利率+管理费率) + 本金*(提前天数*提前还款手续费率+逾期费率*逾期天数)
        // 也等于  利息+管理费+逾期费+提前还款手续费+延期手续费
        $summoney = round($money + $money*$costDay*($dailyInterestRate+$manageRate),2);
        if($today)
        {
            $date = date('Y-m-d',time());
        }else{
            $date = date('Y-m-d',time()+3600*24*$costDay);
        }
        return json_encode(['repaymentPlan'=>['date'=>$date,'amount'=>$summoney]]);
    }

    public function getAgreenoData($applyId = '',$today = 0)
    {
        if(!$applyId)
        {
            $this->error = '参数错误';
            return false;
        }
        $apply = new Appview;
        //获取订单信息
        $applyInfo = $apply->getUserApplyInfo('a.contract_number,b.uid,b.no_agree',["order_number"=>$applyId])->toArray();
        $applyInfo['repayment_plan'] = $this->repaymentPlan($applyId,$today);
        return  $applyInfo;
    }
    
}

 ?>