<?php
// +----------------------------------------------------------------------
// | Description: 系统用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;
use think\Db;
use think\Controller;
use think\controller\Yar;
class LoginClient
{

  public $registrationCenterLogin;

  public function __construct()
  {
     $this->registrationCenterLogin =  \Yaconf::get('tp5.registrationcenter.login');
     $this->client = new \Yar_client($this->registrationCenterLogin);
     $this->client->SetOpt(YAR_OPT_PACKAGER,"php"); // 设置传输方式
  }

  public function login()
  {
      echo $this->client->login();
  }

}

