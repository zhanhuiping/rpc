<?php
// +----------------------------------------------------------------------
// | Description: 系统用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;
use think\Db;
use think\Controller;
use think\controller\Yar;
class PayClient
{

  public $registrationCenterPay;
  public $client;

  public function __construct()
  {
     $this->registrationCenterPay =  \Yaconf::get('tp5.registrationcenter.pay');
     $client = new \Yar_client($this->registrationCenterPay);
     $client->SetOpt(YAR_OPT_PACKAGER,"php"); // 设置传输方式
     $this->client = $client;

  }

  public function pay()
  {
      echo $this->client->pay();
  }

}

