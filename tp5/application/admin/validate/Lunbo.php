<?php

namespace app\admin\validate;
use think\Validate;
/**
* 设置模型
*/
class Lunbo extends Validate{

	protected $rule = array(
		'title'  		=> 'require|chsAlphaNum|length:2,10',
		'sort'  		=> 'number',
		'links'  		=> 'require|url',
	);
	protected $message = array(
		'title.require'    		=> '图名必须填写',
		'title.chsAlphaNum'    	=> '图名只能是汉字、字母和数字',
		'title.length'    		=> '图名长度在2到10位',
		'sort.number'    		=> '排序必须为数字',
		'links.url'    			=> '跳转链接格式无效',
	);
}