<?php
namespace app\admin\validate;
use think\Validate;
/**
 * 设置模型
 */
class BinviteCode extends Validate{

    protected $rule = array(
        'bname', 'require',
        'username'  		=> 'require',
        ['phone', 'require|checkPhone'],
        'status'      	=> 'require|in:0,1',
    );
    protected $message = array(
        'username.require'    	=> '联系人必须填写',
        'bname.require'    	=> '公司名称必填',
        'status.require'    	=> '状态必须填写',
        'status.in'    	=> '状态必须是0或1',
    );

    protected function checkPhone($mobile,$rule) {
        if (strlen ( $mobile ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $mobile )) {
            return "您的手机格式不正确！".$rule;
        } else {
            return true;
        }
    }
}