<?php

namespace app\admin\validate;
use think\Validate;
/**
* 设置模型
*/
class Business extends Validate{

	protected $rule = array(
		'bname'  		=> 'require|chsAlphaNum',
		'bsort'  		=> 'number',
		'icon'  		=> 'require',
		'lend_rate'  	=> 'require|number|between:1,100',
		'interest'  	=> 'require',
		'speed'  		=> 'require|number',
		'speed_type'  	=> 'require|number|in:1,2',
		'links'  		=> 'require',
		'minloan'  		=> 'require|number|gt:0',
		'maxloan'  		=> 'require|number|gt:0',
		'mintime'  		=> 'require|number|gt:0',
		'maxtime'  		=> 'require|number|gt:0',
		'time_type'  	=> 'require|number|in:1,2',
		'needdata'  	=> 'require',
		'condition'  	=> 'require',
		'remark'  		=> 'require',
		'tips'  		=> 'require',
		'tip_detial'  		=> 'require',
		'tip_index'  		=> 'require',
		'loan'  		=> 'require',
		'opinion'  		=> 'require|number',
		);
	protected $message = array(
		'tip_detial.require'    		=> '详情页提示语必须填写',
		'tip_index.require'    		=> '首页列表提示语必须填写',
		'loan.require'    		=> '借款利息描述必须填写',
		'opinion.require'    		=> '好评数必须填写',
		'opinion.number'    		=> '好评数必须为数字',
		'bname.require'    		=> '商家名必须填写',
		'bname.chsAlphaNum'    	=> '商家名只能是汉字、字母和数字',

		'bsort.number'    		=> '排序必须为数字',
		'icon.require'    		=> '商家图标必须填写',
		'icon.url'    			=> '商家图标连接格式无效',
		'lend_rate.require'    		=> '放款通过率必须填写',
		'lend_rate.number'    		=> '放款通过率必须为数字',
		'lend_rate.between'    		=> '放款通过率必须在1 到 100之间',
		'interest.require'    		=> '借款日利率必须填写',

		'speed.require'    			=> '放款速度必须填写',
		'speed.number'    			=> '放款速度必须填写数字',
		'speed_type.require'    	=> '放款速度类型必须填写',
		'speed_type.number'    		=> '放款速度类型必须填数字',
		'speed_type.in'    			=> '放款速度类型选择错误',
		'links.require'    			=> '商家链接必须填写',
		//'links.url'    				=> '商家链接格式错误',

		'minloan.require'           => '最低借款额度必须填写',
		'minloan.number'            => '最低借款额度必须为数字',
		'minloan.gt'            	=> '最低借款额度必须大于0',

		'maxloan.require'           => '最高借款额度必须填写',
		'maxloan.number'            => '最高借款额度必须为数字',
		'maxloan.gt'            	=> '最高借款额度必须大于0',

		'mintime.require'           => '最低借款时间必须填写',
		'mintime.number'            => '最低借款时间必须为数字',
		'mintime.gt'            	=> '最低借款时间必须大于0',

		'maxtime.require'           => '最高借款时间必须填写',
		'maxtime.number'            => '最高借款时间必须为数字',
		'maxtime.gt'            	=> '最高借款时间必须大于0',	

		'time_type.require'    		=> '借款周期时间类型必须填写',
		'time_type.number'    		=> '借款周期时间类型必须填数字',
		'time_type.in'    			=> '借款周期时间类型选择错误',

		'needdata.require'          => '所需材料必须填写',

		'condition.require'         => '申请条件必须填写',

		'remark.require'            => '注意事项必须填写',

		'tips.require'            	=> '商家标签必须填写',

	);
}