<?php

namespace app\admin\validate;
use think\Validate;
/**
* 设置模型
*/
class SystemConfig extends Validate{

	protected $rule = array(
		'APP_NOTICE_01'  		     => 'require',
		'CAN_WITHDRAW'  		     => 'require|in:1,0',
		'REG_IP_DAILY_UP'  		     => 'require|number|gt:0',
		'REG_SMS_CODE_DAILY_UP'      => 'require|number|gt:0',
		'FIND_PWD_SMS_CODE_DAILY_UP' => 'require|number|gt:0',
		'WITHDRAW_LOW'               => 'require|number|gt:0',
		'WITHDRAW_DAILY_TIMES'       => 'require|number|gt:0',
		'APP_SYS_NOTICE_STATUS'       => 'require|in:2,1,0',
		'APP_SYS_NOTICE_CONTENT'             => 'require',
		'APP_NOTICE_02'              => 'require',
		'APP_CONFIG_VERSION'         => 'require',
	    'APP_SYS_NOTIC_LINKS'  		     => 'require|url',
		'CREDIT_UNIT_PRICE'          => 'require|number|gt:0',
		'REWORD_REGISTER_LEVEL_1'    => 'require|number|gt:0',
		'REWORD_REGISTER_LEVEL_2'    => 'require|number|gt:0',
		'APP_SHARE_MINI_LOGO'    => 'require',
		'APP_SHARE_TITLE_IOS'    => 'require',
		'APP_SHARE_CONTENT_IOS'    => 'require',
		'APP_SHARE_THUMBS_IOS'    => 'require',
		'APP_SHARE_PIC_IOS'    => 'require',
		'APP_INDEX_BUSINESS_DETAIL'    => 'require|in:1,0',
		'APP_INDEX_BUSINESS_JUMP'    => 'require|in:1,0',
		'APP_KF_CONNECT'    => 'require',
		'APP_APPLY_SUCCESS'    => 'require',
		'APP_LUNBO_JUMP'    => 'require|in:1,0',
		'APP_COMPANY_COPYRIGHT'    => 'require',
		'APP_VERSION'    => 'require',
		'APP_COIN_NAME'    => 'require',
		'APP_INDEX_BUSINESS_COL__1'    => 'require',
		'APP_INDEX_BUSINESS_COL__2'    => 'require',
		'APP_INDEX_BUSINESS_COL__3'    => 'require',
		'APP_DETAIL_BUSINESS_COL__1'    => 'require',
		'APP_DETAIL_BUSINESS_COL__2'    => 'require',
		'APP_DETAIL_BUSINESS_COL__3'    => 'require',
		'APP_NAME'    => 'require',
		'APP_NV3_USER_INFO'    => 'require',
		'APP_NV3_TX_HISTORY'    => 'require',
		'APP_NV3_HAIBAO'    => 'require',
		'APP_NV3_BROWSE'    => 'require',
		'APP_NV3_TEAM'    => 'require',
		'APP_NV3__ABOUT'    => 'require',
		'APP_NV2_TD_QUERY'    => 'require',
		'APP_NV2_TD_HISTORY'    => 'require',
		'APP_NV2_INVITE'    => 'require',
		'APP_NV2_TEAM'    => 'require',
		'APP_NV2_BT_NAME'    => 'require',
		'APP_NV2_TOTAL_INCOME'    => 'require',
		// 'APP_HAIBAO_GUIZE'    => 'require',
		'APP_USER_INFO_IS'    => 'require',
		'APP_USER_INFO_NOT'    => 'require',
		'APP_SYS_NOTICE_TITLE'    => 'require',
		'APP_SYS_NOTICE_VERSION'    => 'require',
	    
	);
	protected $message = array(
		'APP_NOTICE_01.require'    		      => 'app跑马灯提示语必须填写',
		'CAN_WITHDRAW.require'    		      => '开启提现必须填写',
	    
	    'REG_IP_DAILY_UP.number'    		  => '注册ip每日上限必须为数字',
	    'REG_IP_DAILY_UP.require'    		  => '注册ip每日上限必须填写',
	    'REG_IP_DAILY_UP.gt'    		      => '注册ip每日上限必须大于0',
	    
	    'REG_SMS_CODE_DAILY_UP.number'    	  => '注册短信每日发送上限必须为数字',
	    'REG_SMS_CODE_DAILY_UP.require'    	  => '注册短信每日发送上限必须填写',
	    'REG_SMS_CODE_DAILY_UP.gt'    		  => '注册短信每日发送上限必须大于0',
	     
	    'FIND_PWD_SMS_CODE_DAILY_UP.number'   => '找回密码短信每日发送上限必须为数字',
	    'FIND_PWD_SMS_CODE_DAILY_UP.require'  => '找回密码短信每日发送上限必须填写',
	    'FIND_PWD_SMS_CODE_DAILY_UP.gt'    	  => '找回密码短信每日发送上限必须大于0',
	    
	    'WITHDRAW_LOW.number'    		      => '提现最低值必须为数字',
	    'WITHDRAW_LOW.require'    		      => '提现最低值必须填写',
	    'WITHDRAW_LOW.gt'    		          => '提现最低值必须大于0',
	    
	    'WITHDRAW_DAILY_TIMES.number'    	  => '每日提现次数上限必须为数字',
	    'WITHDRAW_DAILY_TIMES.require'    	  => '每日提现次数上限必须填写',
	    'WITHDRAW_DAILY_TIMES.gt'    		  => '每日提现次数上限必须大于0',
	    
	    'APP_SYS_NOTICE_STATUS.require'    		      => 'app系统状态必须填写',
	    
		'APP_SYS_NOTICE_CONTENT.require'    		  => 'app系统状态 提示语必须填写',
	    
		'APP_NOTICE_02.require'    		      => 'app推广奖励规则提示语必须填写',
	    
		'APP_CONFIG_VERSION.require'    	  => 'app配置版本号必须填写',
	    
	    'APP_SYS_NOTIC_LINKS.require'    			  => '商家链接必须填写',
	    'APP_SYS_NOTIC_LINKS.url'    				  => '商家链接格式错误',
	    
	    'CREDIT_UNIT_PRICE.number'    		  => '征信查询 单价 积分必须为数字',
	    'CREDIT_UNIT_PRICE.require'    		  => '征信查询 单价 积分必须填写',
	    'CREDIT_UNIT_PRICE.gt'    		      => '征信查询 单价 积分必须大于0',
	    
	    'REWORD_REGISTER_LEVEL_1.number'      => '一级邀请注册奖励必须为数字',
	    'REWORD_REGISTER_LEVEL_1.require'     => '一级邀请注册奖励必须填写',
	    'REWORD_REGISTER_LEVEL_1.gt'    	  => '一级邀请注册奖励必须大于0',
	    
	    'REWORD_REGISTER_LEVEL_2.number'      => '二级邀请注册奖励必须为数字',
	    'REWORD_REGISTER_LEVEL_2.require'     => '二级邀请注册奖励必须填写',
	    'REWORD_REGISTER_LEVEL_2.gt'    	  => '二级邀请注册奖励必须大于0',
	    
	    'APP_SHARE_MINI_LOGO.require'    	  => 'App分享logo必须填写',
	    
	    'APP_SHARE_TITLE_IOS.require'    	  => 'IOS 分享标题必须填写',
	    
	    'APP_SHARE_CONTENT_IOS.require'    	  => 'IOS 分享内容必须填写',
	    
	    'APP_SHARE_THUMBS_IOS.require'    	  => 'IOS 分享缩略图必须填写',
	    
	    'APP_SHARE_PIC_IOS.require'    	  => 'IOS 分享图标必须填写',
	    
	    'APP_KF_CONNECT.require'    	  => 'app联系我们，客服按钮必须填写',
	    
	    'APP_APPLY_SUCCESS.require'    	  => 'app点击商家详情，立即申请申请成功提示语必须填写',
	    
	    'APP_COMPANY_COPYRIGHT.require'    	  => 'app企业版权名称必须填写',
	    
	    'APP_VERSION.require'    	  => 'app版本号必须填写',
	    
	    'APP_COIN_NAME.require'    	  => 'app内 金额的名称必须填写',
	    
	    'APP_INDEX_BUSINESS_COL__1.require'    	  => 'app 首页 商家列表 面板字段1 默认“最高额度” 必须填写',
	    
	    'APP_INDEX_BUSINESS_COL__2.require'    	  => 'app 首页 商家列表 面板字段2  默认“利息” 必须填写',
	    
	    'APP_INDEX_BUSINESS_COL__3.require'    	  => 'app 首页 商家列表 面板字段3 默认“放款速度” 必须填写',
	    
	    'APP_DETAIL_BUSINESS_COL__1.require'    	  => 'app 商家详情 栏目字段1 默认“申请条件” 必须填写',
	    
	    'APP_DETAIL_BUSINESS_COL__2.require'    	  => 'app 商家详情 栏目字段2 默认“所需材料” 必须填写',
	    
	    'APP_DETAIL_BUSINESS_COL__3.require'    	  => 'app 商家详情 栏目字段3 默认“注意事项” 必须填写',
	    
	    'APP_NAME.require'    	  => 'app 的名字必须填写',
	    
	    'APP_NV3_USER_INFO.require'    	  => 'app 导航按钮 3 默认“我的资料”必须填写',
	    
	    'APP_NV3_TX_HISTORY.require'    	  => 'app 导航按钮  3 默认“提现记录”必须填写',
	    
	    'APP_NV3_HAIBAO.require'    	  => 'app 导航按钮 3 默认“推广海报”必须填写',
	    
	    'APP_NV3_BROWSE.require'    	  => 'app 导航按钮 3 默认“申请记录”必须填写',
	    
	    'APP_NV3_TEAM.require'    	  => 'app 导航按钮 3 默认“我的团队”必须填写',
	    
	    'APP_NV3__ABOUT.require'    	  => 'app 导航按钮 3 默认“关于我们”必须填写',
	    
	    'APP_NV2_TD_QUERY.require'    	  => 'app 导航按钮 2 大数据征信查询必须填写',
	    
	    'APP_NV2_TD_HISTORY.require'    	  => 'app 导航按钮 2 查询历史必须填写',
	    
	    'APP_NV2_INVITE.require'    	  => 'app 导航按钮 2 推广海报必须填写',
	    
	    'APP_NV2_TEAM.require'    	  => 'app 导航按钮 2 我的团队必须填写',
	    
	    'APP_NV2_BT_NAME.require'    	  => 'app 导航按钮 2 提现按钮 默认“立即提现”必须填写',
	    
	    'APP_NV2_TOTAL_INCOME.require'    	  => 'app 导航按钮 2 收入显示字段 默认“总计收益”必须填写',
	    
	    'APP_HAIBAO_GUIZE.require'    	  => 'app 海报页 赚钱规则按钮 弹出的提示语必须填写',
	    
	    'APP_USER_INFO_IS.require'    	  => 'app 个人中心 信息已完善显示语必须填写',
	    
	    'APP_USER_INFO_NOT.require'    	  => 'app 个人中心 信息未完善显示语必须填写',
	    
	    'APP_SYS_NOTICE_TITLE.require'    	  => 'app 系统提示的标题必须填写',
	    
	    'APP_SYS_NOTICE_VERSION.require'    	  => 'app 系统通知版本必须填写',
	    
	    
	    
	    
	    
	    
	    
	);
}