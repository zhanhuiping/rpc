<?php
namespace app\admin\validate;
use think\Validate;

class BlackList extends Validate
{  
    // 验证规则
	protected $rule = [
		['phone', 'require|checkPhone', '手机号码必填|请检查手机号码'],
        ['id_card', 'checkidnumber', '请检查身份证号码'] // 考虑到在用户列表中直接添加黑名单，可能会没有身份证号码，故身份证号码可为空
	];

	/**
	 * 验证是否是手机号码
	 *
	 * @param string $phone 待验证的号码
	 * @return boolean 如果验证失败返回false,验证成功返回true
	 */
	protected function checkPhone($phone,$rule) {
		if (strlen ( $phone ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $phone )) {
			return "您的手机格式不正确！".$rule;
		} else {
			return true;
		}
	}	 

	/**
	 * 验证身份证号
	 * @param  string  $id_number 身份证号码
	 * @param  array   $rule      验证规则
	 * @return boolean            
	 */
	protected function checkidnumber($id_number,$rule) {
		if($id_number == '') return true; //为空时跳过验证
		if (strlen ( $id_number ) != 18 || ! preg_match ( '/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/', $id_number )) {
			return "您的身份证格式不正确！".$rule;
		} else {
			return true;
		}
	}   
}