<?php
namespace app\admin\validate;
use think\Validate;
/**
 * 设置模型
 */
class UserInfo extends Validate{

    protected $rule = array(
        ['name', 'require|chs'],
        'username'  		=> 'require|length:6,30|unique:user',
        ['phone', 'require|checkPhone'],
        'age'  		=> 'require|number',
        'qq'  		=> 'require|number',
        'sex'  	=> 'require|in:1,0',
    );
    protected $message = array(
        'username.require'    	=> '用户名必须填写',
        'username.length'    	=> '用户名长度在2到10位',
        'username.unique'    	=> '用户名已存在',
        'name.require'    	=> '姓名必填',
        'name.chs'    	=> '姓名必须中文',
        'status.require'    	=> '状态必填',
        'age.number'    		=> '年龄必须为数字',
        'qq.number'    		=> 'qq必须为数字',
        'sex.require'    	=> '性别必填',
        'zhima.require'    	=> '芝麻分必填',
        'province.require'    	=> '省份必填',
    );

    protected function checkPhone($mobile,$rule) {
        if (strlen ( $mobile ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $mobile )) {
            return "您的手机格式不正确！".$rule;
        } else {
            return true;
        }
    }
}