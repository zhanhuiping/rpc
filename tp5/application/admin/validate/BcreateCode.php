<?php
namespace app\admin\validate;
use think\Validate;
/**
 * 设置模型
 */
class BcreateCode extends Validate{

    // 验证规则
    protected $rule = [
        ['phone', 'require|checkPhone'],
        ['connect_bname', 'require|chs|length:2,10'],
        ['bname', 'require|chs|length:2,10'],
    ];
	protected $message = array(
		'phone.require'    	=> '手机必须填写',
	    'connect_bname.require'    	=> '联系人不能为空',
	    'connect_bname.chs'    	=> '联系人要为中文',
	    'bname.require'    	=> '商家名不能为空',
	    'bname.chs'    	=> '商家名要为中文',
	    'connect_bname.length'    	=> '联系人字段长度为2-10',
	    'bname.length'    	=> '商家名字段长度为2-10',
	);

    protected function checkPhone($mobile,$rule) {
        if (strlen ( $mobile ) != 11 || ! preg_match ( '/^1[3|4|5|6|7|8|9][0-9]{9}$/', $mobile )) {
            return "您的手机格式不正确！".$rule;
        } else {
            return true;
        }
    }
}