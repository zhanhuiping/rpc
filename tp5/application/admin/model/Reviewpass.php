<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Reviewpass extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'apply';
    protected $createTime = 'time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = DATETIME;
    // protected $insert = [
    //     'status' => 1,
    // ];


 
   /**
    * @功能       複審通過列表
    * @Author   zhanhuiping
    * @email    18702529695@163.com
    * @DateTime 2017-09-07
    * @param    [type]              $keywords  [description]
    * @param    [type]              $page      [description]
    * @param    [type]              $limit     [description]
    * @param    [type]              $valuedate [description]
    * @return   [type]                         [description]
    */
    public function getDataList($keywords, $page, $limit, $valuedate, $os = '',$status="")
    {
        $map = [];
        if($valuedate)
        {
          $valuedate = substr($valuedate,0,10);
          $valuedate = date('Y-m-d',strtotime("$valuedate +1 day"));
          $map['time'] = ['like', '%'.$valuedate.'%'];
        }
        if ($keywords) {
            $map['phone|order_number|name'] = ['like', '%'.$keywords.'%'];
        }
        if($os)
        {
            $map['os'] = $os;
        }
        if(""!=$status)
        {
            // $map['status'] = $status;
        }

        $dataCount = $this->where($map)->where('status=2')->where('merchandiser is null')->count('id');
        $list = $this
            ->where('merchandiser is null')
            ->where($map);
        // 若有分页
        if ($page && $limit) {
            $list = $this->page($page, $limit);
        }
        
       $maps['status'] = 2;
       
       $list = $this
            // ->field('id,status,time,money,period,order_number,check_time,check_aid,check2_aid,check2_time,name,credit_rate,service_rate,procedure_rate,appoint_time,uid,treasurer,merchandiser,mobile_type,os')
            ->order('is_re_apply desc,auth_type desc, status asc,auth_type desc, merchandiser asc,auth_type desc, id desc,auth_type desc')
            ->where($maps)
            ->select();
        foreach($list as $key=>$value)
        {
            $map2['id'] = $value['check_aid'];
            $map1['id'] = $value['check2_aid'];
            $map3['id'] = $value['merchandiser'];
            
            $list[$key]['user_admin1'] = Db::name('admin_user')->field('username')->where($map2)->find();
            $list[$key]['user_admin2'] = Db::name('admin_user')->field('username')->where($map1)->find();
            $list[$key]['user_admin3'] = Db::name('admin_user')->field('username')->where($map3)->find();

            $count = $this->where('uid','=',$value['uid'])->where('id','<=',$value['id'])->count();
              if($count >=2)
              {
                $oddcount = $count-2;
                $oddinfo = $this->where('uid','=',$value['uid'])->select();
                $merchandiser = $oddinfo[$oddcount]['merchandiser'];
                $list[$key]['merchandiserss'] =Db::name('admin_user')->where('id','=',$merchandiser)->value('username');
              }
            $list[$key]['admin_uid'] = $admin_uid;
        }

        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }

    /**
     * [getDataById 根据主键获取详情]
     * @linchuangbin
     * @DateTime  2017-02-10T21:16:34+0800
     * @param     string                   $id [主键]
     * @return    [array]
     */
    public function getDataById($id = '')
    {
        $data=$this
            ->field('id,money,period,credit_rate,service_rate,procedure_rate,overdue_rate')
            ->where('id',$id)
            ->find();
        return $data;
    }
    /**
     * [createData 新建]
     * @linchuangbin
     * @DateTime  2017-02-10T21:19:06+0800
     * @param     array                    $param [description]
     * @return    [array]                         [description]
     */
    public function createData($param)
    {

        // // 验证
        // $validate = validate($this->name);
        // if (!$validate->check($param)) {
        //     $this->error = $validate->getError();
        //     return false;
        // }
        try {
            $this->data($param)->allowField(true)->save();
            return true;
        } catch(\Exception $e) {
            $this->error = '添加失败';
            return false;
        }
    }    

    /**
     * [updateDataById 编辑]
     * @linchuangbin
     * @DateTime  2017-02-10T21:24:49+0800
     * @param     [type]                   $param [description]
     * @param     [type]                   $id    [description]
     * @return    [type]                          [description]
     */
    public function updateDataById($param, $id)
    {
        $checkData = $this->get($id);
        if (!$checkData) {
            $this->error = '暂无此数据';
            return false;
        }

        // // 验证
        // $validate = validate($this->name);
        // if (!$validate->check($param)) {
        //     $this->error = $validate->getError();
        //     return false;
        // }

        try {
            $this->allowField(true)->save($param, [$this->getPk() => $id]);
            return true;
        } catch(\Exception $e) {
            $this->error = '编辑失败';
            return false;
        }
    }


   /**
    * @功能       m1認領
    * @Author   zhanhuiping
    * @email    18702529695@163.com
    * @DateTime 2017-09-07
    * @param    [type]              $param [description]
    * @return   [type]                     [description]
    */
    public function getclaim($param)
    {
        $id = $param['id'];
        $uid = $param['uid'];
        $map['id'] = $id;
        $info = $this->where($map)->field('merchandiser')->find();
        if($info['merchandiser'])
        {
           return $data = ['code'=>'error','message'=>'已经被认领'];
        }

       $this->startTrans();
       try{
            $admin_uid = $GLOBALS['userInfo']['id'];
            $nowdate = date('Y-m-d',time());
            $mapseq['nowdate'] = ['like','%'.$nowdate.'%'];
            $infocount = Db::name('claim_seq')
                         ->where('auth_admin',2)
                         ->where('admin_id',$admin_uid)
                         ->where($mapseq)
                         ->count();
            $claim_date = ['uid'=>$uid,'apply_id'=>$id,'nowdate'=>$nowdate,'auth_admin'=>2,'seq'=>$infocount+1,'admin_id'=>$admin_uid];
            Db::name('claim_seq')->insert($claim_date);
            
            $third_claim_date = date('Y-m-d H:i:s',time());
            $data = array('merchandiser'=>$admin_uid,'third_claim_date'=>$third_claim_date);
            $info = $this->save($data, ['id' => $id]);  //添加至apply表中
            // 提交事务
            $this->commit(); 
            return $data = ['code'=>'success','message'=>'认领成功'];
            // 提交事务
        $this->commit();
       }catch(\Exception $e)
       {
        $this->rollback();
        return false;
       }
    }

}