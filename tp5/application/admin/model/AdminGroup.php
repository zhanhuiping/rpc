<?php
/**
 * Created by Chen.
 * author: 1481746137@qq.com
 * Date: 2018/5/30
 */

namespace app\admin\model;

use app\admin\model\Common;


class AdminGroup extends Common
{
    protected $name = 'admin_group';

    public function children(){
        return $this->hasMany('AdminGroupUser','group_id','id')->field('username as label,user_id as value,group_id,id');
    }

}



