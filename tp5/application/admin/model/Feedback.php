<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Feedback extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'feedback';
    public function gettypeAttr($value)
    {
        if($value == 0) return '提建议';
        if($value == 1) return '想咨询';
        if($value == 2) return '要投诉';
    }
    public function getimgArrayAttr($value)
    {
        return json_decode($value,true);
    }
    public function getDataList($where = [], $page = 1, $limit = 15 , $order = 'a.create_time DESC' )
    {
     
        $dataCount = $this->alias('a')->where($where)->join('qh_user_info b','a.uid = b.uid','LEFT')->count('a.id');

        $list = $this
            ->alias('a')
            ->where($where);

        // 若有分页
        if ($page && $limit) {
            $list = $list->page($page, $limit);
        }

        $list = $list
            ->field('a.create_time c_time,a.type,a.title,a.contents,a.img_array,a.contact,b.name')
            ->join('qh_user_info b','a.uid = b.uid','LEFT')
            ->order($order)
            ->select();
        $list = modelo2array($list);
        $data['list'] = $list;
        $data['dataCount'] = $dataCount;

        return $data;
    }
    

}