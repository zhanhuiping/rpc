<?php
/**
 * Created by Chen.
 * author: 1481746137@qq.com
 * Date: 2018/3/8
 * Time: 18:30
 */

namespace app\admin\model;


class UserOrder extends Common
{
    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'user_order';
}




