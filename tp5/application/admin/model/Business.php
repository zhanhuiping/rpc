<?php
// +----------------------------------------------------------------------
// | Description: 商家操作类
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;
use app\common\count\Count;
class Business extends Common 
{	
    
	protected $name = 'business';
    protected $createTime = 'create_time';
    protected $updateTime = "update_time";
	protected $autoWriteTimestamp = 'datetime';
	protected $insert = [
		'status' => 1,
	];
    public function getlendRateAttr($value)
    {
    	$value = round($value,0);
        return $value;
    }
    /*public function getinterestAttr($value)
    {	
    	$value = round($value,0);   //去掉小数点后面的数  2就是保留2位小数
        return $value;
    }*/
    public function gettimeTypeAttr($value)
    {
    	$data = ['1'=>"天",'2'=>"月"];
        return $data[$value];
    }
	public function getspeedTypeAttr($value)
	{
		$data = ['1'=>"小时",'2'=>"天"];
		return $data[$value];
	}

    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
	public function getDataList($keywords, $page, $limit)
	{
		$map = [];
		if ($keywords) {
			$map['bname'] = ['like', '%'.$keywords.'%'];
		}
		$dataCount = $this->where($map)->count('id');
		
		$list = $this
				->where($map);
		
		// 若有分页
		if ($page && $limit) {
			$list = $list->page($page, $limit);
		}

		$list = $list 
				->field('*')
				->select();
		$list = modelo2array($list); 
		
		$stime = strtotime(date('Y-m-d',time())) ;
		$etime = strtotime(date('Y-m-d',time())) + 86400;
		foreach($list as $k => $v)
		{
		    // $list[$k]['clicktotal'] = Db::table('qh_user_browse')->where('bid',$v['id'])->count('id');
		    $list[$k]['clicktotal'] =100;
		    // $sql = "SELECT count('id') as count from qh_user_browse where UNIX_TIMESTAMP(create_time) between ". $stime. " and ". $etime;
		    // $addclick = Db::query($sql);
		    // $list[$k]['clicktoday'] = $addclick[0]['count'];
		    $list[$k]['clicktoday'] =10;
		}
		
		$data['list'] = $list;
		$data['dataCount'] = $dataCount;
		
		return $data;
	}

	/**
	 * 功能:  商家详情
	 * @Author   zhanhuiping
	 * @email    18702529695@163.com
	 * @DateTime 2017-09-08
	 * @param    [type]              $param [description]
	 * @return   [type]                     [description]
	 */
	public function getDataByIds($param)
	{
		$bid = $param['id'];   //这里传递过来的是bid而非id
        $map['bid'] = $bid;
        $data = $this->where($map)->find();
        $matchdata = Db::name('business_match')->where($map)->find();
        $data['match'] = $matchdata;
        return $data;
	}
	/**
	 * 创建用户
	 * @param  array   $param  [description]
	 */
	public function createData($param)
	{
		if($param['img']){
			$param['icon']=$param['img'];
		}
		$param['bid'] = Count::myrand2(6,'yifu_bid',6);
		// 验证
		$validate = validate($this->name);
		if (!$validate->check($param)) {
			$this->error = $validate->getError();
			return false;
		}
		$res = $this->data($param)->allowField(true)->save();
		if($res){
			return true;
		}
	}

	/**
	 * 通过id修改用户
	 * @param  array   $param  [description]
	 */
	public function updateDataById($param, $id)
	{
		$map['bid'] = $id;
		$businessdata = $this->allowField(['bname','bsort','icon','wechat_img','status','qq','auto_order_able',
                          'hand_order_able','dig_order_able','dig_auto_able','accounts','is_credit',
                          'lend_rate','interest','links','para1','para2','remark','type','maxloan',
                          'loancycle','levelstar','opinion','needdata','type_two','minloan','right',
                          'mou_order','mode','order_coin_add','order_coin_add_status','is_online','is_rand',
                          'corner_text','jing_jia','online_week','tmp_type','credit_num','credit_history','is_hot',
                          'coin_back','pro_back'
			              ])->save($param,$map);
		$match = $param['match'];
		$data = ['bname'=>$match['bname'],'days_rate'=>$match['zhima_lower'],'zhima_upper'=>$match['zhima_upper'],
		         'is_credit'=>$match['is_credit'],'is_jiebei'=>$match['is_jiebei'],'is_huabei'=>$match['is_huabei'],
		         'apply_re_range'=>$match['apply_re_range'],
		         'apply_able_days'=>$match['apply_able_days'],'apply_re_hand_c'=>$match['apply_re_hand_c'],
		         'limit_area'=>$match['limit_area'],'limit_auto_order'=>$match['limit_auto_order'],
		         'limit_hand_order'=>$match['limit_hand_order'],'limit_dig_order'=>$match['limit_dig_order'],
		         'temp_dig_order_time'=>$match['temp_dig_order_time'],'age_lower'=>$match['update_time'],
		         'age_upper'=>$match['age_upper'],'work'=>$match['work']];
		$info = Db::name('business_match')->where($map)->update($data);
		if(!$info&&!$businessdata)
		{
			$this->error = "保存失败";
			return false;
		}
		return true;
	}
	

	/**
	 * 通过id修改用户删除状态
	 * @param  array   $param  [description]
	 */
	public function delectDataById($id)
	{
	    $re = $this->where($this->getPk(),$id)->setField('status', -1);
	    if($re){
	        return true;
	    }else{
	        $this->error = '删除失败';
	        return false;
	    }
	}

	/*
	 * 通过条件查询一条匹配记录
	 *
	 * @param  array $where 
	 *
	 * @author lxb
	 */
	public function getMatchOne($where = []) 
	{
		return Db::name('business_match')->where($where)->find();
	}

	/*
	 *获取we_config一条配置
	 * @param  array  $where 
	 * @param  string $field
	 *
	 */
	public function getWeconfigField($where = [] ,$field) 
	{
		return $re = Db::name('we_config')->where($where)->value($field);
	}

	/*
	*保存商家匹配数据
	* @param array $where 
	* @param array  $data
	*/
	public function saveMatchData($where= [] , &$data = [])
	{
		return Db::name('business_match')->where($where)->update($data);
	}

	/*
	*添加商家匹配数据
	* @param array  $data
	*/
	public function addMatchData(&$data = [])
	{
		return Db::name('business_match')->insert($data);
	}

	/*
	*查询商家字段数据
	* @param array  $data
	*/
	public function getBusinessField($where = [] ,$field)
	{
		return $this->where($where)->value($field);
	}

}