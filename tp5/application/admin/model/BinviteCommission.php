<?php
// +----------------------------------------------------------------------
// | Description: 商家操作类
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;
use think\Cache;
class BinviteCommission extends Common
{

    protected $name = 'binvite_commission';
    protected $createTime = 'create_time';
    protected $updateTime = "update_time";
    protected $autoWriteTimestamp = 'datetime';
    protected $insert = [
        'status' => 1,
    ];
    public function getstatusAttr($value)
    {
        $data = ['1'=>"正常",'0'=>"禁用"];
        return $data[$value];
    }

    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
//     public function getDataList($keywords, $page, $limit)
//     {
//         $map = [];
//         $where=[];
//         $id=$GLOBALS['userInfo']['id'];
// //      $id = 17;
//         $row = Db::table('qh_admin_access')->where('user_id',$id)->field('group_id')->find();
//         if($row['group_id'] == 20)
//         {
//             $where['b.bid']=$id;
//         }elseif($id == 1 ){
//             $where = 1;
//         }
        
        
//         if ($keywords) {
//             $map['c.username'] = ['like', '%'.$keywords.'%'];
//         }
//         $dataCount = $this->where($map)->count('id');

//         $list = $this
//             ->where($map);

//         // 若有分页
//         if ($page && $limit) {
//             $list = $list->page($page, $limit);
//         }
//         $list = $this->alias('a')
//             ->join('qh_binvite_code b','a.bcode = b.bcode','left')
//             ->join('qh_user c','a.uid = c.uid','left')
//             ->field('a.id,a.bcode,a.creat_time,b.bname,c.username')
//             ->where($where)
//             ->order('a.id desc')
//             ->select();
//         $list = modelo2array($list);
//         $data['list'] = $list;
//         $data['dataCount'] = $dataCount;
//         return $data;
//     }
//     
    

  /**
   * 这段代码有些冗余，不过解决问题，还是挺好的。
    * 可以改进
   * 功能:     邀请用户连接
   * @Author   zhanhuiping
   * @email    18702529695@163.com
   * @DateTime 2017-12-21
   * @param    [type]              $keywords [description]
   * @param    [type]              $page     [description]
   * @param    [type]              $limit    [description]
   * @return   [type]                        [description]
   */
   public function getDataLists($keywords, $page, $limit,$valuetime)
   {

   
      # 时间
       if($valuetime)
       {

          $data_id = $this->get_date($valuetime,$type=1);
          

       }else
       {

          $data_id = $this->get_date($valuetime,$type=0);
      

       }

       
        $start_time = $data_id['start_time'];
        $end_time   = $data_id['end_time'];
      

      
      if($keywords)
      {
          
         
         $where_count['phone|name'] = $keywords;         
         $map['phone|name'] = $keywords;         

      }

       $admin_id = $GLOBALS['userInfo']['id'];
       $group_id = Db::name('admin_access')->where(['user_id'=>$admin_id])->limit(1)->value('group_id');
 
       if($group_id != config('business_group_id'))
       {
          $tonxingzheng = 1;
       }
       $info_shanjia = Db::name('binvite_code')->where(['bid'=>$admin_id])->field('bcode,bname')->find();
       
       $pj_types = config('pj_type');
       
       if ($tonxingzheng == 1) {
          $info_shanjia['bcode'] = '';
          $where_count = "bcode is not null";
          $gd_where_bcode = "bcode is not null";
          $gd_where_bcodes = "bcode is not null";
          $pj_re = 'pj_type = '."'$pj_types'";
       }else{
          $bcode = $info_shanjia['bcode'];
          $map['bcode'] = $bcode;
          $where_count = ['bcode' => $info_shanjia['bcode']];
          $gd_where_bcodes = 'bcode = '."'$bcode'";
          $pj_re = 'pj_type = '."'$pj_types'";
       }
       
       


      //   # 统计iOS，Android，other手机的注册人数：
      

       $dataCount = Db::name('binvite_commission')->where($where_count)
                                                  ->where(['pj_type'=>$pj_types])
                                                  ->where('id','>=',$data_id['id_start'])
                                                  ->where('id','<=',$data_id['id_end'])
                                                  ->count('id');
 




 $res =  Db::query( "SELECT date_format(creat_time, '%Y-%m-%d') AS time, count(id) AS count FROM qh_binvite_commission where $gd_where_bcodes  and  creat_time BETWEEN '$start_time' and '$end_time' and $pj_re GROUP BY time");


$count_zs = [];
foreach ($res as $key => $value) {
 


   if($value['count'] > 20){
      $count_zs[] = (int)($value['count']*config('qudao_lv')+2);
   }else{
      $count_zs[] = $value['count'];
   }
          
 
}


$count_zz = 0;
for($i=0;$i< count($count_zs);$i++){
  $count_zz=$count_zz+ $count_zs[$i];
}


$num  = (int)($count_zz);

if($num > $dataCount)
{
  $num = $dataCount;
}
 

       if($page == (int)($num/15)+1)
       {
           
           if($num == 0)
           {

              $list = [];

           }else{
               $list = Db::name('binvite_commission')->where($map)
                                              ->where(['pj_type'=>$pj_types])
                                              ->where('id','>=',$data_id['id_start'])
                                              ->where('id','<=',$data_id['id_end'])
                                              ->limit($num%15)
                                              ->order('id desc')
                                              ->select();
           }
          


       }else{
         $pages = (int)($num/15);
         if($page > $pages){
          $page = 1;
         }
         if($num < 15)
         {
            $list = Db::name('binvite_commission')->where($map)
                                              ->where(['pj_type'=>$pj_types])
                                              ->where('id','>=',$data_id['id_start'])
                                              ->where('id','<=',$data_id['id_end'])
                                              ->order('id desc')
                                              ->limit($num)
                                              ->select();
         }else{
           $list = Db::name('binvite_commission')->where($map)
                                              ->where(['pj_type'=>$pj_types])
                                              ->where('id','>=',$data_id['id_start'])
                                              ->where('id','<=',$data_id['id_end'])
                                              ->order('id desc')
                                              ->page($page,15)
                                              ->select();
         }
         
       }
       

        

        foreach ($list as $key => $value) {
            $uid = $value['uid'];
            $map_username['uid'] = $uid;
            
            $list[$key]['username']    = $value['phone'];
            $list[$key]['user_time']   = strtotime($value['creat_time']);     
            $bname = Db::name('binvite_code')->where(['bcode'=>$value['bcode']])->value('bname');
            $list[$key]['bname']       = $bname;
      
            
        }




       $data['list']        = $list;
       $data['count']       = $num;
       $data['dataCount']   = $num;
       $data['admin_id']    = $admin_id;
       $data['group_id']    = $group_id;
       $data['shenqing_cs'] = $shenqing_cs;
       $data['s']           = $s;
       $data['fk_count']    = $fk_count;
        
        return $data;
}





   /**
    * 功能:     
    * @Author   Mrzhp
    * @email    18702529695@163.com
    * @DateTime 2018-02-09
    * @param    [type]              $keywords  [description]
    * @param    [type]              $page      [description]
    * @param    [type]              $limit     [description]
    * @param    [type]              $valuetime [description]
    * @return   [type]                         [description]
    */
    public function getDataList($keywords, $page, $limit,$valuetime)
    {

     
    
      
      # 时间
       if($valuetime)
       {

          $date_time = $this->get_date($valuetime,$type=1);
          $map['user_time'] = $date_time;

       }else
       {

          $date_time = $this->get_date($valuetime,$type=0);
          $map['user_time'] = $date_time;

       }
      
      
      if($keywords)
      {
          
         $keywords = trim($keywords);
         $uid =  Db::name('user')->where(['phone'=>$keywords])->value('uid');
         $map['uid'] = $uid;         

      }

   

      # 基础准备
       $admin_id = $GLOBALS['userInfo']['id'];
       
       $group_id = Db::name('admin_access')->where(['user_id'=>$admin_id])->value('group_id');
      

       if( $group_id !=20)
       { 


        $info_binvite = Db::name('binvite_code')->field('bcode,bname')->where('id > 0')->select();
        $arr_binvite = [];
        foreach($info_binvite as $key=>$value)
        {

         $arr_binvite[$value['bcode']] = $value['bname'];

        }

         $map['bcode'] = ['<>',''];
         $order = 'id desc';

       }else
       {
         
         $info_shanjia = Db::name('binvite_code')->where(['bid'=>$admin_id])->field('bcode,bname')->find();
         $bcode        = $info_shanjia['bcode'];
         $bname        = $info_shanjia['bname'];
         $map['bcode'] = $bcode;
         $order = 'yaoqingtongji desc';

       }


        // 总人数
        $dataCount = Db::name('binvite_about')->where($map)->count('id');
        

        // 系统
        $android = Db::name('binvite_about')->where($map)->where('os = 1')->count('id');
        $ios     = Db::name('binvite_about')->where($map)->where('os = 2')->count('id');
        $other   = Db::name('binvite_about')->where($map)->where('os = 3')->count('id');

       
        // 申请量&&放款量  
        $order_count = Db::name('binvite_about')->where('id > 0')->where($map)->field('sum(fk_cs) as fk_cs,sum(shenqing_cs) as shenqing_cs')->select();
        
        $fk_count    = $order_count[0]['fk_cs'];
        $shenqing_cs = $order_count[0]['shenqing_cs'];

         

        // 主体框架
        $list = Db::name('binvite_about')->field('bcode,uid,os,user_time,name,yaoqingtongji,id')
                                         ->where($map)
                                         ->page($page,$limit)
                                         ->where('id > 0')
                                         ->order($order)
                                         ->select();

        
        foreach($list as $key=>$value)
        {
          
          if($admin_id != 1)
          {

             $list[$key]['bname'] = $bname;

          }else
          {

             $list[$key]['bname'] = $arr_binvite[$value['bcode']];

          
          }
          
             $list[$key]['username'] = Db::name('user')->where('uid>0')->where(['uid'=>$value['uid']])->value('username');
             $list[$key]['mobile_type'] = Db::name('user_info')->where('id>0')->where(['uid'=>$value['uid']])->value('mobile_type');
             

        }
         
        $data['android']     = $android;
        $data['dataCount']   = $dataCount;
        $data['ios']         = $ios;
        $data['other']       = $other;      
        $data['fk_count']    = $fk_count;      
        $data['shenqing_cs'] = $shenqing_cs;    
        $data['list']        = $list;  
        $data['admin_id']    = $admin_id;
        $data['group_id']    = $group_id;

        return $data;
        

    }



   /**
    * 功能:     推广列表时间转换
    * @Author   Mrzhp
    * @email    18702529695@163.com
    * @DateTime 2018-02-09
    * @param    [type]              $type [description]
    * @return   [type]                    [description]
    */
   public function get_date($valuetime,$type)
   {

       switch ($type) {
         case '0':
           $start_time ='2017-01-01 00:00:00';
           $end_time   = date('Y-m-d H:i:s',(time()+24*60*60));
           $id_start = Db::name('binvite_commission')->where('creat_time','>',$start_time)->value('id');
           $id_end = Db::name('binvite_commission')->where('creat_time','>',$end_time)->value('id');
           if(!$id_start||!$id_end)
           {
             $id_start = 0;
             $id_end = Db::name('binvite_commission')->order('id desc')->value('id');
             $id_end == null ? $id_end =2:$id_end = $id_end;
           }
        
           break;
         case '1':
           $start_time = date('Y-m-d H:i:s',strtotime($valuetime[0]));
           $end_time   = date('Y-m-d H:i:s',strtotime($valuetime[1]));
           $id_start = Db::name('binvite_commission')->where('creat_time','>',$start_time)->value('id');
           $id_end = Db::name('binvite_commission')->where('creat_time','>',$end_time)->value('id');
           if($id_start && !$id_end)
           { 
             $id_end = Db::name('binvite_commission')->order('id desc')->value('id');
           }else if (!$id_start && !$id_end) {
             $id = Db::name('binvite_commission')->order('id desc')->value('id');
             $id_start = $id+1;
             $id_end = $id+3;
           }
           break;
         default:
           # code...
           break;
       }
       

        $data['id_start']   = $id_start;
        $data['id_end']     = $id_end;
        $data['start_time'] = $start_time;
        $data['end_time']   = $end_time;
        return $data;
       

   }





    /**
    * 功能:     推广统计
    * @Author   zhanhuiping
    * @email    18702529695@163.com
    * @DateTime 2018-01-05
    * @param    [type]              $param [description]
    * @return   [type]                     [description]
    */
   public function getsearch($param)
   {


      # 渠道商
      $qudao = Db::name('binvite_code')->select();

      # 逻辑判断
      if(!$param['valuetime']){
        $time = date('Y-m-d',time());
        $start_time = $time.' 00:00:00';
        $end_time   = $time.' 23:59:59';
       
      }elseif ($param['valuetime']) {
        $time1 = $param['valuetime'][0];
        $time2 = $param['valuetime'][1];
        $start_time = date('Y-m-d',strtotime($time1));
        $end_time   = date('Y-m-d',strtotime($time2));
      }
      
      $date =  array('BETWEEN',[$start_time,$end_time]);  //时间段
      $map_qudao = $param['qudao'];
      


      #业务判断
      
      // 1,今日数据
       $today_list = $this->get_time_map($map_qudao,$date); 
        // p($today_list);exit;
      // 2,累计数据
       $all_list = $this->get_time_map($map_qudao);
       $data = array(
              'today'=>$today_list,
              'all'  =>$all_list,
              'qudaos' => $qudao
       );

      return $data;
   }


   /**
    * 功能:     推广统计
    * @Author   zhanhuiping
    * @email    18702529695@163.com
    * @DateTime 2018-01-05
    * @param    [type]              $data      [description]
    * @param    [type]              $map_qudao [description]
    * @return   [type]                         [description]
    */
   public function get_time_map($map_qudao = '',$date = '')
   {

         if($date){
            $where_user['user_time'] = $date;
            $where_apply['user_time'] = $date;
         }else{
            $start_time = '2017-01-01 00:00:00';
            $end_time = date('Y-m-d H:i:s',(time()+24*60*60*10));
            $date =  array('BETWEEN',[$start_time,$end_time]);  //时间段
            $where_user['user_time'] = $date;
            $where_apply['user_time'] = $date;
         }
          
          # 渠道数据中，注册总量，iOS设备，安卓设备，其它设备
           $data_user = $this->get_userInfo_about($where_user,$map_qudao);  
           
        
           $user_count   = $data_user[0];      // 总注册量
           $user_android = $data_user[1];      // android 注册
           $user_ios     = $data_user[2];      // ios 注册
           $user_other   = $data_user[3];      // other

          # 申请about
           $where_shenqin_status = 'shenqing_cs';
           $data_apply_shenqin_number = $this->get_apply_about($map_qudao,$where_shenqin_status,$where_apply);  // 申请次数
           
            
           $where_m1pass_status = 'thirdpass_cs';
           $data_apply_m1pass_number = $this->get_apply_about($map_qudao,$where_m1pass_status,$where_apply);  //  审核通过数
           
           $where_yuqi_status = 'yuqi_cs';
           $data_apply_yuqi_number = $this->get_apply_about($map_qudao,$where_yuqi_status,$where_apply);  //  逾期数
           
           $where_fk_status = 'fk_cs';
           $data_apply_fk_number = $this->get_apply_about($map_qudao,$where_fk_status,$where_apply);  //  放款数
           
           $where_hk_status = 'hk_cs';
           $data_apply_hk_number = $this->get_apply_about($map_qudao,$where_hk_status,$where_apply);  //  还款次数
           
           $where_yq_status = 'yanqi_cs';
           $data_apply_yq_number = $this->get_apply_about($map_qudao,$where_yq_status,$where_apply);  //  延期次数
          
           
           # 续贷相关
           $where_xd_status = 'is_xd';
           $data_apply_xudai_number = $this->get_apply_about($map_qudao,$where_xd_status,$where_apply);  //  续贷次数
        

           
           # 1审核通过率：审核通过率 = 回访通过次数/申请次数
            if($data_apply_shenqin_number == 0||$data_apply_m1pass_number == 0){
              $data_apply_m1pass_number_rate = 0;
            }else{
              $data_apply_m1pass_number_rate = substr(($data_apply_m1pass_number/$data_apply_shenqin_number),0,5)*100;
            }

           # 2逾期率：逾期率=逾期单数/放款数
           
            if($data_apply_fk_number == 0 || $data_apply_yuqi_number == 0){
              $data_apply_yuqi_number_rate = 0;
            }else{
              $data_apply_yuqi_number_rate = substr(($data_apply_yuqi_number/$data_apply_fk_number),0,5)*100;
            }

           # 3放款率：放款率=放款数/申请次数
            if($data_apply_shenqin_number == 0 || $data_apply_fk_number == 0){
              $data_apply_fk_number_rate = 0;
            }else{
              $data_apply_fk_number_rate = substr(($data_apply_fk_number/$data_apply_shenqin_number),0,5)*100;
            }

           # 4还款率 ：还款率=还款次数/放款次数
            if($data_apply_fk_number == 0 || $data_apply_hk_number == 0){
              $data_apply_hk_number_rate = 0;
            }else{
              $data_apply_hk_number_rate = substr(($data_apply_hk_number/$data_apply_fk_number),0,5)*100;
            }
            
           # 5续贷率：续贷率=续贷用户数/首次完成订单用户数
            $hk_user_cs = $this->get_hk_user_cs($map_qudao,$where_apply);
            if($hk_user_cs == 0 || $data_apply_xudai_number ==0){
              $data_apply_xudai_number_rate = 0;
            }else{
              $data_apply_xudai_number_rate = substr(($data_apply_xudai_number/$hk_user_cs),0,5)*100;
            }
             
           # 6延期率：延期率=延期用户数/成功放款数
            if($data_apply_fk_number == 0 || $data_apply_yq_number == 0){
              $data_apply_yq_number_rate = 0;
            }else{
              $data_apply_yq_number_rate = substr(($data_apply_yq_number/$data_apply_fk_number),0,5)*100;
            }


           $list['user_count'] = $user_count;
           $list['user_android'] = $user_android;
           $list['user_ios'] = $user_ios;
           $list['user_other'] = $user_other;
           $list['data_apply_shenqin_number'] = $data_apply_shenqin_number;
           $list['data_apply_m1pass_number'] = $data_apply_m1pass_number;
           $list['data_apply_yuqi_number'] = $data_apply_yuqi_number;
           $list['data_apply_fk_number'] = $data_apply_fk_number;
           $list['data_apply_hk_number'] = $data_apply_hk_number;
           $list['data_apply_yq_number'] = $data_apply_yq_number;
           $list['data_apply_xudai_number'] = $data_apply_xudai_number;

           // 率
           $list['data_apply_m1pass_number_rate'] = $data_apply_m1pass_number_rate;
           $list['data_apply_yuqi_number_rate'] = $data_apply_yuqi_number_rate;
           $list['data_apply_fk_number_rate'] = $data_apply_fk_number_rate;
           $list['data_apply_hk_number_rate'] = $data_apply_hk_number_rate;
           $list['data_apply_shenqin_number_rate'] = $data_apply_shenqin_number_rate;
           $list['data_apply_yq_number_rate'] = $data_apply_yq_number_rate;
           $list['data_apply_xudai_number_rate'] = $data_apply_xudai_number_rate;
           return $list;
   }


   /**
    * 功能: 此查询尚有改善的空间，或者代码压缩的空间
    * 但是为了减少一个联表的过程把所有的查询都
    * 写出来了
    * @Author   zhanhuiping
    * @email    18702529695@163.com
    * @DateTime 2018-01-08
    * @return   [type]              [description]
    */
   public function get_userInfo_about($where_user,$where_bcode)
   {
     
      # 如果bcode没有传递过来，则将所有渠道的打印出来
      if(!$where_bcode){
        $where_bcodes['bcode'] = ['<>',""];
      }else{
        $where_bcodes['bcode'] = $where_bcode;
      }

      $all = [];
      for ($i=0; $i <=3 ; $i++) { 
         if($i == 0){
            $branch =  Db::name('binvite_about')->where($where_bcodes)
                                                ->where($where_user)
                                                ->count();

         }else{
           $branch =  Db::name('binvite_about')->where($where_bcodes)
                                                ->where($where_user)
                                                ->where(['os'=>$i])
                                                ->count();
         
         }

         $all [] = $branch;

      }
       
       return $all;
   }



  /**
   * 功能:     此代码是计算申请量的代码
   * @Author   zhanhuiping
   * @email    18702529695@163.com
   * @DateTime 2018-01-08
   * @param    [type]              $where_bcode  [description]
   * @param    [type]              $where_status [description]
   * @return   [type]                            [description]
   */
  public function get_apply_about($where_bcode,$status = '',$where_apply)
  {
   
   # 如果bcode没有传递过来，则将所有渠道的打印出来
   if(!$where_bcode){
      $where_bcodes['bcode'] = ['<>',""];
   }else{
      $where_bcodes['bcode'] = $where_bcode;
   }
   

   $apply_number = Db::name('binvite_about')->field("sum($status) as number")
                                            ->where($where_apply)
                                            ->where($where_bcodes)->select();

  
   
   return $apply_number[0]['number'];

  }


  
  /**
   * 功能:     user_cs
   * @Author   zhanhuiping
   * @email    18702529695@163.com
   * @DateTime 2018-01-15
   * @param    [type]              $where_bcode [description]
   * @param    [type]              $where_apply [description]
   * @return   [type]                           [description]
   */
   public function get_hk_user_cs($where_bcode,$where_apply)
   {
      $where_bcodes['bcode'] = $where_bcode;
      $hk_user_cs = Db::name('binvite_about')->where('hk_cs > 0')
                               ->where($where_bcodes)
                               ->where($where_apply)
                               ->count();
     
      return $hk_user_cs;
   }
  
  /**
   * 功能:     推广列表
   * @Author   zhanhuiping
   * @email    18702529695@163.com
   * @DateTime 2018-01-11
   * @param    [type]              $param [description]
   * @return   [type]                     [description]
   */
  public function getbaccountlist($param)
  {
      

     
      if(!$param['valuetime'][0]){
        $type = $param['type'];
        $date_list = $this->get_valuetime($type);
        $time1 = $date_list['time1'];
        $time2 = $date_list['time2'];

      }else{
         $time1 = date('Y-m-d H:i:s',strtotime($param['valuetime'][0]));
         $time2 = date('Y-m-d H:i:s',strtotime($param['valuetime'][1]));
      }
       
      $date = array('BETWEEN',[$time1,$time2]);

      $list =  $this->get_tuiguan($date);
      
      return $list;
      

  }


  /**
   * 功能:     时间转换
   * @Author   zhanhuiping
   * @email    18702529695@163.com
   * @DateTime 2018-01-11
   * @param    [type]              $type [description]
   * @return   [type]                    [description]
   */
  public function get_valuetime($type)
  { 

     switch ($type) {
        case '1':
           $time1 = date('Y-m-d',time());
           $time2 = date('Y-m-d',(time()+60*60*24));

          break;
        case '2':
           $time1 = date('Y-m-d',(time()-(60*60*24*7)));
           $time2 = date('Y-m-d',time());
          break;
        case '3':
           $time1 = '2017-01-01';
           $time2 = date('Y-m-d',(time()+60*60*24));
          break;
        default:
          break;
      }
      
      $date_list['time1'] = $time1;
      $date_list['time2'] = $time2;

      return $date_list;
  }
  
  /**
   * 功能:     推广--获取列表信息
   * @Author   zhanhuiping
   * @email    18702529695@163.com
   * @DateTime 2018-01-11
   * @param    [type]              $date [description]
   * @param    [type]              $type [description]
   * @return   [type]                    [description]
   */
  public function get_tuiguan($date)
  {
       $map_user['user_time'] = $date;
       $map_time['time'] = $date;
       $field = 'bcode,
                 COUNT(id) AS number,
                 SUM(shenqing_cs) AS shenqing_cs, 
                 SUM(fk_cs) AS fk_cs,
                 SUM(hk_cs) AS hk_cs,
                 SUM(thirdpass_cs) AS thirdpass_cs,
                 SUM(yanqi_cs) AS yanqi_cs,
                 SUM(yuqi_cs) AS yuqi_cs,
                 SUM(is_xd) AS xudai';

      $list = Db::name('binvite_about')->field($field)
                                       ->group('bcode')
                                       ->where($map_user) 
                                       ->select();
     
      foreach($list as $key => $value){
        $map['bcode'] = $value['bcode'];
        $list[$key]['bname'] = Db::name('binvite_code')->where($map)->value('bname');
        $list[$key]['shenqing_cs'] = Db::name('binvite_about')->where($map_time)->where($map)->sum('shenqing_cs');
        $list[$key]['fk_cs_rate'] = $this->get_apply_rate($value['shenqing_cs'],$value['fk_cs']); //放款率
        $list[$key]['hk_cs_rate'] = $this->get_apply_rate($value['fk_cs'],$value['hk_cs']);       //还款率
        $list[$key]['xudai_rate'] = $this->get_apply_rate($value['hk_cs'],$value['xudai'],1,$date,$value['bcode']);       //续贷率
        $list[$key]['yuqi_cs_rate'] = $this->get_apply_rate($value['fk_cs'],$value['yuqi_cs']);        //逾期率
        $list[$key]['zhuanhua_rate'] = $this->get_apply_rate($value['number'],$list[$key]['shenqing_cs']);        //转化率
      }

      return $list;
  }


  
  public function get_apply_rate($fenmu,$fenzi,$type = 0,$date = '',$value = '')
  {
     if($type == 1){
      $map_user['user_time'] = $date;
     
       $fenmu = Db::name('binvite_about')->where($map_user) 
                                         ->where('bcode','=',$value)
                                         ->where('hk_cs > 0')
                                         ->count();
     }
     if(!$fenmu){
        $apply_rate = 0;
     }else{
        $apply_rate = substr(($fenzi/$fenmu),0,5)*100;
     }
     
     
     return $apply_rate.'%';

  }

}