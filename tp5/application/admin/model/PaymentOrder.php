<?php
// +----------------------------------------------------------------------
// | Description: 岗位
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use app\admin\model\Common;

class PaymentOrder extends Common 
{

	protected $name = 'payment_order';
	

	/**
	 * [getAData 获取数据]
	 * @linchuangbin
	 * @DateTime  2017-02-10T21:07:18+0800
	 * @return    [array]                      
	 */
	public function getDatas($where)
	{
		return $this->where($where)->find();
	}
	/**
	 * [getDataList 获取列表]
	 * @param  array  $where [description]
	 * @param  string $field [description]
	 * @return [type]        [description]
	 */
	public function getDataList($where = [], $field= '*')
	{
		return $this->field($field)->where($where)->select();
	}
}