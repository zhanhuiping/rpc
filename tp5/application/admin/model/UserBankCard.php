<?php
// +----------------------------------------------------------------------
// | Description: 岗位
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use app\admin\model\Common;

class UserBankCard extends Common 
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
	protected $name = 'user_bank_card';

	/**
	 * [getData 获取数据]
	 * @param  string $field [description]
	 * @param  array  $where [description]
	 * @return [type]        [description]
	 */
	public function getDatas($field = '*', $where = [])
	{
		return $this->field($field)->where($where)->find();
	}
}