<?php
// +----------------------------------------------------------------------
// | Description: 商家操作类
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Withdraw extends Common 
{	
    
	protected $name = 'score_withdraw';
    protected $createTime = 'create_time';
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';
	protected $insert = [
		'status' => 1,
	];

    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
	public function getDataList($keywords, $page, $limit)
	{
		$map = [];
		if ($keywords) {
			$map['phone'] = ['like', '%'.$keywords.'%'];
		}
		$dataCount = $this->where($map)->count('id');
		
		$list = $this
				->where($map);
		
		// 若有分页
		if ($page && $limit) {
			$list = $list->page($page, $limit);
		}

		$list = $list 
				->field('*')
				->select();
		$list = modelo2array($list); 
		$data['list'] = $list;
		$data['dataCount'] = $dataCount;
		
		return $data;
	}

	/**
	 * [getDataById 根据主键获取详情]
	 * @linchuangbin
	 * @DateTime  2017-02-10T21:16:34+0800
	 * @param     string                   $id [主键]
	 * @return    [array]                       
	 */
	public function getDataById($id = '')
	{
		$data = $this->get($id);
		if (!$data) {
			$this->error = '暂无此数据';
			return false;
		}
		return $data;
	}
	/**
	 * 提现操作
	 * @zwr
	 * @DateTime 2017-07-11T09:40:39+0800
	 * @param    integer                 
	 * @return   [type]                    
	 */
	public function tx($id=0)
	{
        if(!$id){
			$this->error = '没有获取到id';
			return false;
        }
        $res = $this->get($id);
        $status=$res->status;
        if((int)$status===-1){
			$this->error = '该提现记录已被关闭';
			return false;
        }else if((int)$status===1){
			$this->error = '该提现记录已被提现成功';
			return false;
        } 
        $res->status = 1;
        #管理员操作时间
        $res->operate_time = date("Y-m-d H:i:s",time());
        #操作用户id
        $res->operate_uid = $GLOBALS['userInfo']['id'];   
        $resSave = $res->save(); 
        if(!$resSave){
			$this->error = '提现失败';
			return false;
        }else{
            return true;
        }		

	}
	public function cancelTx($id=0)
	{
        if(!$id){
			$this->error = '没有获取到id';
			return false;
        }
        $res = $this->get($id);
        $status=$res->status;
        if((int)$status===-1){
			$this->error = '该提现记录已被关闭';
			return false;
        }else if((int)$status===1){
			$this->error = '该提现记录已被提现成功';
			return false;
        }
        #获取提现参数
        $uid = $res->uid ;
        $value = $res->value ;
        $opreate_uid = $GLOBALS['userInfo']['id'];
		$this->startTrans();
		try {
            $res->status = -1;
            $res->operate_time = date("Y-m-d H:i:s",time());
            $res->operate_uid =  $opreate_uid;   
            $res->save();
            #关闭体现返还积分
            model('score')->handle($uid,$value,$opreation="setInc");
            #积分添加 写入日志
            model('ScoreLog')->addlog($uid,$opreate_uid,$value,$opretion="setInc",$logid=4,$type="提现被关闭");
			$this->commit() ;
			return true ;
		} catch(\Exception $e) {
			$this->rollback();
			$this->error = '提现失败';
			return false;
		}		                 
	}


}