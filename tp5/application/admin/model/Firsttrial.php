<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;
use app\admin\model\Documentary;
class Firsttrial extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'apply';
    protected $createTime = 'time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = DATETIME;
    // protected $insert = [
    //     'status' => 1,
    // ];

 
    /**
     * [getDataList 列表]
     * @AuthorHTL 湛慧平
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
    public function getDataList($keywords, $page, $limit,$valuedate,$os = '',$status="")
    {
        $map = [];
        if($valuedate)
        {
          $valuedate = substr($valuedate,0,10);
          $valuedate = date('Y-m-d',strtotime("$valuedate +1 day"));
          $map['time'] = ['like', '%'.$valuedate.'%'];
        }
        if ($keywords) {
            $map['phone|order_number|name'] = ['like', '%'.$keywords.'%'];
        }
        if($os)
        {
            $map['os'] = $os;
        }
        if(""!=$status)
        {
            $map['status'] = $status;
        }
        
        //获取后台管理用户id
        $admin_uid = $GLOBALS['userInfo']['id'];
        if($admin_uid !=1)
        {
           $map['check_aid'] = $admin_uid;
        }
        $dataCount = $this->where($map)->count('id');

        $list = $this
            ->where($map);
        // 若有分页
        if ($page && $limit) {
            $list = $list->page($page, $limit);
        }
        
       
       $list = $list->field('auth_type,id,status,time,money,period,order_number,check_time,check_aid,name,credit_rate,service_rate,procedure_rate,phone,uid,mobile_type,os')
                     ->order('status asc,auth_type desc,first_claim_date desc,auth_type desc, id desc,auth_type desc')
                     ->select();

       foreach ($list as $key => $value) 
       {
          $mapuid['uid'] = $value['uid'];
          $count = $this->where($mapuid)->where('id','<=',$value['id'])->count();

          if($count >=2)
          {
            $oddcount = $count-2;
            $oddinfo = $this->field('status')->where($mapuid)->select();
            $happen = $oddinfo[$oddcount]['status'];
            $list[$key]['happen'] = $happen;
          }
          
          $mapseq['apply_id'] = $value['id'];
          $dataseq = Db::name('claim_seq')->field('seq')->where($mapseq)->find();
          if(!$dataseq)
          {
            $list[$key]['seq'] = '';
          }else{
              $list[$key]['seq'] = $dataseq['seq'];
            }

            $list[$key]['count'] = $count;
       }
    
        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }

    /**
     * [getDataById 根据主键获取详情]
     * @linchuangbin
     * @DateTime  2017-02-10T21:16:34+0800
     * @param     string                   $id [主键]
     * @return    [array]
     */
    public function getDataById($id = '')
    {
        $data=$this
            ->field('id,money,period,credit_rate,service_rate,procedure_rate,overdue_rate')
            ->where('id',$id)
            ->find();
        return $data;
    }
    /**
     * [createData 新建]
     * @湛慧平
     * @DateTime  2017-08-12T21:19:06+0800
     * @param     array                    $param [description]
     * @return    [array]                         [description]
     */
    public function createDatas($discount,$order_number,$multipleSelection,$uid,$id)
    {
         $map['order_number'] = $order_number;
         //处理报告
         $multipleSelection = $this->handlerReport($multipleSelection, $uid);
         $multipleSelection = json_encode($multipleSelection);
         $check_time = date('Y-m-d H:i:s',time());
         // $admin_uid = $GLOBALS['userInfo']['id'];
         $admin_uid = 9999;
         $data = [
                   'status'            => 2,
                   'is_report'         => 1,
                   'meets'             => $multipleSelection,
                   'remark'            => $discount,
                   'check_time'        => $check_time,
                   'check2_aid'        => 9999,
                   'check2_time'       => $check_time,
                   'check2_time'       => $check_time,
                   'second_claim_date' => $check_time,
                   'review'            => '自动通过复审',
                 ];
         $admin_data = ['status'=>1,'uid'=>$uid,'date'=>$check_time,'admin_user_id'=>$admin_uid,'apply_id'=>$id,'authstatus'=>1];
         Db::name('admin_user_clien')->insert($admin_data);
         $info = $this->save($data,$map);
         if(!$info)
         {
            return $info;
         }
         return $info;
         
    }
    /**
     * [channgeReport 生成报告]
     * @param  string $order_number [description]
     * @param  array  $meets        [description]
     * @return [type]               [description]
     */
    public function channgeReport($order_number = '', $meets = [])
    {
        if(!$order_number)
        {
            $this->error = '订单号不能为空';
        }
        //处理报告
        $multipleSelection = $this->handlerReport($meets);
        $multipleSelection = json_encode($multipleSelection);
        $data['is_report'] = 1 ;
        $data['meets'] = $multipleSelection;
        $map['order_number'] = $order_number;
        $info = $this->save($data,$map);
        if(!$info)
        {
            $this->error = '报告生成失败';
            return false;
        }
        return $info;
    }
    
     /**
     * [createData 新建]
     * @湛慧平
     * @DateTime  2017-02-10T21:19:06+0800
     * @param     array                    $param [description]
     * @return    [array]                         [description]
     */
    public function xcreateData($discount,$order_number,$multipleSelection,$uid,$id)
    {
         $map['order_number'] = $order_number;
         //放款订单不可驳回
         $documentary = new Documentary;
          $re = $documentary->checkOrder($order_number);
          if(!$re)
          {
              $this->error = $documentary->getError();
              return false;
          }
         $multipleSelection = $this->handlerReport($multipleSelection, $uid);
         $multipleSelection = json_encode($multipleSelection);
         $check_time = date('Y-m-d H:i:s',time());
         $admin_uid = $GLOBALS['userInfo']['id'];
         $admin_data = ['status'=>0,'uid'=>$uid,'date'=>$check_time,'admin_user_id'=>$admin_uid,'apply_id'=>$id,'authstatus'=>1];
         $data = ['status'=>6,'remark'=>$discount,'check_time'=>$check_time,'meets'=>$multipleSelection,'is_report'=> 1];
         Db::name('admin_user_clien')->insert($admin_data);
         $info = $this->save($data,$map);
         if(!$info)
         {
          return false;
          exit;
         }
          return true;
    }
    /**
     * [updateDataById 编辑]
     * @linchuangbin
     * @DateTime  2017-02-10T21:24:49+0800
     * @param     [type]                   $param [description]
     * @param     [type]                   $id    [description]
     * @return    [type]                          [description]
     */
    public function updateDataById($param, $id)
    {
        $checkData = $this->get($id);
        if (!$checkData) {
            $this->error = '暂无此数据';
            return false;
        }

        // // 验证
        // $validate = validate($this->name);
        // if (!$validate->check($param)) {
        //     $this->error = $validate->getError();
        //     return false;
        // }

        try {
            $this->allowField(true)->save($param, [$this->getPk() => $id]);
            return true;
        } catch(\Exception $e) {
            $this->error = '编辑失败';
            return false;
        }
    }


    //添加自定义配置
    public function conditionadd($uid,$condition)
    {
          $data = ['uid'=>$uid,'content'=>$condition];
          $info = Db::name('admin_condition')->insert($data);
          if(!$info)
          {
             return false;
             exit;
          }
          return true;
    }

    

    
    public function firstrevoked($param)
    {

        //判断撤销订单是否是最新订单
        $apply_info = $this->field('order_number,status,merchandiser2,transfer')->where(['uid'=>$param['uid']])->order('time DESC')->find();
        if($apply_info)
        {
            if($apply_info['order_number'] !== $param['order_number'])
            {
                $this->error = '撤销失败,不是最新有效订单';
                return false;
            }elseif( $apply_info['status'] == 5 || ($apply_info['status'] == 3  &&  $apply_info['merchandiser2'] == '' && $apply_info['transfer'] !== 1) )
            {
                $this->error = '撤销失败,该订单已放款或已完成 1';
                return false;
            }

        }else{
            $this->error = '撤销失败,无效订单';
            return false;
        }
        
        $map['order_number'] = $param['order_number'];
        $info = $this->field('status,merchandiser,merchandiser2,mtwo_admin_user_id,treasurer,mtwotransfer,transfer,is_re_apply')->where($map)->find();
        // 初审 认领/通过/驳回撤销 0申请 2复审通过
        if($param['status'] == 1||$param['status'] ==6 || $param['status']==0 || ($param['status']==2 && $info['merchandiser'] ==''))
        {
            // 初审通过且没有认领
            if($info['status'] !=1&&$info['status'] !=6 && $info['status'] !=0 && ($info['status'] !=2 || $info['merchandiser'] !='' ))
            {
                $this->error = "初审无法撤销";
                return false;
            }
            //如果是续借 无法撤回到初审
            if($info['is_re_apply'] == 1 && $info['merchandiser'] == '')
            {
                $this->error = '撤销失败,该订单为自动续借';
                return false;
            }
             $this->startTrans();
             try{

              $data = ['status'=>0];

              #通过/驳回撤销后 报告也清空
              $data['is_report'] = 0;

              //认领撤回
              if($param['status']==0) {
                  $data['check_aid']=NULL; //初审人员
                  $data['check_time']=NULL;//认领时间
                  $data['first_claim_date']=NULL; //初审认领时间
              }

              $info = $this->save($data,$map);
              $revoked = ['revoked'=>0];
              $admin_uid = $GLOBALS['userInfo']['id'];
              $where =['apply_id'=>$param['id'],'authstatus'=>1,'admin_user_id'=>$admin_uid,'revoked'=>1];
              Db::name('admin_user_clien')->where($where)->update($revoked);
              $this->commit();
              return true;
            } catch (\Exception $e) {
                // 回滚事务

              $this->rollback();
              $this->error = '撤销失败:'.$e->getMessage();
              return false;
            }


        }

 
         // 复审通过/驳回/撤销
        if(($param['status'] == 2 &&  $info['merchandiser']=='') ||$param['status'] ==7)
        {
               if($info['status'] !=2&&$info['status'] !=7)
               {

                 $this->error = "复审无法撤销";
                 return false;
               }
             //开启事物
             $this->startTrans();
             try{

              $data = ['status'=>1];
              $info = $this->save($data,$map);
              $revoked = ['revoked'=>0];
              $admin_uid = $GLOBALS['userInfo']['id'];
              $where =['apply_id'=>$param['id'],'authstatus'=>2,'admin_user_id'=>$admin_uid,'revoked'=>1];
              Db::name('admin_user_clien')->where($where)->update($revoked);
              $this->commit();
              return true;
            } catch (\Exception $e) {
                // 回滚事务
              $this->rollback();
              $this->error = '撤销失败';
              return false;
            }

        }

       
         // M1 回访通过/或者驳回/认领/移交M2 撤销
        if($param['status'] == 9||$param['status'] == 10||$param['status'] == 5 ||$param['status'] == 2 || $param['status'] == 9 || ( $info['mtwotransfer'] == 1  &&  $info['mtwo_admin_user_id'] ==''))
        {
            if($info['status'] !=9&&$info['status'] !=10&&$info['status'] !=5 && ($info['status'] !=2 || $info['merchandiser'] =='' ) &&( $info['mtwotransfer'] != 1 || $info['mtwo_admin_user_id'] !='') )
            {
             $this->error = "M1无法撤销";
             return false;
            }
           if($info['status']==5)
           {

                  //开启事物
               $this->startTrans();
               try{

                $data = ['status'=>3];
                $info = $this->save($data,$map);
                $revoked = ['revoked'=>0];
                $admin_uid = $GLOBALS['userInfo']['id'];
                $where =['apply_id'=>$param['id'],'authstatus'=>4,'admin_user_id'=>$admin_uid,'revoked'=>1];
                Db::name('admin_user_clien')->where($where)->update($revoked);
                $this->commit();
                return true;
              } catch (\Exception $e) {
                  // 回滚事务
                $this->rollback();
                $this->error = '撤销失败';
                return false;
              }


           }elseif($info['transfer']==1){//移交撤回
               //开启事物
               $this->startTrans();
               try{


                   $data = [];
                   $data['transfer']=0;//移交字段
                   $data['transfer_info']=NULL;//移交信息

                   #撤销移交后，延时移交自动+1天 避免自动移交
//                   $delay = date('Y-m-d H:i:s',strtotime('+1 day'));
//                   $data['delay_transfer_date'] = $delay;


                   $info = $this->save($data,$map);
                   $revoked = ['revoked'=>0];
                   $admin_uid = $GLOBALS['userInfo']['id'];
                   $where =['apply_id'=>$param['id'],'authstatus'=>4,'admin_user_id'=>$admin_uid,'revoked'=>1];
                   Db::name('admin_user_clien')->where($where)->update($revoked);
                   $this->commit();
                   return true;
               } catch (\Exception $e) {
                   // 回滚事务
                   $this->rollback();
                   $this->error = '撤销失败';
                   return false;
               }


           }elseif($info['status']==2){//认领撤回
               //开启事物
               $this->startTrans();
               try{

                   $data = [];
                   $data['merchandiser']=NULL;//跟单人员
                   $data['third_claim_date']=NULL;//回访认领时间


                   $info = $this->save($data,$map);
//                   $revoked = ['revoked'=>0];
//                   $admin_uid = $GLOBALS['userInfo']['id'];
//                   $where =['apply_id'=>$param['id'],'authstatus'=>4,'admin_user_id'=>$admin_uid,'revoked'=>1];
//                   Db::name('admin_user_clien')->where($where)->update($revoked);
                   $this->commit();
                   return true;
               } catch (\Exception $e) {
                   // 回滚事务
                   $this->rollback();
                   $this->error = '撤销失败'.$e->getMessage();
                   return false;
               }
           }else{// 回访通过/回访拒绝
                //开启事物
               $this->startTrans();
               try{

                $data = ['status'=>2];
                $data['meets3'] = NULL; //回访命中条件
                $info = $this->save($data,$map);
                $revoked = ['revoked'=>0];
                $admin_uid = $GLOBALS['userInfo']['id'];
                $where =['apply_id'=>$param['id'],'authstatus'=>4,'admin_user_id'=>$admin_uid,'revoked'=>1];
                Db::name('admin_user_clien')->where($where)->update($revoked);
                $this->commit();
                return true;
              } catch (\Exception $e) {
                  // 回滚事务
                $this->rollback();
                $this->error = '撤销失败';
                return false;
              }
           }

        }

        //M2 撤销认领
        if(  ($param['status'] == 3 || $param['status'] == 4) && $info['mtwotransfer']==1 )
        {
            $this->startTrans();
            try{

                $data = [];
                //认领撤回

                $data['mtwo_admin_user_id']=NULL; //M2人员
                $data['fifth_claim_date']=NULL; //M2认领时间


                $info = $this->save($data,$map);
                $revoked = ['revoked'=>0];
                $admin_uid = $GLOBALS['userInfo']['id'];
                $where =['apply_id'=>$param['id'],'authstatus'=>1,'admin_user_id'=>$admin_uid,'revoked'=>1];
                Db::name('admin_user_clien')->where($where)->update($revoked);
                $this->commit();
                return true;
            } catch (\Exception $e) {
                // 回滚事务

                $this->rollback();
                $this->error = '撤销失败:'.$e->getMessage();
                return false;
            }


        }


    }

    
    //财务撤回按钮
    public function finance($param)
    {
      //判断撤销订单是否是最新订单
        $apply_info = $this->field('order_number,status')->where(['uid'=>$param['uid']])->order('time DESC')->find();
        if($apply_info)
        {
            if($apply_info['order_number'] !== $param['order_number'])
            {
                $this->error = '撤销失败,不是最新有效订单';
                return false;
            }elseif($apply_info['status'] == 5 || $apply_info['status'] == 3)
            {
                $this->error = '撤销失败,该订单已放款或已完成';
                return false;
            }

        }else{
            $this->error = '撤销失败,无效订单';
            return false;
        }
        $map['order_number'] = $param['order_number'];
        $info = $this->field('status')->where($map)->find();
         // 财务放款/拒绝放款撤销按钮
        if($param['status']==3||$param['status']==11||$param['status']==5 || $param['status']==9)
        {
            if($info['status'] !=3&&$info['status'] !=11&&$info['status'] !=5 && $param['status']!=9)
           {
             $this->error = "无法撤销";
             return false;
           }

            if($info['status']==5)
           {
                $openuid['uid'] = $param['uid'];
                $data = ['status'=>3];
                $revoked = ['revoked'=>0];
                $admin_uid = $GLOBALS['userInfo']['id'];
                $where =['apply_id'=>$param['id'],'authstatus'=>3,'admin_user_id'=>$admin_uid,'revoked'=>1];
                $user_info = Db('user_info')->where($openuid)->field('open_gid')->find();
                $arrayopengid = json_decode($user_info['open_gid']);
                $countopenid = count($arrayopengid);
                  //开启事物
               $this->startTrans();
               try{ 
                    $info = $this->save($data,$map);
                    Db::name('admin_user_clien')->where($where)->update($revoked);
                    if($countopenid ==2)
                    {
                      $dataopen = ['open_gid'=>null];
                      Db::name('user_info')->where($openuid)->update($dataopen);
                    }else
                    {
                      array_pop($arrayopengid);  
                      $open_gid = json_encode($arrayopengid);
                      $dataopen = ['open_gid'=>$open_gid];
                      Db::name('user_info')->where($openuid)->update($dataopen);
                    }
                    $this->commit();
                    return true;  
              } catch (\Exception $e) {
                      // 回滚事务
                    $this->rollback();
                    $this->error = '撤销失败';
                    return false;
              }

          
           }elseif($info['status']==9){//认领撤回

                $this->startTrans();
                try{

                    $data = [];
                    $data['treasurer']=NULL; //财务人员
                    $data['myloan_claim_date']=NULL;//认领时间
                    $data['fives_claim_date']=NULL; //财务认领时间

                    $info = $this->save($data,$map);
                    $revoked = ['revoked'=>0];
                    $admin_uid = $GLOBALS['userInfo']['id'];
                    $where =['apply_id'=>$param['id'],'authstatus'=>3,'admin_user_id'=>$admin_uid,'revoked'=>1];
                    Db::name('admin_user_clien')->where($where)->update($revoked);
                    $this->commit();
                    return true;
                } catch (\Exception $e) {
                    // 回滚事务
                    $this->rollback();
                    $this->error = '撤销失败';
                    return false;
                }

            }else{//拒绝放款/已放款
                //开启事物
               $this->startTrans();
               try{
                //判断撤销订单中是否有申请中的订单
                $count = $this->where(['uid'=>$param['uid'],'status'=>['in','0,1,2,3,4,9,12']])->count();
                if($count > 0)
                {
                    $this->error = '撤销失败，该用户拥有有效订单';
                    return false;
                }
                $data = ['status'=>9,'appoint_time'=>null];
                $info = $this->save($data,$map);
                $revoked = ['revoked'=>0];
                $admin_uid = $GLOBALS['userInfo']['id'];
                $where =['apply_id'=>$param['id'],'authstatus'=>3,'admin_user_id'=>$admin_uid,'revoked'=>1];
                Db::name('admin_user_clien')->where($where)->update($revoked);
                $this->commit(); 
                return true;  
              } catch (\Exception $e) {
                  // 回滚事务
                $this->rollback();
                $this->error = '撤销失败';
                return false;
              }
           }
        
        }else{
            $this->error = '撤销失败';
            return false;
        }
    }


    // 补充资料
    public function datainfo($param)
    {
        $admin_id   = $GLOBALS['userInfo']['id'];
        $id         = $param['id'];
        $uid        = $param['uid'];
        $img        = $param['img'];
        $datatype   = $param['datatype'];
        $dataname   = $param['dataname'];
        $datatime   = date("Y-m-d H:i:s",time());
        $data = ['apply_id'=>$id,'admin_id'=>$admin_id,
                  'uid'=>$uid,'datacontent'=>$img,
                  'dataname'=>$dataname,'datatime'=>$datatime,
                  'datatype'=>$datatype];

        $info = Db::name('data_supplement')->insert($data);
        if(!$info)
           {
              $this->error = "上传失败";
              return false;
           }
        $where['uid'] = $uid;

        
        
        $datainfo = Db::name('data_supplement')->where($where)->where('is_delete is null')->select();
        foreach($datainfo as $key=>$value)
        {
           $map['id'] = $value['admin_id'];
           $admin_user = Db::name('admin_user')->where($map)->field('username')->find();
           $datainfo[$key]['datauser'] = $admin_user['username'];
        }
        $data['datainfo'] = $datainfo;
        return $data;
    }

    //刪除
    public function datadetele($param)
    {
      $data = $param['datamultipleSelection'];
      $arr =array();
      foreach($data as $key=>$value)
      {
        $arr[] = $value['id'];
      }
      
      $this->startTrans();
      try{
        $where['id'] = array('in',$arr);
        $del = Db::name('data_supplement')->where($where)->update(['is_delete'=>3]);
          // 提交事务
        $this->commit(); 
        return true;   
      } catch (\Exception $e) {
          // 回滚事务
        $this->rollback();
        $this->error = '删除失败';
        return false;
      }
    }
    /**
     * [handlerReport 处理报告]
     * @param  array  &$data [description]
     * @return [type]        [description]
     */
    public function handlerReport(array &$data = [], $uid = '')
    {
      //字段信息
       $column = Db::name('admin_condition')->where(["status" => 1, 'step' => 1])->field('id,content')->select();
       //报告
       $creditinfo = Db::name('user_info')->where(['uid' => $uid])->field('operator_search_id,id_card,mongo_operator_report,operator_id,city,job,zmf')->find();
        $bizData = array(
            'userId'    => $creditinfo['operator_id'],
            'mongoRes'  => $creditinfo['mongo_operator_report'],
            'channel'   => config('operator_channel'),
            'idNumber'  => $creditinfo['id_card'],
            'search_id' => $creditinfo['operator_search_id'],
        );

      $datas = report_detail($bizData);

      $report = isset($datas['data']['JsonData']['json']) ? $datas['data']['JsonData']['json'] : [];
       //符合规则
       $ids = [];
       $result = [];
       if($data)
       {
          foreach ($data as $value) {
              $ids[] = $value['id'];
            }
       }
       foreach ($column as $v) {
          $info = ['id' => $v['id'], 'content' => $v['content']];
          //处理字段
          $re = $this->handlerField($v['id'], $report, $creditinfo);
          $info['user_data'] = $re;
          if(in_array($v['id'], $ids))
          {
              $info['status'] = 0;
          }else{
              $info['status'] = 1;
          }

          $result[] = $info;
       }
       return $result;
    }

    /**
     * [handlerField 处理字段]
     * @param  integer $num       [description]
     * @param  array   &$report   [description]
     * @param  array   &$userInfo [description]
     * @return [type]             [description]
     */
    public function handlerField($num = 1, array &$report = [],array &$userInfo = [])
    {
        $str = '';
        switch ($num) {
          //姓名验证
            case 1:
                if (!isset($report['basic_info']['name_check'])) {
                    return '';
                }
                $name_check = $report['basic_info']['name_check'];
                if ($name_check == 1) {
                    $str = '完全一致';
                }elseif($name_check == 2)
                {
                    $str = '基本一致';
                }elseif($name_check == 3)
                {
                    $str = '不一致';
                }elseif($name_check == 4)
                {
                    $str = '无法验证';
                }elseif($name_check == 5)
                {
                    $str = '无法验证';
                }
                break;
            case 2:
                $str = !empty(isset($userInfo['id_card']))? substr($userInfo['id_card'], 6, 4) .'年': '-';
                break;
            //注册时间
            case 3:
                $str = isset($report['basic_info']['reg_time'])? floor((time() - strtotime($report['basic_info']['reg_time']))/86400) .'天' : '';
                break;
            //互通数量
            case 4:
                $str = isset($report['user_portrait']['both_call_cnt'])? $report['user_portrait']['both_call_cnt'].'个' : 0;
                break; 
            //静默天数
            case 5:
                $str = isset($report['user_portrait']['silent_days']['max_interval'])? $report['user_portrait']['silent_days']['max_interval'].'天' : 0;
                break;
            //夜间
            case 6:
                $str = isset($report['user_portrait']['night_activities']['monthly_avg_seconds_ratio'])? $report['user_portrait']['night_activities']['monthly_avg_seconds_ratio'] * 100 .'%': 0;
                break;
             //110 120次数
            case 7:
                $str = ($report['special_cate'][0]['talk_cnt'] + $report['special_cate'][1]['talk_cnt']) . '次';
                break;
            //月均消费
            case 8:
                $str = isset($report['basic_info']['monthly_avg_consumption'])? $report['basic_info']['monthly_avg_consumption'] .'元': 0;
                break;
            //贷款 互金类数量
            case 9:
                $str = ($report['special_cate'][2]['talk_cnt'] + $report['special_cate'][3]['talk_cnt']) . '家';
                break;
            //逾期
            case 10:
                $str = isset($report['risk_analysis']['single_overdue'][0]['hit_cnt'])? $report['risk_analysis']['single_overdue'][0]['hit_cnt'].'次': 0;
                break;
             //主叫被叫互通
            case 10:
                $str = isset($report['risk_analysis']['contacts_overdue'][2]['hit_cnt'])? $report['risk_analysis']['contacts_overdue'][2]['hit_cnt'] .'次': 0;
                break;
            //工作
            case 11:
                $str = isset($userInfo['job']) ? $userInfo['job']: 0;
                break;
            case 12:
                $str = '请往上操作';
                break;
            //芝麻分
            case 13:
                $str = isset($userInfo['zmf']) ? $userInfo['zmf'].'分': 0;
                break;
            //定位
            case 14:
                $str = isset($userInfo['city']) ? $userInfo['city']: '';
                break;
            case 15:
                $str = '请往上操作';
                break;
            case 16:
                $str = '请往上操作';
                break;
            //主叫逾期
            case 17:
                $str += isset($report['risk_analysis']['contacts_overdue'][0]['hit_cnt'])? $report['risk_analysis']['contacts_overdue'][0]['hit_cnt']: 0;
                $str += isset($report['risk_analysis']['contacts_overdue'][1]['hit_cnt'])? $report['risk_analysis']['contacts_overdue'][1]['hit_cnt']: 0;
                $str += isset($report['risk_analysis']['contacts_overdue'][2]['hit_cnt'])? $report['risk_analysis']['contacts_overdue'][2]['hit_cnt']: 0;
                if(isset($report['risk_analysis']['single_overdue']))
                {
                    foreach ($report['risk_analysis']['single_overdue'] as $key => $value) {
                      if($key == 0)
                      {
                          continue;
                      }
                        $str += $report['risk_analysis']['single_overdue'][$key]['hit_cnt'];
                    }
                }
                $str .= '次';
                break;
            default:
                # code...
                break;
        }
        return $str;
    }

}
