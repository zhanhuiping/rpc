<?php
/**
 * M1跟单移交列表
 */

namespace app\admin\model;
use think\Db;

class Transfer extends Common
{
    protected $name = 'apply';
    /**
     * M1跟单移交列表
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-08T14:40:23+0800
     * @param    string                   $keywords 关键字
     * @param    integer                  $page     页码
     * @param    integer                  $limit    每页数量
     * @return   array
     */
    public function getDataList($keywords, $page=1, $limit=15, $valuedate, $os = '',$status = "")
    {
        $map = array();
        if($valuedate)
        {
          $valuedate = substr($valuedate,0,10);
          $valuedate = date('Y-m-d',strtotime("$valuedate +1 day"));
          $map['time'] = ['like', '%'.$valuedate.'%'];
        }
        if ($keywords) {
            $map['phone|order_number|name'] = ['like', '%'.$keywords.'%'];
        }
        if($os)
        {
            $map['os'] = $os;
        }

        $map['status'] = array('in','3,4,5');
        if(in_array($status, [3,4,5]))
        {
            $map['status'] = $status;
        }
        
        $map['merchandiser2'] = ['exp', ' is null'];
        $map['transfer'] = 1;
        $dataCount = $this->where($map)->count();
        
        $list = $this
            ->where($map)
            ->order('merchandiser2 asc')
            ->page($page, $limit)
            ->select();
        $list = modelo2array($list);
        foreach($list as $key=>$value)
        {
            $list[$key]['status'] = $this->setOverdue($value['id'],$value['appoint_time'],$value['status']);  //逾期处理
            $list[$key]['info']   = $this->statusInfo($list[$key]['status']); //状态信息
            $list[$key]['user_admin1'] = $this->getNameById($value['check_aid']);      //初审员
            $list[$key]['user_admin2'] = $this->getNameById($value['check2_aid']);     //复审员
            $list[$key]['user_admin3'] = $this->getNameById($value['treasurer']);      //财务员
            $list[$key]['user_admin4'] = $this->getNameById($value['merchandiser']);   //跟单员M1
            $list[$key]['user_admin5'] = $this->getNameById($value['merchandiser2']);  //催收员M2
            $list[$key]['repay'] = $this->getRepay($list[$key]['status'], $value, $value['overdue_money']);//应还金额
        }
        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }

    /**
     * 还款认领
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-07T16:31:55+0800
     * @param    int       $id         申请单id
     * @param    int       $admin_uid  管理员id
     * @return   boolean                              
     */
    public function claim($id,$uid,$admin_uid)
    {
         $map['id'] = $id;
         $info = $this->field('merchandiser2')->where($map)->find();
         if($info['merchandiser2'])
         {
           return $data = ['code'=>'error','message'=>'已经被认领'];
         }

         $this->startTrans();
         try{
  
            $nowdate = date('Y-m-d',time());
            $mapseq['nowdate'] = ['like','%'.$nowdate.'%'];
            $infocount = Db::name('claim_seq')
                         ->where('auth_admin',4)
                         ->where('admin_id',$admin_uid)
                         ->where($mapseq)
                         ->count();
            $claim_date = ['uid'=>$uid,'apply_id'=>$id,'nowdate'=>$nowdate,'auth_admin'=>4,'seq'=>$infocount+1,'admin_id'=>$admin_uid];
            Db::name('claim_seq')->insert($claim_date);
            
            $fourth_claim_date = date('Y-m-d H:i:s',time());
            $data = array('merchandiser2'=>$admin_uid,'fourth_claim_date'=>$fourth_claim_date);
            $info = $this->save($data, ['id' => $id]);  //添加至apply表中
            // 提交事务
            $this->commit(); 
            return $data = ['code'=>'success','message'=>'认领成功'];
         }catch (\Exception $e) {
              // 回滚事务
            $this->rollback();
            $this->error = '失败';
            return false;
       }
       
    }

}