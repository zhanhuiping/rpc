<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Myloanpass extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'apply';
    protected $createTime = 'time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = DATETIME;
    // protected $insert = [
    //     'status' => 1,
    // ];


 
   /**
    * @功能       複審通過列表
    * @Author   zhanhuiping
    * @email    18702529695@163.com
    * @DateTime 2017-09-07
    * @param    [type]              $keywords  [description]
    * @param    [type]              $page      [description]
    * @param    [type]              $limit     [description]
    * @param    [type]              $valuedate [description]
    * @return   [type]                         [description]
    */
    public function getDataList($keywords, $page, $limit, $valuedate, $os = '',$status="")
    {
        $map = [];
        if($valuedate)
        {
          $valuedate = substr($valuedate,0,10);
          $valuedate = date('Y-m-d',strtotime("$valuedate +1 day"));
          $map['time'] = ['like', '%'.$valuedate.'%'];
        }
        if ($keywords) {
            $map['phone|order_number|name'] = ['like', '%'.$keywords.'%'];
        }
        if($os)
        {
            $map['os'] = $os;
        }
        if(""!=$status)
        {
            // $map['status'] = $status;
        }
        $maps['treasurer'] = ['exp','is null'];

        $maps['status'] = array('in','5,3,4,12');
        //财务改动 加回访通过
        //$maps['status'] = array('in','5,4,12,9');
        if(config('PAY_MODE') == Myloans::PAY_MODE_OFFLINE){
            $maps['status'] = array('in','5,4,12,9');
        }


        $dataCount = $this->where($map)->where($maps)->count('id');
        $list = $this
            ->where($map);
        // 若有分页
        if ($page && $limit) {
            $list = $this->page($page, $limit);
        }
        
       

       $list = $this->order('status desc,auth_type desc, check2_time asc,auth_type desc, id desc,auth_type desc')
                    //->where('treasurer is null')
                    ->where($maps)
                    ->select();
//                    echo $this->getLastSql();die;
        $list = modelo2array($list);
        foreach($list as $key=>$value)
        {
            $map2['id'] = $value['check_aid'];
            $map1['id'] = $value['check2_aid'];
            $map3['id'] = $value['merchandiser'];

            $list[$key]['user_admin1'] = Db::name('admin_user')->field('username')->where($map2)->find();
            $list[$key]['user_admin2'] = Db::name('admin_user')->field('username')->where($map1)->find();
            $list[$key]['user_admin3'] = Db::name('admin_user')->field('username')->where($map3)->find();
            $list[$key]['admin_uid'] = $admin_uid;


            $list[$key]['already_money'] = Documentary::alreadyRepay($value, $list[$key]['status']);//已还总金额
            $list[$key]['repay'] = $this->getRepay($list[$key]['status'], $value, $list[$key]['already_money']);//应还总金额
            $list[$key]['surplus'] = $this->getSurplus( $list[$key]['repay'],$list[$key]['status'] );//剩余应还金额
        }

        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }

    /**
     * [getDataById 根据主键获取详情]
     * @linchuangbin
     * @DateTime  2017-02-10T21:16:34+0800
     * @param     string                   $id [主键]
     * @return    [array]
     */
    public function getDataById($id = '')
    {
        $data=$this
            ->field('id,money,period,credit_rate,service_rate,procedure_rate,overdue_rate')
            ->where('id',$id)
            ->find();
        return $data;
    }
    /**
     * [createData 新建]
     * @linchuangbin
     * @DateTime  2017-02-10T21:19:06+0800
     * @param     array                    $param [description]
     * @return    [array]                         [description]
     */
    public function createData($param)
    {

        // // 验证
        // $validate = validate($this->name);
        // if (!$validate->check($param)) {
        //     $this->error = $validate->getError();
        //     return false;
        // }
        try {
            $this->data($param)->allowField(true)->save();
            return true;
        } catch(\Exception $e) {
            $this->error = '添加失败';
            return false;
        }
    }    

    /**
     * [updateDataById 编辑]
     * @linchuangbin
     * @DateTime  2017-02-10T21:24:49+0800
     * @param     [type]                   $param [description]
     * @param     [type]                   $id    [description]
     * @return    [type]                          [description]
     */
    public function updateDataById($param, $id)
    {
        $checkData = $this->get($id);
        if (!$checkData) {
            $this->error = '暂无此数据';
            return false;
        }

        // // 验证
        // $validate = validate($this->name);
        // if (!$validate->check($param)) {
        //     $this->error = $validate->getError();
        //     return false;
        // }

        try {
            $this->allowField(true)->save($param, [$this->getPk() => $id]);
            return true;
        } catch(\Exception $e) {
            $this->error = '编辑失败';
            return false;
        }
    }


   /**
    * @功能       财务認領
    * @Author   zhanhuiping
    * @email    18702529695@163.com
    * @DateTime 2017-09-07
    * @param    [type]              $param [description]
    * @return   [type]                     [description]
    */
    public function getclaim($param)
    {
        $id = $param['id'];
        $uid = $param['uid'];
        $map['id'] = $id;
        $info = $this->where($map)->field('treasurer')->find();
        if($info['treasurer'])
        {
           return $data = ['code'=>'error','message'=>'已经被认领'];
        }

       $this->startTrans();
       try{
            $admin_uid = $GLOBALS['userInfo']['id'];
            // $nowdate = date('Y-m-d',time());
            // $mapseq['nowdate'] = ['like','%'.$nowdate.'%'];
            // $infocount = Db::name('claim_seq')
            //              ->where('auth_admin',2)
            //              ->where('admin_id',$admin_uid)
            //              ->where($mapseq)
            //              ->count();
            // $claim_date = ['uid'=>$uid,'apply_id'=>$id,'nowdate'=>$nowdate,'auth_admin'=>2,'seq'=>$infocount+1,'admin_id'=>$admin_uid];
            // Db::name('claim_seq')->insert($claim_date);
            
            $myloan_claim_date = date('Y-m-d H:i:s',time());
            $data = array('treasurer'=>$admin_uid,'myloan_claim_date'=>$myloan_claim_date);
            $info = $this->save($data, ['id' => $id]);  //添加至apply表中
            // 提交事务
            $this->commit(); 
            return $data = ['code'=>'success','message'=>'认领成功'];
            // 提交事务
        $this->commit();
       }catch(\Exception $e)
       {
        $this->rollback();
        return false;
       }
    }

}