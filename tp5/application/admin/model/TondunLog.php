<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use com\verify\HonrayVerify;

class TondunLog extends Common
{
    
    protected $name = 'td_account';
    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
    public function getDataList($keywords, $page, $limit)
    {
        $map = [];
        if ($keywords) {
            $map['c.name'] = ['like', '%'.$keywords.'%'];
        }
        $dataCount = $this->count('id'); 
        //查询条件
        /* $dataCount = $this->alias('a')
         ->join('qh_td_preloan_apply b','a.report_id = b.report_id','left')
         ->where($map)
         ->count('id');*/
        $list = $this
        ->where($map);
        // 若有分页
        if ($page && $limit) {
            $list = $this->page($page, $limit);
        }
        $list=$this->alias('a')
        ->join('qh_user_info c','a.uid = c.uid','left')
        ->field('a.id,c.name,c.phone,a.total_times,a.first_times,a.last_time_str')
        /* ->order('id desc') */
        ->select();
        $list = modelo2array($list);
        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }
    
    
}