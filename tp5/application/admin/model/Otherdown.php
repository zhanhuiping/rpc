<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;
class Otherdown extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'apply';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = DATETIME;
    // protected $insert = [
    //     'status' => 1,
    // ];
  

    /**
     * [getDataList 列表]
     * @AuthorHTL 湛慧平
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */



  
    public function getDataList($keywords, $page, $limit,$admin_uid,$valuedate, $os = '',$status="")
    {
        $map = [];
        if($valuedate)
        {
          $valuedate = substr($valuedate,0,10);
          $valuedate = date('Y-m-d',strtotime("$valuedate +1 day"));
          $map['time'] = ['like', '%'.$valuedate.'%'];
        }
        if ($keywords) {
            $map['phone|order_number|name'] = ['like', '%'.$keywords.'%'];
        }
        if($os)
        {
            $map['os'] = $os;
        }
        // if(""!=$status)
        // {
        //     $map['status'] = $status;
        // }

        $map['status'] = 8;
        $dataCount = $this->where($map)->count('id');
        $list = $this
            ->where($map);
        // 若有分页
        if ($page && $limit) {
            $list =  $list->page($page, $limit);
        }
      
        $list =  $this->field('id,status,time,money,period,order_number,check_time,check2_time,appoint_time,check2_aid,check_aid,name,uid')
                     ->order('status asc, check_aid asc, id desc')
                     ->select();
 
        foreach($list as $key=>$value)
        {
            $list[$key]['status'] = $this->setOverdue($value['id'],$value['appoint_time'],$value['status']);  //逾期处理     
            $mapuid['uid'] = $value['uid'];
            $count = $this->where($mapuid)->where('id','<=',$value['id'])->count();
            if($count>=2)
            {
              $addcount = $count-2;
              $checkinfo = $this->where($mapuid)->field('check_aid,merchandiser,status,id')->select();
              $check_aid = $checkinfo[$addcount]['check_aid'];
              $ids = $checkinfo[$addcount]['id'];
              $merchandiser = $checkinfo[$addcount]['merchandiser'];
              $statuss = $checkinfo[$addcount]['status'];
              if($statuss ==5)
              {
                $datacheckaid = ['check_aid'=>$check_aid];
                Db::name('apply')->where('id','>',$ids)->where($mapuid)->update($datacheckaid);
              }
              $list[$key]['merchandiser'] = Db::name('admin_user')->field('username')->where('id',$merchandiser)->find();
              $list[$key]['happen'] = $statuss;
             
            }

            //判断驳回条件
            $addressdata = Db::name('user_info')->where($mapuid)->field('address,id_card')->find();
            $is_reject_area = $this->is_reject_area($addressdata['address']);
            $is_reject_age  = $this->is_reject_age($addressdata['id_card']);
            if($is_reject_age&&$is_reject_area)
            {
               $list[$key]['mismatch'] = '定位年龄不符合';
            }
            elseif($is_reject_age)
            {
              $list[$key]['mismatch'] = '年龄不符合';
            }
            elseif($is_reject_area)
            {
              $list[$key]['mismatch'] = '地址不符合';
            }


            $list[$key]['counts'] = $count;
            $list[$key]['admin_uid'] = $admin_uid;
        }

        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }



    /**
     * 是否在拒绝放款的地区
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-22T16:46:27+0800
     * @param    string      $address    定位的地址
     * @return   boolean     true：表示在拒绝的地区 
     */
    public function is_reject_area($address='')
    {
      $appSysConfig = controller('v2/Config')->getAppConfigs();
      $area = $appSysConfig['data']['APP_REJECT_AREA'];
      // $address = '湖北省宜昌市枝江县';
      // $area = '内蒙古/新疆/西藏/四川/宁夏/荆州/宜昌/淮南/大同/毕节/黔州/楚雄/上饶/萍乡';
      $area_arr = explode('/', $area); 
      foreach ($area_arr as $key => $value) {
        $find = stripos($address,$value);
        if(false !== $find){
          return 'success';
        }
      } 
      return false;
    }

  /**
   * 是否在拒绝放款年龄
   * @author   yhq <934797303@qq.com>
   * @DateTime 2017-08-25T16:09:11+0800
   * @param    string      $id_card   身份证号码
   * @return   boolean     true：表示在拒绝放款年龄
   */
    public function is_reject_age($id_card='')
    {
      $appSysConfig = controller('v2/Config')->getAppConfigs();
      $age_min = $appSysConfig['data']['APP_ALLOW_AGE_MIN']; //允许最小年龄
      $age_max = $appSysConfig['data']['APP_ALLOW_AGE_MAX']; //允许最大年龄
      $year = (int)substr($id_card,6,4);
      $age  = (int)date('Y',time()) - $year;  //用户年龄
      if($age > (int)$age_max || $age < (int)$age_min){
        return $age;
      }
      return false;
    }

    

}