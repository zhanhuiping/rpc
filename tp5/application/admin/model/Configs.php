<?php
// +----------------------------------------------------------------------
// | Description: 后台系统配置model
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------
namespace app\common\model;

use think\Model;
use think\Db;

class Configs extends Model
{
    protected $name = 'system_config';
    
    /**
     * 后台修改配置
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-30T17:35:47+0800
     * @param    [type]                   $param [description]
     * @return   [type]                          [description]
     */
    public function updateData($param)
    {
        $os = $param['os'];

        # 依据不同的系统读取不同的数据表
        switch ((int)$os) {
            case 1:    //安卓
                $db = Db::name('system_config');
                break;
            case 2:    //IOS
                $db = Db::name('system_config_ios');
                break;
            case 1001: //腾讯应用宝
                $db = Db::name('system_config_yyb');
                break;
            default:  //IOS,默认用ios数据表，因为之前版本没有传这个参数
                $db = Db::name('system_config_ios');
                break;
        }
        // 验证
        // $validate = validate($this->name);
        // if (!$validate->check($param)) {
        //     $this->error = $validate->getError();
        //     return false;
        // }
        foreach($param as $k => $v)
        {
            $map['name'] = $k;
            if($map['name'] === 'SYS_TIME')
            {
                $v = !$v ? '' : date('Y-m-d H:i:s',strtotime($v));
            }
            $res = $db->where($map)->find();
            if($res) {
                $time = date("Y-m-d H:i:s",time()) ;
                $db->where($map)->update(['update_time'=>$time,'value'=>$v]);
            }
        }
        if ($res !== false) {
            return true;
        }
        $this->error = '更新失败';
        return false;
    }    
}