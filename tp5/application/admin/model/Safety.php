<?php
// +----------------------------------------------------------------------
// | Description: 安全模块操作
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;
use think\Cache;
class Safety extends Common
{

    protected $name = 'wind_jc_url';
    // protected $createTime = 'create_time';
    // protected $updateTime = "update_time";
    // protected $autoWriteTimestamp = 'datetime';
    // protected $insert = [
    //     'status' => 1,
    // ];
    // public function getstatusAttr($value)
    // {
    //     $data = ['1'=>"正常",'0'=>"禁用"];
    //     return $data[$value];
    // }

/**
 * 功能:     用户绑定手机号码
 * @param    [type]              $keywords [description]
 * @param    [type]              $page     [description]
 * @param    [type]              $limit    [description]
 * @return   [type]                        [description]
 */
   public function getDataLists($keywords, $page, $limit)
   {
      
      //p($keywords);
      //模糊查询
      if($keywords!=""){
        $map['username|realname|phone'] = ['like','%'.$keywords.'%'];
      }
      else {
        $map ="";
      }
      
      $datacount = Db::name('admin_user')->where('id !=1')->where($map)->count('id');

      $list =  Db::name('admin_user')->where('id !=1')->where($map)->field('username,create_time,phone,realname,id')->page($page,$limit)->select();
      foreach ($list as $key => $value) {
        $list[$key]['create_time'] = date('Y-m-d H:i:s',$value['create_time']);
      }

       //p($list);
      $data['list'] = $list;
      $data['datacount'] = $datacount;
      return $data;
    }


/**
 * 功能:      修改用户绑定的号码
 * @param    [type]              $keywords [description]
 * @param    [type]              $page     [description]
 * @param    [type]              $limit    [description]
 * @return   [type]              $admin_id [description]
*/
    public function getDataEdit($param)
    {
      if(!$param['nphone']){
         return ['code'=>400,'error'=>'输入的为空'];
      }
      elseif(!preg_match("/^1[34578]{1}\d{9}$/",$param['nphone'])){
         return ['code'=>400,'error'=>'请正确填写电话号码'];
      }

      //p($disable_ip);
      $admin_id = $GLOBALS['userInfo']['id'];
      //p($admin_id);
      $create_time = time();
      $create_time = date('Y-m-d H:i:s',$create_time);
      $data = [
        'phone'=>$param['nphone']
      ];
      $res = Db::name('admin_user')->where('id',$param['item'])->update($data);
      return $res;
    }


/**
 * 功能:     用户登录验证码
 * @param    [type]              $keywords [description]
 * @param    [type]              $page     [description]
 * @param    [type]              $limit    [description]
 * @return   [type]                        [description]
*/
     public function getDlVerifDataLists($keywords, $page, $limit,$valuetime)
   {
      //p($keywords);
      //模糊查询出uid
      //echo 333;die();

      # 日期判断
      if(!$valuetime){
        $time = date('Y-m-d',time());
        $start_time = $time.' 00:00:00';
        $end_time   = $time.' 23:59:59';
       
      }else{
        $time1 = $$valuetime[0];
        $time2 = $valuetime[1];
        $start_time = date('Y-m-d',strtotime($time1));
        $end_time   = date('Y-m-d',strtotime($time2));
      }
      
      $date =  ['BETWEEN',[$start_time,$end_time]];  //时间段

      #用户名判断
      if($keywords!=""){
        $map['username|phone'] = ['like','%'.$keywords.'%'];
      }
      else {
        $map ="";
      }
      $uid = '';
      $list1 = Db::name('admin_user')->where($map)->field('id')->select();
      //p($list1);
      $list1=empty($list1)?"":$list1;
      //p($list1);
      if($list1){       
        foreach ($list1 as $key => $value) {
          $uid[] =$value['id'];
        }
      //uid
        //$uid = implode($uid, ',');
      }
      $map2['create_time'] = $date;
      $map2['uid'] = ['in',$uid];
      //p($map2);

      $datacount = Db::name('admin_user_verify')->where('uid !=1')->where($map2)->count('id');

      $list =  Db::name('admin_user_verify')
                        ->where('uid !=1')
                        ->where($map2)
                        ->field('uid,account,verify,status,create_time, channel,id')
                        ->page($page,$limit)
                        ->select();
      foreach ($list as $key => $value) {

      // //判断渠道
      //   if($list[$key]['channel'] == 0){
      //     $list[$key]['channel'] = '没有渠道';
      //   }
      //   else if($list[$key]['channel'] == 1){
      //     $list[$key]['channel'] = '国都';
      //   }
      //   else{
      //     $list[$key]['channel'] = '大于';
      //   }

      // //判断状态
      //   if($list[$key]['status'] == 1){
      //     $list[$key]['status'] = '未使用';
      //   }
      //   else{
      //     $list[$key]['status'] = '已使用';
      //   }

        $now = Db::name('admin_user')->where('id',$list[$key]['uid'])->find();
        $list[$key]['uid'] = $now['username'];
      }
      //p($list[0]['channel']);die(0);
      $data['list'] = $list;
      $data['datacount'] = $datacount;
      return $data;
    }


/**
 * 功能:      单项删除用户验证那验证
 * @param    [type]              $keywords [description]
 * @param    [type]              $page     [description]
 * @param    [type]              $limit    [description]
 * @return   [type]              $admin_id [description]
*/
    public function getDlVerifDelDataLists($id)
    {
      $res = Db::name('admin_user_verify')->where('id',$id)->delete();

      if($res == 1){
        return $res;
      }
      else{
        return ['code'=>400,'error'=>'删除失败，请稍后重试'];
      }
    }

    
/**
 * 功能:     用户登录验证码 
 * @param    [type]              $keywords [description]
 * @param    [type]              $page     [description]
 * @param    [type]              $limit    [description]
 * @return   [type]                        [description]
*/
     public function getFkVerifDataLists($keywords, $page, $limit)
   {     
       //模糊查询出uid
      if($keywords!=""){
        $map['username|phone'] = ['like','%'.$keywords.'%'];
      }
      else {
        $map ="";
      }
      $uid = '';
      $list1 = Db::name('admin_user')->where($map)->field('id')->select();
      empty($list1)?$list1="":$list1=$list1;
      
      if($list1){
          foreach ($list1 as $key => $value) {
          $uid[] =$value['id'];
        }
        //uid
        $uid = implode($uid, ',');
      }
      //p($uid);
      $uid_array['user_id'] = ['in',$uid];

      $datacount = Db::name('smsverification')->where('user_id !=1')->where($uid_array)->count('id');

      $list =  Db::name('smsverification')
                      ->where('user_id !=1')
                      ->where($uid_array)
                      ->field('user_id,phone,smsverification,create_date,id')
                      ->page($page,$limit)->select();
      foreach ($list as $key => $value) {      
        $now = Db::name('admin_user')->where('id !=1')->where('id',$list[$key]['user_id'])->find();
        $list[$key]['user_id'] = $now['username'];
      }
      //p($list[0]['channel']);die(0);
      $data['list'] = $list;
      $data['datacount'] = $datacount;
      return $data;
    }


/**
 * 功能:      禁止ip表
 * @param    [type]              $keywords [description]
 * @param    [type]              $page     [description]
 * @param    [type]              $limit    [description]
 * @return   [type]              $admin_id [description]
*/
     public function getDisableIpDataLists($keywords, $page, $limit,$admin_id)
   {     
      //p($keywords);

      //模糊查询
      if($keywords!=""){
        $map['disable_ip'] = ['like','%'.$keywords.'%'];
      }
      else {
        $map ="";
      }

      $datacount = Db::name('user_disable_ip')->where($map)->count('id');

      $list =  Db::name('user_disable_ip')->where($map)->field('user_id,disable_ip,status,create_time,id')->page($page,$limit)->select();
      foreach ($list as $key => $value) {
        //$list[$key]['create_time'] = date('Y-m-d H:i:s',$value['create_time']);
        
        $now = Db::name('admin_user')->where('id',$list[$key]['user_id'])->find();
        $list[$key]['user_id'] = $now['username'];
        //判断状态
        // if($list[$key]['status'] == 1){
        //   $list[$key]['status'] = '已经禁用';
        // }
        // else{
        //   $list[$key]['status'] = '未禁用';
        // }

      }
       //p($list);
      $data['list'] = $list;
      $data['datacount'] = $datacount;
      return $data;
    } 


/**
 * 功能:      添加禁止ip
 * @param    [type]              $keywords [description]
 * @param    [type]              $page     [description]
 * @param    [type]              $limit    [description]
 * @return   [type]              $admin_id [description]
*/
    public function getDisableIpAddDataLists($disable_ip)
    {

    //判断是否存在
      $now =Db::name('user_disable_ip')->where('disable_ip',$disable_ip)->find();
      if($now){
        return ['code'=>400,'error'=>'该IP已经被禁用了'];
      }

      $admin_id = $GLOBALS['userInfo']['id'];
      $create_time = time();
      $create_time = date('Y-m-d H:i:s',$create_time);
      $data = [
        'user_id'=>$admin_id,
        'disable_ip'=>$disable_ip,
        'create_time'=>$create_time,
        'status'=>1,
      ];
      $res = Db::name('user_disable_ip')->insert($data);
      return $res;
      //p($res);
    }


    public function windcontroller_index($keywords, $page, $limit)
    {

      //模糊查询
      if($keywords!=""){
        $map['username'] = ['like','%'.$keywords.'%'];
      }
      else {
        $map ="";
      }
      
      $datacount = Db::name('wind_jc_url')->where($map)->count('id');

      $list =  Db::name('wind_jc_url')->where($map)->page($page,$limit)->select();

       //p($list);
      $data['list'] = $list;
      $data['datacount'] = $datacount;
      return $data;

    }



    /**
     * 功能:     风控URL添加
     * @Author   Mrzhp
     * @email    18702529695@163.com
     * @DateTime 2018-05-24
     * @param    [type]              $param [description]
     * @return   [type]                     [description]
     */
    public function windcontroller_url($param)
    {
      $pjname            = !empty($param['pjname']) ? $param['pjname']: '';
      $url_formal        = !empty($param['url_formal']) ? $param['url_formal']: '';
      $url_test          = !empty($param['url_test']) ? $param['url_test']: '';
      $biaoshi           = !empty($param['biaoshi']) ? $param['biaoshi']: '';
      $url_status        = !empty($param['url_status']) ? $param['url_status']: '1';
      $smsVerification_s = !empty($param['smsVerification_s']) ? $param['smsVerification_s']: '';
      $user_id_k   = $GLOBALS['userInfo']['id'];
      $create_time = date('Y-m-d H:i:s');
      if(!$url_formal || !$url_test || !$biaoshi || !$url_status){
        return ['code'=>400,'error'=>'填写为空'];
      }
      $data = [
                'url_formal'  => $url_formal,
                'url_test'    => $url_test,
                'biaoshi'     => $biaoshi,
                'url_status'  => $url_status,
                'user_id_k'   => $user_id_k,
                'pjname'   => $pjname,
                'create_time' => $create_time
              ];
       
      $map['biaoshi'] = $biaoshi;
      $id = $this->where($map)->value('id');
      if($id){
        $res = $this->save($data,$map);
      }else{
        $res = $this->save($data);
      }
      if(!$res){
        return false;
      }else{
        return $data = $param;
      }

    }

  

/**
 * 功能:      单项删除禁止ip
 * @param    [type]              $keywords [description]
 * @param    [type]              $page     [description]
 * @param    [type]              $limit    [description]
 * @return   [type]              $admin_id [description]
*/
    public function getDisableIpDelDataLists($id)
    {
      $res = Db::name('user_disable_ip')->where('id',$id)->delete();

      if($res == 1){
        return $res;
      }
      else{
        return ['code'=>400,'error'=>'删除失败，请稍后重试'];
      }
    }


/**
 * 功能:      更改禁止ip
 * @param    [type]              $keywords [description]
 * @param    [type]              $page     [description]
 * @param    [type]              $limit    [description]
 * @return   [type]              $admin_id [description]
*/
    public function getDisableIpEditDataLists($id)
    {

      $res = Db::name('user_disable_ip')->where('id',$id)->find();
      $now = ($res['status'] == 1)?2:1;
      $data =[
        'status'=>$now
      ];
      $res = Db::name('user_disable_ip')->where('id',$id)->update($data);
      if($res == 1){
        return $res;
      }
      else{
        return ['code'=>400,'error'=>'失败，请稍后重试'];
      }
    }
}

    





