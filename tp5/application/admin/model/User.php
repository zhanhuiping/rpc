<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;
use common\Utils;
use think\cache\driver\Redis;
use app\v2\model\Verify;
class User extends Common 
{	
    
    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
	protected $name = 'admin_user';
    protected $createTime = 'create_time';
    protected $updateTime = false;
	protected $autoWriteTimestamp = true;
	protected $insert = [
		'status' => 1,
	];  
	/**
	 * 获取用户所属所有用户组
	 * @param  array   $param  [description]
	 */
    public function groups()
    {
        return $this->belongsToMany('group', '__ADMIN_ACCESS__', 'group_id', 'user_id');
    }

    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
	public function getDataList($keywords, $page, $limit)
	{
		$map = [];
		if ($keywords) {
			$map['username|realname'] = ['like', '%'.$keywords.'%'];
		}

		// 默认除去超级管理员
		$map['user.id'] = array('neq', 1);
		$dataCount = $this->alias('user')->where($map)->count('id');
		$admin_id = $GLOBALS['userInfo']['id'];
		if($admin_id !== 1){
           $map['user.pj_type'] = config('pj_type');
		}
		
		$list = $this
				->where($map)
				->alias('user')
				->join('__ADMIN_STRUCTURE__ structure', 'structure.id=user.structure_id', 'LEFT')
				->join('__ADMIN_POST__ post', 'post.id=user.post_id', 'LEFT');
		
		// 若有分页
		if ($page && $limit) {
			$list = $list->page($page, $limit);
		}

		$list = $list 
				->field('user.*,structure.name as s_name, post.name as p_name')
				->select();
		
		$data['list'] = $list;
		$data['dataCount'] = $dataCount;
		
		return $data;
	}

	/**
	 * [getDataById 根据主键获取详情]
	 * @linchuangbin
	 * @DateTime  2017-02-10T21:16:34+0800
	 * @param     string                   $id [主键]
	 * @return    [array]                       
	 */
	public function getDataById($id = '')
	{
		$data = $this->get($id);
		if (!$data) {
			$this->error = '暂无此数据';
			return false;
		}
		$data['groups'] = $this->get($id)->groups;
		return $data;
	}
	/**
	 * 创建用户
	 * @param  array   $param  [description]
	 */
	public function createData($param)
	{
		if (empty($param['groups'])) {
			$this->error = '请至少勾选一个用户组';
			return false;
		}

        $is_username =  $this->where(['username'=>trim($param['username']),'pj_type'=>config('pj_type')])->find();
       
	    if($is_username)
	    {
	      	 $this->error = '名字重复,请重新取名';
			 return false;
	     }


	      $group_id_rs = config('group_id_rs');

		   $admin_id = $GLOBALS['userInfo']['id'];
		   if($admin_id != 1){
		     $group_id = Db::name('admin_access')->where(['user_id'=>$admin_id])->value('group_id');
		     if($group_id == $group_id_rs['group_id']){
		     	if(count($param['groups'])>1){
		     		$this->error = '请选择商家组,否则无法创建';
				    return false;
		     	}elseif ($param['groups'][0] != 20) {
		     		$this->error = '请选择商家组,否则无法创建';
				    return false;
		     	}
		     }
		   }
		// 验证
		// $validate = validate($this->name);
		// if (!$validate->check($param)) {
		// 	$this->error = $validate->getError();
		// 	return false;
		// }

		$this->startTrans();
		try {
			$param['password'] = user_md5($param['password']);
			$param['pj_type'] = config('pj_type');
			$this->data($param)->allowField(true)->save();

			foreach ($param['groups'] as $k => $v) {
				$userGroup['user_id'] = $this->id;
				$userGroup['group_id'] = $v;
				$userGroups[] = $userGroup;
			}
			$bcode_user['group_id']    = $userGroup['group_id'];
			$bcode_user['admin_id_b']  = $userGroup['user_id'];
			$bcode_user['admin_id_k']  = $GLOBALS['userInfo']['id'];;
			$bcode_user['create_date'] = date('Y-m-d H:i:s',time());
			$bcode_user['pj_type']     = config('pj_type');
			Db::name('binvite_bcode_user')->insert($bcode_user);
			Db::name('admin_access')->insertAll($userGroups);

			$this->commit();
			return true;
		} catch(\Exception $e) {
			$this->rollback();
			$this->error = '添加失败';
			return false;
		}
	}


	/**
	 * 通过id修改用户
	 * @param  array   $param  [description]
	 */
	public function updateDataById($param, $id)
	{
		// 不能操作超级管理员
		if ($id == 1) {
			$this->error = '非法操作';
			return false;
		}
		$checkData = $this->get($id);
		if (!$checkData) {
			$this->error = '暂无此数据';
			return false;
		}
		if (empty($param['groups'])) {
			$this->error = '请至少勾选一个用户组';
			return false;
		}
		$this->startTrans();

		try {
			Db::name('admin_access')->where('user_id', $id)->delete();
			foreach ($param['groups'] as $k => $v) {
				$userGroup['user_id'] = $id;
				$userGroup['group_id'] = $v;
				$userGroups[] = $userGroup;
			}
			Db::name('admin_access')->insertAll($userGroups);

			if (!empty($param['password'])) {
				$param['password'] = user_md5($param['password']);
			}
			 $this->allowField(true)->save($param, ['id' => $id]);
			 $this->commit();
			 return true;

		} catch(\Exception $e) {
			$this->rollback();
			$this->error = '编辑失败';
			return false;
		}
	}

	/**
	 * [login 登录]
	 * @AuthorHTL
	 * @DateTime  2017-02-10T22:37:49+0800
	 * @param     [string]                   $u_username [账号]
	 * @param     [string]                   $u_pwd      [密码]
	 * @param     [string]                   $verifyCode [验证码]
	 * @param     Boolean                  	 $isRemember [是否记住密码]
	 * @param     Boolean                    $type       [是否重复登录]
	 * @return    [type]                               [description]
	 */
	public function login($username, $password, $verifyCode = '', $isRemember = false, $type = false)
	{
		$username = trim($username);
        if (!$username) {
			$this->error = '帐号不能为空';
			return false;
		}
		if (!$password){
			$this->error = '密码不能为空';
			return false;
		}
        
        
		$map['username'] = $username;
		$map['pj_type'] = config('pj_type');
		$userInfo = $this->where($map)->find();
    	if (!$userInfo) {
			$this->error = '账号或密码错误';
			return false;
    	}
    	if (user_md5($password) !== $userInfo['password']) {
			$this->error = '账号或密码错误';
			return false;
    	}
    	if ($userInfo['status'] === 0) {
			$this->error = '帐号已被禁用';
			return false;
    	}


    	//获取短信信息
    	
        
     //   $group_id_info = Db::name('admin_user')->alias('a')
					//                           ->join('qh_admin_access b','a.id = b.user_id')
					//                           ->field('b.group_id')
					//                           ->where(['a.username'=>$username])
					//                           ->select();
  
 
    	// $channelStatus = ($group_id_info[0]['group_id'] != 20 || (count($group_id_info) > 1)) ? false : false;
     //    if($channelStatus)
     //    {

        
		   //  	if(!$verifyCode)
		   //  	{
		   //  		$this->error = '请填写短信验证码';
					// return false;
		   //  	}
		   //  	$adminUserVerify = Db::name('admin_user_verify');
		   //  	$Verifynfo = $adminUserVerify->where(['uid' => $userInfo['id']])->field('id,uid,verify,status,create_time')->order('id DESC')->find();
		   //  	if($Verifynfo['verify'] !== $verifyCode)
		   //  	{
		   //  		$this->error = '验证码错误';
					// return false;
		   //  	}
		   //  	if($Verifynfo['status'] == 2)
		   //  	{
		   //  		$this->error = '验证码已使用!请重新发送';
					// return false;
		   //  	}
		   //  	//五分钟时效性
		   //  	if( (time()-strtotime($Verifynfo['create_time'])) > (24*3600))
		   //  	{
		   //  		$this->error = '验证码时效性已过!请重新发送';
					// return false;
		   //  	}


    	// }

		/*以下先关闭*/
       

        // 获取菜单和权限
        $dataList = $this->getMenuAndRule($userInfo['id']);


        if (!$dataList['menusList']) {
			$this->error = '没有权限';
			return false;
        }

        if ($isRemember || $type) {
        	$secret['username'] = $username;
        	$secret['password'] = $password;
        	$data['rememberKey'] = encrypt($secret);
        }

        // 保存缓存        
        session_start();
        $info['userInfo'] = $userInfo;
        $info['sessionId'] = session_id();
        $authKey = user_md5($userInfo['username'].$userInfo['password']);
        $info['_AUTH_LIST_'] = $dataList['rulesList'];
        $info['authKey'] = $authKey;
        cache('Auth_'.$authKey, null);
        cache('Auth_'.$authKey, $info, config('LOGIN_SESSION_VALID'));
        // 返回信息
        $data['authKey']		= $authKey;
        $data['sessionId']		= $info['sessionId'];
        $data['userInfo']		= $userInfo;
        $data['authList']		= $dataList['rulesList'];
        $data['menusList']		= $dataList['menusList'];

        # 记录登录日志
        $ip =  getClientIP(); // 获取ip地址
        $login_date = date('Y-m-d H:i:s',time());
        $name = $info['userInfo']['username'];
        $admin_id = $info['userInfo']['id'];
        
         // 移动设备
         if(isset ($_SERVER['HTTP_X_WAP_PROFILE'])||stristr(isset ($_SERVER['HTTP_VIA']),"wap")){
            $os = 1;
         }else{
         	$os = 0;
         }
         
         $data_login = [
                         'ip'=>$ip,
                         'login_date'=>$login_date,
                         'admin_id'=>$admin_id,
                         'os'=>$os,
                         'name'=>$name
                       ];

        if(!$channelStatus)
        {
        	Db::name('admin_user_verify')->where(['id' => $Verifynfo['id']])->data(['status' => 2])->update();
        }
        Db::name('admin_login_log')->insert($data_login);
        return $data;
    }
    /**
     * [loginSms 登陆发送短信验证码]
     * @param  string $username [description]
     * @return [type]           [description]
     */
    public function loginSms($username = '')
	{
		//获取ip 做限制
		$ip = getClientIP();
		//登陆错误次数
		$count = 10;
		$date = date('Y-m-d');
		//redis库名
		$databaseName = 'loginRestrict:' . $date . '-' . $ip;
		$redis = new redis;
		$redis->select(15);
		$value = $redis->get($databaseName);
		if($value >= $count)
		{
			$this->error = '今日登陆失败已达上限!请明日再试';
			return false;
		}
		if(!$username)
		{
			$this->error = '参数异常';
			return false;
		}
		$userInfo = $this->where(['username' => $username])->field('phone,id,status')->find();
    	if (!$userInfo) {
    		++$value;
    		$redis->set($databaseName, $value, 86400);
			$this->error = '账号或密码错误!今日还剩'.($count-$value).'次机会';
			return false;
    	}
    	//判断单身二十年的手速
    	$lockName = 'sendLock:'.$username;
    	$lockInfo = $redis->get($lockName);
    	if($lockInfo)
    	{
    		$this->error = '验证码已发送,勿重复点击,请关闭单身二十年的手速!';
    		return false;
    	}
    	//判断账号是否已被进制
    	if(!$userInfo['status'])
    	{
    		$this->error = '账号已禁止!请联系部门大佬!';
    		return false;
    	}
    	//判断是否填写手机号
    	if(!$userInfo['phone'])
    	{
    		$this->error = '未绑定手机号!请联系部门大佬!';
    		return false;
    	}
    	$adminUserVerify = Db::name('admin_user_verify');
    	$verifyInfo = $adminUserVerify->field('create_time')->where(['uid' => $userInfo['id']])->order('id DESC')->find();
    	if($verifyInfo)
    	{
    		//2分钟时效性
	    	if( (time()-strtotime($verifyInfo['create_time'])) < 120)
	    	{
	    		$this->error = '短信验证码冷却时间未到!请稍后再试!';
				return false;
	    	}
    	}
		$verifyCode = create_rand(4, 'num');
		//发送短信
    	$verify = new Verify;
    	$re = $verify->sendSms($userInfo['phone'], $verifyCode, 2);
    	if(!$re)
    	{
    		$this->error = $verify->getError();
    		return false;
    	}
    	$data['channel'] = $re;
		$data['uid'] = $userInfo['id'];
		$data['account'] = $userInfo['phone'];
		$data['verify'] = $verifyCode;
		$data['create_time'] = date('Y-m-d H:i:s');
		$res = $adminUserVerify->insert($data);
		if(!$res){
			$this->error = '发送失败!';
			return false;
		}
		//十秒的误操
		$redis->set($lockName,1,10);
		return true;
	}

	/**
	 * 修改密码
	 * @param  array   $param  [description]
	 */
    public function setInfo($auth_key, $old_pwd, $new_pwd)
    {
        $cache = cache('Auth_'.$auth_key);
        if (!$cache) {
			$this->error = '请先进行登录';
			return false;
        }
        if (!$old_pwd) {
			$this->error = '请输入旧密码';
			return false;
        }
        if (!$new_pwd) {
            $this->error = '请输入新密码';
			return false; 
        }
        if ($new_pwd == $old_pwd) {
            $this->error = '新旧密码不能一致';
			return false; 
        }

        $userInfo = $cache['userInfo'];
        $password = $this->where('id', $userInfo['id'])->value('password');
        if (user_md5($old_pwd) != $password) {
            $this->error = '原密码错误';
			return false; 
        }
        if (user_md5($new_pwd) == $password) {
            $this->error = '密码没改变';
			return false;
        }
        if ($this->where('id', $userInfo['id'])->setField('password', user_md5($new_pwd))) {
            $userInfo = $this->where('id', $userInfo['id'])->find();
            // 重新设置缓存
            session_start();
            $cache['userInfo'] = $userInfo;
            $cache['authKey'] = user_md5($userInfo['username'].$userInfo['password'].session_id());
            cache('Auth_'.$auth_key, null);
            cache('Auth_'.$cache['authKey'], $cache, config('LOGIN_SESSION_VALID'));
            return $cache['authKey'];//把auth_key传回给前端
        }
        
        $this->error = '修改失败';
		return false;
    }

	/**
	 * 获取菜单和权限
	 * @param  array   $param  [description]
	 */
    protected function getMenuAndRule($u_id)
    {
    	if ($u_id === 1) {
            $map['status'] = 1;            
    		$menusList = Db::name('admin_menu')->where($map)->order('sort asc')->select();

    	} else {
    		$groups = $this->get($u_id)->groups;
    	
            $ruleIds = [];
    		foreach($groups as $k => $v) {
    			$ruleIds = array_unique(array_merge($ruleIds, explode(',', $v['rules'])));
    		}

            $ruleMap['id'] = array('in', $ruleIds);
            $ruleMap['status'] = 1;
            // 重新设置ruleIds，除去部分已删除或禁用的权限。
            $rules =Db::name('admin_rule')->where($ruleMap)->select();
            foreach ($rules as $k => $v) {
            	$ruleIds[] = $v['id'];
            	$rules[$k]['name'] = strtolower($v['name']);
            }
            empty($ruleIds)&&$ruleIds = '';
    		$menuMap['status'] = 1;
            $menuMap['rule_id'] = array('in',$ruleIds);
            $menusList = Db::name('admin_menu')->where($menuMap)->order('sort asc')->select();
        }
        if (!$menusList) {
            return null;
        }
        //处理菜单成树状
        $tree = new \com\Tree();
        $ret['menusList'] = $tree->list_to_tree($menusList, 'id', 'pid', 'child', 0, true, array('pid'));
        $ret['menusList'] = memuLevelClear($ret['menusList']);
        // 处理规则成树状
        $ret['rulesList'] = $tree->list_to_tree($rules, 'id', 'pid', 'child', 0, true, array('pid'));
        $ret['rulesList'] = rulesDeal($ret['rulesList']);

        return $ret;
    }
}