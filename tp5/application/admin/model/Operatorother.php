<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;
use think\Model;
use think\Db;
class operatorother extends Model
{

   protected $appId = 2010219;
   protected $orgPrivateKey = '-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQDF2blV0r/fl4v9Y3wyEUq2T/aYG0OClHbcr495Bjc+NH9c7OC7
ldu81hvUYQDNIK/vA8GXZeccl+WcJCkRQIICGQwHVQGKjsvdwzXjtILenbF3IIU/
pdX+nZEr4hAT/oQ2ulZ3AXU0lYUxUJ965yS1geGN2ocQrqufcsnGloL6DwIDAQAB
AoGAIBT9A5in+la1oYH3SpsZNVTkFKgfG1Z9q61gI2zJmnGi0WuryV80gXtqidas
7ZZGUQiX5LfmcPGEjvwiGKUQL4PgJ5sPpVw2gjnEY9wehgrRrF5FH7o1Yext2FYH
8OT4nL9VpLtLDEr6eNTvrXFXyMiPbluR9m2LMXfsOwmB95ECQQDoPps/ZCBMl0aL
Ph8rohd7z5x1r/Eca4u73ARWaKteEqjFbtkn4Cqv5vw/VDdH6H8SRdozVIjZuR/n
R59x0L4XAkEA2hZ/l3/3qvlkW97eOdrfPN6SzziB++9fNHwqB5y9IpZexDXVeWv/
/thvt0JLL4a+Jfw7D4WLGIcVHUgfP45WyQJBAKtzvKzZwDaK31tgayaNLYULydfg
IcH3a1sunE0mTIzQfaPzp4EWrqzZe5NNGtqNO3JD+0g1uinxzymw6HbfwdUCQF19
7Z7F5ieumB5mUYAd2zud8vPQtrCJMpgublorEE3GhsO4jFZsPSdEI8g9LDwomFVy
ZuNFjZarOhr+cx7s5zkCQQDn1tsW2Py0TMLLeJyRY7bLyeBgCAj0vyHP58wesiTB
XVLs7uJEW7pPrhhQl4Ob6OxmVRI1tZesBW6b1bOMklB3
-----END RSA PRIVATE KEY-----';
    protected $rong360Url = 'https://openapi.rong360.com/gateway';
    

    
    public function operatorSend($bizData, $method)
    {/*{{{*/
        $params = array(
            'method'    => $method,
            'app_id'    => $this->appId,
            'version'   => '2.0',
            'sign_type' => 'RSA',
            'format'    => 'json',
            'timestamp' => time()
        );
        $params['biz_data'] = json_encode($bizData);

        $params['sign'] = $this->sign($this->getSignContent($params));
        //Yii::log('################################'.$params['sign']);

        $resp = $this->_crulPost($params, $this->rong360Url);\
        var_dump($resp);die;
        return ($resp);
    }/*}}}*/

    public function tianjiSend($bizData, $method)
    {/*{{{*/
        $params = array(
            'method'        => $method,
            'app_id'    => $this->appId,
            'version'   => '2.0',
            'sign_type' => 'RSA',
            'format'    => 'json',
            'timestamp' => time()
        );
        $params['biz_data'] = json_encode($bizData);

        $params['sign'] = $this->sign($this->getSignContent($params));
        //Yii::log('################################'.$params['sign']);
        $resp = $this->_crulPost($params, $this->rong360Url);
        return ($resp);
    }/*}}}*/
    
    protected function getSignContent($params)
    {/*{{{*/
        ksort($params);
    
        $i = 0; 
        $stringToBeSigned = "";
        foreach ($params as $k => $v) {
            if ($i == 0) {
                $stringToBeSigned .= "$k" . "=" . "$v";
            } else {
                $stringToBeSigned .= "&" . "$k" . "=" . "$v";
            }
            $i++;
        }
        unset ($k, $v);
        return $stringToBeSigned;

    }/*}}}*/
    
    protected function sign($data) 
    {/*{{{*/
        $res = $this->orgPrivateKey;
        openssl_sign($data, $sign, $res);
        $sign = base64_encode($sign);
        return $sign;
    }/*}}}*/
    
    private function _crulPost($postData, $url='https://openapisandbox.rong360.com/gateway')
    {/*{{{*/
        if(empty($url)){
            //Yii::log('openapi curl post数据时，目标url为空','error');
            return false;
        }
    
        try
        {
            $curl = curl_init();//创建一个新cURL资源，句柄
            curl_setopt($curl, CURLOPT_URL, $url);//需要获取的URL地址，也可以在curl_init()函数中设置。
            curl_setopt($curl, CURLOPT_POST, 1);//启用时会发送一个常规的POST请求，就像表单提交的一样。
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData));//全部数据使用HTTP协议中的"POST"操作来发送
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);//在启用CURLOPT_RETURNTRANSFER的时候，返回原生的（Raw）输出。
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);//启用时会将服务器服务器返回的"Location: "放在header中递归的返回给服务器
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//禁用后cURL将终止从服务端进行验证
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);//1 检查服务器SSL证书中是否存在一个公用名(common name)，2 检查公用名是否存在，并且是否与提供的主机名匹配。
            curl_setopt($curl, CURLOPT_SSLVERSION, 1);//使用的SSL版本(2 或 3)。默认情况下PHP会自己检测这个值，尽管有些情况下需要手动地进行设置。
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);//在发起连接前等待的时间，如果设置为0，则无限等待。10修改0
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);//设置cURL允许执行的最长秒数。
            $res = curl_exec($curl);//执行cURL会话 ->转webtest.php
            $errno = curl_errno($curl);//返回最后一次的错误代码 没有为0
            $curlInfo = curl_getinfo($curl);//获取一个cURL连接资源句柄的信息
            $errInfo = 'curl_err_detail: ' . curl_error($curl);//返回当前会话最后一次错误的字符串说明 （错误细节）
            $errInfo .= ' curlInfo:'. json_encode($curlInfo);//curl信息 . 返回值的JSON表示形式
            $arrRet = json_decode($res, true);//对 JSON 格式的字符串进行解码 参数为true将返回 array。
            $arrRet['errno']=$errno;
                        //统一记录日志
                        $logLevel = 'info';
                        if(!is_array($arrRet) || $arrRet['error']!=200) {//is_array = 查找变量是否是数组
                            $logLevel = 'warning';//warning = 警告
                        }
                        curl_close($curl);//关闭 cURL 会话
                    }catch(Exception $e)
                        {
                            print_r($e->getMessage());
                        }
                    //Yii::log("openapi curl post url: \t $url \t post: \t " . json_encode($postData) . " \t errno: $errno return: $res " . $errInfo, $logLevel);
                    if($arrRet['errno']==0){
                        return $arrRet;
                    }
                    return $arrRet;
                }/*}}}*/

}