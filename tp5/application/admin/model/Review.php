<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;
use app\admin\model\Documentary;
class Review extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'apply';
    protected $createTime = 'time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = DATETIME;
    // protected $insert = [
    //     'status' => 1,
    // ];

 
    /**
     * [getDataList 列表]
     * @AuthorHTL 湛慧平
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
    public function getDataList($keywords, $page, $limit, $valuedate, $os = '',$status="")
    {
        $map = [];
        if($valuedate)
        {
          $valuedate = substr($valuedate,0,10);
          $valuedate = date('Y-m-d',strtotime("$valuedate +1 day"));
          $map['time'] = ['like', '%'.$valuedate.'%'];
        }
        if ($keywords) {
            $map['phone|order_number|name'] = ['like', '%'.$keywords.'%'];
        }
        if($os)
        {
            $map['os'] = $os;
        }
        if(""!=$status)
        {
            $map['status'] = $status;
        }

        $admin_uid = $GLOBALS['userInfo']['id'];
        $map['check2_aid'] = $admin_uid;
        
       
       //获取后台管理用户id
       
       // $dataCount = $this->where('check2_aid = 1')->count('id');
       $dataCount = Db('apply')->where($map)->count();
       $list = Db('apply')->field('id,status,time,money,period,order_number,check_time,check_aid,check2_aid,name,credit_rate,service_rate,procedure_rate,id_card,phone,mobile_type,os')
                     ->order('status asc, second_claim_date desc,id desc')
                     ->where($map)
                     ->page($page, $limit)
                     ->select();
        foreach($list as $key=>$value)
        {
            $map1['id'] = $value['check_aid'];
            $map2['id'] = $value['check2_aid'];
            $list[$key]['user_admin1'] = Db::name('admin_user')->field('username')->where($map1)->find();
            $list[$key]['user_admin2'] = Db::name('admin_user')->field('username')->where($map2)->find();
            $list[$key]['admin_uid'] = $admin_uid;
            $list[$key]['is_black'] = model('BlackList')->is_black($value['id_card'], $value['phone']);
              $mapseq['apply_id'] = $value['id'];
              $mapseq['auth_admin'] = 1;
              $dataseq = Db::name('claim_seq')->field('seq')->where($mapseq)->find();
              if(!$dataseq)
              {
                $list[$key]['seq'] = '';
              }else{
                  $list[$key]['seq'] = $dataseq['seq'];
                }
        }
        
        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }

    /**
     * [getDataById 根据主键获取详情]
     * @linchuangbin
     * @DateTime  2017-02-10T21:16:34+0800
     * @param     string                   $id [主键]
     * @return    [array]
     */
    public function getDataById($id = '')
    {
        $data=$this
            ->field('id,money,period,credit_rate,service_rate,procedure_rate,overdue_rate')
            ->where('id',$id)
            ->find();
        return $data;
    }
    /**
     * [createData 新建]
     * @湛慧平
     * @DateTime  2017-02-10T21:19:06+0800
     * @param     array                    $param [description]
     * @return    [array]                         [description]
     */
    public function createData($param)
    {
         $uid = $param['uid'];
         $discount = $param['discount'];
         $order_number = $param['order_number'];
         $id = $param['id'];
         $map['order_number'] = $order_number;
         $check2_time = date('Y-m-d H:i:s',time());
         $admin_uid = $GLOBALS['userInfo']['id'];
         $data = ['status'=>2,'review'=>$discount,'check2_time'=>$check2_time];
         $admin_data = ['status'=>1,'uid'=>$uid,'date'=>$check2_time,
                       'admin_user_id'=>$admin_uid,'apply_id'=>$id,
                       'authstatus'=>2];
         Db::name('admin_user_clien')->insert($admin_data);
         $info = $this->save($data,$map);
         if(!$info)
         {
          return $info;
          exit;
         }
         return $info;
    }    


    /**
     * [createData 新建]
     * @湛慧平
     * @DateTime  2017-02-10T21:19:06+0800
     * @param     array                    $param [description]
     * @return    [array]                         [description]
     */
    public function xcreateData($param)
    {
         $uid = $param['uid'];
         $discount = $param['discount'];
         $order_number = $param['order_number'];
         $documentary = new Documentary;
         $re = $documentary->checkOrder($order_number);
         if(!$re)
         {
              $this->error = $documentary->getError();
              return false;
         }
         $id = $param['id'];
         $map['order_number'] = $order_number;
         $check2_time = date('Y-m-d H:i:s',time());
         $admin_uid = $GLOBALS['userInfo']['id'];
         $data = ['status'=>7,'review'=>$discount,'check2_time'=>$check2_time];
         $admin_data = ['status'=>0,'uid'=>$uid,'date'=>$check2_time,
                        'admin_user_id'=>$admin_uid,'apply_id'=>$id,
                        'authstatus'=>2];
         Db::name('admin_user_clien')->insert($admin_data);
         $info = $this->save($data,$map);
         if(!$info)
         {
          return $info;
          exit;
         }
         return $info;
         // $data = ['status'=>7,'review'=>$discount,'check2_time'=>$check_time];
         // $info = $this->save($data,$map);
         // if(!$info)
         // {
         //  return false;
         //  exit;
         // }
         //  return true;
    }
    /**
     * [updateDataById 编辑]
     * @linchuangbin
     * @DateTime  2017-02-10T21:24:49+0800
     * @param     [type]                   $param [description]
     * @param     [type]                   $id    [description]
     * @return    [type]                          [description]
     */
    public function updateDataById($param, $id)
    {
        $checkData = $this->get($id);
        if (!$checkData) {
            $this->error = '暂无此数据';
            return false;
        }

        // // 验证
        // $validate = validate($this->name);
        // if (!$validate->check($param)) {
        //     $this->error = $validate->getError();
        //     return false;
        // }

        try {
            $this->allowField(true)->save($param, [$this->getPk() => $id]);
            return true;
        } catch(\Exception $e) {
            $this->error = '编辑失败';
            return false;
        }
    }

    // public function getName($uid)
    // {
    //     return $this->where('uid',$uid)->value("name");
    // }
    // public function getPhone($uid)
    // {
    //     return $this->where('uid',$uid)->value("phone");
    // }    
    // public function resetDataById($id)
    // {
    //     // 不能操作超级管理员
    //     if ($id == 1) {
    //         $this->error = '非法操作';
    //         return false;
    //     }
    //     $checkData = $this->where('uid',$id)->find();
    //     if (!$checkData) {
    //         $this->error = '暂无此数据';
    //         return false;
    //     }
    //     /*if($checkData['status']!=1){
    //         $this->error = '该账户已被删除或禁用！';
    //         return false;
    //     }*/
    //     $param=array();
    //     $param['password']=123456;
    //     $param['password'] = user_md5($param['password'],$auth_key = 'qhdkb');
    //     $param['last_login_time']=date('Y-m-d H:i:s',time());;
    //     $res=Db::name('user')->where('uid',$id)->update($param);
    //     if($res){
    //         return true;
    //     }else{
    //         $this->error = '重置失败';
    //         return false;
    //     }
    // }
    // /**
    //  * 获取所有子孙
    //  */
    // public function getAllChild($id, &$data = [])
    // {
    //     $map['pid'] = $id;
    //     $childIds = $this->where($map)->column($this->getPk());
    //     if (!empty($childIds)) {
    //         foreach ($childIds as $v) {
    //             $data[] = $v;
    //             $this->getAllChild($v, $data);
    //         }
    //     }
    //     return $data;
    // }
    // public function enableData($ids = [], $status = 1, $delSon = false)
    // {
    //     if (empty($ids)) {
    //         $this->error = '修改失败';
    //         return false;
    //     }

    //     // 查找所有子元素
    //     if ($delSon && $status === '0') {
    //         foreach ($ids as $k => $v) {
    //             $childIds = $this->getAllChild($v);
    //             $ids = array_merge($ids, $childIds);
    //         }
    //         $ids = array_unique($ids);
    //     }
    //     $list=$this->where('id','in',$ids)->select();
    //     $list = modelo2array($list);
    //     $data['list'] = $list;
    //     $uids=array();
    //     foreach($data['list'] as &$v){
    //         $uids[]=$v['uid'];
    //     }
    //     try {
    //         Db::name('user')->where('uid','in',$uids)->setField('status', $status);
    //         return true;
    //     } catch (\Exception $e) {
    //         $this->error = '操作失败';
    //         return false;
    //     }
    // }
    
    // /**
    //  * [getInvitDataById 根据主键获取邀请人数详情]
    //  * @linchuangbin
    //  * @DateTime  
    //  * @param     string                   $id [主键]
    //  * @return    [array]
    //  */
    // public function getInvitDataById($uid,$page, $limit)
    // {
    //     $dataCount = $this->alias('a')
    //                 ->join('qh_invite_code b','a.uid = b.uid')
    //                 ->where('puid',$uid)
    //                 ->count('b.id');
        
    //     $invitName = $this->where('uid',$uid)->field('name')->find();
    //     $invitName = json_decode($invitName);
    //     $list = $this
    //     ->where(1);
    //     // 若有分页
    //     if ($page && $limit) {
    //         $list = $this->page($page, $limit);
    //     }
        
    //     $list=$this->alias('a')
    //     ->join('qh_invite_code b','a.uid = b.uid')
    //     ->field('a.name,a.qq,a.phone,a.zhima,b.create_time')
    //     ->where('b.puid',$uid)
    //     ->select();
        
    //     $list = modelo2array($list);
    //     $data['list'] = $list;
    //     $data['dataCount'] = $dataCount;
    //     $data['invitName'] = $invitName;
    //     return $data;
    // }

}