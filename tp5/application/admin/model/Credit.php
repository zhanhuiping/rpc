<?php
// +----------------------------------------------------------------------
// | Description:  征信报告
// +----------------------------------------------------------------------
// | Author: yhq <934797303@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;
use app\admin\model\ScoreLog;

class Credit extends Common
{
  protected $name = 'td_preloan_apply';
  public function __construct()
  {
    parent::__construct();
    if('127.0.0.1' == $_SERVER['SERVER_ADDR'] ){
      DEFINE('IS_TEST',true);
    }else{
      DEFINE('IS_TEST',false);
    }
  }

  /**
   * 提交资料
   * @author   yhq <934797303@qq.com>
   * @DateTime 2017-08-23T17:54:18+0800
   * 
   */
  public function tdSubmit($uid){
    # 数据库若存在报告，则直接返回数据
    $report = Db::name('td_preloan_report')->where('uid', $uid)->field('report_id, id')->find();
    if($report){
      return $report;
    }

    $param  = Db::name('user_info')->where('uid', $uid)->field('uid, name, id_card, phone')->find();
    $uid                      = $param['uid'];
    $data1['name']            = $param['name'];
    $data1['mobile']          = $param['phone'];
    $data1['id_number']       = $param['id_card'];
    $data1['loan_term_unit']  = 'DAY';
    $data1['create_time']     = time();
    $data1['create_time_str'] = date("Y-m-d H:i:s");

    $result = preloan_apply($data1,IS_TEST);
    if($result['success']==false){
      $this->error =  $result['reason_desc'];
      return false;
    }

    $data['name']            = $data1['name'];
    $data['id_number']       = $data1['id_number'];
    $data['mobile']          = $data1['mobile'];
    $data['report_id']       = $result['report_id'];
    $data['uid']             = $uid;
    $data['is_test']         = IS_TEST;
    $data["create_time"]     = time();
    $data["create_time_str"] = date('Y-m-d H:i:s',time());
    
    $add["report_id"]        = $result['report_id'];
    $add["uniacid"]          = 2;
    $add['is_test']          = IS_TEST;
    $add['uid']              = $uid;
    $add["create_time"]      = time();
    $add["create_time_str"]  = date('Y-m-d H:i:s',time());
    # 开启事务
    Db::startTrans();
    try {
      Db::name('td_preloan_apply')->insert($data);
      # 把记录添加到用户历史审核记录表里
      $id = Db::name('td_preloan_report')->insertGetId($add);
      # 写日志
      $scoreLogModel = new ScoreLog();
      $scoreLogModel->addlog($uid,$recored=0,$score=0,$opretion="setDec",$logid=1,$type="查征信");
      Db::commit();
      $report['id'] = $id;
      $report['report_id'] = $result['report_id'];
      return $report;
    } catch(\Exception $e) {
      Db::rollback();
      $this->error = '查询失败，请稍后重试';
      return false;
    }

  }


  /**
   * 刷新拉取报告
   * @author   yhq <934797303@qq.com>
   * @DateTime 2017-08-24T16:04:11+0800
   * @param    string       $report_id       报告id
   * @return   boolean                    
   */
  public function refresh($report_id=''){
    $status = Db::name('td_preloan_report')->where('report_id', $report_id)->value('status');
    if((int)$status===1){ 
      return true;
    }

    $result = preloan_report($report_id, IS_TEST);
    if ($result['success'] == false) {
      $this->error = $result['reason_desc'];
      return false;
    }
// dump($result);
// die;
    $add = $result;
    $add['apply_time']     = substr($result['apply_time'],0,-3);
    $add['report_time']    = substr($result['report_time'],0,-3);
    $add['risk_items']     = json_encode($result['risk_items'],JSON_UNESCAPED_UNICODE);
    $add['address_detect'] = json_encode($result['address_detect'],JSON_UNESCAPED_UNICODE);
    $add['status'] = 1;
    unset($add['success']);
    $where['report_id'] = $report_id;
    // $where['status'] = 0;
    $update = Db::name('td_preloan_report')->where($where)->update($add);
    if($update!==false){
      return true;
    }else{
      $this->error = '修改状态失败';
      return false;
    }
  }

}