<?php
// +----------------------------------------------------------------------
// | Description: 用户组
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use app\admin\model\Common;
use think\Db;
class Group extends Common 
{
    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
	protected $name = 'admin_group';

	/**
	 * [getDataList 获取列表]
	 * @linchuangbin
	 * @DateTime  2017-02-10T21:07:18+0800
	 * @return    [array]                         
	 */
	public function getDataList()
	{
		$cat = new \com\Category('admin_group', array('id', 'pid', 'title', 'title'));
		$datas = $cat->getList('', 0, 'id');
		$admin_id = $GLOBALS['userInfo']['id'];
		$group_id = Db::name('admin_access')->where(['user_id'=>$admin_id])->value('group_id');
		$data = [];
		if($group_id == config('group_id_rs')['group_id']){
			foreach ($datas as $key => $value) {

				if($value['id'] == 20){
                    
					$data[0]['id']      = $value['id'];
					$data[0]['title']   = $value['title'];
					$data[0]['rules']   = $value['rules'];
					$data[0]['pid']     = $value['pid'];
					$data[0]['remark']  = $value['remark'];
					$data[0]['status']  = $value['status'];
					$data[0]['p_title'] = $value['p_title'];
					$data[0]['else']    = $value['else'];
					$data[0]['level']   = $value['level'];
				}
			}
		}
		if($group_id != config('group_id_rs')['group_id']){
			return $data = $datas;
		}
		
		return $data;
	}
}