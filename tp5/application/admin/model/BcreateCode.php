<?php
// +----------------------------------------------------------------------
// | Description: 商家操作类
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class BcreateCode extends Common
{

    protected $name = 'binvite_code';
    protected $createTime = 'create_time';
    protected $updateTime = "update_time";
    protected $autoWriteTimestamp = 'datetime';
    protected $insert = [
        'status' => 1,
    ];
    public function getstatusAttr($value)
    {
        $data = ['1'=>"正常",'0'=>"禁用"];
        return $data[$value];
    }

    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
    public function getDataList()
    {
        $uid=$GLOBALS['userInfo']['id'];

        $list = $this
            ->where('bid',$uid)
            ->field('bcode,connect_bname,phone,bname')
            ->select();
        $list = modelo2array($list);
        $data['bcode'] = $list[0]['bcode'];
        $data['connect_bname'] = $list[0]['connect_bname'];
        $data['phone'] = $list[0]['phone'];
        $data['bname'] = $list[0]['bname'];
        $invite = Db::connect('db_business')->table('qh_system_config')->where(['name'=>'APP_INVITE_URL'])->value('value');
        $data['url'] = $invite.'?bcode='.$list[0]['bcode'];
  
        return $data;
    }

    /**
     * [getDataById 根据主键获取详情]
     * @linchuangbin
     * @DateTime  2017-02-10T21:16:34+0800
     * @param     string                   $id [主键]
     * @return    [array]
     */
    public function getDataById($id = '')
    {
        $data = $this->get($id);
        if (!$data) {
            $this->error = '暂无此数据';
            return false;
        }
        $data['groups'] = $this->get($id)->groups;
        return $data;
    }
    /**
     * 通过id修改用户
     * @param  array   $param  [description]
     */
    public function updateDataById($param, $uid)
    {
        
        $validate = validate('BcreateCode');
        if (!$validate->check($param)) {
            $this->error = $validate->getError();
            return false;
        }
        $list['update_time'] = date("Y-m-d H:i:s",time());
        $list['connect_bname'] = $param['connect_bname'];
        $list['phone'] = $param['phone'];
        $list['bname'] = $param['bname'];
        $re=$this->allowField(true)->save($list, ['bid' => $uid]);
        if($re){
            return true;
        }else{
            $this->error = '编辑失败';
            return false;
        }
    }

    /**
     * 创建用户
     * @param  array   $param  [description]
     */
    public function createCode($uid)
    {
        
        $arr=Db::name('admin_user')->where('id',$uid)->find();
        $row=Db::name('binvite_code')->where('bid',$uid)->find();
        if($row['bcode']){
            $this->error = '邀请码已经存在！';
            return false;
        }
        $bcode=$this->creatcode($length = 16);
        $data['bcode']=$bcode;
        $data['bid']=$uid;
        $data['bname']=$arr['username'];
        $data['connect_bname']=$arr['realname'];
        $data['create_time']=date("Y-m-d h:i:s",time());
        $data['pj_type'] = config('pj_type');
        $res = $this->create($data);
        if($res){
            $lsit['bcode'] = $bcode;
            $invite = Db::name('binvite_url')->where(['pj_type'=>config('pj_type')])->value('bcode_url');
            $lsit['url'] = $invite.'?bcode='.$bcode;
            return $lsit;
        }
    }

    /*生成bcode*/
    public function creatcode($length = 16)
    {
        do {
            $code = create_rand($length);
        } while ($this->where(array('bcode'=>$code))->count());

        return $code;
    }

}