<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Lunbo extends Common 
{	
    
	protected $name = 'lunbo';
   

    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
	public function getDataList($page, $limit)
	{
	    $dataCount = $this->count('id');
	    
	    $list = $this
	           ->where(1);
	    // 若有分页
	    if ($page && $limit) {
	        $list = $list->page($page, $limit);
	    }
		$list = $this
		        ->order('sort asc')
				->select();
		$list = modelo2array($list); 
		$data['list'] = $list;
		$data['dataCount'] = $dataCount;
		
		return $data;
	}

	/**
	 * [getDataById 根据主键获取详情]
	 * @linchuangbin
	 * @DateTime  2017-02-10T21:16:34+0800
	 * @param     string                   $id [主键]
	 * @return    [array]                       
	 */
	public function getDataById($id = '')
	{
		$data = $this->get($id);
		if (!$data) {
			$this->error = '暂无此数据';
			return false;
		}
		return $data;
	}
	/**
	 * 添加图片
	 * @param  array   $param  [description]
	 */
	public function createData($param)
	{
		// 验证
		$validate = validate($this->name);
		if (!$validate->check($param)) {
			$this->error = $validate->getError();
			return false;
		}
		$res = $this->data($param)->allowField(true)->save();
		if($res){
			return true;
		}
	}

	/**
	 * 通过id修改图片
	 * @param  array   $param  [description]
	 */
	public function updateDataById($param, $id)
	{
		if (!$param['id']) {
			$this->error = '暂无此数据';
			return false;
		}
		$id=$param['id'];

		$validate = validate($this->name);
		if (!$validate->check($param)) {
			$this->error = $validate->getError();
			return false;
		}
		$param['update_time'] = date("Y-m-d H:i:s",time()) ;
		$re=$this->allowField(true)->save($param, ['id' => $id]);
		if($re){
			return true;
		}else{
			$this->error = '编辑失败';
			return false;
		}
	}

}