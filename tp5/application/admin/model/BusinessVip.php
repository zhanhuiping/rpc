<?php
// +----------------------------------------------------------------------
// | Description: 商家操作类
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
class BusinessVip extends Common 
{	
    
	protected $name = 'business_vip';
   	/**
   	 * [getInfo 获取信息]
   	 * @param  string $field [description]
   	 * @param  array  $where [description]
   	 * @return [type]        [description]
   	 */
   	public function getInfo($field = '*' ,$where = [])
   	{
   		return $this->field($field)->where($where)->find();
   	}
   	/**
   	 * [saveInfo 更新套餐]
   	 * @param  array  &$param [description]
   	 * @param  [type] $where  [description]
   	 * @return [type]         [description]
   	 */
   	public function saveInfo(&$param = [],$where)
   	{
   		return $this->allowField(true)->save($param,$where);
   	}
   	/**
   	 * [addInfo 插入数据]
   	 * @param array &$param [description]
   	 */
   	public function addInfo(&$param = [])
   	{
   		return $this->allowField(true)->save($param);
   	}
}