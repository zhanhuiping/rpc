<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Mchase extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'apply';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = DATETIME;
    // protected $insert = [
    //     'status' => 1,
    // ];
  


    public function getsexAttr($value)
    {
        $type = [0=>'女',1=>'男',2=>"未知"];
        return $type[$value];
    }

    /**
     * [getDataById 根据主键获取详情]
     * @linchuangbin
     * @DateTime  2017-02-10T21:16:34+0800
     * @param     string                   $id [主键]
     * @return    [array]
     */
    public function getDataById($id = '')
    {
        $data=$this->alias('a')
            ->join('qh_user_info b','a.uid = b.uid')
            ->field('a.name,a.phone,b.age,b.sex,b.qq,b.zhima,b.province,b.id_card_img1,
                    b.id_card_img2,b.id_card_img3,b.tb_address,b.wx_wallet,b.alipay_bill,
                    b.wyjt_img,b.wyjt_hk_img,b.open_gid,b.id_card,b.mon_income,b.company,
                    b.company_ads,a.uid,a.money,a.time,a.check_aid,a.check_time,a.check2_aid,
                    a.check2_time,a.merchandiser,a.treasurer,a.fk_time,a.hk_time,a.period,
                    a.appoint_time,a.order_number,a.rate,a.overdue_rate,a.service_rate,
                    a.procedure_rate,a.credit_rate,a.review,a.remark,b.operator_outUniqueId,a.meets,b.uid,a.upload_img,a.status')
            ->where('a.id',$id)
            ->find();
         $uid = $data['uid'];
         $data['showlist'] = json_decode($data['meets']);
         $list = Db::name('admin_condition')->where("status=1 OR uid=$uid")->field('id,content')->select();
         $data['list'] = $list;
        // 初审员
         $mapa['id'] = $data['check_aid'];
         $data['check_username'] = Db::name('admin_user')->field('username')->where($mapa)->find();
        
        //复审员
        $map1['id'] = $data['check2_aid'];
        $data['check2_username'] = Db::name('admin_user')->field('username')->where($map1)->find();
         
        //跟单员
        $map2['id'] = $data['merchandiser'];
        $data['merchandiser'] = Db::name('admin_user')->field('username')->where($map2)->find();
        
        //财务员
        $map3['id'] = $data['treasurer'];
        $data['treasurer'] = Db::name('admin_user')->field('username')->where($map3)->find();
        $map['uid'] = $data['uid'];
  
        $username = Db::name('user')->where($map)->field('username')->find();
        $data['username']=$username;
        
        $bz['apply_id'] = $id;
        $bzs = Db::name('apply_bz')->field('is_friends,is_loans,remarks')->where($bz)->find();
        if(!$bzs)
        {
          $bzs['is_friends'] =0;
          $bzs['is_loans']   =0;
        }
        if(!$bzs['remarks'])
        {
            $bzs['remarks'] ='';
        }
        $data['bzs'] = $bzs;
        $list1 = Db::name('reminder_note')
                                 ->field('admin_uid,contact_name,reminder_status,days_overdue,reminder_time,contact_type')
                                 ->where($bz)nklkikidfee
                                 ->select();
        foreach($list1 as $key=>$value)
        {
             $mapnote['id'] = $value['admin_uid'];
             $admin = Db::name('admin_user')->where($mapnote)->field('username')->find();

             $list1[$key]['username'] = $admin['username']; 
             $data['list1'] = $list1;
        }
      
        return $data;
    }


}