<?php
/**
 * 黑名单
 */
namespace app\admin\model;

use think\Db;
use PHPExcel;
class PartnerList extends Common{

  protected $name = 'partner_list';
  protected $createTime = 'create_time';
  protected $updateTime = 'create_time';
  protected $autoWriteTimestamp = 'datetime';

    /**
     * 黑名单列表
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-11T16:44:14+0800
     * @param    string                   $keywords 关键字
     * @param    integer                  $page     页码
     * @param    integer                  $limit    每页数量
     * @return   array
     */
    public function getDataList($keywords='', $page=1, $limit=15)
    {
      $map = array();
      if(!empty($keywords))
        $map['phone|name'] = ['like', '%'.trim($keywords).'%'];
      $dataCount = $this->where($map)->count();
      $list = $this
      ->where($map)
      ->order('create_time desc')
      ->page($page, $limit)
      ->select();
      $list = modelo2array($list);
      foreach ($list as $key => $value) {
//        $list[$key]['create_user'] = $this->getNameById($value['create_uid']);
      }
      $data['list'] = $list;
      $data['dataCount'] = $dataCount;
      return $data;
    }


    /**
     * 添加黑名单
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-11T16:43:34+0800
     * @param    [type]                   $param [description]
     */
    public function addData($param,$admin_uid='')
    {
      try{
        $validate = validate($this->name);
        if (!$validate->check($param)) {
          $this->error = $validate->getError();
          return false;
        }
        $param['create_uid'] = $admin_uid;
        $id_card = $param['id_card'];
        $phone   = $param['phone'];

        if($id_card && $id_card!=''){
          $where['id_card'] = $id_card;
        }
        if($phone && $phone!=''){
          $where['phone'] = $phone;
        }

        $count = $this->where($where)->count();
        if($count>0){
          $info = $this->allowField(true)->save($param, $where);
        }else{
          $info = $this->data($param)->allowField(true)->isUpdate(false)->save();
        }
        if(!$info){
          $this->error = '添加黑名单失败';
          return false;
        }
        return true;   
      } catch (\Exception $e) {
        $this->error = '添加黑名单失败';
        return false;
      }
    }

    
    /**
     * 单个删除
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-11T16:43:14+0800
     * @param    [type]                   $param [description]
     * @return   [type]                          [description]
     */
    public function delData($param)
    {
      try{
        if($param['id']){ //根据id删除
          $where['id'] = $param['id'];
        }else if($param['phone']){ //根据手机号码或者身份证删除
          $where['phone'] = $param['phone'];
          if($param['id_card']){
            $where2['id_card'] = $param['id_card'];
          }
        }

        $del = $this->where($where)->whereOr($where2)->delete();
        if(!$del){
          $this->error = '删除黑名单失败';
          return false;
        }
        return true;
      } catch (\Exception $e) {
        $this->error = '删除黑名单失败';
        return false;
      }   
    }

    /**
     * 删除多个
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-11T16:42:36+0800
     * @param    [type]                   $param [description]
     * @return   [type]                          [description]
     */
    public function delDatas($param)
    {
      $this->startTrans();
      try{
        $where['id'] = array('in', $param['ids']);
        $del = $this->where($where)->delete();
        if(!$del){
          $this->error = '删除黑名单失败';
          return false;
        }
          // 提交事务
        $this->commit(); 
        return true;   
      } catch (\Exception $e) {
          // 回滚事务
        $this->rollback();
        $this->error = '删除黑名单失败';
        return false;
      }
    }


    /**
     * 将excel数据导入数据库
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-15T17:00:56+0800
     * @param    [type]                   $data [description]
     * @return   [type]                         [description]
     */
    public function import_excel($data, $admin_uid='')
    {
      if(!$data||is_null($data)){
        $this->error = '导入失败';
        return false;
      }
      // $this->startTrans();
      try{
        foreach ($data as $key => $value) {
          $param['name']    = $value[1];
          $param['phone']   = $value[2];
          $param['id_card'] = $value[3];
          // dump($param);
          $this->addData($param,$admin_uid);
        }
        // $this->commit(); 
        return true;   
      } catch (\Exception $e) {
        // $this->rollback();
        $this->error = '导入失败，请检查你的excel数据';
        return false;
      }

    }

    /**
     * 读取Excel并返回数据
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-14T11:22:28+0800
     * @param  string $file excel文件路径
     * @return array        excel文件内容数组
     */
    public function read_excel($file){
      if (!file_exists($file)) {
        $this->error = '文件读取失败';
        return false;
      }
      $type = pathinfo($file);
      $type = strtolower($type["extension"]);

      try{
        ini_set('max_execution_time', '0');
        //#方式一：下载phpexcel放在extend
        // \think\Loader::import('PHPExcel.PHPExcel');
        // \think\Loader::import('PHPExcel.PHPExcel.IOFactory');
        
        //#方式二：composer安装到vendor，头部 use PHPExcel;
        $objPHPExcel = new PHPExcel();

        if($type=='xls'){
          $objReader = \PHPExcel_IOFactory::createReader('Excel5');
        }else{
          $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        }

        $objPHPExcel = $objReader->load($file);
        $sheet = $objPHPExcel->getSheet(0);
        // 取得总行数
        $highestRow = $sheet->getHighestRow();
        // 取得总列数
        $highestColumn = $sheet->getHighestColumn();
        //循环读取excel文件,读取一条,插入一条
        $data=array();
        //从第2行开始读取数据
        for($j=2;$j<=$highestRow;$j++){
            //从A列读取数据
          for($k='A';$k<=$highestColumn;$k++){
                // 读取单元格
            $data[$j][]=$objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue();
          }
        }
        $data = array_merge($data);
        return $data;

      } catch (\Exception $e) {
        $this->error = '读取失败';
        return false;
      }
    }

    /**
     * 用户是否黑名单
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-11T14:54:50+0800
     * @param    string     $id_card 身份证号码
     * @param    string     $phone   手机号码
     * @return   integer    0:否  1:是                  
     */
    public function is_black($id_card = '', $phone = '')
    {
      $where['id_card']  = $id_card;
      $where2['phone']   = $phone;
      $count = $this->where($where)->whereOr($where2)->count();
      return $count>0 ? 1 : 0;
    }


  }
