<?php 
namespace app\admin\model;

use app\admin\model\Common;
class UcenterMember extends Common 
{	
	protected $name = 'ucenter_member';

	public function getlastLoginTimeAttr($value)
	{		
        return date('Y-m-d H:i:s',$value);
	}

	public function getstatusAttr($value)
	{		
		$data = [
			'1' => '正常',
			'0' => '禁用',
			'-1' => '删除',
		];
        return $data[$value];
	}
	/**
	 *获取商家账号管理
	 * @param array $where 
	 * @return array
	 */
	public function getManageList($where = [] ,$page = '1,10')
	{
		return $this->where($where)->page($page)->select();
	}
	/**
	 * [getListCount 获取总数]
	 * @param  [type] $where [description]
	 * @return [type]        [description]
	 */
	public function getListCount($where)
	{
		return $this->where($where)->count('id');
	}
	/**
	 * 获取一条信息
	 * @param  array  $where [description]
	 * @return [type]        [description]
	 */
	public function getMemberFind($where = [])
	{
		return $this->where($where)->find();
	}
	/**
	 * 删除记录
	 * @param  array  $where [description]
	 * @return [type]        [description]
	 */
	public function delMember($where = []) 
	{
		return $this->where($where)->delete();
	}
	/**
	 * [add 添加]
	 * @param [type] &$data [description]
	 */
	public function add(&$data)
	{
		return $this->insert($data);
	}
	/**
     * [saveData 保存数据]
     * @param  array  $where [description]
     * @param  [type] &$data [description]
     * @return [type]        [description]
     */
    public function saveData($where = [],&$data)
    {
        return $this->allowField(true)->save($data,$where);
    }
}
 ?>