<?php
// +----------------------------------------------------------------------
// | Description: 商家操作类
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Score extends Common 
{	
    
	protected $name = 'score';
    protected $createTime = 'create_time';
    protected $updateTime = false;
	protected $autoWriteTimestamp = 'datetime';
	protected $insert = [
		'status' => 1,
	];	
    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
	public function getDataList($keywords, $page, $limit)
	{
		$map = [];
		if ($keywords) {
			$map['b.phone'] = ['like', '%'.$keywords.'%'];
		}
		$dataCount = $this->alias('a')
            		 ->join('qh_user_info b','a.uid = b.uid','left')
					 ->where($map)
					 ->count('a.id');		
		
		$list = $this->alias('a')
            		 ->join('qh_user_info b','a.uid = b.uid','left')
					 ->where($map);
		
		// 若有分页
		if ($page && $limit) {
			$list = $list->page($page, $limit);
		}

		$list = $list 
				->field('*')
				->select();
		$list = modelo2array($list);
		$data['list'] = $list;
		$data['dataCount'] = $dataCount;
		return $data;
	}

	/**
	 * [getDataById 根据主键获取详情]
	 * @linchuangbin
	 * @DateTime  2017-02-10T21:16:34+0800
	 * @param     string                   $id [主键]
	 * @return    [array]                       
	 */
	public function getDataById($id = '')
	{
		$data = $this->get($id);
		if (!$data) {
			$this->error = '暂无此数据';
			return false;
		}
		$data['groups'] = $this->get($id)->groups;
		return $data;
	}
	/**
	 * 查询余额
	 * @zwr
	 * @DateTime 2017-7-5 14:41:16
	 * @param    int        $uid   用户uid
	 * @return   numeric            余额度
	 */
	public function balance($uid)
	{
		return $this->where('uid',$uid)->value('score');
	}
	/**
	 * 积分操作分发方法
	 * @zwr
	 * @DateTime 2017-7-5 14:36:16
	 * @param    int           $uid      用户id
	 * @param    numeric       $value    积分变动值
	 * @param    string        $type     积分变动类型  加积分传 setInc  减 setDec
	 * @return   bool                          
	 */
	public function handle($uid,$value,$type)
	{         

        if(!$uid){
			$this->error = "请选择用户";
			return false;
        }
		if(!is_numeric($value)&& $value < 0){
			$this->error = "积分值必须为正数";
			return false;
		}


        switch ($type) {
        	case 'setInc':
        		return $this->setIncScore($uid,$value);
        		break;
        	case 'setDec':
        		return $this->setDecScore($uid,$value);
        		break;        	
        	default:
        		$this->error = "操作类型错误";
        		return false;
        		break;
        }
	}
	/**
	 * 加积分操作方法
	 * @zwr
	 * @DateTime 2017-7-5 14:38:56
	 * @param    int         $uid      积分变动用户id
	 * @param    numeric     $value    积分变动值
	 * @return   bool  
	 */
	private function setIncScore($uid,$value)
	{	
		$res = $this->where('uid',$uid)->setInc('score', $value);
		if(!$res){
    		$this->error = "加余额失败";
    		return false;			
		}
		return true;
	}
	/**
	 * 减积分操作方法
	 * @zwr
	 * @DateTime 2017-7-5 14:38:56
	 * @param    int         $uid      积分变动用户id
	 * @param    numeric     $value    积分变动值
	 * @return   bool  
	 */	
	private function setDecScore($uid,$value)
	{
		$res = $this->where('uid',$uid)->setDec('score', $value);
		if(!$res){
    		$this->error = "减余额失败";
    		return false;			
		}
		return true;		
	}	


}