<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use app\v2\model\Apply;
use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;
use app\admin\model\Appview;

class Elesalemy extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'user_info';
    protected $createTime = 'time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = DATETIME;
    // protected $insert = [
    //     'status' => 1,
    // ];

    /**
     * [备注放弃信息]
     * @author cxr <1481746137@qq.com>
     */
    public function note(){

    }

    /**
     * 电销池列表
     * @author cxr <1481746137@qq.com>
     * @param $keywords
     * @param $page
     * @param $limit
     * @param $valuedate
     * @param string $os
     * @param string $status
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getDataList($keywords, $page, $limit, $valuedate, $os = '', $status = "", $type = null)
    {
        $map = [];
        #全局
        //不用固定是否申请
//        $map['b.uid'] = ['exp','is null'];
        $map['ele_status'] = 0; // 全部订单


        if ($valuedate) {
            $valuedate = substr($valuedate, 0, 10);
            $valuedate = date('Y-m-d', strtotime("$valuedate +1 day"));
            $map['create_time'] = ['like', '%' . $valuedate . '%'];
        }
        if ($keywords) {
            $map['a.phone|a.name'] = ['like', '%' . $keywords . '%'];
        }
        if ($os) {
            $map['a.os'] = $os;
        }

        # 完成度筛选
        if ($status) {
            $map += Elesale::getFilter($status);
        }

        #完成/放弃
        if ($type) {
            $type == '已完成' && $map['ele_status'] = 1;
            $type == '已放弃' && $map['ele_status'] = 2;
        }


        //获取后台管理用户id
        $admin_uid = $GLOBALS['userInfo']['id'];
        
        if($admin_uid != 1)
        {
            $map['ele_aid'] = $admin_uid;
        }
        else
        {
            $map['ele_aid'] = ['exp', 'is not null'];
        }

        $dataCount = $this->where($map)->alias('a')
            ->join('qh_apply b', 'a.uid = b.uid', 'left')->count('distinct a.id');
        $list = $this
            ->where($map);
        // 若有分页
        if ($page && $limit) {
            $list = $list->page($page, $limit);
        }


        $list = $list
            // ->field('id,status,time,money,period,order_number,check_time,check2_time,appoint_time,check2_aid,check_aid,name,uid,mobile_type,os,merchandiser_time,merchandiser2_time,fk_time,hk_time,monepassdate,lendmoneydate,first_claim_date,second_claim_date,third_claim_date,fourth_claim_date,fives_claim_date')
            ->field('a.*')
            ->with(['reason'])
            ->distinct('a.id')
            ->order('create_time desc,a.id desc')
            ->alias('a')
            ->join('qh_apply b', 'a.uid = b.uid', 'left')
            ->select();
        $list = modelo2array($list);
        // $dataCount = count($list);

        foreach ($list as $key => $value) {
            #是否完成/申请
            $list[$key]['complete'] = $this->isComplete($value['uid']);
            $list[$key]['apply'] = self::isApply($value['uid']);
            #是否放款
            $list[$key]['fk'] = $this->isFk($value['uid']);

            #合并状态信息
            $status = model('v2/UserInfo')->authStateByInfo($value);
            $list[$key] += $status;


            $mapseq['apply_id'] = $value['id'];
            $dataseq = Db::name('claim_seq')->field('seq')->where($mapseq)->find();
            if (!$dataseq) {
                $list[$key]['seq'] = '';
            } else {
                $list[$key]['seq'] = $dataseq['seq'];
            }

        }

        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }


    /**
     *电销完成
     * @author cxr <1481746137@qq.com>
     * @param $id
     * @param $admin_uid
     * @param $uid
     * @return array|bool
     * @throws \think\exception\PDOException
     */
    public function complete($id, $admin_uid, $uid)
    {

        #判断是否完成
        if (!$this->isComplete($uid)) {
            return ['code' => 'error', 'message' => '该订单未完成'];
        }

        $info = $this->get($id);
        $info->ele_status = 1;
        if ($info->save() != false) {
            return ['code' => 'success', 'message' => '操作成功'];
        }

        return ['code' => 'error', 'message' => '操作失败'];

    }

    /**
     * 电销放弃
     * @author cxr <1481746137@qq.com>
     * @param $id
     * @param $admin_uid
     * @param $uid
     * @return array|bool
     * @throws \think\exception\PDOException
     */
    public function giveup($id, $admin_uid, $uid, $reason)
    {

        #判断是否完成或放弃
        $info = $this->get($id);
        if ($info->ele_status) {
            return ['code' => 'error', 'message' => '该订单已完成或已放弃'];
        }

        #放弃理由
        $giveup = GiveupReason::get(['uid' => $uid]);
        if (!$giveup) {
            GiveupReason::create(['uid' => $uid, 'reason' => $reason]);
        } else {
            $giveup->save(['reason' => $reason]);
        }

        #放弃
        $info->ele_status = 2;
        if ($info->save() != false) {
            return ['code' => 'success', 'message' => '放弃成功'];
        }

    }

    /**
     * 电销通话备注
     * @author cxr <1481746137@qq.com>
     */
    public function remark($uid, $remark)
    {
        #备注
        $giveup = GiveupReason::get(['uid' => $uid]);
        if (!$giveup) {
            GiveupReason::create(['uid' => $uid, 'remark' => $remark]);
        } else {
            $giveup->save(['remark' => $remark]);
        }
        return true;
    }

    /**
     * 电销放弃理由
     * @author cxr <1481746137@qq.com>
     * @param $uid
     * @return array|bool
     * @throws \think\exception\PDOException
     */
    public function giveupMsg($uid)
    {

        $info = GiveupReason::get(['uid' => $uid]);
        if ($info->reason) {
            return ['code' => 'success', 'message' => $info->reason];
        }

    }


    /**
     *  电销是否完成（申请 放款）
     * @author cxr <1481746137@qq.com>
     * @param $uid
     * @return bool
     */
    public function isComplete($uid)
    {

        $apply = new appview();
        $map['uid'] = $uid;

        switch (config('ELESALE_COMPLETE')) {
            case '1' : //已放款
                $map['status'] = ['in', [3, 4, 5, 12]];
                break;
            case '2' : //已申请

                break;
        }


        $count = $apply->where($map)->count();
        if ($count) {
            return true;
        }
        return false;
    }

    /**
     *  是否放款
     * @author cxr <1481746137@qq.com>
     * @param $uid
     * @return bool
     */
    public function isFk($uid)
    {

        $apply = new appview();
        $map['uid'] = $uid;
        $map['status'] = ['in', [3, 4, 5, 12]];

        $count = $apply->where($map)->count();
        if ($count) {
            return true;
        }
        return false;
    }


    /**
     * 是否申请
     * @author cxr <1481746137@qq.com>
     * @param $uid
     * @return bool
     * @throws \think\exception\DbException
     */
    public static function isApply($uid)
    {
        $info = appview::get(['uid' => $uid]);
        if ($info) {
            return true;
        }
        return false;
    }

    /**
     * [getDataById 根据主键获取详情]
     * @linchuangbin
     * @DateTime  2017-02-10T21:16:34+0800
     * @param     string $id [主键]
     * @return    [array]
     */
    public function getDataById($id = '')
    {
        $data = $this
            ->field('id,money,period,credit_rate,service_rate,procedure_rate,overdue_rate')
            ->where('id', $id)
            ->find();
        return $data;
    }


    /**
     * 电销报表
     * @author cxr <1481746137@qq.com>
     * @param $start
     * @param $end
     * @return mixed
     */
    public function getReport($start, $end, $page, $limit = 10)
    {

        if (!$start) {

            $start_date = date('Y-m-d', time());
            $start = $start_date . ' 00:00:00';
            $end_date = date('Y-m-d', time() + 24 * 60 * 60);
            $end = $end_date . ' 00:00:00';

        }
        $admin = $GLOBALS['userInfo']['id'];


//        $value1 = $param['params']['value1'];
//        $value2 = $param['params']['value2'];
//        if(!$value1||!$value2)
//        {
//            $start_date = date('Y-m-d',time());
//            $value1     = $start_date.' 00:00:00';
//            $end_date = date('Y-m-d',time()+24*60*60);
//            $value2     = $end_date.' 00:00:00';
//        }else{
//            $value1 = date('Y-m-d H:i:s',strtotime($value1));
//            $value2 = date('Y-m-d H:i:s',strtotime($value2));
//        }
        $date = array('BETWEEN', [$start, $end]);  //时间段

//        p($date);exit;
        $map['ele_time'] = $date;
        if ($admin !== 1) {
            $map['ele_aid'] = $GLOBALS['userInfo']['id'];
        }


//        // 若有分页
//        if ($page && $limit) {
//            $this->page($page, $limit);
//        }
//var_dump($limit);exit;
        $dataCount = $this->where($map)->count('*');
        $info = $this->where($map)
            ->with(['reason'])
            ->order('ele_status asc,id desc')
            ->fetchSql(false)
            ->page($page, $limit)
            ->select();

        $info = collection($info)->each(function ($item) {
            $item['ele_user'] = $this->getNameById($item['ele_aid']);      //认领员
            return $item;
        });

//        return $info;
        $count = $this->where($map)->count();  //当天处理总数
        $count_x = $this->where($map)->where('ele_status = 1')->count();
        if (!$count) {
            $rate_pass = 0;
        } else {
            $rate_pass = ($count_x / $count) * 100;
            $rate_pass = substr($rate_pass, 0, 5);
        }
        $data['info'] = $info;
        $data['dataCount'] = $dataCount;
        $data['rate_pass'] = $rate_pass;
        return $data;

    }

    public function reason()
    {
        return $this->hasOne("GiveupReason", "uid", "uid");
    }

    /**
     * [createData 新建]
     * @湛慧平
     * @DateTime  2017-08-12T21:19:06+0800
     * @param     array $param [description]
     * @return    [array]                         [description]
     */
    public function createData($discount, $order_number, $multipleSelection, $uid, $id)
    {
        $map['order_number'] = $order_number;
        $multipleSelection = json_encode($multipleSelection);
        $check_time = date('Y-m-d H:i:s', time());
        // $admin_uid = $GLOBALS['userInfo']['id'];
        $admin_uid = 9999;
        $data = [
            'status' => 2,
            'remark' => $discount,
            'check_time' => $check_time,
            'meets' => $multipleSelection,
            'check2_aid' => 9999,
            'check2_time' => $check_time,
            'check2_time' => $check_time,
            'second_claim_date' => $check_time,
            'review' => '自动通过复审',
        ];
        $admin_data = ['status' => 1, 'uid' => $uid, 'date' => $check_time, 'admin_user_id' => $admin_uid, 'apply_id' => $id, 'authstatus' => 1];
        Db::name('admin_user_clien')->insert($admin_data);
        $info = $this->save($data, $map);
        if (!$info) {
            return $info;
            exit;
        }
        return $info;

    }


    /**
     * [createData 新建]
     * @湛慧平
     * @DateTime  2017-02-10T21:19:06+0800
     * @param     array $param [description]
     * @return    [array]                         [description]
     */
    public function xcreateData($discount, $order_number, $multipleSelection, $uid, $id)
    {
        $map['order_number'] = $order_number;
        $multipleSelection = json_encode($multipleSelection);
        $check_time = date('Y-m-d H:i:s', time());
        $admin_uid = $GLOBALS['userInfo']['id'];
        $admin_data = ['status' => 0, 'uid' => $uid, 'date' => $check_time, 'admin_user_id' => $admin_uid, 'apply_id' => $id, 'authstatus' => 1];
        $data = ['status' => 6, 'remark' => $discount, 'check_time' => $check_time, 'meets' => $multipleSelection];
        Db::name('admin_user_clien')->insert($admin_data);
        $info = $this->save($data, $map);
        if (!$info) {
            return false;
            exit;
        }
        return true;
    }

    /**
     * [updateDataById 编辑]
     * @linchuangbin
     * @DateTime  2017-02-10T21:24:49+0800
     * @param     [type]                   $param [description]
     * @param     [type]                   $id    [description]
     * @return    [type]                          [description]
     */
    public function updateDataById($param, $id)
    {
        $checkData = $this->get($id);
        if (!$checkData) {
            $this->error = '暂无此数据';
            return false;
        }

        // // 验证
        // $validate = validate($this->name);
        // if (!$validate->check($param)) {
        //     $this->error = $validate->getError();
        //     return false;
        // }

        try {
            $this->allowField(true)->save($param, [$this->getPk() => $id]);
            return true;
        } catch (\Exception $e) {
            $this->error = '编辑失败';
            return false;
        }
    }


    //添加自定义配置
    public function conditionadd($uid, $condition)
    {
        $data = ['uid' => $uid, 'content' => $condition];
        $info = Db::name('admin_condition')->insert($data);
        if (!$info) {
            return false;
            exit;
        }
        return true;
    }


    public function firstrevoked($param)
    {
        //判断撤销订单是否是最新订单
        $apply_info = $this->field('order_number,status,merchandiser2')->where(['uid' => $param['uid']])->order('time DESC')->find();
        if ($apply_info) {
            if ($apply_info['order_number'] !== $param['order_number']) {
                $this->error = '撤销失败,不是最新有效订单';
                return false;
            } elseif ($apply_info['status'] == 5 || ($apply_info['status'] == 3 && $apply_info['merchandiser2'] == '' && !$apply_info['transfer'])) {
                $this->error = '撤销失败,该订单已放款或已完成 1';
                return false;
            }

        } else {
            $this->error = '撤销失败,无效订单';
            return false;
        }

        $map['order_number'] = $param['order_number'];
        $info = $this->field('status,merchandiser,merchandiser2,treasurer,transfer')->where($map)->find();

        // 初审 认领/通过/驳回撤销 0申请 2复审通过
        if ($param['status'] == 1 || $param['status'] == 6 || $param['status'] == 0 || ($param['status'] == 2 && $info['merchandiser'] == '')) {
            // 初审通过且没有认领
            if ($info['status'] != 1 && $info['status'] != 6 && $info['status'] != 0 && ($info['status'] != 2 || $info['merchandiser'] != '')) {
                $this->error = "初审无法撤销";
                return false;
            }


            $this->startTrans();
            try {

                $data = ['status' => 0];

                //认领撤回
                if ($param['status'] == 0) {
                    $data['check_aid'] = NULL; //初审人员
                    $data['check_time'] = NULL;//认领时间
                    $data['first_claim_date'] = NULL; //初审认领时间
                }

                $info = $this->save($data, $map);
                $revoked = ['revoked' => 0];
                $admin_uid = $GLOBALS['userInfo']['id'];
                $where = ['apply_id' => $param['id'], 'authstatus' => 1, 'admin_user_id' => $admin_uid, 'revoked' => 1];
                Db::name('admin_user_clien')->where($where)->update($revoked);
                $this->commit();
                return true;
            } catch (\Exception $e) {
                // 回滚事务

                $this->rollback();
                $this->error = '撤销失败:' . $e->getMessage();
                return false;
            }


        }


        // 复审通过/驳回/撤销
        if (($param['status'] == 2 && $info['merchandiser'] == '') || $param['status'] == 7) {
            if ($info['status'] != 2 && $info['status'] != 7) {

                $this->error = "复审无法撤销";
                return false;
            }

            //开启事物
            $this->startTrans();
            try {

                $data = ['status' => 1];
                $info = $this->save($data, $map);
                $revoked = ['revoked' => 0];
                $admin_uid = $GLOBALS['userInfo']['id'];
                $where = ['apply_id' => $param['id'], 'authstatus' => 2, 'admin_user_id' => $admin_uid, 'revoked' => 1];
                Db::name('admin_user_clien')->where($where)->update($revoked);
                $this->commit();
                return true;
            } catch (\Exception $e) {
                // 回滚事务
                $this->rollback();
                $this->error = '撤销失败';
                return false;
            }

        }


        // M1 回访通过/或者驳回/认领/移交M2 撤销
        if ($param['status'] == 9 || $param['status'] == 10 || $param['status'] == 5 || $param['status'] == 2 || $param['status'] == 9 || ($info['transfer'] == 1 && $info['merchandiser2'] == '')) {
            if ($info['status'] != 9 && $info['status'] != 10 && $info['status'] != 5 && ($info['status'] != 2 || $info['merchandiser'] == '') && ($info['transfer'] != 1 || $info['merchandiser2'] != '')) {
                $this->error = "M1无法撤销";
                return false;
            }

            if ($info['status'] == 5) {

                //开启事物
                $this->startTrans();
                try {

                    $data = ['status' => 3];
                    $info = $this->save($data, $map);
                    $revoked = ['revoked' => 0];
                    $admin_uid = $GLOBALS['userInfo']['id'];
                    $where = ['apply_id' => $param['id'], 'authstatus' => 4, 'admin_user_id' => $admin_uid, 'revoked' => 1];
                    Db::name('admin_user_clien')->where($where)->update($revoked);
                    $this->commit();
                    return true;
                } catch (\Exception $e) {
                    // 回滚事务
                    $this->rollback();
                    $this->error = '撤销失败';
                    return false;
                }


            } elseif ($info['transfer'] == 1) {//移交撤回

                //开启事物
                $this->startTrans();
                try {

                    $data = [];
                    $data['transfer'] = 0;//移交字段
                    $data['transfer_info'] = NULL;//移交信息


                    $info = $this->save($data, $map);
                    $revoked = ['revoked' => 0];
                    $admin_uid = $GLOBALS['userInfo']['id'];
                    $where = ['apply_id' => $param['id'], 'authstatus' => 4, 'admin_user_id' => $admin_uid, 'revoked' => 1];
                    Db::name('admin_user_clien')->where($where)->update($revoked);
                    $this->commit();
                    return true;
                } catch (\Exception $e) {
                    // 回滚事务
                    $this->rollback();
                    $this->error = '撤销失败';
                    return false;
                }


            } elseif ($info['status'] == 2) {//认领撤回

                //开启事物
                $this->startTrans();
                try {

                    $data = [];
                    $data['merchandiser'] = NULL;//跟单人员
                    $data['third_claim_date'] = NULL;//回访认领时间


                    $info = $this->save($data, $map);
//                   $revoked = ['revoked'=>0];
//                   $admin_uid = $GLOBALS['userInfo']['id'];
//                   $where =['apply_id'=>$param['id'],'authstatus'=>4,'admin_user_id'=>$admin_uid,'revoked'=>1];
//                   Db::name('admin_user_clien')->where($where)->update($revoked);
                    $this->commit();
                    return true;
                } catch (\Exception $e) {
                    // 回滚事务
                    $this->rollback();
                    $this->error = '撤销失败' . $e->getMessage();
                    return false;
                }
            } else {// 回访通过/回访拒绝
                //开启事物
                $this->startTrans();
                try {

                    $data = ['status' => 2];
                    $info = $this->save($data, $map);
                    $revoked = ['revoked' => 0];
                    $admin_uid = $GLOBALS['userInfo']['id'];
                    $where = ['apply_id' => $param['id'], 'authstatus' => 4, 'admin_user_id' => $admin_uid, 'revoked' => 1];
                    Db::name('admin_user_clien')->where($where)->update($revoked);
                    $this->commit();
                    return true;
                } catch (\Exception $e) {
                    // 回滚事务
                    $this->rollback();
                    $this->error = '撤销失败';
                    return false;
                }
            }

        }

        //M2 撤销认领
        if (($param['status'] == 3 || $param['status'] == 4) && $info['transfer'] == 1) {

            $this->startTrans();
            try {

                $data = [];
                //认领撤回

                $data['merchandiser2'] = NULL; //M2人员
                $data['merchandiser2_time'] = NULL;//认领时间
                $data['fourth_claim_date'] = NULL; //M2认领时间


                $info = $this->save($data, $map);
                $revoked = ['revoked' => 0];
                $admin_uid = $GLOBALS['userInfo']['id'];
                $where = ['apply_id' => $param['id'], 'authstatus' => 1, 'admin_user_id' => $admin_uid, 'revoked' => 1];
                Db::name('admin_user_clien')->where($where)->update($revoked);
                $this->commit();
                return true;
            } catch (\Exception $e) {
                // 回滚事务

                $this->rollback();
                $this->error = '撤销失败:' . $e->getMessage();
                return false;
            }


        }


    }


    //财务撤回按钮
    public function finance($param)
    {
        //判断撤销订单是否是最新订单
        $apply_info = $this->field('order_number,status')->where(['uid' => $param['uid']])->order('time DESC')->find();
        if ($apply_info) {
            if ($apply_info['order_number'] !== $param['order_number']) {
                $this->error = '撤销失败,不是最新有效订单';
                return false;
            } elseif ($apply_info['status'] == 5 || $apply_info['status'] == 3) {
                $this->error = '撤销失败,该订单已放款或已完成';
                return false;
            }

        } else {
            $this->error = '撤销失败,无效订单';
            return false;
        }
        $map['order_number'] = $param['order_number'];
        $info = $this->field('status')->where($map)->find();
        // 财务放款/拒绝放款撤销按钮
        if ($param['status'] == 3 || $param['status'] == 11 || $param['status'] == 5 || $param['status'] == 9) {
            if ($info['status'] != 3 && $info['status'] != 11 && $info['status'] != 5 && $param['status'] != 9) {
                $this->error = "无法撤销";
                return false;
            }

            if ($info['status'] == 5) {
                $openuid['uid'] = $param['uid'];
                $data = ['status' => 3];
                $revoked = ['revoked' => 0];
                $admin_uid = $GLOBALS['userInfo']['id'];
                $where = ['apply_id' => $param['id'], 'authstatus' => 3, 'admin_user_id' => $admin_uid, 'revoked' => 1];
                $user_info = Db('user_info')->where($openuid)->field('open_gid')->find();
                $arrayopengid = json_decode($user_info['open_gid']);
                $countopenid = count($arrayopengid);
                //开启事物
                $this->startTrans();
                try {
                    $info = $this->save($data, $map);
                    Db::name('admin_user_clien')->where($where)->update($revoked);
                    if ($countopenid == 2) {
                        $dataopen = ['open_gid' => null];
                        Db::name('user_info')->where($openuid)->update($dataopen);
                    } else {
                        array_pop($arrayopengid);
                        $open_gid = json_encode($arrayopengid);
                        $dataopen = ['open_gid' => $open_gid];
                        Db::name('user_info')->where($openuid)->update($dataopen);
                    }
                    $this->commit();
                    return true;
                } catch (\Exception $e) {
                    // 回滚事务
                    $this->rollback();
                    $this->error = '撤销失败';
                    return false;
                }


            } elseif ($info['status'] == 9) {//认领撤回

                $this->startTrans();
                try {

                    $data = [];
                    $data['treasurer'] = NULL; //财务人员
                    $data['myloan_claim_date'] = NULL;//认领时间
                    $data['fives_claim_date'] = NULL; //财务认领时间

                    $info = $this->save($data, $map);
                    $revoked = ['revoked' => 0];
                    $admin_uid = $GLOBALS['userInfo']['id'];
                    $where = ['apply_id' => $param['id'], 'authstatus' => 3, 'admin_user_id' => $admin_uid, 'revoked' => 1];
                    Db::name('admin_user_clien')->where($where)->update($revoked);
                    $this->commit();
                    return true;
                } catch (\Exception $e) {
                    // 回滚事务
                    $this->rollback();
                    $this->error = '撤销失败';
                    return false;
                }

            } else {//拒绝放款/已放款
                //开启事物
                $this->startTrans();
                try {
                    //判断撤销订单中是否有申请中的订单
                    $count = $this->where(['uid' => $param['uid'], 'status' => ['in', '0,1,2,3,4,9,12']])->count();
                    if ($count > 0) {
                        $this->error = '撤销失败，该用户拥有有效订单';
                        return false;
                    }
                    $data = ['status' => 9, 'appoint_time' => null];
                    $info = $this->save($data, $map);
                    $revoked = ['revoked' => 0];
                    $admin_uid = $GLOBALS['userInfo']['id'];
                    $where = ['apply_id' => $param['id'], 'authstatus' => 3, 'admin_user_id' => $admin_uid, 'revoked' => 1];
                    Db::name('admin_user_clien')->where($where)->update($revoked);
                    $this->commit();
                    return true;
                } catch (\Exception $e) {
                    // 回滚事务
                    $this->rollback();
                    $this->error = '撤销失败';
                    return false;
                }
            }

        } else {
            $this->error = '撤销失败';
            return false;
        }
    }


    // 补充资料
    public function datainfo($param)
    {
        $admin_id = $GLOBALS['userInfo']['id'];
        $id = $param['id'];
        $uid = $param['uid'];
        $img = $param['img'];
        $datatype = $param['datatype'];
        $dataname = $param['dataname'];
        $datatime = date("Y-m-d H:i:s", time());
        $data = ['apply_id' => $id, 'admin_id' => $admin_id,
            'uid' => $uid, 'datacontent' => $img,
            'dataname' => $dataname, 'datatime' => $datatime,
            'datatype' => $datatype];

        $info = Db::name('data_supplement')->insert($data);
        if (!$info) {
            $this->error = "上传失败";
            return false;
        }
        $where['uid'] = $uid;


        $datainfo = Db::name('data_supplement')->where($where)->where('is_delete is null')->select();
        foreach ($datainfo as $key => $value) {
            $map['id'] = $value['admin_id'];
            $admin_user = Db::name('admin_user')->where($map)->field('username')->find();
            $datainfo[$key]['datauser'] = $admin_user['username'];
        }
        $data['datainfo'] = $datainfo;
        return $data;
    }

    //刪除
    public function datadetele($param)
    {
        $data = $param['datamultipleSelection'];
        $arr = array();
        foreach ($data as $key => $value) {
            $arr[] = $value['id'];
        }

        $this->startTrans();
        try {
            $where['id'] = array('in', $arr);
            $del = Db::name('data_supplement')->where($where)->update(['is_delete' => 3]);
            // 提交事务
            $this->commit();
            return true;
        } catch (\Exception $e) {
            // 回滚事务
            $this->rollback();
            $this->error = '删除失败';
            return false;
        }
    }

}
