<?php
// +----------------------------------------------------------------------
// | Description: 记录
// +----------------------------------------------------------------------
// | Author: lxb 
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Repayment extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'repayment';
 
    
    public function add(&$data)
    {
        
        return $this->insert($data);
    }

}