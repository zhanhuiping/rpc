<?php
// +----------------------------------------------------------------------
// | Description: 用户
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Userbrowse extends Common
{

    /**
     * 为了数据库的整洁，同时又不影响Model和Controller的名称
     * 我们约定每个模块的数据表都加上相同的前缀，比如微信模块用weixin作为数据表前缀
     */
    protected $name = 'user_browse';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = datetime;
    // protected $insert = [
    //     'status' => 1,
    // ];
    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
    public function getDataList($keywords, $page, $limit)
    {
        $map = [];
        if ($keywords) {
            $map['b.username'] = ['like', '%'.$keywords.'%'];
        }
        $dataCount = $this->alias('a')
            ->join('qh_user b','a.uid = b.uid','left')
            ->where($map)
            ->count('id');

        $list = $this
            ->where($map);
        // 若有分页
        if ($page && $limit) {
            $list = $this->page($page, $limit);
        }

        $list = $this->alias('a')
            ->join('qh_user_info b','a.uid = b.uid')
            ->join('qh_business c','a.bid = c.id')
            ->field('a.create_time,b.name,b.phone,c.bname')
            ->select();
        $list = modelo2array($list);
        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }
    
    public function getDataById($uid,$keywords, $page, $limit)
    {
        $map = [];
        if ($keywords) {
            $map['c.bname'] = ['like', '%'.$keywords.'%'];
        }
        $row['a.uid'] = $uid;
        $dataCount = $this->alias('a')
            ->join('qh_user b','a.uid = b.uid')
            ->where($row)
            ->count('id');
        
        $list = $this
            ->where($map);
        // 若有分页
        if ($page && $limit) {
            $list = $this->page($page, $limit);
        }
        
        $list = $this->alias('a')
            ->join('qh_user_info b','a.uid = b.uid')
            ->join('qh_business c','a.bid = c.id')
            ->where($row)
            ->where($map)
            ->field('a.create_time,b.name,b.phone,c.bname')
            ->select();
        $list = modelo2array($list);
        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }

}