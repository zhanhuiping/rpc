<?php
/**
 * 还款管理
 */

namespace app\admin\model;
use think\Db;

class Repayments extends Common
{
    protected $name = 'apply';
 
    /**
     * 还款管理列表
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-09T09:40:31+0800
     * @param     string    $keywords  关键字
     * @param     integer   $page      当前页数
     * @param     integer   $limit     每页数量
     * @return    json
     */
    public function getDataList($keywords, $page=1, $limit=15)
    {
        $map = array();
        if ($keywords) {
            $map['name'] = ['like', '%'.$keywords.'%'];
        }
        $map['status'] = array('in','3,4,5');
        $dataCount = $this->where($map)->count();
        $list = $this
            ->where($map)
            ->order('order_number,status')
            ->page($page, $limit)
            ->select();
        $list = modelo2array($list);
        foreach($list as $key=>$value)
        {
            $list[$key]['status'] = $this->setOverdue($value['id'],$value['appoint_time'],$value['status']);  //逾期处理
            $list[$key]['info']   = $this->statusInfo($list[$key]['status']); //状态信息
/*            $list[$key]['user_admin1'] = $this->getNameById($value['check_aid']);      //初审员
            $list[$key]['user_admin2'] = $this->getNameById($value['check2_aid']);     //复审员
            $list[$key]['user_admin3'] = $this->getNameById($value['treasurer']);      //财务员
            $list[$key]['user_admin4'] = $this->getNameById($value['merchandiser']);   //跟单员M1
            $list[$key]['user_admin5'] = $this->getNameById($value['merchandiser2']);  //催收员M2*/
        }
        $data['list'] = $list;
        $data['dataCount'] = $dataCount;
        return $data;
    }

    /**
     * 还款认领
     * @author   yhq <934797303@qq.com>
     * @DateTime 2017-08-07T16:31:55+0800
     * @param    integer    $id          申请单id
     * @param    integer    $admin_uid   管理员id
     * @return   boolean                              
     */
    public function claim($id,$admin_uid)
    {
       $data = array('merchandiser'=>$admin_uid);
       $update = $this->save($data, ['id' => $id]);
       if(!$update)
       {
        $this->error = '认领失败';
        return false;
       }
        return true;
    }

}