<?php
// +----------------------------------------------------------------------
// | Description: 商家操作类
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
use com\verify\HonrayVerify;

class Buserbrowse extends Common
{

    protected $name = 'buser_browse';
    protected $createTime = 'create_time';
    protected $updateTime = "update_time";
    protected $autoWriteTimestamp = 'datetime';
    protected $insert = [
        'status' => 1,
    ];
    public function getstatusAttr($value)
    {
        $data = ['1'=>"正常",'0'=>"禁用"];
        return $data[$value];
    }

    /**
     * [getDataList 列表]
     * @AuthorHTL
     * @DateTime  2017-02-10T22:19:57+0800
     * @param     [string]                   $keywords [关键字]
     * @param     [number]                   $page     [当前页数]
     * @param     [number]                   $limit    [t每页数量]
     * @return    [array]                             [description]
     */
    public function getDataList($page, $limit)
    {

        $dataCount = $this->count('id');
        
        $where=[];
        $id=$GLOBALS['userInfo']['id'];
        //      $id = 17;
        $row = Db::table('qh_admin_access')->where('user_id',$id)->field('group_id')->find();
        if($row['group_id'] == 20)
        {
            $where['b.bid']=$id;
        }elseif($id == 1 ){
            $where = 1;
        }
        
        $list = $this
            ->where(1);
        
        // 若有分页
        if ($page && $limit) {
            $list = $list->page($page, $limit);
        }

        $list = $list->alias('a')
            ->join('qh_binvite_code b','a.bcode = b.bcode')
            ->field('b.bname,a.id,a.login_ip,a.create_time,a.uuid,a.imsi,a.os')
            ->order('a.create_time desc')
            ->where($where)
            ->select();
        $list = modelo2array($list);
        $data['list'] = $list;
        $data['dataCount'] = $dataCount;

        return $data;
    }

    /**
     * [getDataById 根据主键获取详情]
     * @linchuangbin
     * @DateTime  2017-02-10T21:16:34+0800
     * @param     string                   $id [主键]
     * @return    [array]
     */
    public function getDataById($id = '')
    {
        $data = $this->get($id);
        if (!$data) {
            $this->error = '暂无此数据';
            return false;
        }
        $data['groups'] = $this->get($id)->groups;
        return $data;
    }
    /**
     * 通过id修改用户
     * @param  array   $param  [description]
     */
    public function updateDataById($param, $id)
    {
        if (!$param['id']) {
            $this->error = '暂无此数据';
            return false;
        }
        $id=$param['id'];
        if($param['status']=='正常'){
            $param['status']=1;
        }elseif($param['status']=='禁用'){
            $param['status']=0;
        }
        $validate = validate($this->name);
        if (!$validate->check($param)) {
            $this->error = $validate->getError();
            return false;
        }
        $param['update_time'] = date("Y-m-d h:i:s",time()) ;
        $re=$this->allowField(true)->save($param, ['id' => $id]);
        if($re){
            return true;
        }else{
            $this->error = '编辑失败';
            return false;
        }
    }

    /**
     * 创建用户
     * @param  array   $param  [description]
     */
    public function createCode($param)
    {
        $id=$param['id'];
        $uid=$GLOBALS['userInfo']['id'];
        if($id!=$uid){
            $this->error = '用户信息不正确！';
            return false;
        }
        $arr=Db::name('admin_user')->where('id',$id)->find();
        if($arr['bcode']){
            $this->error = '邀请码已经存在！';
            return false;
        }
        $bcode=$this->creatcode($length = 11);
        $data['bcode']=$bcode;
        $data['uid']=$id;
        $data['bname']=$arr['username'];
        $data['username']=$arr['realname'];
        $data['create_time']=date("Y-m-d h:i:s",time());
        $res = $this->create($data);
        if($res){
            return $bcode;
        }
    }

    /*生成bcode*/
    public function creatcode($uid,$length = 11,$aCode = "")
    {
        do {
            $code = create_rand($length);
        } while ($this->where(array('bcode'=>$code))->count());

        return $code;
    }

}