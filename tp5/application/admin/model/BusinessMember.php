<?php
// +----------------------------------------------------------------------
// | Description: 商家操作类
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honraytech.com>
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Db;
use app\admin\model\Common;
class BusinessMember extends Common 
{	
    
	protected $name = 'ucenter_member';
   
	/**
	*更改商户账号信息
	* @param array  $data
	* @param array  $param
	*/
	public function memberUpdateData($where = [] ,$param = [])
	{
		return $this->where($where)->update($param);
	}
	/**
	* 添加商户账号信息
	* @param array $param
	* @return mixd 
	*/
	public function addMemberInfo($param = [])
	{
		return $this->data($param)->allowField(true)->save();
	}

}