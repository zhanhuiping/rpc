<?php

/**
 * 行为绑定
 */
\think\Hook::add('app_init','app\\common\\behavior\\InitConfigBehavior');
use com\QRcode;


/**
 * 返回对象
 * @param $array 响应数据
 */
function resultArray(array $array = [])
{
    if(isset($array['data'])) {
        $array['error'] = '';
        $code = 200;
    } elseif (isset($array['error'])) {
        $code = 400;
        $array['data'] = '';
    }
    if(isset($array['code'])) {
        $code = $array['code'];
    }

    $isLogin = defined('IS_LOGIN') ? IS_LOGIN : 0;

    // 错误码处理 2018/4/11
    if(is_numeric($array['error'])){
        $code = (int)$array['error']; //错误码
        // 新增状态码请写在这里
        switch ($array['error']) {
            case '406':
                $array['error'] = '请先设置支付密码';
                break;
            default:
                break;
        } 
    }

    return [
        'code'  => $code,
        'data'  => $array['data'],
        'error' => $array['error'],
        'login' => $isLogin,  // 0：未登录 1：已登录
    ];
}



/**
 * 调试方法
 * @param  array   $data  [description]
 */
function p($data,$die=1)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if ($die) die;
}

/**
 * 用户密码加密方法
 * @param  string $str      加密的字符串
 * @param  [type] $auth_key 加密符
 * @return string           加密后长度为32的字符串
 */
function user_md5($str, $auth_key = '')
{
    return '' === $str ? '' : md5(sha1($str) . $auth_key);
}

function create_rand($length = 5, $type = 'all')
{
    $num = '0123456789';
    $letter = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if ($type == 'num') {
        $chars = $num;
    } elseif ($type == 'letter') {
        $chars = $letter;
    } else {
        $chars = $letter . $num;
    }

    $str = '';
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[mt_rand(0, strlen($chars) - 1)];
    }
    return $str;

}
function getClientIP()  
{  
    global $ip;  
    if (getenv("HTTP_CLIENT_IP"))  
        $ip = getenv("HTTP_CLIENT_IP");  
    else if(getenv("HTTP_X_FORWARDED_FOR"))  
        $ip = getenv("HTTP_X_FORWARDED_FOR");  
    else if(getenv("REMOTE_ADDR"))  
        $ip = getenv("REMOTE_ADDR");  
    else $ip = "Unknow";  
    return $ip;  
} 
function modelo2array($object){
    $data = [];
    foreach($object as $key=>$rowO){
        $data[$key] = $rowO->toArray();
    }
    return $data;
}
/**
 * 生成二维码
 * @param $data 二维码包含的文字内容
 * @param $filename 保存二维码输出的文件名称，*.png
 * @param bool $picPath 二维码输出的路径
 * @param bool $logo 二维码中包含的LOGO图片路径
 * @param string $size 二维码的大小
 * @param string $level 二维码编码纠错级别：L、M、Q、H
 * @param int $padding 二维码边框的间距
 * @param bool $saveandprint 是否保存到文件并在浏览器直接输出，true:同时保存和输出，false:只保存文件
 * return string
 */
function qrcode($data, $filename=false, $picPath = false, $logo = false, $size = '4', $level = 'L', $padding = 2, $saveandprint = false)
{
    vendor("phpqrcode.phpqrcode");//引入工具包
    // 下面注释了把二维码图片保存到本地的代码,如果要保存图片,用$fileName替换第二个参数false
    // $path = $picPath ? $picPath : __ROOT__ . "\\Uploads\\Picture\\QRcode"; //图片输出路径
    $path = './../Uploads/Picture/QRcode/';
    // echo $path;die;
    // $res = mkdir($path);
    if(!is_dir($path)) {
        mkdir($path);
    }
    if(!is_writeable($path)) {
        echo $path;die;
        header('Content-Type:text/html; charset=utf-8');
        exit('目录 [ '.$path.' ] 不可写！');
    }
    //dump($path);exit;
    //在二维码上面添加LOGO
    if (empty($logo) || $logo === false) { //不包含LOGO
        if ($filename == false) {
            QRcode::png($data, false, $level, $size, $padding, $saveandprint); //直接输出到浏览器，不含LOGO
        } else {
            $filename = $path . '/' . $filename; //合成路径
            QRcode::png($data, $filename, $level, $size, $padding, $saveandprint); //直接输出到浏览器，不含LOGO
        }
    } else { //包含LOGO
        if ($filename == false) {
            // $filename=tempnam('','').'.png';//生成临时文件
            die(L('_PARAMETER_ERROR_'));
        } else {
            //生成二维码,保存到文件
            $filename = $path . '\\' . $filename; //合成路径
        }
        QRcode::png($data, $filename, $level, $size, $padding);
        $QR = imagecreatefromstring(file_get_contents($filename));
        $logo = imagecreatefromstring(file_get_contents($logo));
        $QR_width = imagesx($QR);
        $QR_height = imagesy($QR);
        $logo_width = imagesx($logo);
        $logo_height = imagesy($logo);
        $logo_qr_width = $QR_width / 5;
        $scale = $logo_width / $logo_qr_width;
        $logo_qr_height = $logo_height / $scale;
        $from_width = ($QR_width - $logo_qr_width) / 2;
        imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        if ($filename === false) {
            Header("Content-type: image/png");
            imagepng($QR);
        } else {
            if ($saveandprint === true) {
                imagepng($QR, $filename);
                header("Content-type: image/png");//输出到浏览器
                imagepng($QR);
            } else {
                imagepng($QR, $filename);
            }
        }
    }
    return $filename;
}


function get_short_url($url_long){
    $url = "http://api.t.sina.com.cn/short_url/shorten.json?source=3213676317&url_long=".$url_long;
    $res = file_get_contents($url);
    $res_arr = json_decode($res,true);
    if($res_arr[0]['url_short']){
        return $res_arr[0]['url_short'];
    }else{
        return $url_long;
    }
}


/**
 * curl get请求
 * @param  [type] $url [description]
 * @return [type]      [description]
 */    
   function httpGet($url) 
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
     
        $res = curl_exec($curl);
        curl_close($curl);
        return $res;
    }



/**
 * 远程获取数据，POST模式
 * 注意：
 * @param $url 指定URL完整路径地址
 * @param $para 请求的数据
 * return 远程输出的数据
 */
function curlPost($url, $para) {
    $json = json_encode($para);     
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //信任任何证书
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");                          
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(                 
        'Content-Type: application/json',
        'Content-Length: ' . strlen($json))         
    );
    $responseText = curl_exec($curl);
    curl_close($curl);
    return $responseText;
}

/**
 * 远程获取数据，POST模式 form表单提交方式
 * 注意：
 * @param $url 指定URL完整路径地址
 * @param $para 请求的数据
 * return 远程输出的数据
 */
function curlPostForm($url, $para) {
    $param = http_build_query($para);     
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //信任任何证书
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");                          
    curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(                 
        'text/html; charset=utf-8',
        'Content-Length: ' . strlen($param))         
    );
    $responseText = curl_exec($curl);
    curl_close($curl);
    return $responseText;
}


/**
 * 金额处理
 */
function  formatMoney($money){
    if(0 < $money && $money < 0.01) {
        $money = '0.01';
    }
    $money = floatval(round($money, 2));  //四舍五入取两位小数
    return $money;
}

/**
 * 检测时间字符串是否是否是合法的日期模式
 * @param string $value [description]
 */
function checkDateTime($datetime)
{
    $datetime = (string)$datetime;
    if(date('Y-m-d H:i:s',strtotime($datetime)) == $datetime){
        return true;
    }
    return false;
}


/**
 * 手动推送回调
* @param string|array $param  请求参数  入参为数组或者json字符串
 */
function pushNotify($url, $param)
{
    if(!is_array($param) ){
        $param = json_decode($param, true);
    }
    
   return curlPost($url, $param);
}

/**
 * 写入日志文件
 * 会自动按照日期归档
 * @param  [string] $logName          [日志名称]
 * @param  [string] $result           [结果描述 成功或者失败]
 * @param  [string|array] $details    [详情]
 * @param  [boolean] $file_append     [是否追加模式]
 * @example writeLog('test', '成功', ['id'=>1,'id'=>2])
 * @return [boolean]
 */
function writeLog($logName, $result='', $details='', $file_append=true){
    if(is_array($details)){
        $detail = json_encode($detail);
    }
    if($file_append===true || $file_append==='true'){
        $file_append = FILE_APPEND;
    }else{
        $file_append = null;
    }

    $res = @file_put_contents(LOGS_DIR_NAME . $logName . '.log', '时间:' . date('Y-m-d H:i:s', time()) . " 结果:". $result ." 详情:" . $details . "\n\n", $file_append);
    if(!$res){
        return false;
    }
    return true;
}

function resultArray_swoole(array $array = [])
{
    if(isset($array['data'])) {
        $array['error'] = '';
        $code = 200;
    } elseif (isset($array['error'])) {
        $code = 400;
        $array['data'] = '';
    }

    if(isset($array['code'])) {
        $code = $array['code'];
    }

    // 处理code=null的情况
    if(is_null($code)) {
        $array['error'] = '代码异常';
    }

    // 错误码处理 2018/4/11
    if(is_numeric($array['error'])){
        $code = (int)$array['error']; //错误码
        // 新增状态码请写在下面switch
        switch ($array['error']) {
            case '406':
                $array['error'] = '请先设置支付密码';
                break;
            case '1001':
                $array['error'] = '无效的token';
                break;                
            case '1002':
                $array['error'] = 'token过期';
                break;                
            default:
                $array['error'] .= '未定义的状态码';
                break;
        } 
    }

    $isLogin = defined('IS_LOGIN') ? IS_LOGIN : 0;

   $data = [
        'code'  => $code,
        'data'  => $array['data'],
        'error' => $array['error'],
        'login' => $isLogin,  // 0：未登录 1：已登录
    ];

    $data =json_encode($data,JSON_UNESCAPED_SLASHES);
    echo $data;
 
}


