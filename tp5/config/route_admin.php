<?php
// +----------------------------------------------------------------------
// | Description: 基础框架路由配置文件
// +----------------------------------------------------------------------
// | Author: linchuangbin <linchuangbin@honghaiweb.com>
// +----------------------------------------------------------------------


return [
    // 定义资源路由
    '__rest__'=>[


    ],
    'admin/LoginServer/login'         =>  ['admin/LoginServer/login', ['method' => 'POST|GET']],
    'admin/LoginClient/login'         =>  ['admin/LoginClient/login', ['method' => 'POST|GET']],

    'admin/PayServer/pay'             =>  ['admin/PayServer/pay', ['method' => 'POST|GET']],
    'admin/PayClient/pay'             =>  ['admin/PayClient/pay', ['method' => 'POST|GET']],
  

];