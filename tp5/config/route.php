<?php
/**
 * api 路由定义
 */
use think\Route;
// 调用v2模块 
// 
/*账户相关路由*/
Route::any('index/index','index/index/zhanhuiping');   // 注册短信
Route::post('v2/sendRegVoiceVerify','v2/account/sendRegVoiceVerify');   // 注册语音短信
Route::post('v2/register','v2/account/register'); 		    // 注册
Route::post('v2/FirstRegister','v2/account/newRegister'); 		    // 推广第一步注册
Route::post('v2/secondRegister','v2/account/secondRegister'); 		    // 推广第二步注册
Route::post('v2/login','v2/account/login'); 				    // 登陆
Route::post('v2/sendFindVerify','v2/account/sendFindVerify'); // 找回密码短信
Route::post('v2/changPassword','v2/account/changPassword');   // 找回密码
Route::post('v2/feedback','v2/account/feedback');             // 反馈
Route::post('v2/memberCenter','v2/Account/memberCenter');     // 个人中心
Route::get('v2/index2','v2/Account/index2');                // mongodb读取
Route::post('v2/create','v2/Account/create');                // mongodb插入
Route::post('v2/changePayPassword','v2/account/changePayPassword');   // 修改支付密码
Route::post('v2/download','v2/Account/download');                // 记录下载
Route::post('v2/myoverdue','v2/User/getMyOverdueDays');                // 已逾期


/*用户路由*/
Route::post('v2/setUserInfo','v2/user/update');  		    // 修改个人信息 
Route::post('v2/myTeam','v2/user/myTeam'); 				// 我的团队
Route::post('v2/getInviteInfo','v2/user/getInviteInfo');  // 邀请链接
Route::post('v2/userScore','v2/user/score');  			// 用户余额          		
Route::post('v2/getBrowseList','v2/user/getBrowseList');  // 用户浏览历史          		        		
Route::post('v2/myInfo','v2/user/myInfo');                // 用户资料   
Route::post('v2/operatorAuth','v2/user/operatorAuth');    // 运营商认证
Route::post('v2/Operator','v2/user/Operator');            // 是否验证运营商
Route::post('v2/authState','v2/user/authState');            // 认证状态
Route::post('v2/sixnum','v2/user/checkIdLastSix');            // 验证身份后六位数
Route::any('v2/returnUrl','v2/user/returnUrl');          // 获取通话详单唯一标识  
Route::any('v2/notifyUrl','v2/user/notifyUrl');          // 获取用户报告详情  

/*商户*/
Route::post('v2/getList','v2/merchant/getList');  // 商户列表
Route::post('v2/details','v2/merchant/details');  // 商户详情

/*提现*/
Route::post('v2/createWithdraw','v2/score/createWithdraw');  // 申请提现
Route::post('v2/txHistory','v2/score/txHistory');            // 提现历史

/*系统配置*/
Route::post('v2/getAppConfigs','v2/config/getAppConfigs');  // 获取系统配置
Route::post('v2/getLunbo','v2/config/getLunbo');            // 获取轮播图

/*征信*/
Route::post('v2/tddoPreloanCheck','v2/watermelon/creditReport');  // 征信审核信息提交
Route::post('v2/tdhistory','v2/watermelon/reportList');                // 征信历史审核列表
Route::post('v2/tdreport','v2/watermelon/report');                  // 征信审核详情报告
Route::post('v2/tdrefresh','v2/watermelon/refresh');                // 征信审核刷新列表页按钮
/*商家链接用户浏览 注册*/
Route::post('v2/businessRegister','v2/baccount/businessRegister');  // 商家链接注册
Route::post('v2/addBrowse','v2/baccount/addBrowse');                // 商家链接注册

/*用户贷款申请*/
Route::post('v2/loanDetails','v2/apply/loanDetails');   // 借款明细
Route::post('v2/apply','v2/apply/apply');         // 申请
Route::post('v2/myApply','v2/apply/myApply');     // 我的借款列表
Route::post('v2/overdow_day','v2/apply/overdow_day');     // 首页违约天数，剩余邮寄时间
Route::post('v2/myApplyDetails','v2/apply/myApplyDetails');     // 我的借款详情
Route::post('v2/countdown','v2/apply/countdown');     // 审核不通过再次借款倒计时

/*首页*/
Route::post('v2/getCard','v2/Home/getCard');  // 首页卡片数据
Route::post('v2/getGear','v2/Home/getGear');  // 首页档位数据

/*消息*/
Route::post('v2/messpages','v2/message/messpages'); //列表
Route::post('v2/messred','v2/message/messred'); //阅读
Route::post('v2/messageinfo','v2/message/messageinfo'); //消息详情接口

/*图片上传*/
Route::post('v2/uploadBase64','admin/Upload/uploadBase64');  // base64格式上传 
Route::post('v2/uploadQiniu','admin/Upload/uploadQiniu');    // input表单上传
Route::post('v2/qiniuToken','v2/Config/qiniuToken');    // 获取七牛token

/*接收客户端的值*/
Route::post('v2/teladdress','v2/Myinfo/Myinfo'); //通讯录

/*token 验证接口*/
Route::post('v2/tokenverify','v2/account/tokenverify');   // 注册短信

Route::get('v2/pic','v2/account/pic'); // 图片验证码

/*支付  路由*/
Route::post('v2/pay','v2/Order/createorder'); //创建支付链接
Route::any('v2/notify','v2/Order/notify');    //支付回调
Route::any('v2/successPage','v2/Order/successPage');  //支付成功页面 
Route::post('v2/payAction','v2/Order/payAction');  //还款成功后的操作
Route::post('v2/queryPay','v2/Order/queryPay');  //查询订单支付情况
Route::post('v2/BinQuery','v2/Order/BinQuery');  //查询银行卡信息
Route::post('v2/bankcardbindlist','v2/Order/bankcardbindlist');  //查询签约信息
Route::any('v2/setDefaultBankCard','v2/Order/setDefaultBankCard');  //设置默认银行卡
Route::post('v2/bankcardbind','v2/Order/bankcardbind');     //银行卡签约申请
Route::post('v2/saveBankCard','v2/Order/saveBankCard');      //保存银行卡
Route::post('v2/bankcardunbind','v2/Order/bankcardunbind');  //银行卡解约
Route::post('v2/payRecord','v2/Order/payRecord');  //支付记录

/*打款*/
Route::any('v2/paymentOeder','v2/Order/create_payment_oeder'); //创建支付订单
Route::any('v2/payment','v2/Order/payment'); 					// 发起支付
Route::any('v2/notify_payment','v2/Order/notify_payment'); //打款回调
Route::any('v2/confirm_payment','v2/Order/confirm'); // 疑似订单确认
Route::any('v2/queryPayment','v2/Order/queryPayment'); //订单结果查询

Route::post('v2/repayment','v2/apply/repayment'); // 还款页面 
Route::any('v2/contract','v2/apply/contract');    // 查询合同

Route::any('v2/hanlerrepay','v2/Order/repayment'); // 处理自动打款 
/*反馈*/
Route::post('v2/feedback','v2/FeedBack/add'); // 写入 

/*定时脚本*/
Route::any('v2/savePhoneList','v2/Script/savePhoneList');               // 保存需要发送短信的手机号 插入Redis队列【每天9点执行一次】
Route::any('v2/sendMessages','v2/Script/sendMessages');                 // 从队列取出并逐一发送短信【每分钟执行一次】
Route::any('v2/handOverdueScript','v2/Script/handOverdueScript');       // 逾期处理的脚本【不自动执行】
Route::any('v2/pullNotifyUrl','v2/Script/pullNotifyUrl');               // 通话详单异步回调【主动拉取脚本，每分钟执行一次】
Route::any('v2/changeOS','v2/Script/changeOS');                         // 修改user_info系统
Route::any('v2/changeOS_test','v2/Script/changeOS_test');               // 修改user_info系统
Route::any('v2/saveOverdueOrderNumberList','v2/Script/saveOverdueOrderNumberList');    // 逾期订单入队列【每天00:01执行一次】
Route::any('v2/costOverdueOrderNumber','v2/Script/costOverdueOrderNumber');            // 消费逾期订单队列【每分钟执行一次】
Route::any('v2/popPayUserOrderList','v2/Script/popPayUserOrderList');            // 消费打款订单队列【每分钟执行50条】
Route::any('v2/Invite','v2/Script/Invite');            //
Route::any('v2/getRedisInfo','v2/Script/getRegistInfoFromRedis');            //取redis的注册第一步和注册第二步统计数据

// 云峰风控
Route::any('v2/yunFengUrl','v2/YunFengRisk/getUrl'); //获取云峰链接
Route::any('v2/yunFengResult','v2/YunFengRisk/getResult'); //获取云峰结果
Route::any('v2/yunFengRiskNotify','v2/YunFengRisk/notify'); //云峰回调
Route::any('v2/fengkongNotify','v2/YunFengRisk/fengkongNotify'); //云峰风控模型回调

// qq同步助手
Route::any('v2/sesame_qqtb_verify','v2/Sesame/qqtb_verify'); //注册qq同步助手注册
Route::any('v2/sesame_qqtb_home','v2/Sesame/qqtb_home');     //获取通讯录或者短信
Route::any('v2/sesame_qqtb_directories','v2/Sesame/qqtb_directories');     //获取通讯录或者短信
Route::any('v2/sesame_is_fifeen','v2/Sesame/is_fifeen');     //获取通讯录或者短信
Route::any('v2/sesame_Sesame','v2/Sesame/Sesame');     //芝麻分
Route::any('v2/sesame_notifyurl','v2/Sesame/get_sesame_notifyurl'); //芝麻分回调

Route::post('v2/delUser','v2/user/delUser');   // 删除帐号-测试专用
Route::post('v2/delApply','v2/user/delApply');   // 删除上一笔订单-测试专用
Route::any('v2/ceshi','v2/Script/ceshi');   // 测试

// 人脸识别
Route::any('v2/ocr','v2/IdcardOcr/ocr');   // 身份证ocr测试
Route::any('v2/getNumber','v2/IdcardOcr/getNumber');   // 获取语音数字
Route::any('v2/validateVideo','v2/IdcardOcr/validateVideo');   // 上传视频
Route::any('v2/faceVerify','v2/IdcardOcr/faceVerify');   // face认证
Route::post('v2/face','v2/Face/faceVerify');   // face认证SDK版

Route::any('v2/ocrInfo','v2/IdcardOcr/ocrInfo');   // 获取身份证OCR信息

Route::any('v2/ocrReturn','v2/IdcardOcr/returnUrl');          // 获取通话详单唯一标识
Route::any('v2/ocrNotify','v2/IdcardOcr/notifyUrl');          // 获取用户报告详情

Route::any('v2/getAcountInfoApi','v2/VoiceNotice/getAcountInfoApi');          // 获取用户报告详情
Route::post('v2/queryOrder','v2/order/hcQueryOrder');          // 查询汇潮订单

Route::any('v2/notify_auth','v2/ApplyOrder/ApplyNotify'); //认证收费回调

Route::any('v2/contractNotify','v2/FuIou/contractNotify'); //签约异步回调

Route::post('v2/bindCard','v2/FuIou/bindBankCard');//富友绑定银行卡repay

Route::post('v2/FuiouRepay','v2/FuIou/repay');//富友还款

Route::post('v2/FuiouBindCardList','v2/FuIou/BindCardList');//富友绑定银行卡列表

Route::post('v2/bindBankInfo','v2/User/bindBankInfo'); //绑定银行卡列表资料

Route::any('v2/paySuccess','v2/FuIou/paySuccess'); //代付成功回调
Route::any('v2/payRefund','v2/FuIou/paySuccess'); //代付退票回调

Route::any('v2/sendVoice','v2/Script/sendVoice'); // 消费语音电话队列【每天10点到18点每分钟执行一次】
Route::any('v2/voiceNotify','v2/Script/voiceNotify'); //语音话单回调

Route::post('v2/rechargePage','v2/FuIou/rechargePage'); //充值页面

Route::post('v2/recharge','v2/FuIou/recharge'); //充值

Route::get('v2/TestRepayment','v2/Account/testRepay'); //测试代扣 单个订单

Route::post('v2/applyProgress','v2/apply/applyProgress');         // 申请进度
Route::post('v2/transferAuthVip','v2/apply/transferAuthVip');         // 申请转vip认证

Route::any('v2/apiErrorLog','v2/Base/apiErrorLog'); //前端日志收集

Route::any('v2/checkVersion','v2/Base/checkVersion'); //版本检查

Route::any('v2/getUserInfoByPhone','v2/User/getUserInfoByPhone'); //根据手机号查询用户信息
Route::any('v2/updateUserInfoByPhone','v2/User/updateUserInfoByPhone'); //根据手机号更新用户信息

Route::any('v2/payResQuery','v2/Script/payResQuery'); // 查询代付结果
Route::any('v2/handleSpecialPayOrder','v2/Script/handleSpecialPayOrder'); // 处理特殊的代付订单【每天17点执行一次】
//我的订单 start
Route::post('v2/consignment','v2/MyOrder/consignment'); //确认发货
Route::post('v2/consignmentList','v2/MyOrder/consignmentLog'); //发货记录
//我的订单 end

Route::post('v2/partner','v2/Partner/add'); // 写入

Route::any('v2/timeout', 'v2/FuIou/handlerTimeOut'); //处理超时订单


Route::any('v2/protocol','v2/Business/protocol');              						//前台获取商家Id

Route::any('v2/getLinkman','v2/User/getLinkman');									//获取联系人
Route::any('v2/incrByBusinessId','v2/Account/incrByBusinessId');					//商家计数
Route::any('v2/countRedis','v2/User/countRedis');									//商家计数

Route::any('v2/compressUpload','v2/Account/compressAndUpload');									//处理日志 上传 并且 删除
Route::post('v2/tg_sendRegverify','v2/Account/tg_sendRegverify');						//推广注册短信验证码
Route::post('v2/tg_sendRegVoiceVerify','v2/Account/tg_sendRegVoiceVerify');						//推广注册语音验证码