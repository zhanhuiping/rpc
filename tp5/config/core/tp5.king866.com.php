<?php 
// 核心配置
return [
//数据库配置
    'db_business'  => [
        'type'     => 'mysql',
        'hostname'       => '127.0.0.1',
        'database'       => 'ppgo_job2',
        'username'       => 'test',
        'password'       => '123123',    
    ],
    
    // 短信签名前缀 TODO
    "message_prefix" => '【火星钱庄-测试】',

    // 短信渠道  TODO
     'qudao'  => [
           'id'  => '1006',
           'key'  => 'H5SpZ4OI',
    ], 
 
    
    // redis 库选择 TODO
    "redis_select" => '17',
   
    //group_id 配置
    'group_id_rs' => [
        'group_id'=>29
    ],
    //爬虫配置
    'crawler' => [
        'channel' => 10,
        'key' => 'k5nn2g7Aqnn'
    ],

    # 新添加
    'pj_type'=>'H',
    'houtai_name'=>'推广-火星-测试',
    'business_group_id'=>36,
];
