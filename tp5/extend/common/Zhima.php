<?php
namespace common;

use think\cache\driver\Redis;
/**
 * 获取芝麻分
 */
class Zhima{

   public function test()
   {
    echo 343;
   }

    const URL_ZHIMA = "http://api.qhweidai.com/api/zmopApply";  
    /**
     * 获取芝麻分
     */
    public static function getZhimaLink($uid,$notifyurl,$phone,$source =1)
    {

        if(!$uid || !$notifyurl || !$phone || $source) {
            return false;
        }
        $url = self::URL_ZHIMA;
        list($id,$key) = self::getEncryptKey();

        # 加密
        $data['phone'] = $phone;
        $data['channel'] = $id;
        $data['uid'] = $uid;
        $data['sign'] = md5($uid.$key);
        $data['notifyurl'] = $notifyurl;
        return self::curlPost($url,$data);
   }  
   // 获取加密信息
   private static function getEncryptKey()
   {
        return [config('qudao.id'),config('qudao.key')];
   }
   // Curl
   private  static function curlPost($url,$data)
   {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        if (curl_errno($curl)) {return 'ERROR '.curl_error($curl);}    
        curl_close($curl);
        return json_decode($output,true);
   }         
}