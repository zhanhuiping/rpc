<?php
namespace common;

use think\cache\driver\Redis;
/**
 * 短信发送类
 */
class Utils{

    const URL_SMSVERIFY            = "http://api.qhweidai.com/api/sendverify";  // 验证码  发送账号： bjjyzm
    const URL_SMSVERIFY_ACCOUNT    = "bjjyzm";     // 验证码  发送账号：bjjyzm
    const URL_SMSVERIFY_YY         = "http://api.qhweidai.com/api/sendyyverify";  // 语音验证码  发送账号： abjjyy
    const URL_SMSVERIFY_YY_ACCOUNT = "abjjyy";     // 语音验证码  发送账号：abjjyy    
    const URL_SMSREMIND            = "http://api.qhweidai.com/api/sendTxMsg";   // 金融提醒  发送账号：abjjyx02
    const URL_SMSREMIND_ACCOUNT    = "abjjyx02";   // 金融提醒  发送账号：abjjyx02
    const URL_SMSNOTICY            = "http://api.qhweidai.com/api/sendSms";     // 触发通知类  发送账号：abjjsc
    const URL_SMSNOTICY_ACCOUNT    = "abjjsc";     // 触发通知类  发送账号：abjjsc
    const URL_SMSALIDAYU           = "http://api.qhweidai.com/api/alidayuSms";  // 验证码  阿里大鱼短信

    const URL_CHUANGLAN_ACCOUNT    = "chuanglan_N0150134";   // 创蓝验证码短信接口  发送账号：N0150134 强制模板备案备案备案备案
    const URL_CHUANGLAN           = "http://api.qhweidai.com/api/ClsendSms";     // 触发通知类  发送账号：abjjsc    
    ##  渠道说明
    ##  http://doc.qhweidai.com/index.php?s=/16&page_id=1059    
    /*
     * 短信统计
     * @author 火星车11号 <zhongwenrong@outlook.com>
     * @date   2018-04-14
     * @param  integer    $channel 用户统计每个平台 发送短信的总数
     */
    public static function SmsCount($channel=1)
    {
        switch ($channel) {
            case 1:
                $qudao = 'aliyun';
                break;
             case 2:
                $qudao = 'guodu';
                break;           
            default:
                return false;
                break;
        }
        $redis  = new Redis();
        $redis->select(15);
        $todayKey  = $qudao.":".date("Y-m-d");
        $exists = $redis->exists($todayKey);
        if(!$exists){
            #设置前一天的统计数据过期时间
            $yesterdayKey = $qudao.":".date("Y-m-d",strtotime("-1 day"));
            $redis->setTimeout($yesterdayKey,3600*24*30);
        }
        #当前平台 field 自增1  hash字段为项目代号
        $redis->hincrby($todayKey, config('project_name'), 1);
    }
    /**
     * 语音验证码短信接口  
     * 不需要备案 签名必须
     * @author 火星车11号 <zhongwenrong@outlook.com>
     * @date   2018-04-14
     * @param  integer     $phone 手机号
     * @param  string      $msg   短信内容
     * @return array               短信发送结果
     */
    public static function curl_heart_verify_yy($phone,$msg)
    {
        $url = self::URL_SMSVERIFY_YY;
        list($id,$key) = self::getEncryptKey();

        # 加密
        $data['phone'] = $phone;
        $data['msg'] = $msg;
        $data['qudao'] = $id;
        $data['sign'] = md5($data['phone'].$data['msg'].$key);
        # 发送请求
        // {
        //     "status": 1,
        //     "info": "发送成功",
        //     "batch": "X002575180104165053"
        // }        
        $sendRes = self::curlPost($url,$data);
        if($sendRes['status'] == 1 ) {
            self::AjxSmsCount(self::URL_SMSVERIFY_YY_ACCOUNT);
        }
        return $sendRes;
   } 
    /**
     * 验证码短信接口  （国都）
     * 不需要备案 签名必须
     * @author 火星车11号 <zhongwenrong@outlook.com>
     * @date   2018-04-14
     * @param  integer     $phone 手机号
     * @param  string      $msg   短信内容
     * @return array               短信发送结果
     */
    public static function curl_heart_verify($phone,$msg)
    {
        $url = self::URL_SMSVERIFY;
        list($id,$key) = self::getEncryptKey();

        # 加密
        $data['phone'] = $phone;
        $data['msg'] = $msg;
        $data['qudao'] = $id;
        $data['sign'] = md5($data['phone'].$data['msg'].$key);
        # 发送请求
        // {
        //     "status": 1,
        //     "info": "发送成功",
        //     "batch": "X002575180104165053"
        // }        
        $sendRes = self::curlPost($url,$data);
        if($sendRes['status'] == 1 ) {
            self::AjxSmsCount(self::URL_SMSVERIFY_ACCOUNT);
        }
        return $sendRes;
   }   
    /**
     * 验证码短信接口（创蓝）  备案备案备案
     * 需要备案 签名必须
     * @author 火星车11号 <zhongwenrong@outlook.com>
     * @date   2018-05-21
     * @param  integer     $phone 手机号
     * @param  string      $msg   短信内容
     * @return array              短信发送结果
     */
    public static function curl_heart_Chuanglan_verify($phone,$msg)
    {
        $url = self::URL_CHUANGLAN;
        list($id,$key) = self::getEncryptKey();

        # 加密
        $data['phone'] = $phone;
        $data['msg'] = $msg;
        $data['qudao'] = $id;
        $data['sign'] = md5($data['phone'].$data['msg'].$key);
        # 发送请求
        // {
        //     "status": 1,
        //     "info": "发送成功",
        //     "batch": "X002575180104165053"
        // }        
        $sendRes = self::curlPost($url,$data);
        if($sendRes['status'] == 1 ) {
            self::AjxSmsCount(self::URL_CHUANGLAN_ACCOUNT);
        }
        return $sendRes;
   }    
    /**
     * 贷款提醒类短信
     * 所有短信需要备案 签名必须
     * @author 火星车11号 <zhongwenrong@outlook.com>
     * @date   2018-04-14
     * @param  integer     $phone 手机号
     * @param  string      $msg   短信内容
     * @return array               短信发送结果
     */
    public static function curl_heart_notic($phone,$msg)
    {
        $url = self::URL_SMSREMIND;
        list($id,$key) = self::getEncryptKey();

        $data['phone'] = $phone;
        $data['msg'] = $msg;
        $data['qudao'] = $id;
        $data['sign'] = md5($data['phone'].$data['msg'].$key);

        # 发送请求
        // {
        //     "status": 1,
        //     "info": "发送成功",
        //     "batch": "X002575180104165053"
        // }          
        $sendRes = self::curlPost($url,$data);
        if($sendRes['status'] == 1 ) {
            self::AjxSmsCount(self::URL_SMSREMIND_ACCOUNT);
        }
        return $sendRes;
    }
    /**
     * 触发通知类短信发送接口 
     * 所有短信需要备案 签名必须
     * @author 火星车11号 <zhongwenrong@outlook.com>
     * @date   2018-04-14
     * @param  integer     $phone 手机号
     * @param  string      $msg   短信内容
     * @return array               短信发送结果
     */
    public static function curl_heart_notic_wdy($phone,$msg)
    {
        $url = self::URL_SMSNOTICY;
        list($id,$key) = self::getEncryptKey();

        $data['phone'] = $phone;
        $data['msg'] = $msg;
        $data['qudao'] = $id;
        $data['sign'] = md5($data['phone'].$data['msg'].$key);

        # 发送请求
        // {
        //     "status": 1,
        //     "info": "发送成功",
        //     "batch": "X002575180104165053"
        // }          
        $sendRes = self::curlPost($url,$data);
        if($sendRes['status'] == 1 ) {
            self::AjxSmsCount(self::URL_SMSNOTICY_ACCOUNT);
        }
        return $sendRes;
    }
    /**
     * 短信统计接口
     * @author 火星车11号 <zhongwenrong@outlook.com>
     * @date   2018-04-14
     * @param  string     $sms_account  统计的账号
     * @param  string     $project      发送短信的项目名称
     */
    static public function AjxSmsCount($sms_account)
    {

        $redis  = new Redis();
        $redis->select(15);
        $todayKey  = $sms_account.":".date("Y-m-d");
        $exists = $redis->exists($todayKey);
        if(!$exists){
            #设置前一天的统计数据过期时间
            $yesterdayKey = $sms_account.":".date("Y-m-d",strtotime("-1 day"));
            $redis->setTimeout($yesterdayKey,3600*24*30);
        }
        #当前平台 field 自增1  hash字段为项目代号
        $redis->hincrby($todayKey, config('project_name'), 1);
    }    
   /**
    * 获取加密信息
    * @author 火星车11号 <zhongwenrong@outlook.com>
    * @date   2018-04-14
    * @return array     加密数组
    */
   private static function getEncryptKey()
   {
        return [config('qudao.id'),config('qudao.key')];
   }
   /**
    * 公共curl 接口
    * @author 火星车11号 <zhongwenrong@outlook.com>
    * @date   2018-04-14
    * @param  string     $url   请求链接
    * @param  array      $data  请求参数
    * @return json              返回参数
    */
   private  static function curlPost($url,$data)
   {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        if (curl_errno($curl)) {return 'ERROR '.curl_error($curl);}    
        curl_close($curl);
        return json_decode($output,true);
   }

   /**
     * 验证码短信接口  
     * 不需要备案 签名必须
     * @author 锋子 <605993145@qq.com>
     * @date   2018-04-14
     * @param  integer     $phone 手机号
     * @param  string      $code  验证码
     * @param  string      $type  1注册 2是修改密码
     * @param  string      $appName  项目名称
     * @return array              短信发送结果
     */
    public static function curl_alidayu_verify($phone,$code,$type,$appName)
    {
        $url = self::URL_SMSALIDAYU;
        list($id,$key) = self::getEncryptKey();

        # 加密
        $data['phone']   = $phone;
        $data['code']    = $code;
        $data['type']    = $type;
        $data['appName'] = $appName;
        $data['qudao']   = $id;
        $data['sign']    = md5($data['phone'].$data['msg'].$key);
        # 发送请求
        // {
        //     "code": 200,
        //     "data": "发送成功",
        //     "error": ""
        // }        
        $sendRes = self::curlPost($url,$data);
        if($sendRes['code'] == 200 ) {
            self::SmsCount();
        }
        return $sendRes;
   }          
}