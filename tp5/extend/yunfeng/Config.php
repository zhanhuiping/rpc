<?php
namespace yunfeng;

final class Config
{
    const APPKEY =      'ehrbsxq5pu'; //TODO:填入云蜂科技分配的Appkey
    const ACCESSKEY =   'm5UCBBTDBOFkaJmF2fiib3CZNZpWmf7X'; //TODO:填入云蜂科技分配的ak
    const SECRETKEY =   'OoJwZxaRYU0wD8cW5Umez7FT5tBtQXww'; //TODO:填入云蜂科技分配的sk，请务必安全保管该值
    
    const TOKENVERSION = 'v2';
    const HOST = 'api.ibeesaas.com'; //api 域名
    const HOST_H5 = 'h5.ibeesaas.com'; //h5 api 域名
    
    const URLPATH_PREFIX   = "/daas/v1/tasks";
    const URLPATH_TOKEN    = "/fk/daas/v1/h5token";          // web api 1：获取token
    const URLPATH_RESULT   = "/fk/daas/v1/h5token/result";   // web api 2：查询结果
    const URLPATH_DEL_USER = "/fk/daas/v1/h5token/userinfo"; //删除userGid信息
    const URL_H5           = "https://h5.ibeesaas.com/page/creditList/list.html";  // 授信页面地址
    
    const TYPE_CARRIER = "carrier"; //运营商
    const TYPE_TAOBAO = "taobao"; //淘宝
    const TYPE_INSURANCE = "insurance"; //社保
    const TYPE_ELEMENT4 = "element4";//银行卡实名认证
    const TYPE_ELEMENT3 = "element3";//手机实名认证
    const TYPE_BLACK = "black"; //网贷黑名单
    const TYPE_PBC = "pbc";//人行征信报告
    const TYPE_FUND = "fund";//公积金
    const TYPE_ALIPAY = "alipay";//支付宝
    const TYPE_EMAILBILL = "email_bill";//邮箱账单
    const TYPE_BANK = "bank";//网银
    const TYPE_EDUCATION = "education";//学历
    const TYPE_RESUME = "resume";//简历
    const TYPE_MAIMAI = "maimai";//脉脉
    const TYPE_WEIBO = "weibo";//微博
    const TYPE_BAIDU = "baidu";//百度
    const TYPE_JD = "jd";//京东
    const TYPE_BLACKCOURT = "blackcourt";//高法黑名单
    const TYPE_BLACKCRIME = "blackcrime";//犯罪记录黑名单
}
