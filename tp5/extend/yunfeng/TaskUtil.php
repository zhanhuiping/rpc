<?php
namespace yunfeng;

require_once("Config.php");
require_once("Util.php");
require_once("Exception.php");
require_once("TokenHelper.php");

class TaskUtil {

    private static $appKey = Config::APPKEY;
    private static $host = Config::HOST;
    private static $host_h5 = Config::HOST_H5;
    private static $tokenVersion = Config::TOKENVERSION;
    private static $accessKey = Config::ACCESSKEY;
    private static $secretKey = Config::SECRETKEY;

    /**
     * Describe : 正常提交任务
     * Date: 2017年7月13日
     * @param string $taskType 任务类型
     * @param array $bodyJson 请求体
     * @param string $method 请求方法
     * @param int $ttTime 超时时间
     * @return array 返回执行结果
     */
    public static function submitTask($taskType, $bodyJson = array(), $method = 'POST', $ttTime = -1) {

        //验证任务类型
        if (empty($taskType)) {
            throw new Yunfeng_Exception("general_1", "Invalid taskType");
        }

        #生成获取Token的参数
        $urlPath = Config::URLPATH_PREFIX;
        $queryParam = "appKey=" . self::$appKey . "&taskType=" . $taskType;

        $result = self::runTask($urlPath, $queryParam, $bodyJson, $method, $ttTime);

        return $result;
    }

    /**
     * Describe : 查询任务
     * Date: 2017年7月13日
     * @param string $taskNo 任务号
     * @param string $method 提交方法
     * @param int $ttTime 过期时间
     * @return array 返回执行结果
     */
    public static function queryTask($taskNo, $method = 'GET', $ttTime = -1) {

        //验证任务号
        if (empty($taskNo)) {
            throw new Yunfeng_Exception("general_1", "Invalid taskNo");
        }

        #生成获取Token的参数
        $urlPath = Config::URLPATH_PREFIX . "/" . $taskNo . "/result";
        $queryParam = "appKey=" . self::$appKey;

        $result = self::runTask($urlPath, $queryParam, '', $method, $ttTime);

        return $result;

    }

    /**
     * Describe : 补充任务
     * Date: 2017年7月13日
     * @param string $taskNo 任务号
     * @param array $bodyJson 请求体
     * @param string $method 请求方法
     * @param int $ttTime 超时时间
     * @return array 返回执行结果
     */
    public static function patchTask($taskNo, $bodyJson = array(), $method = 'POST', $ttTime = -1) {

        //验证任务号
        if (empty($taskNo)) {
            throw new Yunfeng_Exception("general_1", "Invalid taskNo");
        }

        #生成获取Token的参数
        $urlPath = Config::URLPATH_PREFIX . "/" . $taskNo;
        $queryParam = "appKey=" . self::$appKey;

        $result = self::runTask($urlPath, $queryParam, $bodyJson, $method, $ttTime);

        return $result;

    }

    /**
     * Describe : 通用任务执行方法
     * Date: 2017年7月13日
     * @param string $urlPath API调用url
     * @param string $queryParam API调用url的参数
     * @param array $bodyJson Body请求体
     * @param string $method 请求方法
     * @param int $ttTime 过期时间
     * @return array 返回执行结果
     */
    private static function runTask($urlPath, $queryParam, $bodyJson = array(), $method = 'POST', $ttTime = -1) {

        //验证appkey
        if (empty(self::$appKey)) {
            throw new Yunfeng_Exception("general_1", "Invalid appkey");
        }

        #设置默认过期时间1小时
        if ($ttTime < 0) {
            $ttTime = time() + 3600;
        }

        #生成获取Token的参数
        if (is_array($bodyJson)) {
            $bodyJson = json_encode($bodyJson, 320);
        }

        #get token
        $TokenHelper = new TokenHelper(self::$accessKey, self::$secretKey, self::$tokenVersion);
        $token = $TokenHelper->generateToken($urlPath, $method, $queryParam, $bodyJson, $ttTime);

        #request
        $Util = new Util();
        $url = 'https://' . self::$host . $urlPath . '?' . $queryParam;

        $header[] = "X-IbeeAuth-Token:{$token}";
        $result = $Util->get_curl($url, $method, $bodyJson, $header);

        return $result;

    }



//********************************重新封装的函数************************************//

    /**
     * Describe : 通用H5任务执行方法
     * Date: 2017年7月13日
     * @param string $urlPath API调用url
     * @param string $queryParam API调用url的参数
     * @param array $bodyJson Body请求体
     * @param string $method 请求方法
     * @param int $ttTime 过期时间
     * @return array 返回执行结果
     */
    private static function runH5Task($urlPath, $queryParam, $bodyJson = array(), $method = 'POST', $ttTime = -1) {

        //验证appkey
        if (empty(self::$appKey)) {
            throw new Yunfeng_Exception("general_1", "Invalid appkey");
        }

        #设置默认过期时间1小时
        if ($ttTime < 0) {
            $ttTime = time() + 3600;
        }

        #生成获取Token的参数
        if (is_array($bodyJson)) {
            $bodyJson = json_encode($bodyJson, 320);
        }

        #get token
        $TokenHelper = new TokenHelper(self::$accessKey, self::$secretKey, self::$tokenVersion);
        $token = $TokenHelper->generateToken($urlPath, $method, $queryParam, $bodyJson, $ttTime);

        #request
        $Util = new Util();
        $url = 'https://' . self::$host_h5 . $urlPath . '?' . $queryParam;

        $header[] = "X-IbeeAuth-Token:{$token}";
        $result = $Util->get_curl($url, $method, $bodyJson, $header);

        return $result;

    }


    /**
     * 获取h5 token
     * @param  integer  $ttTime  过期时间 默认-1为1小时 即time() + 3600
     * @return [type]            [description]
     */
    public static function getH5Token($bodyJson = '', $ttTime = -1) {
        #生成获取Token的参数
        $urlPath = Config::URLPATH_TOKEN;
        $queryParam = "taskType=h5_risk_management_1&appKey=" . self::$appKey;

        $result = self::runH5Task($urlPath, $queryParam, $bodyJson, $method = 'POST', $ttTime);
        return $result;
    }


    /**
     * 获取完整的H5授信链接
     * @param  string $bodyJson 四要素
     * @param  string $token    口令
     * @return [type]           [description]
     */
    public static function getH5Url($token = '', $bodyJson = '')
    {
        $urlH5 = Config::URL_H5;

        if(empty($token)){
            $result = self::getH5Token($bodyJson);
            if(empty($result['token'])) {
                return false;
            }
            $token = $result['token'];
        }

        $url = $urlH5 . "?token=" . $token;
        return $url;
    }


    /**
     * 查询结果
     * @param  string  $userGid   四要素认证完成后，返回的用户Gid
     * @param  string  $authType  授信类型
     *   debitcard   银行卡实名验证
     *   creditcard  银行卡实名验证
     *   carrier     运营商数据
     *   taobao      淘宝数据
     *   jd          京东数据
     *   h5_risk_management_1    风控模型
     * @param  integer $ttTime    过期时间 默认-1为1小时 即time() + 3600
     * @return [type]            [description]
     */
    public static function getResult($userGid, $authType) {
        #生成获取Token的参数
        $urlPath = Config::URLPATH_RESULT;
        $queryParam = "authType=" . $authType . "&appKey=" . self::$appKey . '&userGid=' . $userGid;

        $result = self::runH5Task($urlPath, $queryParam, $bodyJson = '', $method = 'GET');
        return $result;
    }


    /**
     * 删除单个userGid的信息  如果已经计费将不能够执行该操作
     * @param  string $userGid 四要素认证完成后，返回的用户Gid
     * @return [type]          [description]
     */
    public static function delUserGid($userGid) {
        #生成获取Token的参数
        $urlPath = Config::URLPATH_DEL_USER . "/" . $userGid;
        $queryParam = "appKey=" . self::$appKey;

        $result = self::runH5Task($urlPath, $queryParam, $bodyJson = '', $method = 'DELETE');
        return $result;
    }


    /**
     * 删除appKey所关联的全部userGid信息  如果已经计费将不能够执行该操作
     * @return [type]          [description]
     */
    public static function delAllUserGid() {
        #生成获取Token的参数
        $urlPath = Config::URLPATH_DEL_USER;
        $queryParam = "appKey=" . self::$appKey;

        $result = self::runH5Task($urlPath, $queryParam, $bodyJson = '', $method = 'DELETE');
        return $result;
    }


}