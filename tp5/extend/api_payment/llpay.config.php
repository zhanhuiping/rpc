﻿<?php
/* *
 * 配置文件
 * 版本：1.2
 * 日期：2014-06-13
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 */

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
//商户编号是商户在连连钱包支付平台上开设的商户号码，为18位数字，如：201408071000001543
$llpay_config['oid_partner'] = '201709220000945943';

//秘钥格式注意不能修改（左对齐，右边有回车符）
$llpay_config['RSA_PRIVATE_KEY'] ='-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQDI1sMN79YCkz1Me5RqmWzQZOt5uLpGH068prPJnIW0uKGG2lQ6
IxUJcQzDqwrV083AmlCxUEf3meuKW8Z3TY9ats8rQKyRu5d3/Y6acSfx/O+Mf7u9
dxQHQqsEQMtBrVoM4bNTjaeD4d8ejYBpzBij2/H3ZqyudlZvyZhyN1KyyQIDAQAB
AoGAF7/91PoVrrxGtXKvvGNkUMh+aCiGLqCpKPn1rNPLo/FpeI79Atr/M9ygi8sA
PwULAcm93RoHSKp1BimQMK+FUhkAS/5nWVrwnfFRObj6rtVCcABKj6wwYtwdtNkr
rMJBG4BFBBV9mFBbb+bEFF1L4dHpK2pbNwsHdsF9i+NaJBkCQQD7AaPa6INNI2LU
WIuwS/akUDc8bJMiunWs04Zpf/4vpg7qjPtJn5/iv0TjDfNoAUZprFyShd4NxE69
q4E7OiXDAkEAzNWfOUKT8DR/23sPihQTqOo1ZVBbD3FROM3hXwoPbz1IPhYD4nbR
w6yw9p7BIWyNJtwOUz0p6vcmGh6TijMggwJAAowEtp7vXiQCx7bSTAqpSQeLV/Ce
GZccxJEs5vsmgCNGOXkOv7N/8tQQBeHJW3/P0Biwo8f59dfNGL2ZxRkU8wJAQjAx
MDaESVUdj5V3Uv3j0GxF7sQFeLr8LOfSEHzWufJKes1wWehdJeaSlE5+R8ek4M6w
zlLwTZ2pMT16HWkh8wJBAL7XxNhmBYD6Gs8GM2G9+ochKk1JnTGQR9LV9qzV+bHA
L+BAu78lSj9JGHKjOcolT2QCG7H6502S3MQDuijvUlo=
-----END RSA PRIVATE KEY-----';	


$llpay_config['LIANLIAN_PUBLICK_KEY'] = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCSS/DiwdCf/aZsxxcacDnooGph3d2JOj5GXWi+
q3gznZauZjkNP8SKl3J2liP0O6rU/Y/29+IUe+GTMhMOFJuZm1htAtKiu5ekW0GlBMWxf4FPkYlQ
kPE0FtaoMP3gYfh+OwI+fIRrpW3ySn3mScnc6Z700nU/VYrRkfcSCbSnRwIDAQAB
-----END PUBLIC KEY-----';

//安全检验码，以数字和字母组成的字符
$llpay_config['key'] = '201408071000001543test_2014082';

//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

//版本号
$llpay_config['version'] = '1.2';

//请求应用标识 为wap版本，不需修改
$llpay_config['app_request'] = '3';


//签名方式 不需修改
$llpay_config['sign_type'] = strtoupper('RSA');

//订单有效时间  分钟为单位，默认为10080分钟（7天） 
$llpay_config['valid_order'] ="10080";

//字符编码格式 目前支持 gbk 或 utf-8
$llpay_config['input_charset'] = strtolower('utf-8');

//访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
$llpay_config['transport'] = 'http';


?>