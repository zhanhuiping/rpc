<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// if(strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone')||strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')){
// 	#关闭数据库切换
//     define('USER_AGENT',"Android");
// }else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Android')){
//     define('USER_AGENT','Android');
// }else{
//     define('USER_AGENT','unknown');
// }

// $key = $_Post['key']





// 应用目录
define('APP_PATH', __DIR__.'/application/');
// 定义配置文件目录和应用目录同级
define('CONF_PATH', __DIR__.'/config/');
// 加载框架引导文件
require './thinkphp/start.php';
