<?php
class Server{

	public $socket;
	public $ip=0;
	public $port=8888;

	public function __construct(){

		$this->socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
		socket_set_option($this->socket,SOL_SOCKET,SO_REUSEADDR,true);
		socket_bind($this->socket,$this->ip,$this->port);
		socket_listen($this->socket);
		
		while(true){
			
			$conSocket = socket_accept($this->socket);
			$data = socket_read($conSocket,1024);
		
			preg_match('/RPC-CLASS:(.*)/',$data,$class);
			preg_match('/RPC-METHOD:(.*)/',$data,$method);
			preg_match('/RPC-PARAM:(.*)/',$data,$param);

			$class = $class[1];
			$method = $method[1];
			require 'service/'.$class.'.php';
			
			$obj = new $class;
			$res = $obj->$method();	
			socket_write($conSocket,$res,strlen($res));


		}
	}

}

new Server();
